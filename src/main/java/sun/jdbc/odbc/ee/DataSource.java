package sun.jdbc.odbc.ee;

import java.sql.Connection;

public interface DataSource {

    Connection getConnection();
}
