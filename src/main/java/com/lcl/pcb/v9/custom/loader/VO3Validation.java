package com.lcl.pcb.v9.custom.loader;

import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.lcl.pcb.v9.generic.loader.V9ValidationDaoManager;
import com.lcl.pcb.v9.idf.jobflow.config.IDFConfig;

public class VO3Validation {
	boolean headerResult = true;
	boolean trailerResult = true;
	boolean bodyResult = true;
	boolean doValidation = true;
	Logger logger = LogManager.getLogger(VO3Validation.class.getName());
	
	public boolean doValidation(ArrayList<String> files) throws Exception {
		logger.info("VO3Validation Called");
		doValidation &= doHeader();
		doValidation &= doBody();
		doValidation &= doTrailer();
		return doValidation;
	}

	public boolean doHeader() throws Exception {
		logger.info("VO3Validation doHeader() Called");
		return headerResult;
	}

	public boolean doTrailer() {
		logger.info("VO3Validation doTrailer() Called");
		 return trailerResult;
	}

	public boolean doBody() throws SQLException {

		logger.info("VO3Validation doBody() Called");

		String sqldelete = "DELETE FROM HAINS_TEMP.INS_HOME_POLICY_STG";
		logger.debug("VO3Validation: doBody - Delete Query for Staging table: "+ sqldelete);
		try {
			V9ValidationDaoManager.execNonQuery(sqldelete);
		} catch (SQLException e) {
			logger.fatal("Error executing query", e);
		}
		logger.debug("VO3Validation: doBody - Records deleted from Staging table");

		return bodyResult;
	}

	public void doErrorLog() throws Exception {
		logger.info("VO3Validation doErrorLog() Called");
	}

	public void doFinalize(ArrayList<String> files) throws Exception {
		logger.info("VO3Validation doFinalize() Called");
	}

}
