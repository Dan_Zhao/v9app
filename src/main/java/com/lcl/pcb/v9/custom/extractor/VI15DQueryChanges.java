package com.lcl.pcb.v9.custom.extractor;


import java.util.Arrays;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.lcl.pcb.v9.idf.jobflow.config.ExtractorConfig;


/**
 * This class is used if additional changes are needed to the original
 * interface sql query in the extractor xml
 * This class is specific to this interface, other custom classes
 * need to be coded for other interfaces if the original query is
 * not the final layout of the extraction flow for that interface.
 */
public class VI15DQueryChanges extends QueryChanges {

		/**
	 * Constructor sets up originalRsArray 
	 * to be the original data that was extracted
	 * from the database in the Extractor process flow
	 * Use parent class's constructor
	 *
	 * @param  String[][] originalRsArray
	 * @return void
	 */		
	public VI15DQueryChanges(String[][] originalRsArray) {
		super(originalRsArray);
	}
	
	Logger logger = LogManager.getLogger(VI15DQueryChanges.class.getName());
	
	/**
	 * override execute method
	 * this method will do the necessary custom
	 * data modifications for the Extractor process flow
	 * for this interface 
	 *
	 * @param  void
	 * @return String[][] new array after modifications
	 */	
	public String[][] execute() throws Exception {
		logger.info("VI15DQueryChanges execute() Called");
		
		int rowCount=originalRsArray.length;
		logger.debug("VI15DQueryChanges rowCount found from DB Result: " + rowCount);

		// loop through and store layout header from config file in newRsArray
		int numFields=ExtractorConfig.getInstance().getExtractorData().getLayout().getLField().size();
		logger.debug("VI15DQueryChanges numFields count found from Layout in Extractor File: " + numFields);
		newRsArray=new String[rowCount][numFields];  //count is number of rows
		
	    String[] header = new String[numFields];    
		for (int i = 0; i < numFields; i++) {
			String fieldName = ExtractorConfig.getInstance().getExtractorData().getLayout().getLField().get(i).getFieldName();	
			logger.debug("VI15DQueryChanges fieldName found from Layout in Extractor File: " + fieldName);
			header[i]=fieldName;
		}
		newRsArray[0]=header;
		
		// reformat the data to the columns that are specified in vi15extractor.xml
	    String[] row = null;
	    int j=1;
		for (int i = 1; i < rowCount; i++) {
			logger.debug("VI15DQueryChanges Reformatting Started on DB Result based on vi15extractor.xml: " + i);

			row = new String[numFields];

			row[0] = "PCPOA"; // source code

			int CLIENT_ID = Arrays.asList(originalRsArray[0]).indexOf(
					"CLIENT_ID");
			row[1] = originalRsArray[i][CLIENT_ID]; //CLIENT id
			logger.debug("VI15DQueryChanges CLIENT_ID Found: " + row[1]);

			row[2] = ""; // last modified date
			
			int POA_Stat_id=Arrays.asList(originalRsArray[0]).indexOf("POA_STAT_ID");
			row[3] = originalRsArray[i][POA_Stat_id]; // status indicator

			row[4] = "I"; // record type
			
			int gender_id=Arrays.asList(originalRsArray[0]).indexOf("GENDER_ID");
			row[5] = originalRsArray[i][gender_id]; // gender

			int FIRST_NAME = Arrays.asList(originalRsArray[0]).indexOf(
					"FIRST_NM");
			int MIDDLE = Arrays.asList(originalRsArray[0]).indexOf(
					"INITIAL");
			int LAST_NAME = Arrays.asList(originalRsArray[0]).indexOf(
					"LAST_NM");
			int SUFFIX = Arrays.asList(originalRsArray[0]).indexOf(
					"SUFFIX");
			row[6] = originalRsArray[i][FIRST_NAME] + " "
					+ originalRsArray[i][MIDDLE] + " "
					+ originalRsArray[i][LAST_NAME] + " "
					+ originalRsArray[i][SUFFIX];

			int ADDRESS_LINE1 = Arrays.asList(originalRsArray[0]).indexOf(
					"ADDR_LN_1");
			row[7] = originalRsArray[i][ADDRESS_LINE1]; // address line1

			int ADDRESS_LINE2 = Arrays.asList(originalRsArray[0]).indexOf(
					"ADDR_LN_2");
			row[8] = originalRsArray[i][ADDRESS_LINE2]; // address line2
			
			int ADDRESS_LINE3 = Arrays.asList(originalRsArray[0]).indexOf(
					"ADDR_LN_3");
			row[9] = originalRsArray[i][ADDRESS_LINE3]; // address line 3

			int CITY = Arrays.asList(originalRsArray[0]).indexOf("CITY");
			row[10] = originalRsArray[i][CITY]; // city

			int PROVINCE = Arrays.asList(originalRsArray[0]).indexOf("PROVINCE");
			row[11] = originalRsArray[i][PROVINCE]; // province

			int ZIPCCODE = Arrays.asList(originalRsArray[0])
					.indexOf("POSTAL_CD");
			row[12] = originalRsArray[i][ZIPCCODE]; // postal code

			int COUNTRY = Arrays.asList(originalRsArray[0]).indexOf(
					"COUNTRY");
			row[13] = originalRsArray[i][COUNTRY]; // country

			int CIFP_DATE_OF_BIRTH = Arrays.asList(originalRsArray[0]).indexOf(
					"DT_OF_BIRTH");
			String dateOfBirth = originalRsArray[i][CIFP_DATE_OF_BIRTH];
			if (!dateOfBirth.equals("")) {
				dateOfBirth = dateOfBirth.replaceAll("[\\-]", "");
				dateOfBirth = dateOfBirth.substring(0, 8);
			}
			row[14] = dateOfBirth; // date of birth

			row[15] = ""; // national id
			int Acct_Id = Arrays.asList(originalRsArray[0]).indexOf(
					"TSYS_ACCT_ID");
			row[16] = originalRsArray[i][Acct_Id]; // display field 1 No Data

			row[17] = ""; // display field 2. customer_type - '0' primary, '2'
							// auth user

			row[18] = "POA"; // display field 3. Data_source

			row[19] = ""; // comments;
//
//			row[20] = ""; // comment 2

			newRsArray[j] = row;
			j++;
			logger.debug("VI15DQueryChanges Reformatting Ended on DB Result based on vi15extractor.xml: " + i);
		}
		int newRsArrayLength=newRsArray.length;
		logger.debug("VI15DQueryChanges newRsArrayLength: " + newRsArrayLength);
		return newRsArray;
	}

}


