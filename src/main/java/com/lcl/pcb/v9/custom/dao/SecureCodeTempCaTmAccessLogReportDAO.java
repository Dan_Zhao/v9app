package com.lcl.pcb.v9.custom.dao;

import java.sql.ResultSet;
import java.util.ArrayList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.lcl.pcb.v9.custom.databean.CaTmAccessLogReport;
import com.lcl.pcb.v9.custom.multistage.idf.MultiStageConfig;

public class SecureCodeTempCaTmAccessLogReportDAO {
	
	private static final Logger logger = LogManager.getLogger(SecureCodeTempCaTmAccessLogReportDAO.class.getName());	

	public static ArrayList<CaTmAccessLogReport> selectAllSecureCodeTempCaTmAccessLogReport() {
		logger.info("SecureCodeTempCaTmAccessLogReportDAO: selectAllSecureCodeTempCaTmAccessLogReport - Execution Started");
		ArrayList<CaTmAccessLogReport> caTmAccessLogReportList = new ArrayList<CaTmAccessLogReport>();
		int childId = MultiStageConfig.getInstance().getChildId();
		logger.debug("SecureCodeTempCaTmAccessLogReportDAO: selectAllSecureCodeTempCaTmAccessLogReport - childId: "+childId);
		int recCnt=0;

		try {
			String sqlSelectVO84BStage = "select * from " + MultiStageConfig.getInstance().getMultiStage().getLoader().get(childId).getSrcloadDbSchema() + ".CA_TM_ACCESS_LOG";
			logger.debug("SecureCodeTempCaTmAccessLogReportDAO: selectAllSecureCodeTempCaTmAccessLogReport - sqlSelectVO84BStage: "+sqlSelectVO84BStage);
			V9MultipleStagesSrcDaoManager.getInstance();
			ResultSet rs=V9MultipleStagesSrcDaoManager.execQuery(sqlSelectVO84BStage);
			while(rs.next()){
				recCnt=recCnt+1;
				CaTmAccessLogReport caTmAccessLogReport = new CaTmAccessLogReport();
				caTmAccessLogReport.setAdminId(rs.getString("ADMIN_ID"));
				caTmAccessLogReport.setFirstNm(rs.getString("FIRST_NM"));
				caTmAccessLogReport.setMiddleNm(rs.getString("MIDDLE_NM"));
				caTmAccessLogReport.setLastNm(rs.getString("LAST_NM"));
				caTmAccessLogReport.setDescription(rs.getString("DESCRIPTION"));
				caTmAccessLogReport.setRptType(rs.getString("RPT_TYPE"));
				caTmAccessLogReport.setStrtDt(rs.getTimestamp("STRT_DT"));
				caTmAccessLogReport.setEndDt(rs.getTimestamp("END_DT"));
				caTmAccessLogReport.setDtAccessed(rs.getTimestamp("DT_ACCESSED"));
				caTmAccessLogReport.setProcessNum(rs.getBigDecimal("PROCESS_NUM"));
				caTmAccessLogReport.setFileName(rs.getString("FILENAME"));
				caTmAccessLogReport.setRecNum(rs.getBigDecimal("REC_NUM"));
				caTmAccessLogReport.setLoadResultUUID(rs.getString("LOAD_RESULT_UUID"));
				caTmAccessLogReportList.add(caTmAccessLogReport);
			}
			logger.debug("SecureCodeTempCaTmAccessLogReportDAO: selectAllSecureCodeTempCaTmAccessLogReport - sqlSelectVO84BStage Records Found: "+recCnt);
			rs.close();
		}
		catch(Exception ex){
			//ex.printStackTrace();
			logger.error(
					"SecureCodeTempCaTmAccessLogReportDAO: selectAllSecureCodeTempCaTmAccessLogReport: Exception has occurred while trying to fetch all records from Staging Table securecode_temp.CA_TM_ACCESS_LOG: ",
					ex);
		}
		finally {
		}
		logger.info("SecureCodeTempCaTmAccessLogReportDAO: selectAllSecureCodeTempCaTxAllTx - Execution Ended");
		return caTmAccessLogReportList;
	}
}