package com.lcl.pcb.v9.custom.extractor;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Super class that is extended by each Interface 
 * that has custom data modifications for extraction
 */

public class QueryChanges {

	String[][] originalRsArray=null;
	String[][] newRsArray= null;
	
	static final Logger logger = LogManager.getLogger(QueryChanges.class.getName());		

	/**
	 * Constructor sets up originalRsArray 
	 * to be the original data that was extracted
	 * from the database in the Extractor process flow
	 *
	 * @param  String[][] originalRsArray
	 * @return void
	 */	
	public QueryChanges(String[][] originalRsArray) {
		this.originalRsArray = originalRsArray;
	}
	
	/**
	 * execute method to be overridden by child classes
	 * this method will do the necessary custom
	 * data modifications for the Extractor process flow
	 *
	 * @param  void
	 * @return String[][] new array after modifications
	 */		
	public String[][] execute() throws Exception {
		return newRsArray;
	}
    
}
