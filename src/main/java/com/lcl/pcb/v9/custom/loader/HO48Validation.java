package com.lcl.pcb.v9.custom.loader;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.lcl.pcb.v9.generic.loader.V9ValidationDaoManager;
import com.lcl.pcb.v9.idf.jobflow.config.IDFConfig;
import com.lcl.pcb.v9.utilities.fileReader.FileUtil;

/**
 * This class is used to validate the csv files generated for HO48 interface. It
 * validates whether the data was previously loaded in table with the
 * ADM_EXTRACT_DT value in the Header csv file.It validates whether trailer
 * record segments count and processed records counts are same and validates the
 * total record count also.
 */
public class HO48Validation {

	ArrayList<String> csvFilesList = new ArrayList<String>();
	ArrayList<String> bodyCsvFilesList = new ArrayList<String>();
	String csvFiles;
	public String headerCsvFile = "";
	BufferedReader br;
	BufferedReader badrecordreader;
	String line = "";
	String dateCreated = "";
	boolean headerResult = true;
	boolean trailerResult = true;
	boolean bodyResult = true;
	boolean doValidation = true;
	HashMap<String, Integer> totRecParsed = new HashMap<String, Integer>();
	HashMap<String, Integer> trailerRecList = new HashMap<String, Integer>();
	public String trailerCsvFile = "";
	public String bodyCsvFile = "";
	int totalRecCount = 0;
	int totalRecTrailerCount = 0;
	int totalNumOfRecords = 0;
	String dtCreated = "";
	String SourceFileId = "";
	java.sql.Date procDT;
	String creationDT;
	String dateFormat = "DD-Mon-YYYY";
	int seqVal = 0, seqValue = 0;
	Logger logger = LogManager.getLogger(HO48Validation.class.getName());
	V9ValidationDaoManager v9Validation = new V9ValidationDaoManager();

	// This method will be invoked from V9Loader class which expects the csv
	// files list for validating header, body and trailer.
	public boolean doValidation(ArrayList<String> files) throws Exception {
		logger.debug("HO48Validation Called");
		this.csvFilesList = files;
		doValidation &= doHeader();
		doValidation &= doBody();
		doValidation &= doTrailer();
		return doValidation;
	}

	public boolean doHeader() throws Exception {
		logger.debug("HO48Validation doHeader() Called");
		//Changes to made Days parameter configurable
		Properties properties = new Properties();
		InputStream inputStream = new FileInputStream("/x/prod/pcb/V9App/InterfaceDefinition/HO48/config.properties");
		properties.load(inputStream);
	    inputStream.close();
	    String days=properties.getProperty("lookbackdays");
	   //Changes to made Days parameter configurable
		for (int i = 0; i < csvFilesList.size(); i++) {
			if (csvFilesList.get(i).contains("HEADER")) {
				headerCsvFile = csvFilesList.get(i);
				try {
					br = new BufferedReader(new FileReader(headerCsvFile));

					try {
						while ((line = br.readLine()) != null) {
							String[] headerVal = line.split("\\|");
							if (headerVal[0].equalsIgnoreCase("H")
									&& headerVal.length > 3) {
								creationDT = headerVal[2];
								DateFormat df = new SimpleDateFormat(
										"dd-MMM-yyyy");
								java.util.Date d = df.parse(creationDT);
								df = new SimpleDateFormat("yyyy-MM-dd");
								String fileCreationDT = df.format(d);
								logger.debug("File Creation Date in HEADER CSV File:"
										+ fileCreationDT);
								v9Validation.getInstance();
								String sql = "SELECT DISTINCT(PROC_DT) FROM TS2.CARDG_FD_MUD_OUT_HDR_TRLR where TO_DATE(PROC_DT, '"
										+ dateFormat
										+ "') > TO_DATE(SYSDATE-"+days+", '"
										+ dateFormat + "')";
								try {
									v9Validation.open();
									ResultSet res = V9ValidationDaoManager
											.execQuery(sql);
									while (res.next()) {
										procDT = res.getDate("PROC_DT");
										String procDate = df.format(procDT);
										logger.debug("Process Date Validation Check Started for procDate:"
												+ procDate);
										if (fileCreationDT
												.equalsIgnoreCase(procDate)) {
											logger.error("PROCESS DATE MISMATCH- Creation Date in File is :"
													+ fileCreationDT
													+ " "
													+ "This date is equal to last "+days+" days PROC_DT in table TS2.CARDG_FD_MUD_OUT_HDR_TRLR ");
											headerResult = false;
										} else {
											logger.debug("Process Date Validation Passed:A File with new Creation Date Found(not equal to last "+days+" days PROC_DT)");
										}
									}
									v9Validation.close();
								} catch (SQLException e) {
									throw e;
								} finally {
									v9Validation.con.close();
								}
							}

						}
						br.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				} catch (FileNotFoundException ex) {
					ex.printStackTrace();
				}
			}
		}
		return headerResult;
	}

	public boolean doTrailer() {
		logger.debug("HO48Validation doTrailer() Called");
		totRecParsed = FileUtil.totalRecParsedList;
		for (int i = 0; i < csvFilesList.size(); i++) {
			if (csvFilesList.get(i).contains("TRAILER")) {
				trailerCsvFile = csvFilesList.get(i);
				try {
					br = new BufferedReader(new FileReader(trailerCsvFile));

					try {
						while ((line = br.readLine()) != null) {
							String[] trailerVal = line.split("\\|");

							if (trailerVal[3].equalsIgnoreCase("T")
									&& trailerVal.length > 6) {
								totalNumOfRecords = Integer
										.parseInt(trailerVal[4].toString());

							}
						}
						br.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				} catch (FileNotFoundException ex) {
					ex.printStackTrace();
				}
			}
		}

		for (Entry<String, Integer> entry : totRecParsed.entrySet()) {
			totalRecCount += (entry.getValue());
		}
		if (totalRecCount == 0) {

			logger.error("No record found in file\n");
			trailerResult = false;
		}
		totalRecCount -= 1;
		if (totalRecCount != totalNumOfRecords) {
			logger.error("Record count mismatch : Reords parsed "
					+ totalRecCount + "  trailer reported " + totalNumOfRecords
					+ "");
			trailerResult = false;
		}
		return trailerResult;
	}

	public boolean doBody() {
		logger.debug("HO48Validation doBody() Called");
		return bodyResult;
	}

	public void doErrorLog() throws Exception {

	}

	public void doFinalize(ArrayList<String> files) throws Exception {

		logger.debug("HO48Validation doFinalize() Called ");
		seqVal = IDFConfig.getInstance().getSequenceNum();
		for (int i = 0; i < files.size(); i++) {
			if (files.get(i).contains("HEADER")) {
				headerCsvFile = files.get(i);
				try {
					br = new BufferedReader(new FileReader(headerCsvFile));

					try {
						while ((line = br.readLine()) != null) {
							String[] headerVal = line.split("\\|");
							if (headerVal[0].equalsIgnoreCase("H1")
									&& headerVal.length > 10) {
								dateCreated = headerVal[1];
								DateFormat df = new SimpleDateFormat(
										"dd-MMM-yyyy");
								java.util.Date d = df.parse(dateCreated);
								df = new SimpleDateFormat("yyyy-MM-dd");
								String db_date_String = df.format(d);

								SourceFileId = "FraudDeterm";
								StringBuilder sb = new StringBuilder("");
								sb.append("UPDATE PCMCPYMT.LOAD_RESULT SET SOURCE_END_DT_CURR = ");
								sb.append("to_timestamp('" + db_date_String
										+ "','yyyy-mm-dd HH24:MI:SS')");
								sb.append(",");
								sb.append("SOURCE_ID = '");
								sb.append(SourceFileId);
								sb.append("'");
								sb.append(" WHERE PROCESS_NUM = '");
								sb.append(seqVal);
								sb.append("'");
								V9ValidationDaoManager.execNonQuery(sb
										.toString());
								sb = null;
							}
						}
					} catch (IOException e) {
						logger.error("Error in HO48 inerface Finalize method.");
						e.printStackTrace();
					}
				} catch (FileNotFoundException ex) {
					ex.printStackTrace();
				}
			}
		}
	}
}
