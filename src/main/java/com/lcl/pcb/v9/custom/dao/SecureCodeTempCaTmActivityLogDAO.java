package com.lcl.pcb.v9.custom.dao;

import java.sql.ResultSet;
import java.util.ArrayList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.lcl.pcb.v9.custom.databean.CaTmActivityLog;
import com.lcl.pcb.v9.custom.multistage.idf.MultiStageConfig;

public class SecureCodeTempCaTmActivityLogDAO {
	
	private static final Logger logger = LogManager.getLogger(SecureCodeTempCaTmActivityLogDAO.class.getName());	

	public static ArrayList<CaTmActivityLog> selectAllSecureCodeTempCaTxAllTx() {
		logger.info("SecureCodeTempCaTmActivityLogDAO: selectAllSecureCodeTempCaTxAllTx - Execution Started");
		ArrayList<CaTmActivityLog> caTmActivityLogList = new ArrayList<CaTmActivityLog>();
		int childId = MultiStageConfig.getInstance().getChildId();
		logger.debug("SecureCodeTempCaTmActivityLogDAO: selectAllSecureCodeTempCaTxAllTx - childId: "+childId);
		int recCnt=0;

		try {
			String sqlSelectVO84CStage = "select * from " + MultiStageConfig.getInstance().getMultiStage().getLoader().get(childId).getSrcloadDbSchema() + ".CA_TM_ACTIVITY_LOG";
			logger.debug("SecureCodeTempCaTmActivityLogDAO: selectAllSecureCodeTempCaTxAllTx - sqlSelectVO84CStage: "+sqlSelectVO84CStage);
			V9MultipleStagesSrcDaoManager.getInstance();
			ResultSet rs=V9MultipleStagesSrcDaoManager.execQuery(sqlSelectVO84CStage);
			while(rs.next()){
				recCnt=recCnt+1;
				CaTmActivityLog caTmActivityLog = new CaTmActivityLog();
				caTmActivityLog.setAdminId(rs.getString("ADMIN_ID"));
				caTmActivityLog.setFirstNm(rs.getString("FIRST_NM"));
				caTmActivityLog.setMiddleNm(rs.getString("MIDDLE_NM"));
				caTmActivityLog.setLastNm(rs.getString("LAST_NM"));
				caTmActivityLog.setDesc(rs.getString("DESCR"));
				caTmActivityLog.setAction(rs.getString("ACTION"));
				caTmActivityLog.setDtAccessed(rs.getTimestamp("DT_ACCESSED"));
				caTmActivityLog.setDtl(rs.getString("DTL"));
				caTmActivityLog.setProcessNum(rs.getBigDecimal("PROCESS_NUM"));
				caTmActivityLog.setFileName(rs.getString("FILENAME"));
				caTmActivityLog.setRecNum(rs.getBigDecimal("REC_NUM"));
				caTmActivityLog.setLoadResultUUID(rs.getString("LOAD_RESULT_UUID"));
				caTmActivityLogList.add(caTmActivityLog);
			}
			logger.debug("SecureCodeTempCaTmActivityLogDAO: selectAllSecureCodeTempCaTxAllTx - sqlSelectVO84CStage Records Found: "+recCnt);
			rs.close();
		}
		catch(Exception ex){
			//ex.printStackTrace();
			logger.error(
					"SecureCodeTempCaTmActivityLogDAO: selectAllSecureCodeTempCaTxAllTx: Exception has occurred while trying to fetch all records from Staging Table securecode_temp.CA_TM_ACTIVITY_LOG: ",
					ex);
		}
		finally {
		}
		logger.info("SecureCodeTempCaTmActivityLogDAO: selectAllSecureCodeTempCaTxAllTx - Execution Ended");
		return caTmActivityLogList;
	}
}