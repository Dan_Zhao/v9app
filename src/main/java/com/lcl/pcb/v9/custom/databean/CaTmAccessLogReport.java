/**
 * 
 */
package com.lcl.pcb.v9.custom.databean;

import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * @author Dev
 *
 */
public class CaTmAccessLogReport {
	private String adminId;
	private String firstNm;
	private String middleNm;
	private String lastNm;
	private String description;
	private String rptType;
	private Timestamp strtDt;
	private Timestamp endDt;
	private Timestamp dtAccessed;
	private BigDecimal processNum;
	private String fileName;
	private BigDecimal recNum;
	private String loadResultUUID;
	/**
	 * @return the adminId
	 */
	public String getAdminId() {
		return adminId;
	}
	/**
	 * @param adminId the adminId to set
	 */
	public void setAdminId(String adminId) {
		this.adminId = adminId;
	}
	/**
	 * @return the firstNm
	 */
	public String getFirstNm() {
		return firstNm;
	}
	/**
	 * @param firstNm the firstNm to set
	 */
	public void setFirstNm(String firstNm) {
		this.firstNm = firstNm;
	}
	/**
	 * @return the middleNm
	 */
	public String getMiddleNm() {
		return middleNm;
	}
	/**
	 * @param middleNm the middleNm to set
	 */
	public void setMiddleNm(String middleNm) {
		this.middleNm = middleNm;
	}
	/**
	 * @return the lastNm
	 */
	public String getLastNm() {
		return lastNm;
	}
	/**
	 * @param lastNm the lastNm to set
	 */
	public void setLastNm(String lastNm) {
		this.lastNm = lastNm;
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	 * @return the rptType
	 */
	public String getRptType() {
		return rptType;
	}
	/**
	 * @param rptType the rptType to set
	 */
	public void setRptType(String rptType) {
		this.rptType = rptType;
	}
	/**
	 * @return the strtDt
	 */
	public Timestamp getStrtDt() {
		return strtDt;
	}
	/**
	 * @param strtDt the strtDt to set
	 */
	public void setStrtDt(Timestamp strtDt) {
		this.strtDt = strtDt;
	}
	/**
	 * @return the endDt
	 */
	public Timestamp getEndDt() {
		return endDt;
	}
	/**
	 * @param endDt the endDt to set
	 */
	public void setEndDt(Timestamp endDt) {
		this.endDt = endDt;
	}
	/**
	 * @return the dtAccessed
	 */
	public Timestamp getDtAccessed() {
		return dtAccessed;
	}
	/**
	 * @param dtAccessed the dtAccessed to set
	 */
	public void setDtAccessed(Timestamp dtAccessed) {
		this.dtAccessed = dtAccessed;
	}
	/**
	 * @return the processNum
	 */
	public BigDecimal getProcessNum() {
		return processNum;
	}
	/**
	 * @param processNum the processNum to set
	 */
	public void setProcessNum(BigDecimal processNum) {
		this.processNum = processNum;
	}
	/**
	 * @return the fileName
	 */
	public String getFileName() {
		return fileName;
	}
	/**
	 * @param fileName the fileName to set
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	/**
	 * @return the recNum
	 */
	public BigDecimal getRecNum() {
		return recNum;
	}
	/**
	 * @param recNum the recNum to set
	 */
	public void setRecNum(BigDecimal recNum) {
		this.recNum = recNum;
	}
	/**
	 * @return the loadResultUUID
	 */
	public String getLoadResultUUID() {
		return loadResultUUID;
	}
	/**
	 * @param loadResultUUID the loadResultUUID to set
	 */
	public void setLoadResultUUID(String loadResultUUID) {
		this.loadResultUUID = loadResultUUID;
	}
	
	
}
