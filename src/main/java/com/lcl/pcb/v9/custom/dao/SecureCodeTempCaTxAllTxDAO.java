package com.lcl.pcb.v9.custom.dao;

import java.sql.ResultSet;
import java.util.ArrayList;

import com.lcl.pcb.v9.custom.databean.CaTmAllTx;
import com.lcl.pcb.v9.generic.loader.V9ValidationDaoManager;

public class SecureCodeTempCaTxAllTxDAO {

	public static void deleteAllSecureCodeTempCaTxAllTx() throws Exception {
		try {
			String sqlDeleteVO84AStage = "delete securecode_temp.CA_TM_ALL_TX";
			V9ValidationDaoManager.execNonQuery(sqlDeleteVO84AStage);
		}
		catch(Exception e){
			throw e;
		}
		finally {
		}
	}

	public static ArrayList<CaTmAllTx> selectAllSecureCodeTempCaTxAllTx() throws Exception {
		
		ArrayList<CaTmAllTx> caTmAllTxList = new ArrayList<CaTmAllTx>();
		
		try {
			String sqlSelectVO84AStage = "select * from securecode_temp.CA_TM_ALL_TX";
			ResultSet rs=V9ValidationDaoManager.execQuery(sqlSelectVO84AStage);
			while(rs.next()){

				CaTmAllTx caTmAllTx=new CaTmAllTx();
				caTmAllTx.setIssuerNm(rs.getString("ISSUER_NM"));
				caTmAllTx.setBeginRange(rs.getInt("BEGIN_RANGE"));
				caTmAllTx.setEndRange(rs.getInt("END_RANGE"));
				caTmAllTx.setBusinessId(rs.getString("BUSINESS_ID"));
				caTmAllTx.setCrdHldrNm(rs.getString("CRD_HLDR_NM"));
				caTmAllTx.setCrdNum  (rs.getString("CRD_NUM  "));
				caTmAllTx.setProxypan(rs.getInt("PROXYPAN"));
				caTmAllTx.setTransProxypan(rs.getInt("TRANS_PROXYPAN"));
				caTmAllTx.setInstanceId(rs.getString("INSTANCE_ID"));
				caTmAllTx.setPurchaseXid(rs.getString("PURCHASE_XID"));
				caTmAllTx.setParesSigningTm(rs.getDate("PARES_SIGNING_TM"));
				caTmAllTx.setRiskAdviceStat(rs.getString("RISK_ADVICE_STAT"));
				caTmAllTx.setAuthentication(rs.getString("AUTHENTICATION"));
				caTmAllTx.setCcy(rs.getString("CCY"));
				caTmAllTx.setAmt(rs.getString("AMT"));
				caTmAllTx.setMerchantNm(rs.getString("MERCHANT_NM"));
				caTmAllTx.setMerchantUrl(rs.getString("MERCHANT_URL"));
				caTmAllTx.setMerchantId(rs.getString("MERCHANT_ID"));
				caTmAllTx.setMerchantCtry(rs.getString("MERCHANT_CTRY"));
				caTmAllTx.setPwdinfoStat(rs.getString("PWDINFO_STAT"));
				caTmAllTx.setVerifyPwdStat(rs.getString("VERIFY_PWD_STAT"));
				caTmAllTx.setHintQStat(rs.getString("HINT_Q_STAT"));
				caTmAllTx.setVerifyHintAnsStat(rs.getString("VERIFY_HINT_ANS_STAT"));
				caTmAllTx.setTransStat(rs.getString("TRANS_STAT"));
				caTmAllTx.setCalloutStat(rs.getString("CALLOUT_STAT"));
				caTmAllTx.setTransReqDt(rs.getDate("TRANS_REQ_DT"));
				caTmAllTx.setDevice(rs.getString("DEVICE"));
				caTmAllTx.setVerifyPwdReqTm(rs.getDate("VERIFY_PWD_REQ_TM"));
				caTmAllTx.setReceiptReqTm(rs.getDate("RECEIPT_REQ_TM"));
				caTmAllTx.setAfterNumFailed(rs.getInt("AFTER_NUM_FAILED"));
				caTmAllTx.setAfterNumDeclines(rs.getInt("AFTER_NUM_DECLINES"));
				caTmAllTx.setTransType(rs.getString("TRANS_TYPE"));
				caTmAllTx.setReason(rs.getString("REASON"));
				caTmAllTx.setHexEncdTransPrf(rs.getString("HEX_ENCD_TRANS_PRF"));
				caTmAllTx.setBase64EncdTransPrf(rs.getString("BASE64_ENCD_TRANS_PRF"));
				caTmAllTx.setCrdhldrIpAddr(rs.getString("CRDHLDR_IP_ADDR"));
				
				caTmAllTx.setRecLoadDtTms(rs.getDate("REC_LOAD_DT_TMS"));
				caTmAllTx.setProcessNum(rs.getInt("PROCESS_NUM"));
				caTmAllTx.setFilename(rs.getString("FILENAME"));
				caTmAllTx.setRecNum(rs.getInt("REC_NUM"));
				caTmAllTx.setLoadResultUuid(rs.getBytes("LOAD_RESULT_UUID"));

				caTmAllTxList.add(caTmAllTx);
			}
		}
		catch(Exception e){
			throw e;
		}
		finally {
		}

		return caTmAllTxList;
	}
}