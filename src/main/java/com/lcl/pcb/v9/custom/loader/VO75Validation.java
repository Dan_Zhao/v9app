package com.lcl.pcb.v9.custom.loader;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.lcl.pcb.v9.generic.loader.LoaderUtil;
import com.lcl.pcb.v9.generic.loader.V9ValidationDaoManager;
import com.lcl.pcb.v9.idf.jobflow.config.IDFConfig;
import com.lcl.pcb.v9.utilities.audit.AuditTablePojo;
import com.lcl.pcb.v9.utilities.audit.AuditTableUtil;
import com.lcl.pcb.v9.utilities.fileReader.FileUtil;

/**
 * This class is used to validate the csv files generated for VO70 interface. It
 * validates whether the data was previously loaded in table with the
 * ADM_EXTRACT_DT value in the Header csv file.It validates whether trailer
 * record segments count and processed records counts are same and validates the
 * total record count also.
 */
public class VO75Validation {

	ArrayList<String> csvFilesList = new ArrayList<String>();
	ArrayList<String> bodyCsvFilesList = new ArrayList<String>();
	ArrayList<String> bprList = new ArrayList<String>();
	String csvFiles;
	public String ediCsvFile = "";
	BufferedReader br, br1, br2, brEdi;
	BufferedReader badrecordreader;
	String line = "", lineRMR = "", lineQTY = "";
	String dateCreated = "";
	boolean headerResult = true;
	boolean trailerResult = true;
	boolean bodyResult = true;
	boolean doValidation = true;
	HashMap<String, Integer> totRecParsed = new HashMap<String, Integer>();
	HashMap<String, Integer> trailerRecList = new HashMap<String, Integer>();
	public String amtCsvFile = "", rmrCsvFile = "", qtyCSVFile = "",
			depCsvFile = "", bprCsvFile="";
	public String bodyCsvFile = "";
	int totalRecCount = 0;
	int totalRMRRecCount = 0;
	int totalNumOfPayments = 0;
	int rptNum = 0, totalQuantityVal = 0;
	String bprCount="";
	double totalPaymentAmount = 0.0, amount = 0.0;
	Set<String> depList = new HashSet<String>(500);
	String dateFormat = "DD-Mon-YYYY HH24:MI:SS";
	String SourceFileId = "";
	Logger logger = LogManager.getLogger(VO70Validation.class.getName());
	V9ValidationDaoManager v9Validation = new V9ValidationDaoManager();
	public static BufferedReader bpr, rmr, dtm, ref;
	static String line1 = "", refLine = "", rLine = "";
	static String rmrLine;
	static String bprLine = "", newNextLine = "";
	int seqValue = IDFConfig.getInstance().getSequenceNum();
	String interfaceName = IDFConfig.getInstance().getIdfData()
			.getFilePropName();
	String fileSizeVal = null;
	int qtyCount = 0;

	// This method will be invoked from V9Loader class which expects the csv
	// files list for validating header, body and trailer.
	public boolean doValidation(ArrayList<String> files) throws Exception {
		logger.debug("VO70Validation Called");
		this.csvFilesList = files;
		doValidation &= doHeader();
		doValidation &= doTrailer();
		doValidation &= doBody();
		return doValidation;
	}

	public boolean doHeader() throws Exception {
		logger.debug("VO75Validation doHeader() Called");
		return headerResult;
	}

	public boolean doTrailer() {
		logger.debug("VO75Validation doTrailer() Called");
		totRecParsed = FileUtil.totalRecParsedList;
		for (Entry<String, Integer> entry : totRecParsed.entrySet()) {
			totalRecCount += (entry.getValue());
		}
		if (totalRecCount == 0) {

			logger.error("No record found in file\n");
			trailerResult = false;
		}
		return trailerResult;
	}

	public boolean doBody() throws IOException, SQLException {
		logger.debug("VO75Validation doBody() Called");
		for (int i = 0; i < csvFilesList.size(); i++) {
			 if (csvFilesList.get(i).contains("QTY")) {
				qtyCSVFile = csvFilesList.get(i);
				try {
					br2 = new BufferedReader(new FileReader(qtyCSVFile));
					try {
						String[] qtyVal = null;
						String qtyNewVal= null;
						String[] qtyNew = null;
						while ((lineQTY = br2.readLine()) != null) {
							qtyVal = lineQTY.split("\\|");

							/*if (qtyVal[0].equalsIgnoreCase("QTY")
									&& qtyVal.length > 2
									&& qtyVal[1].equalsIgnoreCase("46")) {
								qtyCount++;
								totalQuantityVal = totalQuantityVal
										+ Integer
												.parseInt(qtyVal[2].toString());

							}*/
							
							if (qtyVal[0].equalsIgnoreCase("QTY")
									&& qtyVal.length > 2 && qtyVal[1].equalsIgnoreCase("42") ){
								qtyNewVal= br2.readLine();
								qtyNew=qtyNewVal.split("\\|");
								if (qtyNew[0].equalsIgnoreCase("QTY")
									&& qtyNew.length > 2
									&& qtyNew[1].equalsIgnoreCase("46")){
									
									totalQuantityVal = totalQuantityVal
											+ Integer
													.parseInt(qtyNew[2].toString());
									bprList.add(qtyNew[2]);
									
								}							
								
							}
						}
						br.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				} catch (FileNotFoundException ex) {
					ex.printStackTrace();
				}

			} else if (csvFilesList.get(i).contains("BPR")) {
				
				String bprFile = IDFConfig.getInstance().getIdfData()
						.getDatafileCsvFileLocation()
						+ "VO75_BODY_BPR_temp_" + seqValue + ".csv";
				FileWriter file = new FileWriter(bprFile);
				BufferedWriter bw = new BufferedWriter(file);
				BufferedReader bprReader = new BufferedReader(new FileReader(
						csvFilesList.get(i)));
						
							for (int j=0;j<bprList.size();j++){
								while ((line = bprReader.readLine())!=null){
								bprCount= bprList.get(j);
								int bprCountVal= Integer.parseInt(bprCount.toString());
								for (int k=0; k<bprCountVal; k++){
								bw.write(line);
								bw.newLine();
							}
								break;
								
						}
					}
						
							bw.flush();			
				prepareBPR(bprFile);
			} else if (csvFilesList.get(i).contains("AMT")) {
				amtCsvFile = csvFilesList.get(i);
				try {
					br = new BufferedReader(new FileReader(amtCsvFile));
					try {
						String[] amtVal = null;
						while ((line = br.readLine()) != null) {
							amtVal = line.split("\\|");

							if (amtVal[0].equalsIgnoreCase("AMT")
									&& amtVal.length > 2
									&& amtVal[1].equalsIgnoreCase("4")) {

								totalPaymentAmount += Double
										.parseDouble(amtVal[2].toString());

							}
						}
						br.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				} catch (FileNotFoundException ex) {
					ex.printStackTrace();
				}

			} else if (csvFilesList.get(i).contains("RMR")) {
				prepareRMR(csvFilesList.get(i));
				rmrCsvFile = csvFilesList.get(i);
				br1 = new BufferedReader(new FileReader(rmrCsvFile));
				String[] bodyVal = null;
				while ((lineRMR = br1.readLine()) != null) {
					totalRMRRecCount++;
					bodyVal = lineRMR.split("\\|");
					String totalAmt = null;
					if (bodyVal[0].equalsIgnoreCase("RMR")
							&& bodyVal.length > 5) {
						totalAmt = bodyVal[3].toString();
						if (totalAmt.equalsIgnoreCase(null)
								|| totalAmt.equalsIgnoreCase("")) {
							logger.error("VO70 Record Body Segment: Cannot find field Body.Amount");
							bodyResult = false;
						} else {
							amount += Double.parseDouble(bodyVal[3].toString());
						}

					}
				}
				if (Math.abs(amount - totalPaymentAmount) > 0.01) {
					logger.error("Total amount mismatch: Calculated = "
							+ amount + "  From File : " + totalPaymentAmount
							+ "");
					bodyResult = false;
				}

				/*if (qtyCount >= 2) {
					totalQuantityVal = totalQuantityVal / 2;
				}*/
				if (totalRMRRecCount != totalQuantityVal) {
					logger.error("Record count mismatch : Records parsed "
							+ totalRMRRecCount + "  From File: "
							+ totalQuantityVal + "");
					bodyResult = false;
				}

			} else if (csvFilesList.get(i).contains("DTM")) {
				prepareDTM(csvFilesList.get(i));
			} else if (csvFilesList.get(i).contains("DEP")) {
				depCsvFile = csvFilesList.get(i);
				try {
					br = new BufferedReader(new FileReader(depCsvFile));
					try {
						String[] depVal = null;
						while ((line = br.readLine()) != null) {
							depVal = line.split("\\|");
							if (depVal[0].equalsIgnoreCase("DEP")
									&& depVal.length > 2) {
								depList.add(depVal[3]);
							}
						}
						br.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				} catch (FileNotFoundException ex) {
					ex.printStackTrace();
				}
			}

			else if (csvFilesList.get(i).contains("REF")) {
				String refFile = IDFConfig.getInstance().getIdfData()
						.getDatafileCsvFileLocation()
						+ "VO75_BODY_ref_temp_" + seqValue + ".csv";
				FileWriter file = new FileWriter(refFile);
				BufferedWriter bw = new BufferedWriter(file);
				BufferedReader refReader = new BufferedReader(new FileReader(
						csvFilesList.get(i)));
				String[] refNumber = null;
				while ((line = refReader.readLine()) != null) {
					refNumber = line.split("\\|");
					if (refNumber[0].equalsIgnoreCase("REF")
							&& refNumber.length > 2) {

						if (!depList.contains(refNumber[2])
								&& (refNumber[1] != null && !"ZZ"
										.equalsIgnoreCase(refNumber[1]))) {
							bw.write(line);
							bw.newLine();
						}
					}

				}
				bw.flush();
				prepareREF(refFile);
			}
		}

		if (csvFilesList.size() > 4) {
			String ediFile = IDFConfig.getInstance().getIdfData()
					.getDatafileCsvFileLocation()
					+ "VO75_BODY_EDI_" + seqValue + ".csv";
			FileWriter file = new FileWriter(ediFile);
			BufferedWriter bw = new BufferedWriter(file);
			BufferedReader refReader = rmr;

			while (refReader.ready()) {

				newNextLine = getBPRNext() + "|" + getRMRNext() + "|"
						+ getDTMNext() + "|" + getREFNext();
				bw.write(newNextLine);
				bw.newLine();
			}
			bw.flush();
			for (int j = 0; j < FileUtil.totalCSVFileNamesList.size(); j++) {
				FileUtil.totalCSVFileNamesList.clear();
			}
			FileUtil.totalCSVFileNamesList.add(ediFile);
		}
		insertLaodResult();
		return bodyResult;
	}

	public static void prepareBPR(String f) throws IOException {
		bpr = new BufferedReader(new FileReader(f));
		/*while ((line1 = bpr.readLine()) != null) {
			bprLine = line1;
		}*/
	}

	public static void prepareRMR(String f) throws FileNotFoundException {
		rmr = new BufferedReader(new FileReader(f));
	}

	public static void prepareDTM(String f) throws FileNotFoundException {
		dtm = new BufferedReader(new FileReader(f));
	}

	public static void prepareREF(String f) throws IOException {
		ref = new BufferedReader(new FileReader(f));
		// ref.readLine();
		// ref.readLine();

	}

	public static String getBPRNext() throws IOException {
		return bpr.readLine();
	}

	public static String getRMRNext() throws IOException {
		return rmr.readLine();
	}

	public static String getDTMNext() throws IOException {
		return dtm.readLine();
	}

	public static String getREFNext() throws IOException {
		return ref.readLine();
	}

	public void insertLaodResult() throws SQLException {
		AuditTablePojo auditPojo = new AuditTablePojo();
		auditPojo.setProcessNumber(seqValue + "");
		auditPojo.setSchemaName(IDFConfig.getInstance().getIdfData()
				.getCsvloadDbSchema());
		auditPojo.setSourceInterface(interfaceName);
		auditPojo
				.setSourceNM(IDFConfig
						.getInstance()
						.getIdfData()
						.getDatafileDatafileLocation()
						.substring(
								IDFConfig.getInstance().getIdfData()
										.getDatafileDatafileLocation()
										.lastIndexOf("/") + 1));
		fileSizeVal = IDFConfig.getInstance().getIdfData().getFileSize();
		if (fileSizeVal != null && !fileSizeVal.isEmpty()) {
			auditPojo.setSourceSize(fileSizeVal);
		} else {
			auditPojo.setSourceSize("-9");
		}

		auditPojo.setTableName("EDI_PYMT_TRANS");
		String sourceDTLRec = Integer.toString(totalRMRRecCount);
		auditPojo.setSourceDtlRec(sourceDTLRec);

		auditPojo = AuditTableUtil.insertAudit(auditPojo);
		auditPojo = null;
	}

	public void doErrorLog() throws Exception {

	}

	public void doFinalize(ArrayList<String> files) throws Exception {

		logger.debug("VO75Validation doFinalize() Called ");
		int successCount = LoaderUtil.successLoadCount;
		int failureCount = LoaderUtil.errorLoadCount;
		for (int i = 0; i < files.size(); i++) {
			if (files.get(i).contains("EDI")) {
				ediCsvFile = files.get(i);
				try {
					brEdi = new BufferedReader(new FileReader(ediCsvFile));

					try {
						line = brEdi.readLine();
						String[] ediVal = line.split("\\|");
						if (ediVal[0].equalsIgnoreCase("BPR")
								&& ediVal.length > 10) {
							dateCreated = ediVal[8];
							SourceFileId = "EDI";
							StringBuilder sb = new StringBuilder("");
							sb.append("UPDATE PCMCPYMT.LOAD_RESULT SET SOURCE_END_DT_CURR = ");
							sb.append("to_timestamp('" + dateCreated
									+ "','yyyy-mm-dd HH24:MI:SS')");
							sb.append(",");
							sb.append("SOURCE_ID = '");
							sb.append(SourceFileId);
							sb.append("'");
							sb.append(",");
							sb.append("TL_REC_LOAD = '");
							sb.append(successCount);
							sb.append("'");
							sb.append(",");
							sb.append("LOAD_ERRORS = '");
							sb.append(failureCount);
							sb.append("'");
							sb.append(" WHERE PROCESS_NUM = '");
							sb.append(seqValue);
							sb.append("'");
							V9ValidationDaoManager.execNonQuery(sb.toString());
							sb = null;
						}

					} catch (IOException e) {
						logger.error("Error in VO75 inerface Finalize method.");
						e.printStackTrace();
					}
				} catch (FileNotFoundException ex) {
					ex.printStackTrace();
				}
			}
		}
	}
}
