package com.lcl.pcb.v9.custom.databean;

import java.util.Date;

public class CaTmAllTx {
	private String issuerNm;
	private int beginRange=0;
	private int endRange=0;
	private String businessId;
	private String crdHldrNm;
	private String crdNum  ;
	private int proxypan=0;
	private int transProxypan=0;
	private String instanceId;
	private String purchaseXid;
	private Date paresSigningTm;
	private String riskAdviceStat;
	private String authentication;
	private String ccy;
	private String amt;
	private String merchantNm;
	private String merchantUrl;
	private String merchantId;
	private String merchantCtry;
	private String pwdinfoStat;
	private String verifyPwdStat;
	private String hintQStat;
	private String verifyHintAnsStat;
	private String transStat;
	private String calloutStat;
	private Date transReqDt;
	private String device;
	private Date verifyPwdReqTm;
	private Date receiptReqTm;
	private int afterNumFailed=0;
	private int afterNumDeclines=0;
	private String transType;
	private String reason;
	private String hexEncdTransPrf;
	private String base64EncdTransPrf;
	private String crdhldrIpAddr;
	
	private Date recLoadDtTms;
	private int processNum=0;
	private String filename;
	private int recNum=0;
	private byte[] loadResultUuid;

	public String getIssuerNm() {
		return issuerNm;
	}
	public void setIssuerNm(String issuerNm) {
		this.issuerNm = issuerNm;
	}
	public int getBeginRange() {
		return beginRange;
	}
	public void setBeginRange(int beginRange) {
		this.beginRange = beginRange;
	}
	public int getEndRange() {
		return endRange;
	}
	public void setEndRange(int endRange) {
		this.endRange = endRange;
	}
	public String getBusinessId() {
		return businessId;
	}
	public void setBusinessId(String businessId) {
		this.businessId = businessId;
	}
	public String getCrdHldrNm() {
		return crdHldrNm;
	}
	public void setCrdHldrNm(String crdHldrNm) {
		this.crdHldrNm = crdHldrNm;
	}
	public String getCrdNum() {
		return crdNum;
	}
	public void setCrdNum(String crdNum) {
		this.crdNum = crdNum;
	}
	public int getProxypan() {
		return proxypan;
	}
	public void setProxypan(int proxypan) {
		this.proxypan = proxypan;
	}
	public int getTransProxypan() {
		return transProxypan;
	}
	public void setTransProxypan(int transProxypan) {
		this.transProxypan = transProxypan;
	}
	public String getInstanceId() {
		return instanceId;
	}
	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}
	public String getPurchaseXid() {
		return purchaseXid;
	}
	public void setPurchaseXid(String purchaseXid) {
		this.purchaseXid = purchaseXid;
	}
	public Date getParesSigningTm() {
		return paresSigningTm;
	}
	public void setParesSigningTm(Date paresSigningTm) {
		this.paresSigningTm = paresSigningTm;
	}
	public String getRiskAdviceStat() {
		return riskAdviceStat;
	}
	public void setRiskAdviceStat(String riskAdviceStat) {
		this.riskAdviceStat = riskAdviceStat;
	}
	public String getAuthentication() {
		return authentication;
	}
	public void setAuthentication(String authentication) {
		this.authentication = authentication;
	}
	public String getCcy() {
		return ccy;
	}
	public void setCcy(String ccy) {
		this.ccy = ccy;
	}
	public String getAmt() {
		return amt;
	}
	public void setAmt(String amt) {
		this.amt = amt;
	}
	public String getMerchantNm() {
		return merchantNm;
	}
	public void setMerchantNm(String merchantNm) {
		this.merchantNm = merchantNm;
	}
	public String getMerchantUrl() {
		return merchantUrl;
	}
	public void setMerchantUrl(String merchantUrl) {
		this.merchantUrl = merchantUrl;
	}
	public String getMerchantId() {
		return merchantId;
	}
	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}
	public String getMerchantCtry() {
		return merchantCtry;
	}
	public void setMerchantCtry(String merchantCtry) {
		this.merchantCtry = merchantCtry;
	}
	public String getPwdinfoStat() {
		return pwdinfoStat;
	}
	public void setPwdinfoStat(String pwdinfoStat) {
		this.pwdinfoStat = pwdinfoStat;
	}
	public String getVerifyPwdStat() {
		return verifyPwdStat;
	}
	public void setVerifyPwdStat(String verifyPwdStat) {
		this.verifyPwdStat = verifyPwdStat;
	}
	public String getHintQStat() {
		return hintQStat;
	}
	public void setHintQStat(String hintQStat) {
		this.hintQStat = hintQStat;
	}
	public String getVerifyHintAnsStat() {
		return verifyHintAnsStat;
	}
	public void setVerifyHintAnsStat(String verifyHintAnsStat) {
		this.verifyHintAnsStat = verifyHintAnsStat;
	}
	public String getTransStat() {
		return transStat;
	}
	public void setTransStat(String transStat) {
		this.transStat = transStat;
	}
	public String getCalloutStat() {
		return calloutStat;
	}
	public void setCalloutStat(String calloutStat) {
		this.calloutStat = calloutStat;
	}
	public Date getTransReqDt() {
		return transReqDt;
	}
	public void setTransReqDt(Date transReqDt) {
		this.transReqDt = transReqDt;
	}
	public String getDevice() {
		return device;
	}
	public void setDevice(String device) {
		this.device = device;
	}
	public Date getVerifyPwdReqTm() {
		return verifyPwdReqTm;
	}
	public void setVerifyPwdReqTm(Date verifyPwdReqTm) {
		this.verifyPwdReqTm = verifyPwdReqTm;
	}
	public Date getReceiptReqTm() {
		return receiptReqTm;
	}
	public void setReceiptReqTm(Date receiptReqTm) {
		this.receiptReqTm = receiptReqTm;
	}
	public int getAfterNumFailed() {
		return afterNumFailed;
	}
	public void setAfterNumFailed(int afterNumFailed) {
		this.afterNumFailed = afterNumFailed;
	}
	public int getAfterNumDeclines() {
		return afterNumDeclines;
	}
	public void setAfterNumDeclines(int afterNumDeclines) {
		this.afterNumDeclines = afterNumDeclines;
	}
	public String getTransType() {
		return transType;
	}
	public void setTransType(String transType) {
		this.transType = transType;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public String getHexEncdTransPrf() {
		return hexEncdTransPrf;
	}
	public void setHexEncdTransPrf(String hexEncdTransPrf) {
		this.hexEncdTransPrf = hexEncdTransPrf;
	}
	public String getBase64EncdTransPrf() {
		return base64EncdTransPrf;
	}
	public void setBase64EncdTransPrf(String base64EncdTransPrf) {
		this.base64EncdTransPrf = base64EncdTransPrf;
	}
	public String getCrdhldrIpAddr() {
		return crdhldrIpAddr;
	}
	public void setCrdhldrIpAddr(String crdhldrIpAddr) {
		this.crdhldrIpAddr = crdhldrIpAddr;
	}
	public Date getRecLoadDtTms() {
		return recLoadDtTms;
	}
	public void setRecLoadDtTms(Date recLoadDtTms) {
		this.recLoadDtTms = recLoadDtTms;
	}
	public int getProcessNum() {
		return processNum;
	}
	public void setProcessNum(int processNum) {
		this.processNum = processNum;
	}
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	public int getRecNum() {
		return recNum;
	}
	public void setRecNum(int recNum) {
		this.recNum = recNum;
	}
	public byte[] getLoadResultUuid() {
		return loadResultUuid;
	}
	public void setLoadResultUuid(byte[] loadResultUuid) {
		this.loadResultUuid = loadResultUuid;
	}

}
