package com.lcl.pcb.v9.custom.dao;

import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import com.lcl.pcb.v9.custom.databean.CaTmAllTx;

public class SecureCodeCaTxAllTxDAO {

	public static boolean isExistSecureCodeCaTxAllTxByBusinessKey(String crdNum, Date transReqDt, String purchaseXid) throws Exception {
		boolean recExist = false;
		
		try {
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd hh24:mm:ss");
			String transReqDtString = dateFormat.format(transReqDt);
			String transReqDtExpr = "to_date('"+transReqDtString+"','yyyy/MM/dd hh24:mi:ss')";
			
			String sqlSelectVO84AStage = "select * from securecode.CA_TM_ALL_TX where " + 
											"CRD_NUM=" + "'" +crdNum + "'" + " and " + 
											"PURCHASE_XID=" + "'" +purchaseXid + "'" + " and " + 
											"TRANS_REQ_DT=" + "'" +transReqDtExpr + "'";
			ResultSet rs=V9MultipleStagesTrgDaoManager.execQuery(sqlSelectVO84AStage);
			
			while(rs.next()){
				recExist = true;
			}
		}
		catch(Exception e){
			throw e;
		}

		return recExist;
	}

	public static void insertSecureCodeCaTxAllTx() throws Exception {

		try {
			StringBuffer columns = new StringBuffer(500);
			columns.append(" ISSUER_NM");
			columns.append(",BEGIN_RANGE");
			columns.append(",END_RANGE");
			columns.append(",BUSINESS_ID");
			columns.append(",CRD_HLDR_NM");
			columns.append(",CRD_NUM  ");
			columns.append(",PROXYPAN");
			columns.append(",TRANS_PROXYPAN");
			columns.append(",INSTANCE_ID");
			columns.append(",PURCHASE_XID");
			columns.append(",PARES_SIGNING_TM");
			columns.append(",RISK_ADVICE_STAT");
			columns.append(",AUTHENTICATION");
			columns.append(",CCY");
			columns.append(",MERCHANT_NM");
			columns.append(",MERCHANT_URL");
			columns.append(",MERCHANT_ID");
			columns.append(",MERCHANT_CTRY");
			columns.append(",PWDINFO_STAT");
			columns.append(",VERIFY_PWD_STAT");
			columns.append(",HINT_Q_STAT");
			columns.append(",VERIFY_HINT_ANS_STAT");
			columns.append(",TRANS_STAT");
			columns.append(",CALLOUT_STAT");
			columns.append(",TRANS_REQ_DT");
			columns.append(",DEVICE");
			columns.append(",VERIFY_PWD_REQ_TM");
			columns.append(",RECEIPT_REQ_TM");
			columns.append(",AFTER_NUM_FAILED");
			columns.append(",AFTER_NUM_DECLINES");
			columns.append(",TRANS_TYPE");
			columns.append(",REASON");
			columns.append(",HEX_ENCD_TRANS_PRF");
			columns.append(",BASE64_ENCD_TRANS_PRF");
			columns.append(",CRDHLDR_IP_ADDR");
			columns.append(",REC_LOAD_DT_TMS");
			columns.append(",PROCESS_NUM");
			columns.append(",FILENAME");
			columns.append(",REC_NUM");
			columns.append(",LOAD_RESULT_UUID");

			String insertColumns =  columns.toString()+",AMT";
			String valueColumns =  columns.toString()+",ROUND(TO_NUMBER(AMT),2)";

			String sqlInsertVO84AStage = "insert into securecode.CA_TM_ALL_TX (" + insertColumns + ") select " + valueColumns + "from securecode_temp.CA_TM_ALL_TX";
			V9MultipleStagesTrgDaoManager.execNonQuery(sqlInsertVO84AStage);
		} catch (Exception e) {
			throw e;
		}
	}

	public static ArrayList<CaTmAllTx> selectAllSecureCodeTempCaTxAllTx() throws Exception {

		ArrayList<CaTmAllTx> caTmAllTxList = new ArrayList<CaTmAllTx>();

		try {
			String sqlSelectVO84AStage = "select * from securecode.CA_TM_ALL_TX  ";
			ResultSet rs=V9MultipleStagesTrgDaoManager.execQuery(sqlSelectVO84AStage);
			while(rs.next()){

				CaTmAllTx caTmAllTx=new CaTmAllTx();
				caTmAllTx.setIssuerNm(rs.getString("ISSUER_NM"));
				caTmAllTx.setBeginRange(rs.getInt("BEGIN_RANGE"));
				caTmAllTx.setEndRange(rs.getInt("END_RANGE"));
				caTmAllTx.setBusinessId(rs.getString("BUSINESS_ID"));
				caTmAllTx.setCrdHldrNm(rs.getString("CRD_HLDR_NM"));
				caTmAllTx.setCrdNum  (rs.getString("CRD_NUM  "));
				caTmAllTx.setProxypan(rs.getInt("PROXYPAN"));
				caTmAllTx.setTransProxypan(rs.getInt("TRANS_PROXYPAN"));
				caTmAllTx.setInstanceId(rs.getString("INSTANCE_ID"));
				caTmAllTx.setPurchaseXid(rs.getString("PURCHASE_XID"));
				caTmAllTx.setParesSigningTm(rs.getDate("PARES_SIGNING_TM"));
				caTmAllTx.setRiskAdviceStat(rs.getString("RISK_ADVICE_STAT"));
				caTmAllTx.setAuthentication(rs.getString("AUTHENTICATION"));
				caTmAllTx.setCcy(rs.getString("CCY"));
				caTmAllTx.setAmt(rs.getString("AMT"));
				caTmAllTx.setMerchantNm(rs.getString("MERCHANT_NM"));
				caTmAllTx.setMerchantUrl(rs.getString("MERCHANT_URL"));
				caTmAllTx.setMerchantId(rs.getString("MERCHANT_ID"));
				caTmAllTx.setMerchantCtry(rs.getString("MERCHANT_CTRY"));
				caTmAllTx.setPwdinfoStat(rs.getString("PWDINFO_STAT"));
				caTmAllTx.setVerifyPwdStat(rs.getString("VERIFY_PWD_STAT"));
				caTmAllTx.setHintQStat(rs.getString("HINT_Q_STAT"));
				caTmAllTx.setVerifyHintAnsStat(rs.getString("VERIFY_HINT_ANS_STAT"));
				caTmAllTx.setTransStat(rs.getString("TRANS_STAT"));
				caTmAllTx.setCalloutStat(rs.getString("CALLOUT_STAT"));
				caTmAllTx.setTransReqDt(rs.getDate("TRANS_REQ_DT"));
				caTmAllTx.setDevice(rs.getString("DEVICE"));
				caTmAllTx.setVerifyPwdReqTm(rs.getDate("VERIFY_PWD_REQ_TM"));
				caTmAllTx.setReceiptReqTm(rs.getDate("RECEIPT_REQ_TM"));
				caTmAllTx.setAfterNumFailed(rs.getInt("AFTER_NUM_FAILED"));
				caTmAllTx.setAfterNumDeclines(rs.getInt("AFTER_NUM_DECLINES"));
				caTmAllTx.setTransType(rs.getString("TRANS_TYPE"));
				caTmAllTx.setReason(rs.getString("REASON"));
				caTmAllTx.setHexEncdTransPrf(rs.getString("HEX_ENCD_TRANS_PRF"));
				caTmAllTx.setBase64EncdTransPrf(rs.getString("BASE64_ENCD_TRANS_PRF"));
				caTmAllTx.setCrdhldrIpAddr(rs.getString("CRDHLDR_IP_ADDR"));

				caTmAllTx.setRecLoadDtTms(rs.getDate("REC_LOAD_DT_TMS"));
				caTmAllTx.setProcessNum(rs.getInt("PROCESS_NUM"));
				caTmAllTx.setFilename(rs.getString("FILENAME"));
				caTmAllTx.setRecNum(rs.getInt("REC_NUM"));
				caTmAllTx.setLoadResultUuid(rs.getBytes("LOAD_RESULT_UUID"));

				caTmAllTxList.add(caTmAllTx);
			}
		}
		catch(Exception e){
			throw e;
		}
		finally {
		}

		return caTmAllTxList;
	}
}