//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.2-147 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2018.05.16 at 09:57:56 PM EDT 
//


package com.lcl.pcb.v9.custom.databean;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.lcl.pcb.v9.custom.vo40b.databean package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.lcl.pcb.v9.custom.vo40b.databean
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link AirTravel }
     * 
     */
    public AirTravel createAirTravel() {
        return new AirTravel();
    }

    /**
     * Create an instance of {@link CustInfo }
     * 
     */
    public CustInfo createCustInfo() {
        return new CustInfo();
    }

    /**
     * Create an instance of {@link Cost }
     * 
     */
    public Cost createCost() {
        return new Cost();
    }

    /**
     * Create an instance of {@link Phone }
     * 
     */
    public Phone createPhone() {
        return new Phone();
    }

    /**
     * Create an instance of {@link Email }
     * 
     */
    public Email createEmail() {
        return new Email();
    }

    /**
     * Create an instance of {@link CarRentalTravel }
     * 
     */
    public CarRentalTravel createCarRentalTravel() {
        return new CarRentalTravel();
    }

    /**
     * Create an instance of {@link OtherTravel }
     * 
     */
    public OtherTravel createOtherTravel() {
        return new OtherTravel();
    }

    /**
     * Create an instance of {@link DateTimeType }
     * 
     */
    public DateTimeType createDateTimeType() {
        return new DateTimeType();
    }

    /**
     * Create an instance of {@link TravelInfo }
     * 
     */
    public TravelInfo createTravelInfo() {
        return new TravelInfo();
    }

    /**
     * Create an instance of {@link TripDetails }
     * 
     */
    public TripDetails createTripDetails() {
        return new TripDetails();
    }

    /**
     * Create an instance of {@link TransFileTrailer }
     * 
     */
    public TransFileTrailer createTransFileTrailer() {
        return new TransFileTrailer();
    }

    /**
     * Create an instance of {@link CruiseTravel }
     * 
     */
    public CruiseTravel createCruiseTravel() {
        return new CruiseTravel();
    }

    /**
     * Create an instance of {@link InsuranceTravel }
     * 
     */
    public InsuranceTravel createInsuranceTravel() {
        return new InsuranceTravel();
    }

    /**
     * Create an instance of {@link Salesfiledetails }
     * 
     */
    public Salesfiledetails createSalesfiledetails() {
        return new Salesfiledetails();
    }

    /**
     * Create an instance of {@link PackageTravel }
     * 
     */
    public PackageTravel createPackageTravel() {
        return new PackageTravel();
    }

    /**
     * Create an instance of {@link FinancialInfo }
     * 
     */
    public FinancialInfo createFinancialInfo() {
        return new FinancialInfo();
    }

    /**
     * Create an instance of {@link CustAddress }
     * 
     */
    public CustAddress createCustAddress() {
        return new CustAddress();
    }

    /**
     * Create an instance of {@link PersonalInfo }
     * 
     */
    public PersonalInfo createPersonalInfo() {
        return new PersonalInfo();
    }

    /**
     * Create an instance of {@link TransactionDetals }
     * 
     */
    public TransactionDetals createTransactionDetals() {
        return new TransactionDetals();
    }

    /**
     * Create an instance of {@link TransFileHeader }
     * 
     */
    public TransFileHeader createTransFileHeader() {
        return new TransFileHeader();
    }

    /**
     * Create an instance of {@link HotelTravel }
     * 
     */
    public HotelTravel createHotelTravel() {
        return new HotelTravel();
    }

    /**
     * Create an instance of {@link ItemCost }
     * 
     */
    public ItemCost createItemCost() {
        return new ItemCost();
    }

    /**
     * Create an instance of {@link RailTravel }
     * 
     */
    public RailTravel createRailTravel() {
        return new RailTravel();
    }

}
