//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.2-147 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2018.05.16 at 09:57:56 PM EDT 
//


package com.lcl.pcb.v9.custom.databean;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 *  
 *                 1  SalesFileName the sales file name received from vendor
 *                 2  Header the file header info
 *                 3  TransDetails a collection of transaction records(0 to unlimited)
 *                 4  Trailer the file trailer info
 * 			
 * 
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SalesFileName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Header" type="{}TransFileHeader"/>
 *         &lt;element name="TransDetails" type="{}TransactionDetals" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Trailer" type="{}TransFileTrailer"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "salesFileName",
    "header",
    "transDetails",
    "trailer"
})
@XmlRootElement(name = "Salesfiledetails")
public class Salesfiledetails {

    @XmlElement(name = "SalesFileName", required = true)
    protected String salesFileName;
    @XmlElement(name = "Header", required = true)
    protected TransFileHeader header;
    @XmlElement(name = "TransDetails")
    protected List<TransactionDetals> transDetails;
    @XmlElement(name = "Trailer", required = true)
    protected TransFileTrailer trailer;

    /**
     * Gets the value of the salesFileName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSalesFileName() {
        return salesFileName;
    }

    /**
     * Sets the value of the salesFileName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSalesFileName(String value) {
        this.salesFileName = value;
    }

    /**
     * Gets the value of the header property.
     * 
     * @return
     *     possible object is
     *     {@link TransFileHeader }
     *     
     */
    public TransFileHeader getHeader() {
        return header;
    }

    /**
     * Sets the value of the header property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransFileHeader }
     *     
     */
    public void setHeader(TransFileHeader value) {
        this.header = value;
    }

    /**
     * Gets the value of the transDetails property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the transDetails property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTransDetails().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TransactionDetals }
     * 
     * 
     */
    public List<TransactionDetals> getTransDetails() {
        if (transDetails == null) {
            transDetails = new ArrayList<TransactionDetals>();
        }
        return this.transDetails;
    }

    /**
     * Gets the value of the trailer property.
     * 
     * @return
     *     possible object is
     *     {@link TransFileTrailer }
     *     
     */
    public TransFileTrailer getTrailer() {
        return trailer;
    }

    /**
     * Sets the value of the trailer property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransFileTrailer }
     *     
     */
    public void setTrailer(TransFileTrailer value) {
        this.trailer = value;
    }

}
