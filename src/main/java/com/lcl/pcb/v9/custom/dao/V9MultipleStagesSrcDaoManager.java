/**
 * 
 */
package com.lcl.pcb.v9.custom.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import sun.jdbc.odbc.ee.DataSource;

/**
 * This V9MultipleStagesSrcDaoManager framework class is used to establish and manage the
 * database connections for interfaces It uses the factory pattern to return the
 * correct database table that is needed by the V9DAOManager
 */
public class V9MultipleStagesSrcDaoManager {
	static final Logger logger = LogManager
			.getLogger(V9MultipleStagesSrcDaoManager.class.getName());

	private DataSource src;
	public Connection con;
	public static String URL;
	public static String USER;
	public static String PASS;

	public V9MultipleStagesSrcDaoManager(){

		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
		} catch (ClassNotFoundException ex) {
			logger.debug("Error: unable to load driver class for Oracle");
			System.out.println("Error: unable to load driver class for Oracle");
			System.exit(1);
		}

		URL = "jdbc:oracle:thin:@"
				+ URL;

		try {
			this.con = DriverManager.getConnection(URL, USER, PASS);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private static class MultiStageValidationSrcDAOSingleton {

		public static final ThreadLocal<V9MultipleStagesSrcDaoManager> INSTANCE;
		static {
			ThreadLocal<V9MultipleStagesSrcDaoManager> dm;
			try {
				dm = new ThreadLocal<V9MultipleStagesSrcDaoManager>() {
					@Override
					protected V9MultipleStagesSrcDaoManager initialValue() {
						try {
							return new V9MultipleStagesSrcDaoManager();
						} catch (Exception e) {
							return null;
						}
					}
				};
			} catch (Exception e) {
				dm = null;
			}
			INSTANCE = dm;
		}

	}

	public static V9MultipleStagesSrcDaoManager getInstance() {
		return MultiStageValidationSrcDAOSingleton.INSTANCE.get();
	}

	public void open() throws SQLException {
		try {
			if (this.con == null || this.con.isClosed())
				this.con = src.getConnection();
		} catch (SQLException e) {
			throw e;
		}
	}

	public void close() throws SQLException {
		try {
			if (this.con != null && !this.con.isClosed())
				this.con.close();
		} catch (SQLException e) {
			throw e;
		}
	}

	public static ResultSet execQuery(String sql) throws SQLException {
		V9MultipleStagesSrcDaoManager x = V9MultipleStagesSrcDaoManager.getInstance();
		PreparedStatement p1 = x.con.prepareStatement(sql);
		ResultSet k = p1.executeQuery();
		return k;
	}

	public static ResultSet execQuery(String sql, int fetchSize) throws SQLException {
		V9MultipleStagesSrcDaoManager x = V9MultipleStagesSrcDaoManager.getInstance();
		PreparedStatement p1 = x.con.prepareStatement(sql);
		p1.setFetchSize(fetchSize);
		ResultSet k = p1.executeQuery();
		return k;
	}
	
	public static int execNonQuery(String sql) throws SQLException {
		V9MultipleStagesSrcDaoManager x = V9MultipleStagesSrcDaoManager.getInstance();
		PreparedStatement p2 = x.con.prepareStatement(sql);
		int k = p2.executeUpdate();
		p2.close();
		return k;
	}
	
}
