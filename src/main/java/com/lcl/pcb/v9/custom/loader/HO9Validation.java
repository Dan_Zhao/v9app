package com.lcl.pcb.v9.custom.loader;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.lcl.pcb.v9.generic.loader.LoaderUtil;
import com.lcl.pcb.v9.generic.loader.V9ValidationDaoManager;
import com.lcl.pcb.v9.idf.jobflow.config.IDFConfig;
import com.lcl.pcb.v9.utilities.fileReader.FileUtil;

/**
 * This class is used to validate the csv files generated for HO9 interface. It
 * validates whether the data was previously loaded in table with the
 * PROC_DT_YYYYDDD value in the Header csv file.It validates whether trailer
 * record segments count and processed records counts are same and validates the
 * total record count also.
 */
public class HO9Validation {

	ArrayList<String> csvFilesList = new ArrayList<String>();
	ArrayList<String> errorLogList = new ArrayList<String>();
	ArrayList<String> badRecordList = null;
	ArrayList<HO9Pojo> errorObjectList = null;
	String csvFiles;
	String logFileName = "";
	String badFileName = "";
	String logName = "";
	String logFileNameVal ="";
	String[] logFileNamePattern ;
	String errorRecord = "Record";
	StringBuilder errorMsg=null;
	String dataFileLocation = "";
	String dataFileName = "";
	String[] segmentPart ;
	String errorRecordRow = "";
	String errorrecord ="";
	String[] apaAppNum;
	String apaAppNumVal ="";
	public String headerCsvFile = "";
	BufferedReader br;
	BufferedReader badrecordreader;
	String line = "";
	String line1= "";
	String processDate = "";
	boolean headerResult = true;
	boolean trailerResult = true;
	boolean bodyResult = true;
	boolean doValidation = true;
	HashMap<String, Integer> totRecParsed = new HashMap<String, Integer>();
	HashMap<String, Integer> trailerRecList = new HashMap<String, Integer>();
	public String trailerCsvFile = "";
	int totalRecCount = 0;
	int totalRecTrailerCount = 0;
	String dateFormat = "DD-Mon-YYYY HH24:MI:SS";
	String SourceFileId = "";
	String seqVal = "";
	Logger logger = LogManager.getLogger(HO9Validation.class.getName());
	V9ValidationDaoManager v9Validation = new V9ValidationDaoManager();
			

	// This method will be invoked from V9Loader class which expects the csv
	// files list for validating header, body and trailer.
	public boolean doValidation(ArrayList<String> files) throws Exception {
		logger.debug("HO9Validation Called");
		this.csvFilesList = files;
		doValidation &= doHeader();
		doValidation &= doTrailer();
		doValidation &= doBody();
		return doValidation;
	}

	public boolean doHeader() throws Exception {
		logger.debug("HO9Validation doHeader() Called");
		for (int i = 0; i < csvFilesList.size(); i++) {
			if (csvFilesList.get(i).contains("HEADER")) {
				headerCsvFile = csvFilesList.get(i);
				try {
					br = new BufferedReader(new FileReader(headerCsvFile));

					try {
						while ((line = br.readLine()) != null) {
							String[] headerVal = line.split("\\|");
							if (headerVal[0].equalsIgnoreCase("1")
									&& headerVal.length > 3) {
								processDate = headerVal[3];
								logger.debug("PROC_DATE in HEADER CSV File:"
										+ processDate);
								v9Validation.getInstance();
								
								String sql = "SELECT TO_CHAR(PROC_DT, '"
										+ dateFormat
										+ "') FROM "+IDFConfig.getInstance().getIdfData().getCsvloadDbSchema()+".BATCH_AUTH_STAGING_HEAD_TRAIL where TO_CHAR(PROC_DT, '"
										+ dateFormat + "')='" + processDate
										+ "'";								
									
								try {
									v9Validation.open();
									ResultSet res = V9ValidationDaoManager
											.execQuery(sql);
									if (res.next()) {

										logger.error("File already loaded for the date <PROC_DT>:"
												+ processDate
												+ " "
												+ "To reload the file, delete all the existing records for this date from all the BATCH_AUTH_STAGING tables first and then restart the job.");
										headerResult = false;
									} else {
										logger.debug("Header Date Validation Passed:A File with new PROC_DT Found");
									}
									v9Validation.close();
								} catch (SQLException e) {
									throw e;
								} finally {
									v9Validation.con.close();
								}
							}

						}
						br.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				} catch (FileNotFoundException ex) {
					ex.printStackTrace();
				}
			}
		}
		return headerResult;
	}

	public boolean doTrailer() {
		logger.debug("HO9Validation doTrailer() Called");
		totRecParsed = FileUtil.totalRecParsedList;
		for (int i = 0; i < csvFilesList.size(); i++) {
			if (csvFilesList.get(i).contains("TRAILER")) {
				trailerCsvFile = csvFilesList.get(i);
				try {
					br = new BufferedReader(new FileReader(trailerCsvFile));

					try {
						while ((line = br.readLine()) != null) {
							String[] trailerVal = line.split("\\|");
							if (trailerVal[4].equalsIgnoreCase("9")
									&& trailerVal.length > 6) {
								trailerRecList.put("2",
										Integer.parseInt(trailerVal[7])); //only one segment called "2" so (segment name/count) will be list of 1 in this case
								trailerRecList.put("TRAILER", 1);								
							}
						}
						br.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				} catch (FileNotFoundException ex) {
					ex.printStackTrace();
				}
			}
		}
		for (Entry<String, Integer> entry : trailerRecList.entrySet()) {
			totalRecTrailerCount += (entry.getValue());
		}

		for (Entry<String, Integer> entry : totRecParsed.entrySet()) {
			totalRecCount += (entry.getValue());
		}

		if (totRecParsed.keySet().equals(trailerRecList.keySet())) {
			if (totalRecCount == totalRecTrailerCount) {
				logger.debug("Total Record Count in File matches with Total Record Count in TRAILER.");
			} else {
				logger.error("Total Record Count in File Mismatches with Record Count in TRAILER.");
				trailerResult = false;
			}

		} else {
			logger.error("HO9 Record Segments"+totRecParsed.keySet()+" doesn't match/not available in the Processed Data file.");
			trailerResult = false;
		}
		return trailerResult;
	}

	public boolean doBody() {
		logger.debug("HO9Validation doBody() Called");
		return bodyResult;
	}
	
	public void doErrorLog() throws Exception{
		errorLogList=LoaderUtil.ErrorlogFileNamesList;
		dataFileLocation=IDFConfig.getInstance().getIdfData().getDatafileDatafileLocation();
		dataFileName=dataFileLocation.substring(dataFileLocation.lastIndexOf("/") + 1);
		if (null != errorLogList && errorLogList.size() != 0) {
			for (int i = 0; i < errorLogList.size(); i++) {
				errorObjectList=new ArrayList<HO9Pojo>();
				logFileName = errorLogList.get(i);
				logFileNameVal=logFileName.substring(logFileName.lastIndexOf("/")+1, logFileName.lastIndexOf("."));
				logFileNamePattern=logFileNameVal.split("_");
				badFileName =IDFConfig.getInstance().getIdfData().getDatafileBadFileLocation()
				+ "/"+ logFileNamePattern[0]+"_"+logFileNamePattern[2]+"_"+logFileNamePattern[3]+"_"+logFileNamePattern[1]+ IDFConfig.getInstance().getIdfData().getDatafileBadNamePattern();
				//logName = logFileName.substring(logFileName.lastIndexOf("/") + 1);
				//segmentPart = logName.split("_");
				try{
					badrecordreader = new BufferedReader(new FileReader(badFileName));
					badRecordList=new ArrayList();
					try{
						while((line1 = badrecordreader.readLine())!= null ){
							badRecordList.add(line1);
						}
					}catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}catch(FileNotFoundException ex){
					ex.printStackTrace();
				}
				try {
					br = new BufferedReader(new FileReader(logFileName));
					int count=0;
					try {
						while ((line = br.readLine()) != null) {
							
							
							if (line.contains(errorRecord)) {
								HO9Pojo errorObject = new HO9Pojo();
								errorObject.setFileName(dataFileName);
								errorObject.setSegmentName(logFileNamePattern[3]);
								errorMsg=new StringBuilder();
								errorMsg.append(line);
								errorRecordRow=line.substring(line.indexOf(" "),
										line.lastIndexOf(":"));
								line=br.readLine();
								errorMsg.append(line);
								errorObject.setErrorrecordRow(errorRecordRow);
								errorObject.setErrorMsg(errorMsg);
								errorrecord=badRecordList.get(count);
								if(errorrecord.length()<= 4000){
									errorObject.setErrorrecord(errorrecord);
								}else{
									errorObject.setErrorrecord(errorrecord.substring(0, 3999));
									logger.info("Error Record length is more than 4000 characters: So inserting first 4000 characters of error record");
								}
								apaAppNum=errorrecord.split("\\|");
								if(apaAppNum.length>1){
									apaAppNumVal=apaAppNum[1];
								}
								errorObject.setApaAPPNUM(apaAppNumVal);
								count ++;
								errorObjectList.add(errorObject);
								errorObject=null;
								errorMsg=null;
								
							}
						}
						badRecordList=null;
						br.close();
						if (errorObjectList != null && errorObjectList.size() > 0) {
							V9ValidationDaoManager.getInstance();
							v9Validation.open();
							insertIntoErrorTable(errorObjectList);
						}
						errorObjectList=null;
					}catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}catch(FileNotFoundException ex){
					ex.printStackTrace();
				}
				}
			v9Validation.close();
			}
	}
	public void insertIntoErrorTable(ArrayList<HO9Pojo> errorRecords)
	throws Exception {
		HO9Pojo errObject;
		String LoadResultUUID = "";
		
		logger.info("Start of Inserting Error Records in ADM Error Table");
		for (int i=0;i<errorRecords.size();i++){
			errObject=(HO9Pojo)errorRecords.get(i);
			int sequenceNum=IDFConfig.getInstance().getSequenceNum();
			StringBuilder sb = new StringBuilder("");
			sb.append("SELECT LOAD_RESULT_UUID FROM "+IDFConfig.getInstance().getIdfData().getCsvloadDbSchema()+".LOAD_RESULT WHERE ");
			sb.append("PROCESS_NUM = ");
			sb.append(sequenceNum);
			ResultSet rs = V9ValidationDaoManager.execQuery(sb.toString());
			if (rs.next())
				LoadResultUUID=rs.getString("LOAD_RESULT_UUID");
			else
				logger.debug("UUID not found");
			
			String insertSql="insert into "+IDFConfig.getInstance().getIdfData().getCsvloadDbSchema()+".TS2_LOAD_ERROR(INTERFACE_NM,FILENAME,SEGMENT_NAME,PROCESS_NUM,ERROR_RECORD_ROW,ERROR_RECORD,ERROR_MSG,REC_CHNG_TMS,REC_LOAD_TMS,V9SEQ,LOAD_RESULT_UUID) values('HO9','"+errObject.getFileName()+"','"+errObject.getSegmentName()+"','"+errObject.getApaAPPNUM()+"','"+errObject.getErrorrecordRow()+"','"+errObject.getErrorrecord()+"','"+errObject.getErrorMsg()+"',sysdate,sysdate,"+sequenceNum+",'"+LoadResultUUID+"')";
			//System.out.println("Insert SQl:"+insertSql.toString());
			int res = V9ValidationDaoManager
					.execNonQuery(insertSql);
			rs = null;
			sb = null;
			
		}
		logger.info("Error Records Inserted in ADM Error Table");
		
	}
	
	public void doFinalize(ArrayList<String> files) throws Exception {

		logger.debug("HO9Validation doFinalize() Called ");
		for (int i = 0; i < files.size(); i++) {
			if (files.get(i).contains("HEADER")) {
				headerCsvFile = files.get(i);
				try {
					br = new BufferedReader(new FileReader(headerCsvFile));

					try {
						while ((line = br.readLine()) != null) {
							String[] headerVal = line.split("\\|");
							if (headerVal[0].equalsIgnoreCase("1")
									&& headerVal.length > 3) {
								processDate = headerVal[3];
								DateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
								java.util.Date d = df.parse(processDate);
								df = new SimpleDateFormat("yyyy-MM-dd");
								String db_date_String = df.format(d);
								String fileTimeVal = processDate.substring(11, 20);
								String processDateVal = db_date_String + fileTimeVal;
								seqVal = headerVal[8];  //PROCESS_NUM
								SourceFileId = "TSYS";
								StringBuilder sb = new StringBuilder("");
								sb.append("UPDATE "+IDFConfig.getInstance().getIdfData().getCsvloadDbSchema()+".LOAD_RESULT SET SOURCE_END_DT_CURR = ");
								sb.append("to_timestamp('" + processDateVal
										+ "','yyyy-mm-dd HH24:MI:SS')");
								sb.append(",");
								sb.append("SOURCE_ID = '");
								sb.append(SourceFileId);
								sb.append("'");
								sb.append(" WHERE PROCESS_NUM = '");
								sb.append(seqVal);
								sb.append("'");
								V9ValidationDaoManager.execNonQuery(sb
										.toString());
								sb = null;
							}
						}
					} catch (IOException e) {
						logger.error("Error in HO9 interface Finalize method.");
						e.printStackTrace();
					}
				} catch (FileNotFoundException ex) {
					ex.printStackTrace();
				}
			}
		}

	}
}


