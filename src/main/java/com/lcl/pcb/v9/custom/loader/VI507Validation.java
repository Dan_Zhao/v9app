package com.lcl.pcb.v9.custom.loader;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.lcl.pcb.v9.custom.dao.V9MultipleStagesSrcDaoManager;
import com.lcl.pcb.v9.custom.multistage.idf.Loader;
import com.lcl.pcb.v9.custom.multistage.idf.MultiStageConfig;
import com.lcl.pcb.v9.generic.loader.LoaderUtil;
import com.lcl.pcb.v9.generic.loader.V9ValidationDaoManager;
import com.lcl.pcb.v9.idf.jobflow.config.IDFConfig;
import com.lcl.pcb.v9.utilities.audit.AuditTablePojo;
import com.lcl.pcb.v9.utilities.audit.AuditTableUtil;
import com.lcl.pcb.v9.utilities.fileReader.FileUtil;

/**
 * This class is used to validate the csv files generated for VO70 interface. It
 * validates whether the data was previously loaded in table with the
 * ADM_EXTRACT_DT value in the Header csv file.It validates whether trailer
 * record segments count and processed records counts are same and validates the
 * total record count also.
 */
public class VI507Validation {

	private static final Logger logger = LogManager
			.getLogger(VI507Validation.class.getName());

	private List<String> csvFilesList;
	private String line;
	private boolean headerResult = true;
	private boolean trailerResult = true;
	private boolean bodyResult = true;
	private boolean doValidation = true;
	private int seqValue = IDFConfig.getInstance().getSequenceNum();
	private String fileSizeVal = null;
	private int badrecords = 0;
	private int recordsLoadedSuccessfully = 0;
	
	// This method will be invoked from V9Loader class which expects the csv
	// files list for validating header, body and trailer.
	public boolean doValidation(ArrayList<String> files) throws Exception {
		logger.debug("VI507Validation Called");
		this.csvFilesList = files;
		doValidation = doHeader() && doTrailer() && doBody();
		return doValidation;
	}

	public boolean doHeader() throws Exception {
		logger.debug("VI507Validation doHeader() Called");
		return headerResult;
	}

	public boolean doTrailer() {
		logger.debug("VI507Validation doTrailer() Called");
		return trailerResult;
	}

	public boolean doBody() throws IOException, SQLException {

		logger.debug("VI507Validation doBody() Called");
		
		File bodyCsvFile;
		File convertedFile;
		
		final List<String> convertedFilesList = new ArrayList<>(csvFilesList.size());
		
		final String csvSeparator = IDFConfig.getInstance().getIdfData().getCsvFileSeparator();
		
		for (int i = 0; bodyResult && i < csvFilesList.size(); i++) {
		
			if (csvFilesList.get(i).contains("BODY")) {
			
				bodyCsvFile = new File(csvFilesList.get(i));
				convertedFile = new File(bodyCsvFile.getParentFile(), "Conv_" + bodyCsvFile.getName());
				
				convertBodyInputFile(bodyCsvFile, csvSeparator, convertedFile);
				convertedFilesList.add(convertedFile.getName());
			}
		}
		
		FileUtil.totalCSVFileNamesList.clear();
		FileUtil.totalCSVFileNamesList.addAll(convertedFilesList);
		
		return bodyResult;
	
	}

	private void convertBodyInputFile(final File bodyCsvFile, final String csvSeparator, final File convertedFile) {
		try (BufferedReader br = new BufferedReader(new FileReader(bodyCsvFile));
				BufferedWriter bw = new BufferedWriter(new FileWriter(convertedFile))) {
		
			
			String[] bodyVal = null;
			
			while (br.ready()) {

				line = br.readLine();
				bodyVal = line.split("\\|");

				if (bodyVal.length == 10) {

					String pcfCustId = null  ;
					if(bodyVal[0].contains("@")){
					 pcfCustId = bodyVal[0].substring(0, 16);
					}
					else if(bodyVal[0].contains("I")){
						 pcfCustId =bodyVal[0].substring(6, 17);
						
					}

					String constantPreString = pcfCustId + csvSeparator
							+ bodyVal[0] + csvSeparator + bodyVal[2];

					bw.write(constantPreString + csvSeparator
							+ "Customer Type" + csvSeparator
							+ bodyVal[4]);
					bw.newLine();

					bw.write(constantPreString + csvSeparator
							+ "Customer Birthdate" + csvSeparator
							+ bodyVal[6]);
					bw.newLine();

					bw.write(constantPreString + csvSeparator
							+ "Customer ID" + csvSeparator + bodyVal[9]);
					bw.newLine();

				} else {
					logger.error("Corrupted data: not enough parts in body row.");
					logger.error("Corrupted Body Row: " + line);
					bodyResult = false;
					break;
				}
				logger.info("Validation 507 - while ends");
			}
			
			br.close();
			bw.close();
		} catch (IOException ex) {
			logger.error("Error while reading/wrting from/to file.", ex);
			bodyResult = false;
			
		}
	}

	public void insertLoadResult() throws SQLException {
		
		
		AuditTablePojo auditPojo = new AuditTablePojo();
		
		auditPojo.setProcessNumber(seqValue + "");
		
		auditPojo.setSchemaName(IDFConfig.getInstance().getIdfData()
				.getCsvloadDbSchema());
		
		auditPojo.setSourceInterface("VI507");
		
		auditPojo
				.setSourceNM(IDFConfig
						.getInstance()
						.getIdfData()
						.getDatafileDatafileLocation()
						.substring(
								IDFConfig.getInstance().getIdfData()
										.getDatafileDatafileLocation()
										.lastIndexOf("/") + 1));
		
		fileSizeVal = IDFConfig.getInstance().getIdfData().getFileSize();
		if (fileSizeVal != null && !fileSizeVal.isEmpty()) {
			auditPojo.setSourceSize(fileSizeVal);
		} else {
			auditPojo.setSourceSize("-9");
		}

		auditPojo.setTableName("CUST_DEMOGRAPHICS_TEMP");
		
		String sourceDTLRec = Integer.toString(recordsLoadedSuccessfully);
		
		auditPojo.setSourceDtlRec(sourceDTLRec);
		
		auditPojo.setLoadError(String.valueOf(badrecords));
		
		auditPojo.setTotalRecordLoad(String.valueOf(recordsLoadedSuccessfully));

		auditPojo = AuditTableUtil.insertAudit(auditPojo);
		auditPojo = null;
	}

	public void doErrorLog() throws Exception {

	}

	public void doFinalize(ArrayList<String> files) throws Exception {

		badrecords = LoaderUtil.errorLoadCount;

		if (badrecords == 0) {

			recordsLoadedSuccessfully = LoaderUtil.successLoadCount;

			String updateQuery = ("MERGE INTO PCL_TEMP.CUST_DEMOGRAPHICS_TEMP T")
					+ (" USING PCL.PCF_PRODUCT P ")
					+ ("ON (LTRIM(SUBSTR(T.ACC_ACCESS_NUM, 0, LENGTH(T.ACC_ACCESS_NUM) - 2), 'I ') = P.ACCOUNT_REF_ID  ")
					+ ("AND T.FILE_TYPE='507'")
					+ (" AND T.ACC_ACCESS_NUM like 'I%' )")
					+ (" WHEN MATCHED THEN")
					+ (" UPDATE SET T.PCF_CUST_ID = P.PCF_CUST_ID");

			logger.info("Updating the PCL_TEMP.CUST_DEMOGRAPHICS_TEMP FOR PCF_CUST_ID column for Account Id :  "
					+ updateQuery);

			int k = V9ValidationDaoManager.execNonQuery(updateQuery);

			logger.info("Number of records updated : " + k);

			updateQuery = null;

			updateQuery = ("MERGE INTO PCL_TEMP.CUST_DEMOGRAPHICS_TEMP T")
					+ (" USING PCL.PCF_CARD C ")
					+ (" ON (RTRIM(SUBSTR(T.ACC_ACCESS_NUM, 0, LENGTH(T.ACC_ACCESS_NUM) - 2), '@') = C.CARD_NUM ")
					+ (" AND T.FILE_TYPE='507'")
					+ (" AND T.ACC_ACCESS_NUM like '%@' ) ")
					+ (" WHEN MATCHED THEN ")
					+ (" UPDATE SET T.PCF_CUST_ID = C.PCF_CUST_ID ");

			logger.info("Updating the PCL_TEMP.CUST_DEMOGRAPHICS_TEMP FOR PCF_CUST_ID column for Account Number :  "
					+ updateQuery);

			int l = V9ValidationDaoManager.execNonQuery(updateQuery);

			logger.info("Number of records updated : " + l);

			logger.info(" **** Total Number of records updated **** : "
					+ (k + l));

			if ((recordsLoadedSuccessfully != (k + l))) {

				logger.info("Since total number of records updated in database do not match"
						+ recordsLoadedSuccessfully + "  !=  " + (k + l));

				logger.info("Start of deleteing data from table PCL_TEMP.CUST_DEMOGRAPHICS_TEMP ");

				String sql = "delete from PCL_TEMP.CUST_DEMOGRAPHICS_TEMP where FILE_TYPE = 507";

			int m =	V9ValidationDaoManager.execNonQuery(sql);
			
			logger.info("Total Number of records deleted : " + m);

			}

			
		}
		
		else {

			logger.info("Bad records : " + badrecords
					+ " Since bad records is not equal to 0 ");
			logger.info("Start of deleteing data from table PCL_TEMP.CUST_DEMOGRAPHICS_TEMP ");

			String sql = "delete from PCL_TEMP.CUST_DEMOGRAPHICS_TEMP where FILE_TYPE = 507";

			V9ValidationDaoManager.execNonQuery(sql);

		}

		logger.debug("VI507Validation doFinalize() Called ");

		insertLoadResult();

	}
	
}
