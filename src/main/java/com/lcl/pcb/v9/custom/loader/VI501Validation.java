package com.lcl.pcb.v9.custom.loader;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.lcl.pcb.v9.generic.loader.LoaderUtil;
import com.lcl.pcb.v9.generic.loader.V9ValidationDaoManager;
import com.lcl.pcb.v9.idf.jobflow.config.IDFConfig;
import com.lcl.pcb.v9.utilities.audit.AuditTablePojo;
import com.lcl.pcb.v9.utilities.audit.AuditTableUtil;
import com.lcl.pcb.v9.utilities.fileReader.FileUtil;

/**
 * This class is used to validate the csv files generated for VI501 interface.
 * It validates and makes the file based on the layout for loading into
 * PCL_TEMP.CUST_DEMOGRAPHICS_TEMP
 * 
 * 
 */
public class VI501Validation {

	public static final Logger logger = LogManager
			.getLogger(VI501Validation.class.getName());
	List<String> csvFilesList = new ArrayList<String>();
	List<String> bodyCsvFilesList = new ArrayList<String>();
	String line = "";
	boolean headerResult = true;
	boolean trailerResult = true;
	boolean bodyResult = true;
	boolean doValidation = true;
	Map<String, Integer> totRecParsed = new HashMap<String, Integer>();
	public String bodyCsvFile = "";
	int totalRecCount = 0;

	V9ValidationDaoManager v9Validation = new V9ValidationDaoManager();
	final int seqValue = IDFConfig.getInstance().getSequenceNum();
	final String interfaceName = IDFConfig.getInstance().getIdfData()
			.getFilePropName();

	/**
	 * This method returns the result of boolean value based on
	 * header/trailer/body validation
	 * 
	 * @param files
	 * @return
	 * @throws Exception
	 */
	public boolean doValidation(ArrayList<String> files) throws Exception {
		logger.debug("VI501Validation Called");
		this.csvFilesList = files;
		doValidation = doHeader() && doTrailer() && doBody();
		return doValidation;
	}

	/**
	 * This method does validation for header file
	 * 
	 * @return
	 * @throws Exception
	 */
	public boolean doHeader() throws Exception {
		logger.debug("VI501Validation doHeader() Called");
		return headerResult;
	}

	/**
	 * This method does validation for trailer part of the file and fetches
	 * total count
	 * 
	 * @return
	 */
	public boolean doTrailer() {
		logger.debug("VI501Validation doTrailer() Called");
		totRecParsed = FileUtil.totalRecParsedList;
		for (Entry<String, Integer> entry : totRecParsed.entrySet()) {
			totalRecCount += entry.getValue();
		}
		totalRecCount -= 1;
		return trailerResult;
	}

	/**
	 * This writes the body segment data into another file based on the table
	 * mapping
	 * 
	 * @return
	 * @throws IOException
	 * @throws SQLException
	 */
	public boolean doBody() throws IOException, SQLException {
		logger.debug("VI501Validation doBody() Called");

		// TODO paths to resolve filepath and use stringbuilder/string.format()
		// instead of multiple string concatenations
		String nameFile = IDFConfig.getInstance().getIdfData()
				.getDatafileCsvFileLocation()
				+ "VI501_BODY_501_FINAL_" + seqValue + ".csv";
		// String
		// nameFile=Paths.get(IDFConfig.getInstance().getIdfData().getDatafileCsvFileLocation()).resolve(dataFileName.replaceFirst("\\.[a-zA-Z]$",
		// String.format("_%d.txt",
		// IDFConfig.getInstance().getSequenceNum()))).toString();
		String[] nameFileVal;
		String pcf_cust_ID;
		for (int i = 0; i < csvFilesList.size(); i++) {
			if (csvFilesList.get(i).contains("BODY_501")) {

				try (BufferedWriter bw = new BufferedWriter(new FileWriter(
						nameFile));
						BufferedReader nameFileReader = new BufferedReader(
								new FileReader(csvFilesList.get(i)))) {

					while ((line = nameFileReader.readLine()) != null) {
						// TODO use stringbuilder/string.format() instead of
						// multiple string concatenations
						nameFileVal = line.split("\\|");
						pcf_cust_ID = nameFileVal[0].replaceAll("\\D+", "");
						// line=pcf_cust_ID + "|" + nameFileVal[0]+ "|501|
						line = pcf_cust_ID + "|" + nameFileVal[0]
								+ "|501|Customer Type|" + nameFileVal[4] + "|";
						bw.write(line);// TODO line followed with the exact
										// values using string.format
						bw.newLine();
						line = pcf_cust_ID + "|" + nameFileVal[0]
								+ "|501|Customer Name|" + nameFileVal[6] + "|";
						bw.write(line);
						bw.newLine();
						line = pcf_cust_ID + "|" + nameFileVal[0]
								+ "|501|Embossing Name|" + nameFileVal[8] + "|";
						bw.write(line);
						bw.newLine();
						line = pcf_cust_ID + "|" + nameFileVal[0]
								+ "|501|Salutation For Customer Name|"
								+ nameFileVal[10] + "|";
						bw.write(line);
						bw.newLine();
						line = pcf_cust_ID + "|" + nameFileVal[0]
								+ "|501|Suffix For Customer Name|"
								+ nameFileVal[12] + "|";
						bw.write(line);
						bw.newLine();
						line = pcf_cust_ID + "|" + nameFileVal[0]
								+ "|501|Custmer ID|" + nameFileVal[14] + "|";
						bw.write(line);
						bw.newLine();
					}

					bw.flush();
				}
			}

		}
		for (int j = 0; j < FileUtil.totalCSVFileNamesList.size(); j++) {
			FileUtil.totalCSVFileNamesList.clear();
		}
		FileUtil.totalCSVFileNamesList.add(nameFile);
		insertLaodResult();
		return bodyResult;
	}

	/**
	 * This method inserts data in LOAD_RESULT table based on pojo's
	 * 
	 * @throws SQLException
	 */
	public void insertLaodResult() throws SQLException {

		AuditTablePojo auditPojo = new AuditTablePojo();
		auditPojo.setProcessNumber(Integer.toString(seqValue));
		auditPojo.setSchemaName(IDFConfig.getInstance().getIdfData()
				.getCsvloadDbSchema());
		auditPojo.setSourceInterface(interfaceName);
		// TODO use File to get File name instead lastIndex
		/*
		 * auditPojo .setSourceNM(IDFConfig .getInstance() .getIdfData()
		 * .getDatafileDatafileLocation() .substring(
		 * IDFConfig.getInstance().getIdfData() .getDatafileDatafileLocation()
		 * .lastIndexOf("/") + 1));
		 */
		auditPojo.setSourceNM(new File(IDFConfig.getInstance().getIdfData()
				.getDatafileDatafileLocation()).getName());
		String fileSizeVal = IDFConfig.getInstance().getIdfData().getFileSize();
		if (fileSizeVal != null && !fileSizeVal.isEmpty()) {
			auditPojo.setSourceSize(fileSizeVal);
		} else {
			auditPojo.setSourceSize("-9");
		}

		auditPojo.setTableName("CUST_DEMOGRAPHICS_TEMP");
		auditPojo.setSourceDtlRec(Integer.toString(totalRecCount));

		AuditTableUtil.insertAudit(auditPojo);
	}

	/**
	 * @throws Exception
	 */
	public void doErrorLog() throws Exception {
		// No actions
	}

	/**
	 * doFinalize method called from V9Loader class before exit of Job , to
	 * update the LOAD_RESULT table with record values
	 * 
	 * @param files
	 * @throws Exception
	 */
	public void doFinalize(ArrayList<String> files) throws Exception {

		logger.debug("VI501Validation doFinalize() Called ");
		int successCount = LoaderUtil.successLoadCount;
		int failureCount = LoaderUtil.errorLoadCount;
		String SourceFileId = "501";
		StringBuilder sb = new StringBuilder(100)
				.append("UPDATE PCL_TEMP.LOAD_RESULT SET ")
				.append("SOURCE_ID = '").append(SourceFileId).append("'")
				.append(",").append("TL_REC_LOAD = '").append(successCount)
				.append("'").append(",").append("LOAD_ERRORS = '")
				.append(failureCount).append("'")
				.append(" WHERE PROCESS_NUM = '").append(seqValue).append("'");
		V9ValidationDaoManager.execNonQuery(sb.toString());
		sb = null;

	}
}
