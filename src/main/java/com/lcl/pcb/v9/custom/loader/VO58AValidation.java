package com.lcl.pcb.v9.custom.loader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.lcl.pcb.v9.dao.V9DAOManager;
import com.lcl.pcb.v9.generic.loader.LoaderUtil;
import com.lcl.pcb.v9.generic.loader.V9ValidationDaoManager;
import com.lcl.pcb.v9.idf.jobflow.config.IDFConfig;
import com.lcl.pcb.v9.utilities.fileReader.FileUtil;

/**
 * This class is used to validate the csv files generated for VO10C interface.
 * It updates SOURCE_END_DT_PREV and SOURCE_END_DT_CURR and SOURCE_FILE_ID
 * values with correct Values/format.
 */

public class VO58AValidation {

	ArrayList<String> csvFilesList = new ArrayList<String>();
	public String headerCsvFile = "";
	BufferedReader br;
	String endDatePrevVal = "";
	String endDateCurVal = "";
	String seqVal = "";
	String vendorId = "";
	String fileId = "";
	String SourceFileId = "";
	String endDatePrev = "";
	String endDateCur = "";
	boolean headerResult = false;			//Changed to false on 26th Feb
	boolean trailerResult = false;			//Changed to false on 26th Feb
	boolean bodyResult = true;
	boolean doValidation = true;
	String[] headerVal = null;
	String fileEndDateCurr = "";
	String dateFormat = "YYYY-MM-DD HH24:MI:SS";
	HashMap<String, Integer> totRecParsed = new HashMap<String, Integer>();
	HashMap<String, Integer> trailerRecList = new HashMap<String, Integer>();
	ArrayList<String> errorLogList = new ArrayList<String>();
	ArrayList<String> badRecordList = null;
	ArrayList<VO58APojo> errorObjectList = null;
	public String trailerCsvFile = "";
	int totalRecTrailerCount = 0;
	int totalRecCount = 0;
	String dataFileLocation = "";
	String dataFileName = "";
	String logFileName = "";
	String badFileName = "";
	String logName = "";
	String logFileNameVal ="";
	String[] logFileNamePattern ;
	String errorRecord = "Record";
	StringBuilder errorMsg=null;
	BufferedReader badrecordreader;
	String line = "";
	String line1= "";
	String errorRecordRow = "";
	String errorrecord ="";
	String[] apaAppNum;
	String apaAppNumVal ="";

	V9DAOManager db = new V9DAOManager();
	Logger logger = LogManager.getLogger(VO58AValidation.class.getName());
	V9ValidationDaoManager v9Validation = new V9ValidationDaoManager();

	/**
	 * This method will be invoked from V9Loader class which expects the csv
	 * files list for validating header, body and trailer.
	 * 
	 * @param files
	 *            This is the parameter of CSV files list
	 * @return boolean This returns true/false value.
	 */
	public boolean doValidation(ArrayList<String> files) throws Exception {
		logger.debug("VO58AValidation Called");
		this.csvFilesList = files;
		doValidation &= doHeader();
		doValidation &= doTrailer();
		doValidation &= doBody();
		return doValidation;
	}

	public boolean doHeader() throws Exception {
		logger.debug("VO58AValidation doHeader() Called");
		for (int i = 0; i < csvFilesList.size(); i++) {
			if (csvFilesList.get(i).contains("HEADER")) {
				headerCsvFile = csvFilesList.get(i);
				try {
					File f = new File(headerCsvFile);
					if(!(f.exists())){
						  logger.error("There is no Trailer Record/Type. Please check the Data file.");
						  db.setLevelWarning(3,"There is no Trailer Record/Type. Please check the Data file.",0);
				    }
					br = new BufferedReader(new FileReader(headerCsvFile));

					try {
						while ((line = br.readLine()) != null) {
							String[] headerVal = line.split("\\|");
							if (headerVal[0].equalsIgnoreCase("HEADER") 
									//&& headerVal[1].equalsIgnoreCase("'BHN'") 
									&& headerVal[1].equalsIgnoreCase("BHN") 		//Removed Quotes around BHN - 4 March 2016
									&& headerVal[2].equalsIgnoreCase("AUTHORIZED 2")
									//&& headerVal[6].equalsIgnoreCase("'2'")
									&& headerVal[6].equalsIgnoreCase("2")			//Removed Quotes around 2 - 4 March 2016
									&& headerVal.length > 5) {
								logger.info("VO58A Header Data Validation Passed");
							    headerResult=true;							//Added on 26th Feb to initialize flag to false
							}
							else {
								logger.error("VO58A Header Data Validation Failed");
							    headerResult=false;
							}
							
						}
						br.close();
					} catch (IOException e) {
						logger.error("VO58AValidation: Exception has occurred: ", e);
					}
				} catch (FileNotFoundException ex) {
					logger.error("VO58AValidation: Exception has occurred: ", ex);
				}
			}
			
		}
		return headerResult;
	}

	public boolean doTrailer() {
		logger.debug("VO58AValidation doTrailer() Called");
		totRecParsed = FileUtil.totalRecParsedList;
		for (int i = 0; i < csvFilesList.size(); i++) {
			if (csvFilesList.get(i).contains("TRAILER")) {
				trailerCsvFile = csvFilesList.get(i);
				try {
					br = new BufferedReader(new FileReader(trailerCsvFile));

					try {
						while ((line = br.readLine()) != null) {
							String[] trailerVal = line.split("\\|");
							if (trailerVal[7].equalsIgnoreCase("TRAILER")
									&& trailerVal.length > 6) {
								trailerRecList.put("AUTHEXTEND",
										Integer.parseInt(trailerVal[8]));
								trailerRecList.put("TRAILER", 1);
							    logger.info("VO58A Trailer Data Validation Passed");
							    trailerResult=true;							//Added on 26th Feb to initialize flag to false
							}
							else {
							    logger.error("VO58A Trailer Data Validation Failed");
							    trailerResult=false;
							}
						}
						br.close();
						
					} catch (IOException e) {
						logger.error("VO58AValidation: Exception has occurred: ", e);
					}
				} catch (FileNotFoundException ex) {
					logger.error("VO58AValidation: Exception has occurred: ", ex);
				}
			}
		}
		for (Entry<String, Integer> entry : trailerRecList.entrySet()) {
			totalRecTrailerCount += (entry.getValue());
			logger.debug("totalRecTrailerCount: "+totalRecTrailerCount);
		}

		for (Entry<String, Integer> entry : totRecParsed.entrySet()) {
			totalRecCount += (entry.getValue());
			logger.debug("totalRecCount: "+totalRecCount);
		}

		if (totRecParsed.keySet().equals(trailerRecList.keySet())) {
			if (totalRecCount == totalRecTrailerCount) {
				logger.debug("Total Record Count in File matches with Total Record Count in TRAILER.");
			} else {
				logger.error("Total Record Count in File Mismatches with Record Count in TRAILER.");
				trailerResult = false;
			}

		} else {
			logger.error("VO58A Record Segments"+totRecParsed.keySet()+" doesn't match/not available in the Processed Data file.");
			trailerResult = false;
		}
		return trailerResult;
	}

	public boolean doBody() throws Exception {
		logger.debug("VO58AValidation doBody() Called");
		return bodyResult;
	}

	public void doErrorLog() throws Exception {
		logger.debug("VO58AValidation doerror() Called");
		errorLogList=LoaderUtil.ErrorlogFileNamesList;
		dataFileLocation=IDFConfig.getInstance().getIdfData().getDatafileDatafileLocation();
		dataFileName=dataFileLocation.substring(dataFileLocation.lastIndexOf("/") + 1);
		if (null != errorLogList && errorLogList.size() != 0) {
			for (int i = 0; i < errorLogList.size(); i++) {
				errorObjectList=new ArrayList<VO58APojo>();
				logFileName = errorLogList.get(i);
				logFileNameVal=logFileName.substring(logFileName.lastIndexOf("/")+1, logFileName.lastIndexOf("."));
				logFileNamePattern=logFileNameVal.split("_");
				badFileName =IDFConfig.getInstance().getIdfData().getDatafileBadFileLocation()
				+ "/"+ logFileNamePattern[0]+"_"+logFileNamePattern[2]+"_"+logFileNamePattern[3]+"_"+logFileNamePattern[1]+ IDFConfig.getInstance().getIdfData().getDatafileBadNamePattern();
				//logName = logFileName.substring(logFileName.lastIndexOf("/") + 1);
				//segmentPart = logName.split("_");
				try{
					badrecordreader = new BufferedReader(new FileReader(badFileName));
					badRecordList=new ArrayList();
					try{
						while((line1 = badrecordreader.readLine())!= null ){
							badRecordList.add(line1);
						}
					}catch (IOException e) {
						// TODO Auto-generated catch block
						logger.error("VO58AValidation: Exception has occurred: ", e);
					}
				}catch(FileNotFoundException ex){
					logger.error("VO58AValidation: Exception has occurred: ", ex);
				}
				try {
					br = new BufferedReader(new FileReader(logFileName));
					int count=0;
					try {
						while ((line = br.readLine()) != null) {
							
							
							if (line.contains(errorRecord)) {
							    VO58APojo errorObject = new VO58APojo();
								errorObject.setFileName(dataFileName);
								errorObject.setSegmentName(logFileNamePattern[3]);
								errorMsg=new StringBuilder();
								errorMsg.append(line);
								errorRecordRow=line.substring(line.indexOf(" "),
										line.lastIndexOf(":"));
								line=br.readLine();
								errorMsg.append(line);
								errorObject.setErrorrecordRow(errorRecordRow);
								errorObject.setErrorMsg(errorMsg);
								errorrecord=badRecordList.get(count);
								if(errorrecord.length()<= 4000){
									errorObject.setErrorrecord(errorrecord);
								}else{
									errorObject.setErrorrecord(errorrecord.substring(0, 3999));
									logger.info("Error Record length is more than 4000 characters: So inserting first 4000 characters of error record");
								}
								apaAppNum=errorrecord.split("\\|");
								if(apaAppNum.length>1){
									apaAppNumVal=apaAppNum[1];
								}
								if(null==apaAppNumVal || "".equalsIgnoreCase(apaAppNumVal)){		//Added this check on 10th March 2016
									apaAppNumVal = "Data Not Available";
								}
								errorObject.setApaAPPNUM(apaAppNumVal);
								count ++;
								errorObjectList.add(errorObject);
								errorObject=null;
								errorMsg=null;
								
							}
						}
						badRecordList=null;
						br.close();
						if (errorObjectList != null && errorObjectList.size() > 0) {
							V9ValidationDaoManager.getInstance();
							v9Validation.open();
							insertIntoErrorTable(errorObjectList);
						}
						errorObjectList=null;
					}catch (IOException e) {
						// TODO Auto-generated catch block
						logger.error("VO58AValidation: Exception has occurred: ", e);
					}
				}catch(FileNotFoundException ex){
					logger.error("VO58AValidation: Exception has occurred: ", ex);
				}
				}
			v9Validation.close();
			}

	}

	/**
	 * This method is used for updating SOURCE_END_DT_PREV,SOURCE_END_DT_CURR
	 * and SOURCE_FILE_ID values with correct Values/format.
	 * 
	 * @param files
	 *            This is the parameter of CSV files list
	 * @return boolean This returns true/false value.
	 */
	public void doFinalize(ArrayList<String> files) throws Exception {

		logger.debug("VO58AValidation doFinalize() Called ");
		
	}
	
	public void insertIntoErrorTable(ArrayList<VO58APojo> errorRecords)
	throws Exception {
	    VO58APojo errObject;
		String loadResultUUID = "";
		String errorRecord = "";
		String apaAPPNUM = "";
		
		logger.info("VO58AValidation: insertIntoErrorTable: Start of Inserting Error Records in GPR Error Table");
		for (int i=0;i<errorRecords.size();i++){
			errObject=(VO58APojo)errorRecords.get(i);
			int sequenceNum=IDFConfig.getInstance().getSequenceNum();
			StringBuilder sb = new StringBuilder("");
			sb.append("SELECT LOAD_RESULT_UUID FROM "+IDFConfig.getInstance().getIdfData().getCsvloadDbSchema()+".LOAD_RESULT WHERE ");
			sb.append("PROCESS_NUM = ");
			sb.append(sequenceNum);
			logger.debug("VO58AValidation: insertIntoErrorTable: LOAD_RESULT Fetch Query: " + sb.toString());
			ResultSet rs = V9ValidationDaoManager.execQuery(sb.toString());
			if (rs.next())
			{
				loadResultUUID=rs.getString("LOAD_RESULT_UUID");
				logger.debug("VO58AValidation: insertIntoErrorTable: LOAD_RESULT LoadResultUUID: " + loadResultUUID);
			}
			else
				logger.debug("VO58AValidation: insertIntoErrorTable: UUID not found");
			
			//Added below conditions on 26th Feb 2016 as per changes in BHN files			
			if(null!=errObject.getErrorrecordRow() && errObject.getErrorrecord().contains("'")){
				errorRecord = errObject.getErrorrecord().replaceAll("'", "''");
			}else{
				errorRecord = errObject.getErrorrecord();
			}
			if(null!=errObject.getApaAPPNUM() && errObject.getApaAPPNUM().contains("'")){
				apaAPPNUM = errObject.getApaAPPNUM().replaceAll("'", "''");
			}else{
				apaAPPNUM = errObject.getApaAPPNUM();
			}
			
			String insertSql="insert into "+IDFConfig.getInstance().getIdfData().getCsvloadDbSchema()+".GPR_LOAD_ERROR(INTERFACE_NM,FILENAME,SEGMENT_NAME,PROCESS_NUM,ERROR_RECORD_ROW,ERROR_RECORD,ERROR_MSG,REC_CHNG_TMS,REC_LOAD_TMS,V9SEQ,LOAD_RESULT_UUID) values('VO58A','"+errObject.getFileName()+"','"+errObject.getSegmentName()+"','"+apaAPPNUM+"','"+errObject.getErrorrecordRow().trim()+"','"+errorRecord+"','"+errObject.getErrorMsg()+"',sysdate,sysdate,"+sequenceNum+",'"+loadResultUUID+"')";
			logger.debug("VO58AValidation: insertIntoErrorTable: Insert SQl:"+insertSql.toString());
			int res = V9ValidationDaoManager
					.execNonQuery(insertSql);
			rs = null;
			sb = null;
			
		}
		logger.info("VO58AValidation: insertIntoErrorTable: Error Records Inserted in GPR Error Table");
		
	}

}
