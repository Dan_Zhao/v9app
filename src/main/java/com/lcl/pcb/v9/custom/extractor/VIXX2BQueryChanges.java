package com.lcl.pcb.v9.custom.extractor;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.lcl.pcb.v9.generic.extractor.V9ExtractQueryDaoManager;
import com.lcl.pcb.v9.idf.jobflow.config.ExtractorConfig;

/**
 * This class is used if additional changes are needed to the original interface
 * sql query in the extractor xml This class is specific to this interface,
 * other custom classes need to be coded for other interfaces if the original
 * query is not the final layout of the extraction flow for that interface.
 */
public class VIXX2BQueryChanges extends QueryChanges {

	/**
	 * Constructor sets up originalRsArray to be the original data that was
	 * extracted from the database in the Extractor process flow Use parent
	 * class's constructor
	 * 
	 * @param String
	 *            [][] originalRsArray
	 * @return void
	 */
	public VIXX2BQueryChanges(String[][] originalRsArray) {
		super(originalRsArray);
	}

	/**
	 * override execute method this method will do the necessary custom data
	 * modifications for the Extractor process flow for this interface
	 * 
	 * @param void
	 * @return String[][] new array after modifications
	 */
	@Override
	public String[][] execute() throws Exception {

		int rowCount = originalRsArray.length;
		logger.debug("VIXX2BQueryChanges rowCount found from DB Result: " + rowCount);
		//
		// loop through and store layout header from config file in newRsArray
		int numFields = ExtractorConfig.getInstance().getExtractorData().getLayout().getLField().size();
		logger.debug("VIXX2BQueryChanges numFields count found from Layout in Extractor File: " + numFields);
		newRsArray = new String[rowCount + 1][]; // count is number of rows
		int col = originalRsArray[0].length;
		newRsArray[0] = new String[col];
		newRsArray[1] = new String[col];

		Date date = Calendar.getInstance().getTime();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		newRsArray[0][0] = "HEADER";
		newRsArray[0][1] = sdf.format(date);
		//newRsArray[0][2] = String.valueOf(rowCount - 1);

		for (int j = 2; j < col; j++) {
			newRsArray[0][j] = "";
		}

		int j = 1;

		for (int i = 1; i < rowCount; i++) {
			newRsArray[j++] = originalRsArray[i];
		}
		newRsArray[j] = new String[col];
		newRsArray[j][0] = "TRAILER";
		newRsArray[j][1] = String.valueOf(rowCount - 1);
		V9ExtractQueryDaoManager v9ExtractQuery = new V9ExtractQueryDaoManager();
		v9ExtractQuery.open();
		ResultSet rs1=null;
		String queryUpdate="select count(*) from (Select distinct PCF_CUST_ID, RECORD_TYPE,RECORD_SUBTYPE FROM PCL_TEMP.CUST_DEMOGRAPHICS_TEMP)";
		try {
			rs1 = v9ExtractQuery.execQuery(queryUpdate);
		} catch (SQLException e) {	
			String errorMsg = e.getMessage();
			logger.error("(Level 3) Result set not returned "+errorMsg);					
			v9ExtractQuery.close();	
		}
		v9ExtractQuery.close();	
		 while (rs1.next()){
		newRsArray[j][2] = rs1.getString(1);
		 }
		for (int i = 3; i < col; i++) {
			newRsArray[j][i] = "";
		}

		return newRsArray;
	}
}
