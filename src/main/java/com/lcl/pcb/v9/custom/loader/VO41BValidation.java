package com.lcl.pcb.v9.custom.loader;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Properties;

import javax.mail.MessagingException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.lcl.pcb.v9.custom.databean.PcmcEmail;
import com.lcl.pcb.v9.dao.V9DAOManager;
import com.lcl.pcb.v9.generic.loader.LoaderUtil;
import com.lcl.pcb.v9.generic.loader.V9ValidationDaoManager;
import com.lcl.pcb.v9.idf.jobflow.config.IDFConfig;
import com.lcl.pcb.v9.utilities.fileReader.FileUtil;

/**
 * This class is used to validate the csv files generated for VO10A interface.
 * It updates SOURCE_END_DT_PREV and SOURCE_END_DT_CURR and SOURCE_FILE_ID
 * values with correct Values/format.
 * 
 */
public class VO41BValidation {

	ArrayList<String> csvFilesList = new ArrayList<String>();
	boolean headerResult = true;
	boolean trailerResult = true;
	boolean bodyResult = true;
	boolean doValidation = true;

	int seqVal = 0;
	public String headerCsvFile = "";
	String line = "";
	String dateCreated = "";
	BufferedReader br;
	String SourceFileId = "";
	String runStartTime = "";
	public static String fileName = "";
	String dateFormat = "YYYYMMDD_HH24MISS";
	HashMap<String, Integer> totRecParsed = new HashMap<String, Integer>();
	public String trailerCsvFile = "";
	public String bodyCsvFile = "";
	int totalRecCount = 0;
	int totalRecTrailerCount = 0;
	public static String from= "VO41BFailureAlert@loblaw.ca"; 

	double totalLitres = 0.0, litres = 0.0;
	double totalAmount = 0.0, amount = 0.0;
	
	
	V9DAOManager db = new V9DAOManager();
	Logger logger = LogManager.getLogger(VO41BValidation.class.getName());
	V9ValidationDaoManager v9Validation = new V9ValidationDaoManager();
	PcmcEmail pCMCEmail =  new  PcmcEmail();

	/**
	 * This method will be invoked from V9Loader class which expects the csv
	 * files list for validating header, body and trailer.
	 * 
	 * @param files
	 *            This is the parameter of CSV files list
	 * @return boolean This returns true/false value.
	 */
	public boolean doValidation(ArrayList<String> files) throws Exception {
		logger.debug("VO41BValidation Called");
		this.csvFilesList = files;
		doValidation &= doHeader();
		doValidation &= doBody();
		doValidation &= doTrailer();
		return doValidation;
	}

	public boolean doHeader() throws Exception {
		logger.debug("VO41BValidation doHeader() Called");
		for (int i = 0; i < csvFilesList.size(); i++) {
			if (csvFilesList.get(i).contains("HEADER")) {
				headerCsvFile = csvFilesList.get(i);
				try {
					br = new BufferedReader(new FileReader(headerCsvFile));

					try {
						while ((line = br.readLine()) != null) {
							String[] headerVal = line.split("\\|");
							if (headerVal[0].equalsIgnoreCase("H")
									&& headerVal.length > 3) {
								runStartTime = headerVal[2];
								fileName = headerVal[1];
								logger.debug("Run Start Time in HEADER CSV File:"
										+ runStartTime);
								v9Validation.getInstance();
								String sql = "SELECT TO_CHAR(RUN_START_TIME, '"
										+ dateFormat
										+ "') FROM PCL_TEMP.ESSO_HEAD_TRAIL where TO_CHAR(RUN_START_TIME, '"
										+ dateFormat + "')='" + runStartTime
										+ "'";
								try {
									v9Validation.open();
									ResultSet res = V9ValidationDaoManager
											.execQuery(sql);
									if (res.next()) {

										logger.error("File already loaded for the date < RUN_START_TIME>:"
												+ runStartTime
												+ " "
												+ "To reload the file, delete all the existing records for this date from Header/Trailer table first and then restart the job.");
										headerResult = false;

										String subject = "PC Financials  ESSO GAS  ESSO sent duplicate data file - " + fileName;
										String content = "ESSO GAS " +fileName + " duplicate ESSO data file received.";

										//pCMCEmail.sendEmail("pcbproductionsupport@pcbank.ca ", subject, content);
										//pCMCEmail.sendEmail("PCB_COE_Triage@loblaw.ca", subject, content);
										pCMCEmail.sendEmail("sejal.zade@loblaw.ca", subject, content);

									} else {
										logger.debug("Header Date Validation Passed:A new file found to Load");
									}
									v9Validation.close();
								} catch (SQLException e) {
									throw e;
								} finally {
									v9Validation.con.close();
								}
							}

						}
						br.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				} catch (FileNotFoundException ex) {
					ex.printStackTrace();
				}
			}
		}
		return headerResult;
	}

	public boolean doTrailer() throws MessagingException {
		logger.debug("VO41BValidation doTrailer() Called");
		totRecParsed = FileUtil.totalRecParsedList;
		for (int i = 0; i < csvFilesList.size(); i++) {
			if (csvFilesList.get(i).contains("TRAILER")) {
				trailerCsvFile = csvFilesList.get(i);
				try {
					br = new BufferedReader(new FileReader(trailerCsvFile));

					try {
						while ((line = br.readLine()) != null) {
							String[] trailerVal = line.split("\\|");

							if (trailerVal[3].equalsIgnoreCase("T")
									&& trailerVal.length > 6) {
								totalRecTrailerCount = Integer
										.parseInt(trailerVal[4].toString());
								totalLitres = Double.parseDouble(trailerVal[5]
										.toString());
								totalAmount = Double.parseDouble(trailerVal[7]
										.toString());

							}
						}
						br.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				} catch (FileNotFoundException ex) {
					ex.printStackTrace();
				}
			}
		}

		for (Entry<String, Integer> entry : totRecParsed.entrySet()) {
			totalRecCount += (entry.getValue());
		} 
		totalRecCount -= 1;
		//This code has been commented as part of CR006 - Start
		/*if (totalRecCount == 0) {

			logger.error("No record found in file\n");
			trailerResult = false;
		}*/
		//This code has been commented as part of CR006 - End
		if (totalRecCount != totalRecTrailerCount) {
			

			String subject = "PC Financials  ESSO GAS - File Validation Errors - " + fileName;
			String content = "Total number of record (Not include header and trailer) not matched to trailer\'s total number of record value.";

		//	pCMCEmail.sendEmail("pcbproductionsupport@pcbank.ca ", subject, content);
		//	pCMCEmail.sendEmail("PCB_COE_Triage@loblaw.ca", subject, content);
			pCMCEmail.sendEmail("sejal.zade@loblaw.ca", subject, content);

			
			logger.error("Record count mismatch : Reords parsed "
					+ totalRecCount + "  trailer reported "
					+ totalRecTrailerCount + "");
			trailerResult = false;
		}
		if (Math.abs(litres - totalLitres) > 0.01) {
			logger.error("Amount total mismatch : summed " + litres
					+ "  trailer reported " + totalLitres + "");
			
			trailerResult = false;
			
			String subject = "PC Financials  ESSO GAS - File Validation Errors - "  +fileName ;
			String content = "Total purchased liters not matched to trailer\'s total purchased liters value";
		
		//	pCMCEmail.sendEmail("pcbproductionsupport@pcbank.ca ", subject, content);
		//	pCMCEmail.sendEmail("PCB_COE_Triage@loblaw.ca", subject, content);
			pCMCEmail.sendEmail("sejal.zade@loblaw.ca", subject, content);
			
		}
         //Added as part of WO18 990 - Sejal -Start
		if (Math.abs(amount - totalAmount) > 0.01) {
			logger.error("Amount total mismatch : summed " + amount
					+ "  trailer reported " + totalAmount + "");
			
			trailerResult = false;
			
			String subject = "PC Financials  ESSO GAS - File Validation Errors - "  +fileName ;
			//String content = "Total purchased liters not matched to trailer\'s total purchased liters value";
			String content = "Total transactions\' amount not matching the trailer\'s total amount value";
		
			//pCMCEmail.sendEmail("pcbproductionsupport@pcbank.ca ", subject, content);
			//pCMCEmail.sendEmail("PCB_COE_Triage@loblaw.ca", subject, content);
			pCMCEmail.sendEmail("sejal.zade@loblaw.ca", subject, content);
			
		}
		//Added as part of WO18 990 - Sejal -End
		
		return trailerResult;
	}

	public boolean doBody() throws Exception {
		logger.debug("VO41BValidation doBody() Called");
		for (int i = 0; i < csvFilesList.size(); i++) {
			if (csvFilesList.get(i).contains("BODY")) {
				bodyCsvFile = csvFilesList.get(i);
				try {
					br = new BufferedReader(new FileReader(bodyCsvFile));
					try {
						while ((line = br.readLine()) != null) {
							String[] bodyVal = line.split("\\|");
							String litresVal = null;
							String amountVal = null;
							if (bodyVal[0].equalsIgnoreCase("X")
									&& bodyVal.length > 6) {
								litresVal = bodyVal[7].toString();
								amountVal =  bodyVal[12].toString();
								if (litresVal.equalsIgnoreCase(null)
										|| litresVal.equalsIgnoreCase("")) {
									logger.error("VO41B Record Body Segment: Cannot find field Body.LitresPurchased");
									bodyResult = false;
								} else {
									litres += Double.parseDouble(bodyVal[7]
											.toString());
								}
								//Validation for totalAmount
								if (amountVal.equalsIgnoreCase(null)
										|| amountVal.equalsIgnoreCase("")) {
									logger.error("VO41B Record Body Segment: Cannot find field Body.Amount");
									bodyResult = false;
								} else {
									amount += Double.parseDouble(bodyVal[12]
											.toString());
								}

							}
						}
						br.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				} catch (FileNotFoundException ex) {
					ex.printStackTrace();
				}
			}
		}
		return bodyResult;
	}

	public void doErrorLog() throws Exception {
		logger.debug("VO41BValidation doerror() Called");
		seqVal = IDFConfig.getInstance().getSequenceNum();
		// Threshold Validation
		int thresholdValue = 0;
		Properties properties = new Properties();
		InputStream inputStream;
		if (properties.isEmpty()) {

			String propFile = IDFConfig.getInstance().getIdfData()
					.getLogFilelogFilelogLocation();
			String propFilePath = propFile.substring(0,
					propFile.lastIndexOf("/"))
					+ "/properties/" + "NacsMapping.properties";
			inputStream = new FileInputStream(propFilePath);

			properties.load(inputStream);
			thresholdValue = Integer.parseInt(properties
					.getProperty("Threshold"));
			inputStream.close();
		}

		int badrecords = 0;
		badrecords = LoaderUtil.errorLoadCount;
		if (badrecords > thresholdValue) {
			logger.info("Total Number of Bad records found:" + badrecords
					+ "which is greater than threshold value:" + thresholdValue);
			logger.info("Start of deleteing data from table PCL_TEMP.ESSO_TRANS_POINT_FDC_STG ");
			String sql = "DELETE FROM PCL_TEMP.ESSO_TRANS_POINT_FDC_STG where PROCESS_NUM='"
					+ seqVal + "'";
			V9ValidationDaoManager.execNonQuery(sql);
			logger.info("Deleted Records from table PCL_TEMP.ESSO_TRANS_POINT_FDC_STG ");
			
			String subject = "PC Financials  ESSO GAS - File Validation Errors - "  +fileName ;
			String content = "ESSO GAS  " +fileName +" is rejected." ;
			
			//pCMCEmail.sendEmail("pcbproductionsupport@pcbank.ca ", subject, content);
			//pCMCEmail.sendEmail("PCB_COE_Triage@loblaw.ca", subject, content);
			pCMCEmail.sendEmail("sejal.zade@loblaw.ca", subject, content);
		}

	}

	public void doFinalize(ArrayList<String> files) throws Exception {

		logger.debug("VO41BValidation doFinalize() Called ");

		seqVal = IDFConfig.getInstance().getSequenceNum();

		for (int i = 0; i < files.size(); i++) {
			if (files.get(i).contains("HEADER")) {
				headerCsvFile = files.get(i);
				try {
					br = new BufferedReader(new FileReader(headerCsvFile));

					try {
						while ((line = br.readLine()) != null) {
							String[] headerVal = line.split("\\|");
							if (headerVal[0].equalsIgnoreCase("H1")
									&& headerVal.length > 10) {
								dateCreated = headerVal[1];
								DateFormat df = new SimpleDateFormat(
										"dd-MMM-yyyy");
								java.util.Date d = df.parse(dateCreated);
								df = new SimpleDateFormat("yyyy-MM-dd");
								String db_date_String = df.format(d);

								SourceFileId = "ESSODataFeed";
								StringBuilder sb = new StringBuilder("");
								sb.append("UPDATE PCL.LOAD_RESULT SET SOURCE_END_DT_CURR = ");
								sb.append("to_timestamp('" + db_date_String
										+ "','yyyy-mm-dd HH24:MI:SS')");
								sb.append(",");
								sb.append("SOURCE_ID = '");
								sb.append(SourceFileId);
								sb.append("'");
								sb.append(" WHERE PROCESS_NUM = '");
								sb.append(seqVal);
								sb.append("'");
								V9ValidationDaoManager.execNonQuery(sb
										.toString());
								sb = null;
							}
						}
					} catch (IOException e) {
						logger.error("Error in VO41B interface Finalize method.");
						e.printStackTrace();
					}
				} catch (FileNotFoundException ex) {
					ex.printStackTrace();
				}
			}
		}
	}
	
	protected Date getSysdate() {
		return new Date(System.currentTimeMillis());
	}
}
