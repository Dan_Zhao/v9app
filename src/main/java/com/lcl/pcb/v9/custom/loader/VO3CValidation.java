package com.lcl.pcb.v9.custom.loader;

import java.io.File;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.lcl.pcb.v9.generic.loader.V9ValidationDaoManager;
import com.lcl.pcb.v9.idf.jobflow.config.IDFConfig;

public class VO3CValidation {
	boolean headerResult = true;
	boolean trailerResult = true;
	boolean bodyResult = true;
	boolean doValidation = true;
	Logger logger = LogManager.getLogger(VO3CValidation.class.getName());

	public boolean doValidation(ArrayList<String> files) throws Exception {
		logger.debug("VO3CValidation Called");
		doValidation &= doHeader();
		doValidation &= doBody();
		doValidation &= doTrailer();
		return doValidation;
	}

	public boolean doHeader() throws Exception {
		logger.debug("VO3CValidation doHeader() Called // No Header to Validate");
		return headerResult;
	}

	public boolean doTrailer() {
		logger.debug("VO3CValidation doTrailer() Called // No Trailer to Validate");
		 return trailerResult;
	}

	public boolean doBody() {

		logger.info("VO3CValidation doBody() Called");

		String sqldelete = "DELETE FROM HAINS.TEMP_INS_AUTO_QUOTE";
		logger.debug("VO3CValidation: doBody - Delete Query for Staging table: "+ sqldelete);
		try {
			V9ValidationDaoManager.execNonQuery(sqldelete);
		} catch (SQLException e) {
			logger.fatal("Error executing query", e);
			return false;
		}
		logger.debug("VO3CValidation: doBody - Records deleted from Staging table");

		return bodyResult;
	}

	public void doErrorLog() throws Exception {
		logger.debug("VO3CValidation doErrorLog() Called");
	}

	public void doFinalize(ArrayList<String> files) throws Exception {
		logger.debug("VO3CValidation doFinalize() Called : STARTED");
		//CALL sequence 
		String selectHdrID="SELECT HAINS.INS_FILE_HDR_SEQ.NEXTVAL FROM DUAL";
		String sqldelete = "DELETE FROM HAINS.TEMP_INS_FILE_HDR";

		logger.debug("VO3CValidation: doBody - select sequence Query: "+ selectHdrID);

		try (ResultSet hdrIDresultSet=V9ValidationDaoManager.execQuery(selectHdrID);){
			String hdrID="0";
			if(hdrIDresultSet.next())
			{
				 hdrID=hdrIDresultSet.getString(1);
				logger.debug("VO3CValidation: doBody - HDR ID : "+ hdrID);

			}
			//truncate hdr temp
			V9ValidationDaoManager.execNonQuery(sqldelete);
			//insert into hdr temp
			logger.debug("VO3CValidation: doBody - Delete Query for header table: "+ sqldelete);

			StringBuilder sqlinsert = new StringBuilder("insert into HAINS.temp_ins_file_hdr (hdr_id, file_name, file_type, rec_create_tms) values (")
					.append(hdrID).append(" , '").append( new File(IDFConfig.getInstance().getIdfData()
							.getDatafileDatafileLocation()).getName()).append("' , 'AUTO_QUOTE' ,").append("SYSDATE)");
			logger.debug("VO3CValidation: doBody - Insert Query for header table: "+ sqlinsert);

			V9ValidationDaoManager.execNonQuery(sqlinsert.toString());
			//Update temp table for HDR
			String updateHdrID="update HAINS.TEMP_INS_AUTO_QUOTE set HDR_ID = "+hdrID;
			logger.debug("VO3CValidation: doBody - update Query for  HAINS.TEMP_INS_AUTO_QUOTE table to set HDR_ID : "+ updateHdrID);

			V9ValidationDaoManager.execNonQuery(updateHdrID);

		} catch (SQLException e) {
			logger.fatal("Error executing query", e);
		}
		
		
		
		logger.debug("VO3CValidation doFinalize() Called : ENDED");

	}

}
