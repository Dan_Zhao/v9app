/**
 * 
 */
package com.lcl.pcb.v9.custom.multistage.idf;

/**
 * Singleton class that populates MultiStage data with the unmarshalled 
 * information from the idf.xml file. 
 * This class is referenced throughout the application for 
 * interface specific logic.
 */
public class MultiStageConfig {
	
	private MultiStage multiStage = null;
	private int childId = 0;

	private static MultiStageConfig instance = null;

	protected MultiStageConfig() {
		// Exists to defeat instantiation.
	}

	public static MultiStageConfig getInstance() {
		if (instance == null) {
			instance = new MultiStageConfig();
		}
		return instance;
	}

	/**
	 * @return the multiStage
	 */
	public MultiStage getMultiStage() {
		return multiStage;
	}

	/**
	 * @param multiStage the multiStage to set
	 */
	public void setMultiStage(MultiStage multiStage) {
		this.multiStage = multiStage;
	}

	/**
	 * @return the childId
	 */
	public int getChildId() {
		return childId;
	}

	/**
	 * @param childId the childId to set
	 */
	public void setChildId(int childId) {
		this.childId = childId;
	}

}
