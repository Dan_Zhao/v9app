package com.lcl.pcb.v9.custom.loader;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.lcl.pcb.v9.generic.loader.V9ValidationDaoManager;
import com.lcl.pcb.v9.idf.jobflow.config.IDFConfig;
import com.lcl.pcb.v9.utilities.fileReader.FileUtil;

/**
 * This class is used to validate the csv files generated for VO70 interface. It
 * validates whether the data was previously loaded in table with the
 * ADM_EXTRACT_DT value in the Header csv file.It validates whether trailer
 * record segments count and processed records counts are same and validates the
 * total record count also.
 */
public class VO70Validation {

	ArrayList<String> csvFilesList = new ArrayList<String>();
	ArrayList<String> bodyCsvFilesList = new ArrayList<String>();
	String csvFiles;
	public String headerCsvFile = "";
	BufferedReader br;
	BufferedReader badrecordreader;
	String line = "";
	String dateCreated = "";
	boolean headerResult = true;
	boolean trailerResult = true;
	boolean bodyResult = true;
	boolean doValidation = true;
	HashMap<String, Integer> totRecParsed = new HashMap<String, Integer>();
	HashMap<String, Integer> trailerRecList = new HashMap<String, Integer>();
	public String trailerCsvFile = "";
	public String bodyCsvFile = "";
	int totalRecCount = 0;
	int totalRecTrailerCount = 0;
	int totalNumOfPayments = 0;
	int rptNum = 0;
	double totalPaymentAmount = 0.0, amount = 0.0;
	String dateFormat = "DD-Mon-YYYY HH24:MI:SS";
	String dtCreated = "";
	String SourceFileId = "";
	int seqVal = 0, seqValue=0;
	Logger logger = LogManager.getLogger(VO70Validation.class.getName());
	V9ValidationDaoManager v9Validation = new V9ValidationDaoManager();

	// This method will be invoked from V9Loader class which expects the csv
	// files list for validating header, body and trailer.
	public boolean doValidation(ArrayList<String> files) throws Exception {
		logger.debug("VO70Validation Called");
		this.csvFilesList = files;
		doValidation &= doHeader();
		doValidation &= doBody();
		doValidation &= doTrailer();
		return doValidation;
	}

	public boolean doHeader() throws Exception {
		logger.debug("VO70Validation doHeader() Called");
		return headerResult;
	}

	public boolean doTrailer() {
		logger.debug("VO70Validation doTrailer() Called");
		totRecParsed = FileUtil.totalRecParsedList;
		for (int i = 0; i < csvFilesList.size(); i++) {
			if (csvFilesList.get(i).contains("TRAILER")) {
				trailerCsvFile = csvFilesList.get(i);
				try {
					br = new BufferedReader(new FileReader(trailerCsvFile));

					try {
						while ((line = br.readLine()) != null) {
							String[] trailerVal = line.split("\\|");

							if (trailerVal[9].equalsIgnoreCase("T1")
									&& trailerVal.length > 15) {
								totalNumOfPayments = Integer
										.parseInt(trailerVal[11].toString());
								totalPaymentAmount = Double
										.parseDouble(trailerVal[13].toString());
								rptNum= Integer.parseInt(trailerVal[6].toString());

							}
						}
						br.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				} catch (FileNotFoundException ex) {
					ex.printStackTrace();
				}
			}
		}

		for (Entry<String, Integer> entry : totRecParsed.entrySet()) {
			totalRecCount += (entry.getValue());
		}
		if (totalRecCount == 0) {

			logger.error("No record found in file\n");
			trailerResult = false;
		}
		totalRecCount -= 1;
		if (totalRecCount != totalNumOfPayments) {
			logger.error("Record count mismatch : Reords parsed "
					+ totalRecCount + "  trailer reported "
					+ totalNumOfPayments + "");
			trailerResult = false;
		}
		if (Math.abs(amount - totalPaymentAmount) > 0.01) {
			logger.error("Amount total mismatch : summed " + amount
					+ "  trailer reported " + totalPaymentAmount + "");
			trailerResult = false;
		}

		return trailerResult;
	}

	public boolean doBody() {
		logger.debug("VO70Validation doBody() Called");
		for (int i = 0; i < csvFilesList.size(); i++) {
			if (csvFilesList.get(i).contains("BODY")) {
				bodyCsvFile = csvFilesList.get(i);
				try {
					br = new BufferedReader(new FileReader(bodyCsvFile));
					try {
						while ((line = br.readLine()) != null) {
							String[] bodyVal = line.split("\\|");
							String paymentAmt = null;
							if (bodyVal[0].equalsIgnoreCase("PD")
									&& bodyVal.length > 6) {
								paymentAmt = bodyVal[3].toString();
								if (paymentAmt.equalsIgnoreCase(null)
										|| paymentAmt.equalsIgnoreCase("")) {
									logger.error("VO70 Record Body Segment: Cannot find field Body.Amount");
									bodyResult = false;
								} else {
									amount += Double.parseDouble(bodyVal[3]
											.toString());
								}

							}
						}
						br.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				} catch (FileNotFoundException ex) {
					ex.printStackTrace();
				}
			}
		}
		/*seqValue = IDFConfig.getInstance()
				.getSequenceNum();
		
		for (int j = 0; j < FileUtil.totalCSVFileNamesList.size(); j++) {
			System.out.println("------------------------"+FileUtil.totalCSVFileNamesList.get(j)); 	
			
		}
		FileUtil.totalCSVFileNamesList.remove("InterfaceDefinition/VO70/csv/VO70_BODY_PD_"+seqValue+".csv");
		String fileName= "InterfaceDefinition/VO70/csv/VO70_BODY_PD_" + seqValue + ".csv";
		System.out.println("-----------###############-----------"+fileName);*/
		
		return bodyResult;
	}

	public void doErrorLog() throws Exception {

	}

	public void doFinalize(ArrayList<String> files) throws Exception {

		logger.debug("VO70Validation doFinalize() Called ");
		seqVal = IDFConfig.getInstance()
				.getSequenceNum();
		for (int i = 0; i < files.size(); i++) {
			if (files.get(i).contains("HEADER")) {
				headerCsvFile = files.get(i);
				try {
					br = new BufferedReader(new FileReader(headerCsvFile));

					try {
						while ((line = br.readLine()) != null) {
							String[] headerVal = line.split("\\|");
							if (headerVal[0].equalsIgnoreCase("H1")
									&& headerVal.length > 10) {
								dateCreated = headerVal[1];
								DateFormat df = new SimpleDateFormat(
										"dd-MMM-yyyy");
								java.util.Date d = df.parse(dateCreated);
								df = new SimpleDateFormat("yyyy-MM-dd");
								String db_date_String = df.format(d);

								
								SourceFileId = "TELPAY";
								StringBuilder sb = new StringBuilder("");
								sb.append("UPDATE PCMCPYMT.LOAD_RESULT SET SOURCE_END_DT_CURR = ");
								sb.append("to_timestamp('" + db_date_String
										+ "','yyyy-mm-dd HH24:MI:SS')");
								sb.append(",");
								sb.append("SOURCE_ID = '");
								sb.append(SourceFileId);
								sb.append("'");
								sb.append(" WHERE PROCESS_NUM = '");
								sb.append(seqVal);
								sb.append("'");
								V9ValidationDaoManager.execNonQuery(sb
										.toString());
								sb = null;
							}
						}
					} catch (IOException e) {
						logger.error("Error in VO70 inerface Finalize method.");
						e.printStackTrace();
					}
				} catch (FileNotFoundException ex) {
					ex.printStackTrace();
				}
			}
		}
		//Updating TELPAY_PYMT_TRANS table with data from TELPAY_PYMT_HDR table
		
		csvFilesList = FileUtil.totalCSVFileNamesList;
		for (int i = 0; i < csvFilesList.size(); i++) {
			if (csvFilesList.get(i).contains("TRAILER")) {
				trailerCsvFile = csvFilesList.get(i);
				try {
					br = new BufferedReader(new FileReader(trailerCsvFile));

					try {
						while ((line = br.readLine()) != null) {
							String[] trailerVal = line.split("\\|");

							if (trailerVal[9].equalsIgnoreCase("T1")
									&& trailerVal.length > 15) {
								rptNum= Integer.parseInt(trailerVal[6].toString());
								dtCreated = trailerVal[1];

							}
						}
						br.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				} catch (FileNotFoundException ex) {
					ex.printStackTrace();
				}
			}
		}
		
		String updateSql="Update PCMCPYMT.TELPAY_PYMT_TRANS A SET(DT_CREATED,RPT_NUM, HDR_ID)= (Select B.DT_CREATED, B.RPT_NUM, max(B.HDR_ID) from PCMCPYMT.TELPAY_PYMT_HDR B where B.RPT_NUM='"+rptNum+"' and TO_CHAR(DT_CREATED, '"+ dateFormat + "')='" + dtCreated +"' GROUP BY DT_CREATED, RPT_NUM ) where A.HDR_ID= '"+seqVal+"' ";
		logger.debug("Update Query:"+updateSql);
		logger.debug("Update Query:"+rptNum);
		logger.debug("Update Query:"+seqVal);
		V9ValidationDaoManager.execNonQuery(updateSql.toString());
	}
}
