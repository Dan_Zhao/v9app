package com.lcl.pcb.v9.custom.loader;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.lcl.pcb.v9.generic.loader.LoaderUtil;
import com.lcl.pcb.v9.generic.loader.V9ValidationDaoManager;
import com.lcl.pcb.v9.idf.jobflow.config.IDFConfig;
import com.lcl.pcb.v9.utilities.audit.AuditTablePojo;
import com.lcl.pcb.v9.utilities.audit.AuditTableUtil;
import com.lcl.pcb.v9.utilities.fileReader.FileUtil;

/**
 * This class is used to validate the csv files generated for VI502 interface.
 * It validates and makes the file based on the layout for loading into
 * PCL_TEMP.CUST_DEMOGRAPHICS_TEMP
 * 
 */
public class VI502Validation {

	Logger logger = LogManager.getLogger(VI502Validation.class.getName());
	ArrayList<String> csvFilesList = new ArrayList<String>();
	String csvFiles;
	String line = "";
	boolean headerResult = true;
	boolean trailerResult = true;
	boolean bodyResult = true;
	boolean doValidation = true;
	HashMap<String, Integer> totRecParsed = new HashMap<String, Integer>();
	int totalRecCount = 0;
	V9ValidationDaoManager v9Validation = new V9ValidationDaoManager();
	int seqValue = IDFConfig.getInstance().getSequenceNum();
	String interfaceName = IDFConfig.getInstance().getIdfData()
			.getFilePropName();
	String fileSizeVal = null;

	/**
	 * This method returns the result of boolean value based on
	 * header/trailer/body validation
	 * 
	 * @param files
	 * @return
	 * @throws Exception
	 */
	public boolean doValidation(ArrayList<String> files) throws Exception {
		logger.debug("VI502Validation Called");
		this.csvFilesList = files;
		doValidation = doHeader() && doTrailer() && doBody();
		return doValidation;
	}

	/**
	 * This method does validation for header file
	 * 
	 * @return
	 * @throws Exception
	 */
	public boolean doHeader() throws Exception {
		logger.debug("VI502Validation doHeader() Called");
		return headerResult;
	}

	/**
	 * This method does validation for trailer part of the file and fetches
	 * total count
	 * 
	 * @return
	 */
	public boolean doTrailer() {
		logger.debug("VI502Validation doTrailer() Called");
		totRecParsed = FileUtil.totalRecParsedList;
		for (Entry<String, Integer> entry : totRecParsed.entrySet()) {
			totalRecCount += (entry.getValue());
		}
		if (totalRecCount == 0) {

			logger.error("No record found in file\n");
			trailerResult = false;
		} else {
			totalRecCount -= 1;
		}
		return trailerResult;
	}

	/**
	 * This writes the body segment data into another file based on the table
	 * mapping
	 * 
	 * @return
	 * @throws IOException
	 * @throws SQLException
	 */
	public boolean doBody() throws IOException, SQLException {
		logger.debug("VI502Validation doBody() Called");
		String nameFile = IDFConfig.getInstance().getIdfData()
				.getDatafileCsvFileLocation()
				+ "VI502_BODY_502_FINAL_" + seqValue + ".csv";
		String[] nameFileVal;
		String pcf_cust_ID;
		String homePhoneNum;
		String businessPhoneNum;
		for (int i = 0; i < csvFilesList.size(); i++) {
			if (csvFilesList.get(i).contains("BODY_502")) {
				try (BufferedWriter bw = new BufferedWriter(new FileWriter(
						nameFile));
						BufferedReader nameFileReader = new BufferedReader(
								new FileReader(csvFilesList.get(i)))) {

					while ((line = nameFileReader.readLine()) != null) {
						nameFileVal = line.split("\\|");

						if (nameFileVal.length > 16) {

							if (nameFileVal[0].contains("@")) {
								pcf_cust_ID = nameFileVal[0].substring(0, 13);
							} else if (nameFileVal[0].contains("I")) {
								pcf_cust_ID = nameFileVal[0].substring(6, 17);
							} else {
								logger.error("Corrupted data: Column#1 identifier not recognized. Not consisting of either '@' or 'I'.");
								logger.error("Corrupted Body Row: " + line);
								bodyResult = false;
								break;
							}

							if (nameFileVal[6].equals("99999999999999999")
									|| nameFileVal[6]
											.equals("$$$$$$$$$$$$$$$$$")) {
								homePhoneNum = "$";
							} else {
								homePhoneNum = nameFileVal[6].replaceAll(
										"[^a-zA-Z0-9]", " ");
							}
							line = pcf_cust_ID + "|" + nameFileVal[0]
									+ "|PHONENUM|" + homePhoneNum + "|01|01|";
							bw.write(line);
							bw.newLine();
							if (nameFileVal[8].equals("99999999999999999")
									|| nameFileVal[8]
											.equals("$$$$$$$$$$$$$$$$$")) {
								businessPhoneNum = "$";
							} else {
								businessPhoneNum = nameFileVal[8].replaceAll(
										"[^a-zA-Z0-9]", " ");
							}
							line = pcf_cust_ID + "|" + nameFileVal[0]
									+ "|PHONENUM|" + businessPhoneNum
									+ "|01|03|";
							bw.write(line);
							bw.newLine();

						} else {
							logger.error("Corrupted data: not enough parts in body row.");
							logger.error("Corrupted Body Row: " + line);
							bodyResult = false;
							break;
						}
					}
					bw.flush();

				}

			}
		}
		for (int j = 0; j < FileUtil.totalCSVFileNamesList.size(); j++) {
			FileUtil.totalCSVFileNamesList.clear();
		}
		FileUtil.totalCSVFileNamesList.add(nameFile);
		insertLaodResult();
		return bodyResult;
	}

	/**
	 * This method inserts data in LOAD_RESULT table based on pojo's
	 * 
	 * @throws SQLException
	 */
	public void insertLaodResult() throws SQLException {
		AuditTablePojo auditPojo = new AuditTablePojo();
		auditPojo.setProcessNumber(seqValue + "");
		auditPojo.setSchemaName(IDFConfig.getInstance().getIdfData()
				.getCsvloadDbSchema());
		auditPojo.setSourceInterface(interfaceName);
		auditPojo.setSourceNM(new File(IDFConfig.getInstance().getIdfData()
				.getDatafileDatafileLocation()).getName());
		fileSizeVal = IDFConfig.getInstance().getIdfData().getFileSize();
		if (fileSizeVal != null && !fileSizeVal.isEmpty()) {
			auditPojo.setSourceSize(fileSizeVal);
		} else {
			auditPojo.setSourceSize("-9");
		}

		auditPojo.setTableName("CUST_DEMOGRAPHICS_TEMP");
		String sourceDTLRec = Integer.toString(totalRecCount);
		auditPojo.setSourceDtlRec(sourceDTLRec);

		auditPojo = AuditTableUtil.insertAudit(auditPojo);
		auditPojo = null;
	}

	/**
	 * @throws Exception
	 */
	public void doErrorLog() throws Exception {

	}

	/**
	 * doFinalize method called from V9Loader class before exit of Job , to
	 * update the LOAD_RESULT table with record values
	 * 
	 * @param files
	 * @throws Exception
	 */
	public void doFinalize(ArrayList<String> files) throws Exception {

		logger.debug("VI502Validation doFinalize() Called ");
		int successCount = LoaderUtil.successLoadCount;
		int failureCount = LoaderUtil.errorLoadCount;
		String SourceFileId = "502";
		StringBuilder sb = new StringBuilder("");
		sb.append("UPDATE PCL_TEMP.LOAD_RESULT SET ");
		sb.append("SOURCE_ID = '");
		sb.append(SourceFileId);
		sb.append("'");
		sb.append(",");
		sb.append("TL_REC_LOAD = '");
		sb.append(successCount);
		sb.append("'");
		sb.append(",");
		sb.append("LOAD_ERRORS = '");
		sb.append(failureCount);
		sb.append("'");
		sb.append(" WHERE PROCESS_NUM = '");
		sb.append(seqValue);
		sb.append("'");
		V9ValidationDaoManager.execNonQuery(sb.toString());
		sb = null;
		logger.info("VI506Validation doFinalize() : start of updating PCF_CUST_ID ");

		if (failureCount == 0) {/*
			
			String fetchFromTemp = "select * from PCL_TEMP.CUST_DEMOGRAPHICS_TEMP";
			 ResultSet rs = V9ValidationDaoManager.execQuery(fetchFromTemp);
			
			HashMap<String, String> accessNumMap = new HashMap<>(500);
			String accessNumberList = null;
			rs.setFetchSize(2);
			while(rs.next()){
				String accessNumber = rs.getString("ACC_ACCESS_NUM").substring(0, rs.getString("ACC_ACCESS_NUM").length()-3);
				accessNumMap.put(accessNumber, rs.getString("PCF_CUST_ID"));
				accessNumberList = accessNumber+", ";
			}
				accessNumberList = accessNumberList.substring(0,accessNumberList.length()-1);
			
			String fetchTS2 = "select CIFP_ACCOUNT_ID6 and CIFP_CUSTOMER_ID_2 from TS2.CIF_CARD_CURR  where CIFP_ACCOUNT_NUM IN (" + accessNumberList + " )";
			Connection connection = DriverManager.getConnection("jdbc:oracle:thin:@lclvad246.ngco.com:1527/APP_IT303", "IPPCLCLAPP01", "Summ3R$2018");
			PreparedStatement preparedStatement = null;
		//	connection.
			
			String updateQuery = new StringBuilder(280)
					.append("MERGE INTO PCL_TEMP.CUST_DEMOGRAPHICS_TEMP T USING (SELECT DISTINCT PCF_CUST_ID , ")
					.append("ACCOUNT_REF_ID ,rn from (SELECT   DISTINCT  a.ACCOUNT_REF_ID ,a.PCF_CUST_ID ,Row_number() OVER(PARTITION BY a.ACCOUNT_REF_ID ORDER BY a.REC_CREATE_TMS DESC) rn ")
					.append(" FROM PCL.PCF_PRODUCT a JOIN PCL_TEMP.CUST_DEMOGRAPHICS_TEMP C ")
					.append("ON (LTRIM(SUBSTR(C.ACC_ACCESS_NUM, 0, LENGTH(C.ACC_ACCESS_NUM) - 2), 'I ') = a.ACCOUNT_REF_ID) JOIN PCL.PCF_CUST b ")
					.append("ON a.PCF_CUST_ID = b.PCF_CUST_ID AND b.CUST_TYPE='0')where rn=1)P ")
					.append("ON (LTRIM(SUBSTR(T.ACC_ACCESS_NUM, 0, LENGTH(T.ACC_ACCESS_NUM) - 2), 'I ') = P.ACCOUNT_REF_ID AND T.RECORD_TYPE='01' AND T.ACC_ACCESS_NUM like 'I%') ")
					.append("WHEN MATCHED THEN UPDATE SET T.PCF_CUST_ID = P.PCF_CUST_ID")
					.toString();

			logger.info("Updating the PCL_TEMP.CUST_DEMOGRAPHICS_TEMP FOR PCF_CUST_ID column for Account Id :  "
					+ updateQuery);

			int k = V9ValidationDaoManager.execNonQuery(updateQuery);

			logger.info("Number of records updated for Acc Ref ID: " + k);

			updateQuery = new StringBuilder(280)
					.append("MERGE INTO PCL_TEMP.CUST_DEMOGRAPHICS_TEMP T")
					.append(" USING PCL.PCF_CARD C ")
					.append(" ON (RTRIM(SUBSTR(T.ACC_ACCESS_NUM, 0, LENGTH(T.ACC_ACCESS_NUM) - 2), '@') = C.CARD_NUM ")
					.append(" AND T.RECORD_TYPE='01'")
					.append(" AND T.ACC_ACCESS_NUM like '%@' ) ")
					.append(" WHEN MATCHED THEN ")
					.append(" UPDATE SET T.PCF_CUST_ID = C.PCF_CUST_ID ")
					.toString();

			logger.info("Updating the PCL_TEMP.CUST_DEMOGRAPHICS_TEMP FOR PCF_CUST_ID column for Account Number :  "
					+ updateQuery);

			int l = V9ValidationDaoManager.execNonQuery(updateQuery);

			logger.info("Number of records updated For Acc Number: " + l);

			logger.info(" **** Total Number of records updated **** : "
					+ (k + l));

			if (successCount != (k + l)) {

				logger.info("Since total number of records updated in database do not match"
						+ successCount + "  !=  " + (k + l));
				logger.error("Since total number of records updated with PCF_CUST_ID in database do not match with the number of records loaded from file, so exiting the job");
				System.exit(1);

			}

		*/}

		else {

			logger.info("Bad records : "
					+ failureCount
					+ " Since bad records is not equal to 0 , so not updating teh PCF_CUST_ID in the CUST_DEMOGRAPHICS_TEMP table");
		}

		logger.info("VI502Validation doFinalize() : end ");

	}
}
