package com.lcl.pcb.v9.custom.extractor;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.lcl.pcb.v9.idf.jobflow.config.ExtractorConfig;


/**
 * This class is used if additional changes are needed to the original
 * interface sql query in the extractor xml
 * This class is specific to this interface, other custom classes
 * need to be coded for other interfaces if the original query is
 * not the final layout of the extraction flow for that interface.
 */
public class VI39QueryChanges extends QueryChanges {

		/**
	 * Constructor sets up originalRsArray 
	 * to be the original data that was extracted
	 * from the database in the Extractor process flow
	 * Use parent class's constructor
	 *
	 * @param  String[][] originalRsArray
	 * @return void
	 */		
	public VI39QueryChanges(String[][] originalRsArray) {
		super(originalRsArray);
	}
	
	Logger logger = LogManager.getLogger(VI39QueryChanges.class.getName());
	
	/**
	 * override execute method
	 * this method will do the necessary custom
	 * data modifications for the Extractor process flow
	 * for this interface 
	 *
	 * @param  void
	 * @return String[][] new array after modifications
	 */	
	public String[][] execute() throws Exception {
		logger.info("VI39QueryChanges execute() Called");

		int rowCount = originalRsArray.length;
		logger.debug("VI39QueryChanges rowCount found from DB Result: " + rowCount);
		//
		// loop through and store layout header from config file in newRsArray
		int numFields = ExtractorConfig.getInstance().getExtractorData().getLayout().getLField().size();
		logger.debug("VI39QueryChanges numFields count found from Layout in Extractor File: " + numFields);
		newRsArray = new String[2][]; // count is number of rows
		newRsArray[0] = new String[5];
		newRsArray[1] = new String[5];

		//months[0] = new int[31];
		
		int j = 1;
		for (int i = 0; i < 3 ;) // <-- outer loop starts here
		//int i=0;
		{
			// String headerValue = originalRsArray[j][i];
			newRsArray[0][i] = originalRsArray[j][i]; //H

			i++;
			newRsArray[0][i] = originalRsArray[j][i]; // file name

			i++;
		
			String runstarttime = originalRsArray[j][i];
			//SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
			SimpleDateFormat sdf1 = new SimpleDateFormat("yyyyMMdd_HHmmss");
			
			String date = sdf1.format(sdf.parse(runstarttime));
			
			newRsArray[0][i] = date; //date
			i++;
			
			//break;

		}
		//newRsArray.

	//	newRsArray[0] = header;
		for (int i = 4, k = 0; i < 8; ) // <-- outer loop starts here
		{
			// String trailerValue = originalRsArray[j][i];
			// T
			newRsArray[1][k] = originalRsArray[j][i];
			
			i++;k++;
			//No. of records from trailer
			int digits = Integer.parseInt(originalRsArray[j][i]);
			String padded = String.format("%010d", digits);
			newRsArray[1][k] = padded;
			
			i++;k++;
			//Litres purchased
			double litresPurchased = Double.parseDouble(originalRsArray[j][i]);
			newRsArray[1][k] = originalRsArray[j][i];
			
			i++;k++;
			//run end time
			String runendtime = originalRsArray[j][i];
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
			
			SimpleDateFormat sdf1 = new SimpleDateFormat("yyyyMMdd_HHmmss");
			
			String date = sdf1.format(sdf.parse(runendtime));
			
			newRsArray[1][k] = date;
			
			i++;k++;
			DecimalFormat df = new DecimalFormat("000000000000000.00");
			double totalAmtTrailer = Double.parseDouble(originalRsArray[j][i]);
			//String paddedAmt = df.format(totalAmtTrailer);
			newRsArray[1][k] = originalRsArray[j][i];
			//newRsArray[1][k] = paddedAmt;
			
			break;
			
		}

	//	newRsArray[1] = trailer;
		
		for (int i = 0; i < newRsArray.length; i++) {

			for (int k = 0; k < newRsArray[i].length; k++) {
				
				if( null == newRsArray[i][k]){
					
					newRsArray[i][k] = "";
					
				}

			}
		}

		return newRsArray;
	}

}


