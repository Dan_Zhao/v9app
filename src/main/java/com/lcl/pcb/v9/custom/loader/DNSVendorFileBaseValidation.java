package com.lcl.pcb.v9.custom.loader;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.lcl.pcb.v9.generic.loader.V9ValidationDaoManager;
import com.lcl.pcb.v9.idf.jobflow.config.IDFConfig;

public class DNSVendorFileBaseValidation
{
	String SourceFileId = null;
	
	HashMap<String, Integer> totRecParsed = new HashMap<String, Integer>();
	HashMap<String, Integer> trailerRecList = new HashMap<String, Integer>();
	ArrayList<String> csvFilesList = new ArrayList<String>();
	public String headerCsvFile = "";
	public String bodyCsvFile = "";
	public String trailerCsvFile = "";
	public String line = "";
	int trailerDtlRecCount = 0;
	int trailerCount = 0;
	int headerCount = 0;
	int totalDtlRecCount = 0;
	BufferedReader br;
	String processDate = "";
	V9ValidationDaoManager v9Validation = new V9ValidationDaoManager();

	boolean headerResult = false;
	boolean trailerResult = false;
	boolean bodyResult = false;
	boolean doValidation = false;
	Logger logger = null;

	public DNSVendorFileBaseValidation()
	{
		headerResult = true;
		trailerResult = true;
		bodyResult = true;
		doValidation = true;
		logger = LogManager.getLogger(DNSVendorFileBaseValidation.class.getName());
	}

	public boolean doValidation(ArrayList files)
			throws Exception
			{
		logger.debug("DNSVendorFileBaseValidation Called");
		this.csvFilesList = files;
		logger.debug("csvFilesList=" + files);
		doValidation &= doHeader();
		doValidation &= doBody();
		doValidation &= doTrailer();

		return doValidation;
			}

	public boolean doHeader() throws Exception {
		logger.debug("DNSVendorFileBaseValidation doHeader() Called");
		logger.debug("csvFilesList.size()="+csvFilesList.size());
		for (int i = 0; i < csvFilesList.size(); i++) {
			logger.debug("csvFilesList.get(i)="+csvFilesList.get(i));
			if (csvFilesList.get(i).contains("HEADER")) {
				headerCsvFile = csvFilesList.get(i);
				try {
					br = new BufferedReader(new FileReader(headerCsvFile));

					try {
						while ((line = br.readLine()) != null) {
							logger.debug("line="+line);
							String[] headerVal = line.split("\\|");
							logger.debug("headerVal="+headerVal);
							if (headerVal[0].equalsIgnoreCase("H")) {
								headerCount++;
								if (headerCount>1) {
									logger.error("More than 1 Header records  found"); 
									headerResult = false;
								}
								if (!headerVal[1].equals(SourceFileId)) 	{
									logger.error("Interface Name in Header is not " + SourceFileId);
									headerResult = false;
								}

								if (!headerVal[2].equals("")) processDate = headerVal[2];
								logger.debug("PROC_DATE in HEADER CSV File:" + processDate);

								v9Validation.getInstance();
								String sql = "SELECT * FROM  DNSCNST_TEMP.DNS_STG_HDR_TRLR where CURR_PROC_START_TS= to_timestamp('"+ processDate +"', 'yyyymmddhh24miss')";

								try {
									v9Validation.open();
									ResultSet res = V9ValidationDaoManager
											.execQuery(sql);
									if (res.next()) {

										logger.error("File already loaded for the date <YYYYDDDHH24MISS>:"
												+ processDate
												+ " "
												+ "To reload the file, delete all the existing records for this date from the DNSCNST_TEMP.<LOADING TABLE> and DNSCNST_TEMP.DNS_STG_HDR_TRLR first and then restart the job.");
										headerResult = false;
									} else {
										logger.debug("Header Date Validation Passed:A File with new CURR_PROC_START_TS YYYYMMDDHH24MISS Found" + processDate);
									}
									v9Validation.close();
								} catch (SQLException e) {
									throw e;
								} finally {
									v9Validation.con.close();
								}
							}

						}
						br.close();
						if (headerCount<1) {
							logger.error("Header records is missing"); 
							headerResult = false;
						}
					} catch (IOException e) {
						e.printStackTrace();
					}
				} catch (FileNotFoundException ex) {
					ex.printStackTrace();
				}
			}
		}
		return headerResult;
	}

	public boolean doTrailer()
	{
		logger.debug("DNSVendorFileBaseValidation doTrailer() Called");
		logger.debug("csvFilesList.size()="+csvFilesList.size());
		for (int i = 0; i < csvFilesList.size(); i++) {
			logger.debug("csvFilesList.get(i)="+csvFilesList.get(i));
			if (csvFilesList.get(i).contains("TRAILER")) {
				trailerCsvFile = csvFilesList.get(i);
				try {
					br = new BufferedReader(new FileReader(trailerCsvFile));

					try {
						while ((line = br.readLine()) != null) {
							logger.debug("line="+line);
							String[] trailerVal = line.split("\\|");
							if (trailerVal[3].equalsIgnoreCase("T")
									&& trailerVal[4].length() > 0) {
								trailerCount++;
								trailerDtlRecCount=Integer.parseInt(trailerVal[4]); 
							}
						}
						br.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				} catch (FileNotFoundException ex) {
					ex.printStackTrace();
				}
			}
		}

		if (trailerCount < 1) {
			logger.error("Trailer record is missing");
		} else if (trailerCount > 1) {
			logger.error("More than one trailer record is found.");
			trailerResult = false;
		}

		if (trailerDtlRecCount != totalDtlRecCount) {
			logger.error("Total number of detail records does not match the count in the trailer.");
			trailerResult = false;
		}
		return trailerResult;
	}

	public boolean doBody()
	{
		logger.debug("DNSVendorFileBaseValidation doBody() Called");

		for (int i = 0; i < csvFilesList.size(); i++) {
			if (csvFilesList.get(i).contains("Body") ) {
				logger.debug("csvFilesList.get(i)="+csvFilesList.get(i));				
				bodyCsvFile = csvFilesList.get(i);
				try {
					br = new BufferedReader(new FileReader(bodyCsvFile));

					try {
						while ((line = br.readLine()) != null) {
							totalDtlRecCount++;
						}
						br.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				} catch (FileNotFoundException ex) {
					ex.printStackTrace();
				}
			}
		}

		return bodyResult;
	}

	public void doErrorLog()
			throws Exception
			{
			}

	public void doFinalize(ArrayList files)
			throws Exception
			{
		logger.debug("DNSVendorFileBaseValidation doFinalize() Called ");
		String dataFile = IDFConfig.getInstance().getIdfData().getDatafileDatafileLocation();
		String fileName = null;
		String fileDate = null;
		String endDatePrevVal = null;
		String endDateCurVal = null;

		String schemaname = null;
		int tl_rec_load =0;
		int load_errors=0;
		StringBuilder sb = new StringBuilder("");
		int seqVal;
		if (dataFile.lastIndexOf("\\") > 0) {
			fileName = dataFile
					.substring(dataFile.lastIndexOf("\\") + 1);
		}
		if (dataFile.lastIndexOf("/") > 0) {
			fileName = dataFile.substring(dataFile.lastIndexOf("/") + 1);
		}

		int fileDatePos = fileName.lastIndexOf("_", fileName.lastIndexOf("_") - 1);
		if (fileDatePos > 0)
			fileDate=fileName.substring(fileDatePos+1,fileName.lastIndexOf('_'));
		//			fileDate=fileName.substring(fileDatePos+1,fileDatePos+9);

		try{
			endDatePrevVal = fileDate;
			endDateCurVal = fileDate;
			seqVal = IDFConfig.getInstance().getSequenceNum();

			logger.debug("DNSVendorFileBaseValidation doFinalize() Called - read tl_rec_load");
			String sqlCntTlRecLoad = "SELECT count(*) from DNSCNST_TEMP.DNS_STAGING where PROCESS_NUM = " + Integer.toString(seqVal);
			ResultSet rs=V9ValidationDaoManager.execQuery(sqlCntTlRecLoad);
			while(rs.next()){tl_rec_load=rs.getInt(1);}

			schemaname = IDFConfig.getInstance().getIdfData().getCsvloadDbSchema();

			logger.debug("DNSVendorFileBaseValidation doFinalize() Called - update load_result");
			sb.append("UPDATE "+schemaname+".LOAD_RESULT SET ");
			sb.append("SOURCE_END_DT_CURR = ");
			if (fileDate==null) {
				sb.append("CURRENT_TIMESTAMP ");
			} else {
				sb.append("TO_TIMESTAMP ('"+endDateCurVal+"', 'YYYYMMDDhh24miss')");
			}
			sb.append(",");
			sb.append("SOURCE_ID = '");
			sb.append(SourceFileId);
			sb.append("',");
			sb.append("TL_REC_LOAD = ");
			sb.append(Integer.toString(tl_rec_load));
			sb.append(",");
			sb.append("LOAD_ERRORS = ");
			sb.append(Integer.toString(load_errors));
			sb.append(" WHERE PROCESS_NUM = '");
			sb.append(seqVal);
			sb.append("'");
			logger.debug("sb.toString()="+sb.toString());
			V9ValidationDaoManager.execNonQuery(sb
					.toString());
			sb = null; 
		}catch(Exception e){
			logger.error("DNSVendorFileBaseValidation doFinalize() Called: Loading successful, LOAD_RESULT update Failure Please check"+sb.toString());
			e.printStackTrace();
		}
			}
}
