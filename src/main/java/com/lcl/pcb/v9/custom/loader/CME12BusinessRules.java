/**
 * 
 */
package com.lcl.pcb.v9.custom.loader;

/**
 * @author Dev
 *
 */
public class CME12BusinessRules {
	
	public String businessRuleId;
	
	public String businessRuleDesc;
	
	public String businessRuleQualifier;
	
	public String businessRuleRefTable;
	
	public String businessRuleRefCol;
	
	public String businessRuleRefValue;
	
	public String customerAcctXref;

	/**
	 * @return the businessRuleId
	 */
	public String getBusinessRuleId() {
		return businessRuleId;
	}

	/**
	 * @param businessRuleId the businessRuleId to set
	 */
	public void setBusinessRuleId(String businessRuleId) {
		this.businessRuleId = businessRuleId;
	}

	/**
	 * @return the businessRuleDesc
	 */
	public String getBusinessRuleDesc() {
		return businessRuleDesc;
	}

	/**
	 * @param businessRuleDesc the businessRuleDesc to set
	 */
	public void setBusinessRuleDesc(String businessRuleDesc) {
		this.businessRuleDesc = businessRuleDesc;
	}

	/**
	 * @return the businessRuleQualifier
	 */
	public String getBusinessRuleQualifier() {
		return businessRuleQualifier;
	}

	/**
	 * @param businessRuleQualifier the businessRuleQualifier to set
	 */
	public void setBusinessRuleQualifier(String businessRuleQualifier) {
		this.businessRuleQualifier = businessRuleQualifier;
	}

	/**
	 * @return the businessRuleRefTable
	 */
	public String getBusinessRuleRefTable() {
		return businessRuleRefTable;
	}

	/**
	 * @param businessRuleRefTable the businessRuleRefTable to set
	 */
	public void setBusinessRuleRefTable(String businessRuleRefTable) {
		this.businessRuleRefTable = businessRuleRefTable;
	}


	/**
	 * @return the businessRuleRefValue
	 */
	public String getBusinessRuleRefValue() {
		return businessRuleRefValue;
	}

	/**
	 * @param businessRuleRefValue the businessRuleRefValue to set
	 */
	public void setBusinessRuleRefValue(String businessRuleRefValue) {
		this.businessRuleRefValue = businessRuleRefValue;
	}

	/**
	 * @return the customerAcctXref
	 */
	public String getCustomerAcctXref() {
		return customerAcctXref;
	}

	/**
	 * @param customerAcctXref the customerAcctXref to set
	 */
	public void setCustomerAcctXref(String customerAcctXref) {
		this.customerAcctXref = customerAcctXref;
	}

	/**
	 * @return the businessRuleRefCol
	 */
	public String getBusinessRuleRefCol() {
		return businessRuleRefCol;
	}

	/**
	 * @param businessRuleRefCol the businessRuleRefCol to set
	 */
	public void setBusinessRuleRefCol(String businessRuleRefCol) {
		this.businessRuleRefCol = businessRuleRefCol;
	}
	
	

}
