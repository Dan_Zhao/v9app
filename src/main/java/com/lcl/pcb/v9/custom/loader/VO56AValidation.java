package com.lcl.pcb.v9.custom.loader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.lcl.pcb.v9.dao.V9DAOManager;
import com.lcl.pcb.v9.generic.loader.LoaderUtil;
import com.lcl.pcb.v9.generic.loader.V9ValidationDaoManager;
import com.lcl.pcb.v9.generic.security.V9SecureAES;
import com.lcl.pcb.v9.idf.jobflow.config.IDFConfig;
import com.lcl.pcb.v9.utilities.fileReader.FileUtil;

/**
 * This class is used to validate the csv files generated for VO10C interface.
 * It updates SOURCE_END_DT_PREV and SOURCE_END_DT_CURR and SOURCE_FILE_ID
 * values with correct Values/format.
 */

public class VO56AValidation{

	ArrayList<String> csvFilesList = new ArrayList<String>();
	public String headerCsvFile = "";
	BufferedReader br;
	String line = "";
	String endDatePrevVal = "";
	String endDateCurVal = "";
	String seqVal = "";
	String vendorId = "";
	String fileId = "";
	String SourceFileId = "";
	String endDatePrev = "";
	String endDateCur = "";
	boolean headerResult = false;			//Changed to false on 26th Feb
	boolean trailerResult = false;			//Changed to false on 26th Feb
	boolean bodyResult = true;				
	boolean doValidation = true;
	String[] headerVal = null;
	String fileEndDateCurr = "";
	String dateFormat = "YYYY-MM-DD HH24:MI:SS";
	HashMap<String, Integer> totRecParsed = new HashMap<String, Integer>();
	HashMap<String, Integer> trailerRecList = new HashMap<String, Integer>();
	public String trailerCsvFile = "";
	int totalRecTrailerCount = 0;
	int totalRecCount = 0;
	ArrayList<String> errorLogList = new ArrayList<String>();
	ArrayList<String> badRecordList = null;
	ArrayList<VO56APojo> errorObjectList = null;
	String dataFileLocation = "";
	String dataFileName = "";
	String logFileName = "";
	String badFileName = "";
	String logName = "";
	String logFileNameVal ="";
	String[] logFileNamePattern ;
	String errorRecord = "Record";
	StringBuilder errorMsg=null;
	BufferedReader badrecordreader;
	
	String line1= "";
	String errorRecordRow = "";
	String errorrecord ="";
	String[] apaAppNum;
	String apaAppNumVal ="";

	V9DAOManager db = new V9DAOManager();
	Logger logger = LogManager.getLogger(VO56AValidation.class.getName());
	V9ValidationDaoManager v9Validation = new V9ValidationDaoManager();
	IDFConfig idfConfig = IDFConfig.getInstance();
	
	//Changes below for the change in schema name for table Nf_Accnt_Prev_Extend_Hold from GPR to GPR_TEMP
	private String driverClass;
    private String connectionURLGPR;
    private String userIDGPR;
    private String userPasswordGPR;
    private String schemaNameGPR;
    private Connection conGPR;
    private String connectionURLGPRTemp;
    private String userIDGPRTemp;
    private String userPasswordGPRTemp;
    private String schemaNameGPRTemp;
    private Connection conGPRTemp;
    private String batchLoadSize;
    private String columnListNfAccntPrevExtend = "ACCOUNT_ID_256\r\n" + 
    		",OPEN_DATE\r\n" + 
    		",EXPIRATION_DATE\r\n" + 
    		",CARDHOLDER_ID_CODE\r\n" + 
    		",CARDHOLDER_ID_VALU\r\n" + 
    		",FIRST_NAME\r\n" + 
    		",LAST_NAME\r\n" + 
    		",MIDDLE_INITIAL\r\n" + 
    		",ADDRESS_LINE_1\r\n" + 
    		",ADDRESS_LINE_2\r\n" + 
    		",CITY\r\n" + 
    		",STATE\r\n" + 
    		",ZIP_CODE\r\n" + 
    		",COUNTRY_CODE\r\n" + 
    		",PRIMARY_PHONE_NUMBER\r\n" + 
    		",SECONDARY_PHONE_NUMBER\r\n" + 
    		",STATUS\r\n" + 
    		",CURR_BAL\r\n" + 
    		",CURR_BAL_SIGN\r\n" + 
    		",SUB_PGM_ID_VALUE\r\n" + 
    		",DATE_OF_BIRTH\r\n" + 
    		",ACH_DDA_ACCOUNT_NUMBER\r\n" + 
    		",CUSTOMER_ID\r\n" + 
    		",CIP_ID\r\n" + 
    		",MAIL_DATE\r\n" + 
    		",RELOADABLE_INDICATOR\r\n" + 
    		",AVAILABLE_BALANCE\r\n" + 
    		",AVAILABLE_BALANCE_SIGN\r\n" + 
    		",CUSTOMER_EMAIL_ADDRESS\r\n" + 
    		",IP_ADDRESS_OF_APPLICATION\r\n" + 
    		",PRIMARY_CARD_INDICATOR\r\n" + 
    		",CARD_ACTIVATION_DATE\r\n" + 
    		",GROUP_ID\r\n" + 
    		",EMPLOYMENT_STATUS\r\n" + 
    		",OCCUPATION\r\n" + 
    		",INCOME\r\n" + 
    		",SIN_CARD_#\r\n" + 
    		",PRIVACY_FLAG\r\n" + 
    		",PRIVACY_DATE\r\n" + 
    		",PRIVACY_DTS\r\n" + 
    		",DNS\r\n" + 
    		",LOYALTY_CUSTOMER_NUMBER\r\n" + 
    		",ACCEPTANCE_ID_VERIFICATION\r\n" + 
    		",PC_POINTS_T_AND_C_FLAG\r\n" + 
    		",PC_POINTS_T_AND_C_DATE\r\n" + 
    		",PC_POINTS_T_AND_C_DTS\r\n" + 
    		",MAINTENANCE_FEE_FLAG\r\n" + 
    		",MAINTENANCE_FEE_AMOUNT\r\n" + 
    		",MAINTENANCE_FEE_DATE\r\n" + 
    		",MAINTENANCE_FEE_DTS\r\n" + 
    		",CARDHOLDER_AGGREMENT_FLAG\r\n" + 
    		",CARDHOLDER_AGREE_DATE\r\n" + 
    		",CARDHOLDER_AGGREMENT_DTS\r\n" + 
    		",ID_VERIFICATION_FLAG\r\n" + 
    		",ID_VERIFICATION_DATE\r\n" + 
    		",ID_VERIFICATION_DTS\r\n" + 
    		",ID_VERIFICATION_METHOD\r\n" + 
    		",ID_VERIFICATION_RESULT\r\n" + 
    		",ELECTRONIC_DISCLOSURE_FLAG\r\n" + 
    		",ELECTRONIC_DISCLOSURE_DATE\r\n" + 
    		",ELECTRONIC_DISCLOSURE_DTS\r\n" + 
    		",ALERT_T_AND_C_FLAG\r\n" + 
    		",ALERT_T_AND_C_DATE\r\n" + 
    		",ALERT_T_AND_C_DTS\r\n" + 
    		",SMS_ALERTS\r\n" + 
    		",EMAIL_ALERTS\r\n" + 
    		",ALERT_OPT_STATUS\r\n" + 
    		",MARKETING_PREFERENCES_OPT_STAT\r\n" + 
    		",MARKETING_PREFERENCES_OPT_DATE\r\n" + 
    		",MARKETING_PREFERENCES_OPT_TIME\r\n" + 
    		",ENTITY\r\n" + 
    		",CHANNEL\r\n" + 
    		",VENDOR\r\n" + 
    		",DATE_REISSUE_ACTION\r\n" + 
    		",PLACEHOLDER1\r\n" + 
    		",PLACEHOLDER2\r\n" + 
    		",ACCOUNT_ID\r\n" + 
    		",REC_CREATE_TMS\r\n" + 
    		",REC_CHNG_TMS\r\n" + 
    		",REC_CHNG_ID\r\n" + 
    		",LAST_TRANSACTION_DATE\r\n" + 
    		",LAST_STATUS_DATE";

	/**
	 * This method will be invoked from V9Loader class which expects the csv
	 * files list for validating header, body and trailer.
	 * 
	 * @param files
	 *            This is the parameter of CSV files list
	 * @return boolean This returns true/false value.
	 */
	public boolean doValidation(ArrayList<String> files) throws Exception {
		logger.debug("VO56AValidation Called");
		this.csvFilesList = files;
		doValidation &= doHeader();
		doValidation &= doTrailer();
		doValidation &= doBody();
		//doValidation &= backupNfTables();
		if(headerResult && trailerResult) backupNfTables();
		return doValidation;
	}

	private void backupNfTables() {
	    // TODO Auto-generated method stub
	   try {
		   //Get DB connection for GPR_TEMP Schema
		   getConnection();
		   
	       //String sql1="delete from GPR.Nf_Accnt_Prev_Extend_Hold";
		   //Replacing above statement with below method call due to change in schema name - 29 Feb 2016
		   emptyNfAccntPrevExtendHold();
		   
	       //String sql2="Insert Into GPR.Nf_Accnt_Prev_Extend_Hold (Select * From GPR.Nf_Accnt_Prev_Extend)";	//Added Schema Name - 21 Feb 2016
		   //Replacing above statement with below method call due to change in schema name - 29 Feb 2016
		   insertNfAccntPrevExtendHold();
	       String sql3="Delete From GPR.Nf_Accnt_Prev_Extend";
	       String sql4="Insert Into GPR.Nf_Accnt_Prev_Extend (Select * from GPR.Nf_Accnt_Curr_Extend)";
	       String sql5="Delete From GPR.Nf_Accnt_Curr_Extend";
		   
	    V9ValidationDaoManager.getInstance();
	    v9Validation.open();
	    logger.debug("Executing NF tables back up before load");
	    //V9ValidationDaoManager.execNonQuery(sql1);		//Commented as this would be handled by emptyNfAccntPrevExtendHold
	    //V9ValidationDaoManager.execNonQuery(sql2);
	    V9ValidationDaoManager.execNonQuery(sql3);
	    V9ValidationDaoManager.execNonQuery(sql4);
	    V9ValidationDaoManager.execNonQuery(sql5);
	    logger.debug("NF tables data moving completed");
	    v9Validation.close();
	   }
	   catch(Exception ex){
	    //   ex.printStackTrace();
	       logger.error("VO56AValidation: Exception has occurred: ", ex);
	   }
	    
	  //  return false;
	}

	public boolean doHeader() throws Exception {
		logger.debug("VO56AValidation doHeader() Called");
		for (int i = 0; i < csvFilesList.size(); i++) {
			if (csvFilesList.get(i).contains("HEADER")) {
				headerCsvFile = csvFilesList.get(i);
				try {
					File f = new File(headerCsvFile);
					if(!(f.exists())){
						  logger.error("There is no Trailer Record/Type. Please check the Data file.");
						  db.setLevelWarning(3,"There is no Trailer Record/Type. Please check the Data file.",0);
				    }
					br = new BufferedReader(new FileReader(headerCsvFile));

					try {
						while ((line = br.readLine()) != null) {
							String[] headerVal = line.split("\\|");
							logger.debug("headerVal[0]"+headerVal[0]+"headerVal[1]"+headerVal[1]+"headerVal[2]"+headerVal[2]+"headerVal[6]"+headerVal[6]);
							if (headerVal[0].equalsIgnoreCase("HEADER")
								//&& headerVal[1].equalsIgnoreCase("'BHN'") 	//Added Single Apostrophes ' around BHN - 21 Feb 2016
								&& headerVal[1].equalsIgnoreCase("BHN") 	//Removed Single Apostrophes ' around BHN - 4 Mar 2016
								&& headerVal[2].equalsIgnoreCase("NON-FINANCIAL 2")
								//&& headerVal[6].equalsIgnoreCase("'2'")		//Added Single Apostrophes ' around 2 - 21 Feb 2016
								&& headerVal[6].equalsIgnoreCase("2")		//Removed Single Apostrophes ' around 2 - 4 Mar 2016
								&& headerVal.length > 6) {
							    logger.info("VO56A Header Data Validation Passed");
							    headerResult=true;							//Added on 26th Feb to initialize flag to false
							}
							else {
							    logger.error("VO56A Header Data Validation Failed");
							    headerResult=false;
							}
						}
						br.close();
					} catch (IOException e) {
						//e.printStackTrace();
					    logger.error("VO56AValidation: Exception has occurred: ", e);
					}
				} catch (FileNotFoundException ex) {
					//ex.printStackTrace();
					logger.error("VO56AValidation: Exception has occurred: ", ex);
				}
			}
			
		}
		logger.debug("headerResult"+headerResult);
		return headerResult;
	}

	public boolean doTrailer() {
		logger.debug("VO56AValidation doTrailer() Called");
		totRecParsed = FileUtil.totalRecParsedList;
		for (int i = 0; i < csvFilesList.size(); i++) {
			if (csvFilesList.get(i).contains("TRAILER")) {
				trailerCsvFile = csvFilesList.get(i);
				try {
					br = new BufferedReader(new FileReader(trailerCsvFile));

					try {
						while ((line = br.readLine()) != null) {
							String[] trailerVal = line.split("\\|");
							if (trailerVal[7].equalsIgnoreCase("TRAILER")
									&& trailerVal.length > 6) {
							    logger.debug("trailerVal[7]"+trailerVal[7]+"trailerVal[8]"+trailerVal[8]);
								trailerRecList.put("NFACCNTCUR",
										Integer.parseInt(trailerVal[8]));
								trailerRecList.put("TRAILER", 1);
							    logger.info("VO56A Trailer Data Validation Passed");
							    trailerResult=true;							//Added on 26th Feb to initialize flag to false
							}
							else {
							    logger.error("VO56A Trailer Data Validation Failed");
							    trailerResult=false;
							}
						}
						br.close();
						
					} catch (IOException e) {
						//e.printStackTrace();
						logger.error("VO56AValidation: Exception has occurred: ", e);
					}
				} catch (FileNotFoundException ex) {
					//ex.printStackTrace();
					logger.error("VO56AValidation: Exception has occurred: ", ex);
				}
			}
		}
		for (Entry<String, Integer> entry : trailerRecList.entrySet()) {
			totalRecTrailerCount += (entry.getValue());
			logger.debug("totalRecTrailerCount: "+totalRecTrailerCount);
		}

		for (Entry<String, Integer> entry : totRecParsed.entrySet()) {
			totalRecCount += (entry.getValue());
			logger.debug("totalRecCount: "+totalRecCount);
		}
		logger.debug("totRecParsed.keySet()"+totRecParsed.keySet()+"trailerRecList.keySet()"+trailerRecList.keySet());
		if (totRecParsed.keySet().equals(trailerRecList.keySet())) {
		    logger.debug("****totalRecCount"+totalRecCount+"---totalRecTrailerCount"+totalRecTrailerCount);
			if (totalRecCount == totalRecTrailerCount) {
				logger.debug("Total Record Count in File matches with Total Record Count in TRAILER.");
			} else {
				logger.error("Total Record Count in File Mismatches with Record Count in TRAILER.");
				trailerResult = false;
			}

		} else {
			logger.error("VO56A Record Segments"+totRecParsed.keySet()+" doesn't match/not available in the Processed Data file.");
			trailerResult = false;
		} 
		return trailerResult;
	}

	public boolean doBody() throws Exception {
		logger.info("VO56AValidation doBody() Called");
		logger.debug("bodyResult"+bodyResult);
		return bodyResult;
	}

	public void doErrorLog() throws Exception {
		logger.info("VO56AValidation doerror() Called");
		
		errorLogList=LoaderUtil.ErrorlogFileNamesList;
		dataFileLocation=IDFConfig.getInstance().getIdfData().getDatafileDatafileLocation();
		dataFileName=dataFileLocation.substring(dataFileLocation.lastIndexOf("/") + 1);
		if (null != errorLogList && errorLogList.size() != 0) {
			for (int i = 0; i < errorLogList.size(); i++) {
				errorObjectList=new ArrayList<VO56APojo>();
				logFileName = errorLogList.get(i);
				logFileNameVal=logFileName.substring(logFileName.lastIndexOf("/")+1, logFileName.lastIndexOf("."));
				logFileNamePattern=logFileNameVal.split("_");
				badFileName =IDFConfig.getInstance().getIdfData().getDatafileBadFileLocation()
				+ "/"+ logFileNamePattern[0]+"_"+logFileNamePattern[2]+"_"+logFileNamePattern[3]+"_"+logFileNamePattern[1]+ IDFConfig.getInstance().getIdfData().getDatafileBadNamePattern();
				//logName = logFileName.substring(logFileName.lastIndexOf("/") + 1);
				//segmentPart = logName.split("_");
				try{
					badrecordreader = new BufferedReader(new FileReader(badFileName));
					badRecordList=new ArrayList();
					try{
						while((line1 = badrecordreader.readLine())!= null ){
							badRecordList.add(line1);
						}
					}catch (IOException e) {
						// TODO Auto-generated catch block
						//e.printStackTrace();
						logger.error("VO56AValidation: Exception has occurred: ", e);
					}
				}catch(FileNotFoundException ex){
					//ex.printStackTrace();
					logger.error("VO56AValidation: Exception has occurred: ", ex);
				}
				try {
					br = new BufferedReader(new FileReader(logFileName));
					int count=0;
					try {
						while ((line = br.readLine()) != null) {
							
							
							if (line.contains(errorRecord)) {
							    VO56APojo errorObject = new VO56APojo();
								errorObject.setFileName(dataFileName);
								errorObject.setSegmentName(logFileNamePattern[3]);
								errorMsg=new StringBuilder();
								errorMsg.append(line);
								errorRecordRow=line.substring(line.indexOf(" "),
										line.lastIndexOf(":"));
								line=br.readLine();
								errorMsg.append(line);
								errorObject.setErrorrecordRow(errorRecordRow);
								errorObject.setErrorMsg(errorMsg);
								errorrecord=badRecordList.get(count);
								if(errorrecord.length()<= 4000){
									errorObject.setErrorrecord(errorrecord);
								}else{
									errorObject.setErrorrecord(errorrecord.substring(0, 3999));
									logger.info("Error Record length is more than 4000 characters: So inserting first 4000 characters of error record");
								}
								apaAppNum=errorrecord.split("\\|");
								if(apaAppNum.length>1){
									//apaAppNumVal=apaAppNum[1];
									apaAppNumVal=apaAppNum[0];		//Changed to below on 29th Feb 2016 to log ACCOUNT_ID_256
								}
								if(null==apaAppNumVal || "".equalsIgnoreCase(apaAppNumVal)){		//Added this check on 10th March 2016
									apaAppNumVal = "Data Not Available";
								}
								errorObject.setApaAPPNUM(apaAppNumVal);
								count ++;
								errorObjectList.add(errorObject);
								errorObject=null;
								errorMsg=null;
								
							}
						}
						badRecordList=null;
						br.close();
						if (errorObjectList != null && errorObjectList.size() > 0) {
							V9ValidationDaoManager.getInstance();
							v9Validation.open();
							insertIntoErrorTable(errorObjectList);
						}
						errorObjectList=null;
					}catch (IOException e) {
						// TODO Auto-generated catch block
						//e.printStackTrace();
						logger.error("VO56AValidation: Exception has occurred: ", e);
					}
				}catch(FileNotFoundException ex){
					//ex.printStackTrace();
					logger.error("VO56AValidation: Exception has occurred: ", ex);
				}
				}
			v9Validation.close();
			}


	}

	/**
	 * This method is used for updating SOURCE_END_DT_PREV,SOURCE_END_DT_CURR
	 * and SOURCE_FILE_ID values with correct Values/format.
	 * 
	 * @param files
	 *            This is the parameter of CSV files list
	 * @return boolean This returns true/false value.
	 */
	public void doFinalize(ArrayList<String> files) throws Exception {

		logger.debug("VO56AValidation doFinalize() Called ");
	}
	public void insertIntoErrorTable(ArrayList<VO56APojo> errorRecords)
	throws Exception {
	    VO56APojo errObject;
		String LoadResultUUID = "";
		String errorRecord = "";
		String apaAPPNUM = "";
		
		logger.info("Start of Inserting Error Records in GPR Error Table");
		for (int i=0;i<errorRecords.size();i++){
			errObject=(VO56APojo)errorRecords.get(i);
			int sequenceNum=IDFConfig.getInstance().getSequenceNum();
			StringBuilder sb = new StringBuilder("");
			sb.append("SELECT LOAD_RESULT_UUID FROM "+IDFConfig.getInstance().getIdfData().getCsvloadDbSchema()+".LOAD_RESULT WHERE ");
			sb.append("PROCESS_NUM = ");
			sb.append(sequenceNum);
			ResultSet rs = V9ValidationDaoManager.execQuery(sb.toString());
			if (rs.next())
				LoadResultUUID=rs.getString("LOAD_RESULT_UUID");
			else
				logger.debug("UUID not found");
			
			//Added below conditions on 26th Feb 2016 as per changes in BHN files			
			if(null!=errObject.getErrorrecordRow() && errObject.getErrorrecord().contains("'")){
				errorRecord = errObject.getErrorrecord().replaceAll("'", "''");
			}else{
				errorRecord = errObject.getErrorrecord();
			}
			if(null!=errObject.getApaAPPNUM() && errObject.getApaAPPNUM().contains("'")){
				apaAPPNUM = errObject.getApaAPPNUM().replaceAll("'", "''");
			}else{
				apaAPPNUM = errObject.getApaAPPNUM();
			}
			String insertSql="insert into "+IDFConfig.getInstance().getIdfData().getCsvloadDbSchema()+".GPR_LOAD_ERROR(INTERFACE_NM,FILENAME,SEGMENT_NAME,PROCESS_NUM,ERROR_RECORD_ROW,ERROR_RECORD,ERROR_MSG,REC_CHNG_TMS,REC_LOAD_TMS,V9SEQ,LOAD_RESULT_UUID) values('VO56A','"+errObject.getFileName()+"','"+errObject.getSegmentName()+"','"+apaAPPNUM+"','"+errObject.getErrorrecordRow().trim()+"','"+errorRecord+"','"+errObject.getErrorMsg()+"',sysdate,sysdate,"+sequenceNum+",'"+LoadResultUUID+"')";
			logger.debug("Insert SQl:"+insertSql.toString());
			int res = V9ValidationDaoManager
					.execNonQuery(insertSql);
			rs = null;
			sb = null;
			
		}
		logger.info("Error Records Inserted in GPR Error Table");
		
	}
	
	/**
	 * 
	 */
	public void getConnection(){
		logger.info("VO56AValidation: getConnection: Execution has Started");
		InputStream in = null;
    	try
    	{
    		Properties prop = new Properties();
    		String path = "InterfaceDefinition/VO56A/properties/jdbcBHNVO56A.properties";
    		String copybookxmlFileLocation = idfConfig.getIdfData().getCopybookxmlFileLocation();
    		path = copybookxmlFileLocation.substring(0, copybookxmlFileLocation.lastIndexOf("/")) + "/jdbcBHNVO56A.properties";
    		logger.debug("VO56AValidation: getConnection: Properties File Location" + path);
    		//String path = "jdbcBHNVO56A.properties";
    		FileInputStream fi = new FileInputStream(path);
    		//in = this.getClass().getClassLoader().getResourceAsStream(path);
    		prop.load(fi);
    		driverClass = prop.getProperty("driverClass");
    		
    		connectionURLGPR = prop.getProperty("connectionURL1");
    		logger.debug("VO56AValidation: getConnection: connectionURLGPR" + connectionURLGPR);
    		userIDGPR = prop.getProperty("userID1");
    		userPasswordGPR = getDecryptedValue(prop.getProperty("userPassword1"));
    		schemaNameGPR = prop.getProperty("schemaName1");
    		
    		connectionURLGPRTemp = prop.getProperty("connectionURL2");
    		logger.debug("VO56AValidation: getConnection: connectionURLGPRTemp" + connectionURLGPRTemp);
    		userIDGPRTemp = prop.getProperty("userID2");
    		userPasswordGPRTemp = getDecryptedValue(prop.getProperty("userPassword2"));
    		schemaNameGPRTemp = prop.getProperty("schemaName2");

    		batchLoadSize = prop.getProperty("batchLoadSize");

    		Class.forName(driverClass).newInstance();
    		this.conGPR = DriverManager.getConnection(connectionURLGPR, userIDGPR, userPasswordGPR);
    		this.conGPRTemp = DriverManager.getConnection(connectionURLGPRTemp, userIDGPRTemp, userPasswordGPRTemp);
    		logger.info("VO56AValidation: getConnection: Execution has Ended");
    	}
    	catch(Exception e)
    	{
			logger.error("VO56AValidation: getConnection: Exception has occurred: ", e);
    	}
    	finally
        {
            try
            {
            	if(in != null) in.close ();
            }
            catch(IOException e)
            {
    			logger.error("VO56AValidation: getConnection: Exception has occurred: ", e);
            }
        }
	}
	
	public String getDecryptedValue(String encryptedText){
		logger.info("VO56AValidation: getDecryptedValue: Execution Started");
		V9SecureAES security = new V9SecureAES();
		return security.decrypt(encryptedText);
	}
	
	public void emptyNfAccntPrevExtendHold(){
		logger.info("VO56AValidation: emptyNfAccntPrevExtendHold: Execution Started");
        String query = "delete from " + schemaNameGPRTemp + ".Nf_Accnt_Prev_Extend_Hold";
        logger.debug("VO56AValidation: emptyNfAccntPrevExtendHold: Query: " + query);
        try {
			PreparedStatement ps = conGPRTemp.prepareStatement(query);
			ps.executeUpdate();
	        logger.debug("VO56AValidation: emptyNfAccntPrevExtendHold: Table Nf_Accnt_Prev_Extend_Hold was emptied");
			logger.info("VO56AValidation: emptyNfAccntPrevExtendHold: Execution Ended");
		} catch (SQLException e) {
			logger.error("VO56AValidation: emptyNfAccntPrevExtendHold: Exception has occurred: ", e);
		}

	}
	
	public void insertNfAccntPrevExtendHold(){
		logger.info("VO56AValidation: insertNfAccntPrevExtendHold: Execution Started");
		PreparedStatement psInsert  = null;
		PreparedStatement psFetch  = null;
		ResultSet resultFetch = null;
		int resultFetchCount = 0;
		int i=0;
		int batchLoadThreshold = Integer.parseInt(batchLoadSize);
		String fetchQuery = "SELECT " + columnListNfAccntPrevExtend + " FROM " + schemaNameGPR + ".NF_ACCNT_PREV_EXTEND";
		logger.debug("VO56AValidation: insertNfAccntPrevExtendHold: fetchQuery: " + fetchQuery);
		String insertQuery = "INSERT INTO " + schemaNameGPRTemp + ".NF_ACCNT_PREV_EXTEND_HOLD (" + columnListNfAccntPrevExtend + ") VALUES"
				+ "(?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?)";
		logger.debug("VO56AValidation: insertNfAccntPrevExtendHold: insertQuery: " + insertQuery);

		try {
			//TO-DO: Get all records from Nf_Accnt_Prev_Extend_Hold
			psFetch = conGPR.prepareStatement(fetchQuery,ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			resultFetch = psFetch.executeQuery();
			resultFetchCount = getRowCount(resultFetch);
			logger.debug("VO56AValidation: insertNfAccntPrevExtendHold: resultFetchCount from table NF_ACCNT_PREV_EXTEND: " + resultFetchCount);
            psInsert = conGPRTemp.prepareStatement(insertQuery);
    		long start = System.currentTimeMillis();

			while(resultFetch.next()) {
	            psInsert.setString(1, resultFetch.getString("ACCOUNT_ID_256"));
	            psInsert.setTimestamp(2, resultFetch.getTimestamp("OPEN_DATE"));
	            psInsert.setTimestamp(3, resultFetch.getTimestamp("EXPIRATION_DATE"));
	            psInsert.setBigDecimal(4, resultFetch.getBigDecimal("CARDHOLDER_ID_CODE"));
	            psInsert.setString(5, resultFetch.getString("CARDHOLDER_ID_VALU"));
	            psInsert.setString(6, resultFetch.getString("FIRST_NAME"));
	            psInsert.setString(7, resultFetch.getString("LAST_NAME"));
	            psInsert.setString(8, resultFetch.getString("MIDDLE_INITIAL"));
	            psInsert.setString(9, resultFetch.getString("ADDRESS_LINE_1"));
	            psInsert.setString(10, resultFetch.getString("ADDRESS_LINE_2"));
	            psInsert.setString(11, resultFetch.getString("CITY"));
	            psInsert.setString(12, resultFetch.getString("STATE"));
	            psInsert.setString(13, resultFetch.getString("ZIP_CODE"));
	            psInsert.setString(14, resultFetch.getString("COUNTRY_CODE"));
	            psInsert.setBigDecimal(15, resultFetch.getBigDecimal("PRIMARY_PHONE_NUMBER"));
	            psInsert.setBigDecimal(16, resultFetch.getBigDecimal("SECONDARY_PHONE_NUMBER"));
	            psInsert.setString(17, resultFetch.getString("STATUS"));
	            psInsert.setBigDecimal(18, resultFetch.getBigDecimal("CURR_BAL"));
	            psInsert.setString(19, resultFetch.getString("CURR_BAL_SIGN"));
	            psInsert.setString(20, resultFetch.getString("SUB_PGM_ID_VALUE"));
	            psInsert.setTimestamp(21, resultFetch.getTimestamp("DATE_OF_BIRTH"));
	            psInsert.setString(22, resultFetch.getString("ACH_DDA_ACCOUNT_NUMBER"));
	            psInsert.setString(23, resultFetch.getString("CUSTOMER_ID"));
	            psInsert.setString(24, resultFetch.getString("CIP_ID"));
	            psInsert.setTimestamp(25, resultFetch.getTimestamp("MAIL_DATE"));
	            psInsert.setString(26, resultFetch.getString("RELOADABLE_INDICATOR"));
	            psInsert.setBigDecimal(27, resultFetch.getBigDecimal("AVAILABLE_BALANCE"));
	            psInsert.setString(28, resultFetch.getString("AVAILABLE_BALANCE_SIGN"));
	            psInsert.setString(29, resultFetch.getString("CUSTOMER_EMAIL_ADDRESS"));
	            psInsert.setString(30, resultFetch.getString("IP_ADDRESS_OF_APPLICATION"));
	            psInsert.setString(31, resultFetch.getString("PRIMARY_CARD_INDICATOR"));
	            psInsert.setTimestamp(32, resultFetch.getTimestamp("CARD_ACTIVATION_DATE"));
	            psInsert.setString(33, resultFetch.getString("GROUP_ID"));
	            psInsert.setString(34, resultFetch.getString("EMPLOYMENT_STATUS"));
	            psInsert.setString(35, resultFetch.getString("OCCUPATION"));
	            psInsert.setBigDecimal(36, resultFetch.getBigDecimal("INCOME"));
	            psInsert.setString(37, resultFetch.getString("SIN_CARD_#"));
	            psInsert.setString(38, resultFetch.getString("PRIVACY_FLAG"));
	            psInsert.setTimestamp(39, resultFetch.getTimestamp("PRIVACY_DATE"));
	            psInsert.setTimestamp(40, resultFetch.getTimestamp("PRIVACY_DTS"));
	            psInsert.setString(41, resultFetch.getString("DNS"));
	            psInsert.setString(42, resultFetch.getString("LOYALTY_CUSTOMER_NUMBER"));
	            psInsert.setString(43, resultFetch.getString("ACCEPTANCE_ID_VERIFICATION"));
	            psInsert.setString(44, resultFetch.getString("PC_POINTS_T_AND_C_FLAG"));
	            psInsert.setTimestamp(45, resultFetch.getTimestamp("PC_POINTS_T_AND_C_DATE"));
	            psInsert.setTimestamp(46, resultFetch.getTimestamp("PC_POINTS_T_AND_C_DTS"));
	            psInsert.setString(47, resultFetch.getString("MAINTENANCE_FEE_FLAG"));
	            psInsert.setString(48, resultFetch.getString("MAINTENANCE_FEE_AMOUNT"));
	            psInsert.setTimestamp(49, resultFetch.getTimestamp("MAINTENANCE_FEE_DATE"));
	            psInsert.setTimestamp(50, resultFetch.getTimestamp("MAINTENANCE_FEE_DTS"));
	            psInsert.setString(51, resultFetch.getString("CARDHOLDER_AGGREMENT_FLAG"));
	            psInsert.setTimestamp(52, resultFetch.getTimestamp("CARDHOLDER_AGREE_DATE"));
	            psInsert.setTimestamp(53, resultFetch.getTimestamp("CARDHOLDER_AGGREMENT_DTS"));
	            psInsert.setString(54, resultFetch.getString("ID_VERIFICATION_FLAG"));
	            psInsert.setTimestamp(55, resultFetch.getTimestamp("ID_VERIFICATION_DATE"));
	            psInsert.setTimestamp(56, resultFetch.getTimestamp("ID_VERIFICATION_DTS"));
	            psInsert.setString(57, resultFetch.getString("ID_VERIFICATION_METHOD"));
	            psInsert.setString(58, resultFetch.getString("ID_VERIFICATION_RESULT"));
	            psInsert.setString(59, resultFetch.getString("ELECTRONIC_DISCLOSURE_FLAG"));
	            psInsert.setTimestamp(60, resultFetch.getTimestamp("ELECTRONIC_DISCLOSURE_DATE"));
	            psInsert.setTimestamp(61, resultFetch.getTimestamp("ELECTRONIC_DISCLOSURE_DTS"));
	            psInsert.setString(62, resultFetch.getString("ALERT_T_AND_C_FLAG"));
	            psInsert.setTimestamp(63, resultFetch.getTimestamp("ALERT_T_AND_C_DATE"));
	            psInsert.setTimestamp(64, resultFetch.getTimestamp("ALERT_T_AND_C_DTS"));
	            psInsert.setString(65, resultFetch.getString("SMS_ALERTS"));
	            psInsert.setString(66, resultFetch.getString("EMAIL_ALERTS"));
	            psInsert.setString(67, resultFetch.getString("ALERT_OPT_STATUS"));
	            psInsert.setString(68, resultFetch.getString("MARKETING_PREFERENCES_OPT_STAT"));
	            psInsert.setTimestamp(69, resultFetch.getTimestamp("MARKETING_PREFERENCES_OPT_DATE"));
	            psInsert.setTimestamp(70, resultFetch.getTimestamp("MARKETING_PREFERENCES_OPT_TIME"));
	            psInsert.setString(71, resultFetch.getString("ENTITY"));
	            psInsert.setString(72, resultFetch.getString("CHANNEL"));
	            psInsert.setString(73, resultFetch.getString("VENDOR"));
	            psInsert.setTimestamp(74, resultFetch.getTimestamp("DATE_REISSUE_ACTION"));
	            psInsert.setString(75, resultFetch.getString("PLACEHOLDER1"));
	            psInsert.setString(76, resultFetch.getString("PLACEHOLDER2"));
	            psInsert.setString(77, resultFetch.getString("ACCOUNT_ID"));
	            psInsert.setTimestamp(78, resultFetch.getTimestamp("REC_CREATE_TMS"));
	            psInsert.setTimestamp(79, resultFetch.getTimestamp("REC_CHNG_TMS"));
	            psInsert.setString(80, resultFetch.getString("REC_CHNG_ID"));
	            psInsert.setTimestamp(81, resultFetch.getTimestamp("LAST_TRANSACTION_DATE"));
	            psInsert.setTimestamp(82, resultFetch.getTimestamp("LAST_STATUS_DATE"));

                psInsert.addBatch();
                 
                if(i%batchLoadThreshold == 0) {
                	psInsert.executeBatch();
                    logger.debug("VO56AValidation: insertNfAccntPrevExtendHold: Batch Execution done");
                }
                i++;
			}
            logger.debug("VO56AValidation: insertNfAccntPrevExtendHold: Batch Execution done");
            psInsert.executeBatch();
            logger.debug("VO56AValidation: insertNfAccntPrevExtendHold: Time Taken= "+(System.currentTimeMillis()-start));
             
        } catch (SQLException e) {
            logger.error("VO56AValidation: insertNfAccntPrevExtendHold: Exception has occurred",e);
        }finally{
            try {
                psInsert.close();
                psFetch.close();
                conGPR.close();
                conGPRTemp.close();
            } catch (SQLException e) {
                logger.error("VO56AValidation: insertNfAccntPrevExtendHold: Exception has occurred",e);
            }
        }
		
	}
	
	public int getRowCount(ResultSet result) throws SQLException{
		logger.info("VO56AValidation: getRowCount - Started");
		int rowCount=0;
		rowCount = result.last() ? result.getRow() : 0;
		if (rowCount == 0)
		// If there was no current row
		return rowCount;
		else {
		// If there WAS a current row
			result.beforeFirst();
		}
		logger.info("VO56AValidation: getRowCount - Ended");
		return rowCount;
	}
	
}
