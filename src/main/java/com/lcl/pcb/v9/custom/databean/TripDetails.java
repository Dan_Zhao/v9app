//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.2-147 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2018.05.16 at 09:57:56 PM EDT 
//


package com.lcl.pcb.v9.custom.databean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 * 				2 TripDesc TRIPDESCRIPTION
 * 				3 TripType TRIPTYPE (travel code)
 * 				4 TripGoId   Going ID(used for flight number if travel code is A-Air)
 * 				5 TripGoMERCHANT  Going MERCHANT ID
 * 				6 TripGoCity Going CITY 
 * 				7 TripGoCountry   Going COUNTRY 
 * 				8 TripGoDate   Going DATE and TIME
 * 				9 TripGoConfNum   Going CONFIRMATION NUMBER
 * 				10 TripGoClass  Going CLASS (if travel code is A thin it means seating, T : room booking, H: room booking) 
 * 				11 TripRetId  Return ID(used for flight number if travel code is A-Air)
 * 				12 TripRetMerchant  Return MERCHANT ID
 * 				13 TripRetCity  Return CITY 
 * 				14 TripRetCountry  Return COUNTRY 
 * 				15 TripRetDate  Return DATE and TIME
 * 				16 TripRetConfNum  Return CONFIRMATION NUMBER
 * 				17 TripRetClass  Return CLASS (if travel code is A thin it means seating, T : room booking, H: for hotel rating) 
 * 
 * 
 * 
 * 			
 * 
 * <p>Java class for TripDetails complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TripDetails">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TripDesc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TripType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TripGoId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TripGoMerchant" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TripGoCity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TripGoCountry" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TripGoDate" type="{}DateTimeType" minOccurs="0"/>
 *         &lt;element name="TripGoConfNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TripGoClass" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TripRetId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TripRetMerchant" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TripRetCity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TripRetCountry" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TripRetDate" type="{}DateTimeType" minOccurs="0"/>
 *         &lt;element name="TripRetConfNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TripRetClass" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TripDetails", propOrder = {
    "tripDesc",
    "tripType",
    "tripGoId",
    "tripGoMerchant",
    "tripGoCity",
    "tripGoCountry",
    "tripGoDate",
    "tripGoConfNum",
    "tripGoClass",
    "tripRetId",
    "tripRetMerchant",
    "tripRetCity",
    "tripRetCountry",
    "tripRetDate",
    "tripRetConfNum",
    "tripRetClass"
})
public class TripDetails {

    @XmlElement(name = "TripDesc")
    protected String tripDesc;
    @XmlElement(name = "TripType")
    protected String tripType;
    @XmlElement(name = "TripGoId")
    protected String tripGoId;
    @XmlElement(name = "TripGoMerchant")
    protected String tripGoMerchant;
    @XmlElement(name = "TripGoCity")
    protected String tripGoCity;
    @XmlElement(name = "TripGoCountry")
    protected String tripGoCountry;
    @XmlElement(name = "TripGoDate")
    protected DateTimeType tripGoDate;
    @XmlElement(name = "TripGoConfNum")
    protected String tripGoConfNum;
    @XmlElement(name = "TripGoClass")
    protected String tripGoClass;
    @XmlElement(name = "TripRetId")
    protected String tripRetId;
    @XmlElement(name = "TripRetMerchant")
    protected String tripRetMerchant;
    @XmlElement(name = "TripRetCity")
    protected String tripRetCity;
    @XmlElement(name = "TripRetCountry")
    protected String tripRetCountry;
    @XmlElement(name = "TripRetDate")
    protected DateTimeType tripRetDate;
    @XmlElement(name = "TripRetConfNum")
    protected String tripRetConfNum;
    @XmlElement(name = "TripRetClass")
    protected String tripRetClass;

    /**
     * Gets the value of the tripDesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTripDesc() {
        return tripDesc;
    }

    /**
     * Sets the value of the tripDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTripDesc(String value) {
        this.tripDesc = value;
    }

    /**
     * Gets the value of the tripType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTripType() {
        return tripType;
    }

    /**
     * Sets the value of the tripType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTripType(String value) {
        this.tripType = value;
    }

    /**
     * Gets the value of the tripGoId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTripGoId() {
        return tripGoId;
    }

    /**
     * Sets the value of the tripGoId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTripGoId(String value) {
        this.tripGoId = value;
    }

    /**
     * Gets the value of the tripGoMerchant property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTripGoMerchant() {
        return tripGoMerchant;
    }

    /**
     * Sets the value of the tripGoMerchant property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTripGoMerchant(String value) {
        this.tripGoMerchant = value;
    }

    /**
     * Gets the value of the tripGoCity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTripGoCity() {
        return tripGoCity;
    }

    /**
     * Sets the value of the tripGoCity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTripGoCity(String value) {
        this.tripGoCity = value;
    }

    /**
     * Gets the value of the tripGoCountry property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTripGoCountry() {
        return tripGoCountry;
    }

    /**
     * Sets the value of the tripGoCountry property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTripGoCountry(String value) {
        this.tripGoCountry = value;
    }

    /**
     * Gets the value of the tripGoDate property.
     * 
     * @return
     *     possible object is
     *     {@link DateTimeType }
     *     
     */
    public DateTimeType getTripGoDate() {
        return tripGoDate;
    }

    /**
     * Sets the value of the tripGoDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link DateTimeType }
     *     
     */
    public void setTripGoDate(DateTimeType value) {
        this.tripGoDate = value;
    }

    /**
     * Gets the value of the tripGoConfNum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTripGoConfNum() {
        return tripGoConfNum;
    }

    /**
     * Sets the value of the tripGoConfNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTripGoConfNum(String value) {
        this.tripGoConfNum = value;
    }

    /**
     * Gets the value of the tripGoClass property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTripGoClass() {
        return tripGoClass;
    }

    /**
     * Sets the value of the tripGoClass property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTripGoClass(String value) {
        this.tripGoClass = value;
    }

    /**
     * Gets the value of the tripRetId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTripRetId() {
        return tripRetId;
    }

    /**
     * Sets the value of the tripRetId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTripRetId(String value) {
        this.tripRetId = value;
    }

    /**
     * Gets the value of the tripRetMerchant property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTripRetMerchant() {
        return tripRetMerchant;
    }

    /**
     * Sets the value of the tripRetMerchant property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTripRetMerchant(String value) {
        this.tripRetMerchant = value;
    }

    /**
     * Gets the value of the tripRetCity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTripRetCity() {
        return tripRetCity;
    }

    /**
     * Sets the value of the tripRetCity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTripRetCity(String value) {
        this.tripRetCity = value;
    }

    /**
     * Gets the value of the tripRetCountry property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTripRetCountry() {
        return tripRetCountry;
    }

    /**
     * Sets the value of the tripRetCountry property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTripRetCountry(String value) {
        this.tripRetCountry = value;
    }

    /**
     * Gets the value of the tripRetDate property.
     * 
     * @return
     *     possible object is
     *     {@link DateTimeType }
     *     
     */
    public DateTimeType getTripRetDate() {
        return tripRetDate;
    }

    /**
     * Sets the value of the tripRetDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link DateTimeType }
     *     
     */
    public void setTripRetDate(DateTimeType value) {
        this.tripRetDate = value;
    }

    /**
     * Gets the value of the tripRetConfNum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTripRetConfNum() {
        return tripRetConfNum;
    }

    /**
     * Sets the value of the tripRetConfNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTripRetConfNum(String value) {
        this.tripRetConfNum = value;
    }

    /**
     * Gets the value of the tripRetClass property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTripRetClass() {
        return tripRetClass;
    }

    /**
     * Sets the value of the tripRetClass property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTripRetClass(String value) {
        this.tripRetClass = value;
    }

}
