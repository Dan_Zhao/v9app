package com.lcl.pcb.v9.custom.loader;

import java.util.ArrayList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class VO3HValidation extends DNSVendorFileBaseValidation
{
	Logger logger = null;

	public VO3HValidation()
	{
		super.SourceFileId="VO-3H";
		logger = LogManager.getLogger(VO3HValidation.class.getName());
	}

	public boolean doValidation(ArrayList files)
			throws Exception
			{
		logger.debug("VO3Validation Called");
		return super.doValidation(files);
			}

	public void doFinalize(ArrayList files)
			throws Exception
			{
		logger.debug("VO3Validation doFinalize() Called ");
		super.doFinalize(files);
			}
}
