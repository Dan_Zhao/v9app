package com.lcl.pcb.v9.custom.loader;

import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.lcl.pcb.v9.generic.loader.V9ValidationDaoManager;

public class VO3AValidation {
	boolean headerResult = true;
	boolean trailerResult = true;
	boolean bodyResult = true;
	boolean doValidation = true;
	Logger logger = LogManager.getLogger(VO3AValidation.class.getName());

	public boolean doValidation(ArrayList<String> files) throws Exception {
		logger.debug("VO3AValidation Called");
		doValidation &= doHeader();
		doValidation &= doBody();
		doValidation &= doTrailer();
		return doValidation;
	}

	public boolean doHeader() throws Exception {
		logger.debug("VO3AValidation doHeader() Called");
		return headerResult;
	}

	public boolean doTrailer() {
		logger.debug("VO3AValidation doTrailer() Called");
		 return trailerResult;
	}

	public boolean doBody() {

		logger.info("VO3AValidation doBody() Called");

		String sqldelete = "DELETE FROM HAINS_TEMP.INS_AUTO_POLICY_STG";
		logger.debug("VO3AValidation: doBody - Delete Query for Staging table: "+ sqldelete);
		try {
			V9ValidationDaoManager.execNonQuery(sqldelete);
		} catch (SQLException e) {
			logger.fatal("Error executing query", e);
		}
		logger.debug("VO3AValidation: doBody - Records deleted from Staging table");

		return bodyResult;
	}

	public void doErrorLog() throws Exception {
		logger.debug("VO3AValidation doErrorLog() Called");
	}

	public void doFinalize(ArrayList<String> files) throws Exception {
		logger.debug("VO3AValidation doFinalize() Called");
	}

}
