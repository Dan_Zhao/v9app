package com.lcl.pcb.v9.custom.loader;

public class HO74Pojo {
	private String fileName; 
	private String segmentName;
	private String apaAPPNUM;
	private String errorrecordRow;
	private String errorrecord;
	private StringBuilder errorMsg;
	
	
	public StringBuilder getErrorMsg() {
		return errorMsg;
	}
	public void setErrorMsg(StringBuilder errorMsg) {
		this.errorMsg = errorMsg;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getSegmentName() {
		return segmentName;
	}
	public void setSegmentName(String segmentName) {
		this.segmentName = segmentName;
	}
	public String getApaAPPNUM() {
		return apaAPPNUM;
	}
	public void setApaAPPNUM(String apaAPPNUM) {
		this.apaAPPNUM = apaAPPNUM;
	}
	public String getErrorrecordRow() {
		return errorrecordRow;
	}
	public void setErrorrecordRow(String errorrecordRow) {
		this.errorrecordRow = errorrecordRow;
	}
	public String getErrorrecord() {
		return errorrecord;
	}
	public void setErrorrecord(String errorrecord) {
		this.errorrecord = errorrecord;
	}
	
}
