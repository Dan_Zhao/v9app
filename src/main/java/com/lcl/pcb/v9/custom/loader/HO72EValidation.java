package com.lcl.pcb.v9.custom.loader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.lcl.pcb.v9.dao.V9DAOManager;
import com.lcl.pcb.v9.generic.loader.LoaderUtil;
import com.lcl.pcb.v9.generic.loader.V9ValidationDaoManager;
import com.lcl.pcb.v9.idf.jobflow.config.IDFConfig;
import com.lcl.pcb.v9.utilities.fileReader.FileUtil;

/**
 * This class is used to validate the csv files generated for HO72E interface.
 * It updates SOURCE_END_DT_PREV and SOURCE_END_DT_CURR and SOURCE_FILE_ID
 * values with correct Values/format.
 * 
 */
public class HO72EValidation {

	ArrayList<String> csvFilesList = new ArrayList<String>();
	ArrayList<String> errorLogList = new ArrayList<String>();
	public String headerCsvFile = "";
	BufferedReader br;
	String line = "";
	String endDatePrevVal = "";
	String endDateCurVal = "";
	String seqVal = "";
	String vendorId = "";
	String fileId = "";
	String SourceFileId = "";
	String endDatePrev = "";
	String endDateCur = "";
	boolean headerResult = true;
	boolean trailerResult = true;
	boolean bodyResult = true;
	boolean doValidation = true;
	String[] headerVal = null;
	String scoreLogExtractDate = "";
	String dateFormat = "YYYYMMDD";
	String dataFileLocation = "";
	String dataFileName = "";
	ArrayList<HO72EPojo> errorObjectList = null;
	String logFileName = "";
	String logFileNameVal ="";
	String[] logFileNamePattern ;
	String badFileName = "";
	BufferedReader badrecordreader;
	ArrayList<String> badRecordList = null;
	String line1= "";
	String errorRecord = "Record";
	StringBuilder errorMsg=null;
	String errorRecordRow = "";
	String errorrecord ="";
	/*String[] apaAppNum;
	String apaAppNumVal ="";*/
	
	HashMap<String, Integer> totRecParsed = new HashMap<String, Integer>();
	HashMap<String, Integer> trailerRecList = new HashMap<String, Integer>();

	V9DAOManager db = new V9DAOManager();
	Logger logger = LogManager.getLogger(HO72EValidation.class.getName());
	V9ValidationDaoManager v9Validation = new V9ValidationDaoManager();
	public String trailerCsvFile = "";
	int totalRecTrailerCount = 0;
	int totalRecCount = 0;

	/**
	 * This method will be invoked from V9Loader class which expects the csv
	 * files list for validating header, body and trailer.
	 * 
	 * @param files
	 *            This is the parameter of CSV files list
	 * @return boolean This returns true/false value.
	 */
	public boolean doValidation(ArrayList<String> files) throws Exception {
		logger.debug("HO72EValidation Called");
		this.csvFilesList = files;
		doValidation &= doHeader();
		doValidation &= doTrailer();
		doValidation &= doBody();
		return doValidation;
	}

	public boolean doHeader() throws Exception {
		logger.debug("HO72EValidation doHeader() Called");
		for (int i = 0; i < csvFilesList.size(); i++) {
			if (csvFilesList.get(i).contains("HEDR")) {
				headerCsvFile = csvFilesList.get(i);
				try {
					File f = new File(headerCsvFile);
					if(!(f.exists())){
						  logger.error("There is no Trailer Record/Type. Please check the Data file.");
						  db.setLevelWarning(3,"There is no Trailer Record/Type. Please check the Data file.",0);
				    }
					br = new BufferedReader(new FileReader(headerCsvFile));

					try {
						while ((line = br.readLine()) != null) {
							String[] headerVal = line.split("\\|");
							if (headerVal[0].equalsIgnoreCase("HEDR")
									&& headerVal.length > 3) {
								scoreLogExtractDate = headerVal[3];
								logger.debug("SCORES_VALID_EXTR_DT in HEADER CSV File:"
										+ scoreLogExtractDate);
								v9Validation.getInstance();
								String sql = "SELECT TO_CHAR(SCORES_VALID_EXTR_DT, '"
										+ dateFormat
										+ "') FROM SCORES_VAL_HEAD_TRAIL where TO_CHAR(SCORES_VALID_EXTR_DT, '"
										+ dateFormat + "')='" + scoreLogExtractDate
										+ "'";
								try {
									v9Validation.open();
									ResultSet res = V9ValidationDaoManager
											.execQuery(sql);
									if (res.next()) {

										logger.error("File already loaded for the date < SCORES_VALID_EXTR_DT>:"
												+ scoreLogExtractDate
												+ " "
												+ "To reload the file, delete all the existing records for this date from all the ADM HO72E tables first and then restart the job.");
										db.setLevelWarning(4,"File already loaded for the date < SCORES_VALID_EXTR_DT>:"
												+ scoreLogExtractDate
												+ " "
												+ "To reload the file, delete all the existing records for this date from all the ADM HO72E tables first and then restart the job.",0);
										headerResult = false;
									} else {
										logger.debug("Header Date Validation Passed:A File with new SCORES_VALID_EXTR_DT Date Found");
									}
									v9Validation.close();
								} catch (SQLException e) {
									throw e;
								} finally {
									v9Validation.con.close();
								}
							}

						}
						br.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				} catch (FileNotFoundException ex) {
					ex.printStackTrace();
				}
			}
		}
		return headerResult;
	}

	public boolean doTrailer() {
		logger.debug("HO72EValidation doTrailer() Called");
		totRecParsed = FileUtil.totalRecParsedList;
		for (int i = 0; i < csvFilesList.size(); i++) {
			if (csvFilesList.get(i).contains("TRAILER")) {
				trailerCsvFile = csvFilesList.get(i);
				try {
					br = new BufferedReader(new FileReader(trailerCsvFile));

					try {
						while ((line = br.readLine()) != null) {
							String[] trailerVal = line.split("\\|");
							if (trailerVal[4].equalsIgnoreCase("TRLR")
									&& trailerVal.length > 4) {
								trailerRecList.put("SCOR",
										Integer.parseInt(trailerVal[7]));
								trailerRecList.put("SCRD",
										Integer.parseInt(trailerVal[9]));
								trailerRecList.put("TRAILER", 1);
								
							}
						}
						br.close();
						
					} catch (IOException e) {
						e.printStackTrace();
					}
				} catch (FileNotFoundException ex) {
					ex.printStackTrace();
				}
			}
		}
		for (Entry<String, Integer> entry : trailerRecList.entrySet()) {
			totalRecTrailerCount += (entry.getValue());
		}

		for (Entry<String, Integer> entry : totRecParsed.entrySet()) {
			totalRecCount += (entry.getValue());
		}

		if (totRecParsed.keySet().equals(trailerRecList.keySet())) {
			if (totalRecCount == totalRecTrailerCount) {
				logger.debug("Total Record Count in File matches with Total Record Count in TRAILER.");
			} else {
				logger.error("Total Record Count in File Mismatches with Record Count in TRAILER.");
				trailerResult = false;
			}

		} else {
			logger.error("HO72E Record Segments"+totRecParsed.keySet()+" doesn't match/not available in the Processed Data file.");
			trailerResult = false;
		}
		return trailerResult;
	}


	public boolean doBody() throws Exception {
		logger.debug("HO72EValidation doBody() Called");
		return bodyResult;
	}

	/*public void doErrorLog() throws Exception {
		logger.debug("HO72EValidation doerror() Called");

	}*/
	
	public void doErrorLog() throws Exception{
		//logger.debug("HO72AValidation doerror() Called");
		errorLogList=LoaderUtil.ErrorlogFileNamesList;
		dataFileLocation=IDFConfig.getInstance().getIdfData().getDatafileDatafileLocation();
		dataFileName=dataFileLocation.substring(dataFileLocation.lastIndexOf("/") + 1);
		if (null != errorLogList && errorLogList.size() != 0) {
			for (int i = 0; i < errorLogList.size(); i++) {
				errorObjectList=new ArrayList<HO72EPojo>();
				logFileName = errorLogList.get(i);
				logFileNameVal=logFileName.substring(logFileName.lastIndexOf("/")+1, logFileName.lastIndexOf("."));
				logFileNamePattern=logFileNameVal.split("_");
				badFileName =IDFConfig.getInstance().getIdfData().getDatafileBadFileLocation()
				+ "/"+ logFileNamePattern[0]+"_"+logFileNamePattern[2]+"_"+logFileNamePattern[3]+"_"+logFileNamePattern[1]+ IDFConfig.getInstance().getIdfData().getDatafileBadNamePattern();
				//logName = logFileName.substring(logFileName.lastIndexOf("/") + 1);
				//segmentPart = logName.split("_");
				try{
					badrecordreader = new BufferedReader(new FileReader(badFileName));
					badRecordList=new ArrayList();
					try{
						while((line1 = badrecordreader.readLine())!= null ){
							badRecordList.add(line1);
						}
					}catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}catch(FileNotFoundException ex){
					ex.printStackTrace();
				}
				try {
					br = new BufferedReader(new FileReader(logFileName));
					int count=0;
					try {
						while ((line = br.readLine()) != null) {
							
							
							if (line.contains(errorRecord)) {
								HO72EPojo errorObject = new HO72EPojo();
								errorObject.setFileName(dataFileName);
								errorObject.setSegmentName(logFileNamePattern[3]);
								errorMsg=new StringBuilder();
								errorMsg.append(line);
								errorRecordRow=line.substring(line.indexOf(" "),
										line.lastIndexOf(":"));
								line=br.readLine();
								errorMsg.append(line);
								errorObject.setErrorrecordRow(errorRecordRow);
								errorObject.setErrorMsg(errorMsg);
								errorrecord=badRecordList.get(count);
								if(errorrecord.length()<= 4000){
									errorObject.setErrorrecord(errorrecord);
								}else{
									errorObject.setErrorrecord(errorrecord.substring(0, 3999));
									logger.info("Error Record length is more than 4000 characters: So inserting first 4000 characters of error record");
								}
								/*apaAppNum=errorrecord.split("\\|");
								if(apaAppNum.length>1){
									apaAppNumVal=apaAppNum[1];
								}
								errorObject.setApaAPPNUM(apaAppNumVal);*/
								count ++;
								errorObjectList.add(errorObject);
								errorObject=null;
								errorMsg=null;
								
							}
						}
						badRecordList=null;
						br.close();
						if (errorObjectList != null && errorObjectList.size() > 0) {
							V9ValidationDaoManager.getInstance();
							v9Validation.open();
							insertIntoErrorTable(errorObjectList);
						}
						errorObjectList=null;
					}catch (IOException e) {
						e.printStackTrace();
					}
				}catch(FileNotFoundException ex){
					ex.printStackTrace();
				}
				}
			v9Validation.close();
			}
	}
	
	public void insertIntoErrorTable(ArrayList<HO72EPojo> errorRecords)
	throws Exception {
		HO72EPojo errObject;
		String LoadResultUUID = "";
		
		logger.info("Start of Inserting Error Records in ADM BATCH Error Table");
		
		for (int i=0;i<errorRecords.size();i++){
			errObject=(HO72EPojo)errorRecords.get(i);
			int sequenceNum=IDFConfig.getInstance().getSequenceNum();
			
			StringBuilder sb = new StringBuilder("");
			sb.append("SELECT LOAD_RESULT_UUID FROM LOAD_RESULT WHERE ");
			sb.append("PROCESS_NUM = ");
			sb.append(sequenceNum);
			ResultSet rs = V9ValidationDaoManager.execQuery(sb.toString());
			if (rs.next())
				LoadResultUUID=rs.getString("LOAD_RESULT_UUID");
			else
				logger.debug("UUID not found");
						
			//String insertSql="insert into ADM_STD_DISP_ERROR(FILENAME,SEGMENT_NAME,APA_APP_NUM,ERROR_RECORD_ROW,ERROR_RECORD,ERROR_MSG,REC_LOAD_TMS,REC_CHNG_TMS,REC_CREATE_TMS,REC_CHANGE_TMS,V9SEQ,LOAD_RESULT_UUID) values('"+errObject.getFileName()+"','"+errObject.getSegmentName()+"','"+errObject.getApaAPPNUM()+"','"+errObject.getErrorrecordRow()+"','"+errObject.getErrorrecord()+"','"+errObject.getErrorMsg()+"',sysdate,sysdate,sysdate,sysdate,"+sequenceNum+",'"+LoadResultUUID+"')";
			//INTERFACE
			//System.out.println("Insert SQl:"+insertSql.toString());
			String insertSql="insert into ADM_BATCH_ERROR_LOG(INTERFACE,FILENAME,SEGMENT_NAME,ERROR_RECORD_ROW,ERROR_RECORD,ERROR_MSG,REC_LOAD_TMS,REC_CHNG_TMS,REC_CREATE_TMS,REC_CHANGE_TMS,V9SEQ,LOAD_RESULT_UUID) values('111','"+errObject.getFileName()+"','"+errObject.getSegmentName()+"','"+errObject.getErrorrecordRow()+"','"+errObject.getErrorrecord()+"','"+errObject.getErrorMsg()+"',sysdate,sysdate,sysdate,sysdate,"+sequenceNum+",'"+LoadResultUUID+"')";
			int res = V9ValidationDaoManager
					.execNonQuery(insertSql);
			rs = null;
			sb = null;
			
		}
		logger.info("Error Records Inserted in ADM BATCH Error Table");
		
	}

	/**
	 * This method is used for updating SOURCE_END_DT_PREV,SOURCE_END_DT_CURR
	 * and SOURCE_FILE_ID values with correct Values/format.
	 * 
	 * @param files
	 *            This is the parameter of CSV files list
	 * @return boolean This returns true/false value.
	 */
	public void doFinalize(ArrayList<String> files) throws Exception {

		logger.debug("HO72EValidation doFinalize() Called ");
		for (int i = 0; i < files.size(); i++) {
			if (files.get(i).contains("HEADER")) {
				headerCsvFile = files.get(i);
				logger.debug("headerCsvFile: " + headerCsvFile);
				try {
					br = new BufferedReader(new FileReader(headerCsvFile));
					while ((line = br.readLine()) != null) {
						String[] headerVal = line.split("\\|");
						if (headerVal[0].equalsIgnoreCase("HEDR")
								&& headerVal.length > 3) {
							scoreLogExtractDate = headerVal[3];
							seqVal = headerVal[10];
							SourceFileId = "TSYS";
							StringBuilder sb = new StringBuilder("");
							sb.append("UPDATE LOAD_RESULT SET SOURCE_END_DT_PREV = ");
							sb.append("to_timestamp('" + scoreLogExtractDate
									+ "','yyyymmdd')");
							sb.append(",");
							sb.append("SOURCE_END_DT_CURR = ");
							sb.append("to_timestamp('" + scoreLogExtractDate
									+ "','yyyymmdd')");
							sb.append(",");
							sb.append("SOURCE_ID = '");
							sb.append(SourceFileId);
							sb.append("'");
							sb.append(" WHERE PROCESS_NUM = '");
							sb.append(seqVal);
							sb.append("'");
							V9ValidationDaoManager.execNonQuery(sb
									.toString());
							sb = null;

						}
					}
				} catch (Exception ex) {
					logger.error("Error in HO72E inerface Finalize method.");
					ex.printStackTrace();
				}
			}
		}

	}

}
