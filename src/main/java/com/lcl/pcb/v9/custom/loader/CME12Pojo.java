/**
 * 
 */
package com.lcl.pcb.v9.custom.loader;

import java.sql.Timestamp;

/**
 * @author Dev POJO for CME12 Interface (CME Kills for CLI)
 * 
 */
public class CME12Pojo {

	private String indexVal;
	
	private String status;

	public String campaignType;

	public String statusInd;

	public String dispositionStatus;

	public String offerId;

	public String lastOfferstatus;

	public Timestamp lastOfferStatusDate;

	public String customerRef;

	public String tsysAccountId;
	
	public String businessRuleId;
	
	public String businessRuleValue;
	
	public String businesssId1Value;
	
	public String businesssId2Value;
	
	public String businesssId3Value;
	
	public String businesssId4Value;
	
	public String businesssId5Value;
	
	public String businesssId6Value;
	
	public String businesssId7Value;
	
	public String businesssId8Value;
	
	public String businesssId9Value;
	
	public String businesssId10Value;
	
	public String businesssId11Value;
	
	public String businesssId12Value;
	
	public String businesssId13Value;
	
	public String businesssId14Value;
	
	public String businesssId15Value;
	
	public String businesssId16Value;
	
	public String getBusinessRuleId() {
		return businessRuleId;
	}

	public void setBusinessRuleId(String businessRuleId) {
		this.businessRuleId = businessRuleId;
	}

	public String getBusinessRuleValue() {
		return businessRuleValue;
	}

	public void setBusinessRuleValue(String businessRuleValue) {
		this.businessRuleValue = businessRuleValue;
	}

	public String getCampaignType() {
		return campaignType;
	}

	public void setCampaignType(String campaignType) {
		this.campaignType = campaignType;
	}

	public String getStatusInd() {
		return statusInd;
	}

	public void setStatusInd(String statusInd) {
		this.statusInd = statusInd;
	}

	public String getDispositionStatus() {
		return dispositionStatus;
	}

	public void setDispositionStatus(String dispositionStatus) {
		this.dispositionStatus = dispositionStatus;
	}

	public String getOfferId() {
		return offerId;
	}

	public void setOfferId(String offerId) {
		this.offerId = offerId;
	}

	public String getLastOfferstatus() {
		return lastOfferstatus;
	}

	public void setLastOfferstatus(String lastOfferstatus) {
		this.lastOfferstatus = lastOfferstatus;
	}

	public String getCustomerRef() {
		return customerRef;
	}

	public void setCustomerRef(String customerRef) {
		this.customerRef = customerRef;
	}

	public String getTsysAccountId() {
		return tsysAccountId;
	}

	public void setTsysAccountId(String tsysAccountId) {
		this.tsysAccountId = tsysAccountId;
	}

	public Timestamp getLastOfferStatusDate() {
		return lastOfferStatusDate;
	}

	public void setLastOfferStatusDate(Timestamp lastOfferStatusDate) {
		this.lastOfferStatusDate = lastOfferStatusDate;
	}
	
	public String getIndexVal() {
		return indexVal;
	}

	public void setIndexVal(String indexVal) {
		this.indexVal = indexVal;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getBusinesssId1Value() {
		return businesssId1Value;
	}

	public void setBusinesssId1Value(String businesssId1Value) {
		this.businesssId1Value = businesssId1Value;
	}

	public String getBusinesssId2Value() {
		return businesssId2Value;
	}

	public void setBusinesssId2Value(String businesssId2Value) {
		this.businesssId2Value = businesssId2Value;
	}

	public String getBusinesssId3Value() {
		return businesssId3Value;
	}

	public void setBusinesssId3Value(String businesssId3Value) {
		this.businesssId3Value = businesssId3Value;
	}

	public String getBusinesssId4Value() {
		return businesssId4Value;
	}

	public void setBusinesssId4Value(String businesssId4Value) {
		this.businesssId4Value = businesssId4Value;
	}

	public String getBusinesssId5Value() {
		return businesssId5Value;
	}

	public void setBusinesssId5Value(String businesssId5Value) {
		this.businesssId5Value = businesssId5Value;
	}

	public String getBusinesssId6Value() {
		return businesssId6Value;
	}

	public void setBusinesssId6Value(String businesssId6Value) {
		this.businesssId6Value = businesssId6Value;
	}

	public String getBusinesssId7Value() {
		return businesssId7Value;
	}

	public void setBusinesssId7Value(String businesssId7Value) {
		this.businesssId7Value = businesssId7Value;
	}

	public String getBusinesssId8Value() {
		return businesssId8Value;
	}

	public void setBusinesssId8Value(String businesssId8Value) {
		this.businesssId8Value = businesssId8Value;
	}

	public String getBusinesssId9Value() {
		return businesssId9Value;
	}

	public void setBusinesssId9Value(String businesssId9Value) {
		this.businesssId9Value = businesssId9Value;
	}

	public String getBusinesssId10Value() {
		return businesssId10Value;
	}

	public void setBusinesssId10Value(String businesssId10Value) {
		this.businesssId10Value = businesssId10Value;
	}

	public String getBusinesssId11Value() {
		return businesssId11Value;
	}

	public void setBusinesssId11Value(String businesssId11Value) {
		this.businesssId11Value = businesssId11Value;
	}

	public String getBusinesssId12Value() {
		return businesssId12Value;
	}

	public void setBusinesssId12Value(String businesssId12Value) {
		this.businesssId12Value = businesssId12Value;
	}

	public String getBusinesssId13Value() {
		return businesssId13Value;
	}

	public void setBusinesssId13Value(String businesssId13Value) {
		this.businesssId13Value = businesssId13Value;
	}

	public String getBusinesssId14Value() {
		return businesssId14Value;
	}

	public void setBusinesssId14Value(String businesssId14Value) {
		this.businesssId14Value = businesssId14Value;
	}

	public String getBusinesssId15Value() {
		return businesssId15Value;
	}

	public void setBusinesssId15Value(String businesssId15Value) {
		this.businesssId15Value = businesssId15Value;
	}

	public String getBusinesssId16Value() {
		return businesssId16Value;
	}

	public void setBusinesssId16Value(String businesssId16Value) {
		this.businesssId16Value = businesssId16Value;
	}

}
