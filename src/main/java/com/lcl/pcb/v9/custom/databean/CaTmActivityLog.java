/**
 * 
 */
package com.lcl.pcb.v9.custom.databean;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;

/**
 * @author Dev
 *
 */
public class CaTmActivityLog {
	private String adminId;
	private String firstNm;
	private String middleNm;
	private String lastNm;
	private String desc;
	private String action;
	private Timestamp dtAccessed;
	private String dtl;
	private BigDecimal processNum;
	private String fileName;
	private BigDecimal recNum;
	private String loadResultUUID;
	
	/**
	 * @return the action
	 */
	public String getAction() {
		return action;
	}
	/**
	 * @param action the action to set
	 */
	public void setAction(String action) {
		this.action = action;
	}
	/**
	 * @return the adminId
	 */
	public String getAdminId() {
		return adminId;
	}
	/**
	 * @param adminId the adminId to set
	 */
	public void setAdminId(String adminId) {
		this.adminId = adminId;
	}
	/**
	 * @return the firstNm
	 */
	public String getFirstNm() {
		return firstNm;
	}
	/**
	 * @param firstNm the firstNm to set
	 */
	public void setFirstNm(String firstNm) {
		this.firstNm = firstNm;
	}
	/**
	 * @return the middleNm
	 */
	public String getMiddleNm() {
		return middleNm;
	}
	/**
	 * @param middleNm the middleNm to set
	 */
	public void setMiddleNm(String middleNm) {
		this.middleNm = middleNm;
	}
	/**
	 * @return the lastNm
	 */
	public String getLastNm() {
		return lastNm;
	}
	/**
	 * @param lastNm the lastNm to set
	 */
	public void setLastNm(String lastNm) {
		this.lastNm = lastNm;
	}
	/**
	 * @return the desc
	 */
	public String getDesc() {
		return desc;
	}
	/**
	 * @param desc the desc to set
	 */
	public void setDesc(String desc) {
		this.desc = desc;
	}
	/**
	 * @return the dtAccessed
	 */
	public Date getDtAccessed() {
		return dtAccessed;
	}
	/**
	 * @param dtAccessed the dtAccessed to set
	 */
	public void setDtAccessed(Timestamp dtAccessed) {
		this.dtAccessed = dtAccessed;
	}
	/**
	 * @return the dtl
	 */
	public String getDtl() {
		return dtl;
	}
	/**
	 * @param dtl the dtl to set
	 */
	public void setDtl(String dtl) {
		this.dtl = dtl;
	}
	/**
	 * @return the processNum
	 */
	public BigDecimal getProcessNum() {
		return processNum;
	}
	/**
	 * @param processNum the processNum to set
	 */
	public void setProcessNum(BigDecimal processNum) {
		this.processNum = processNum;
	}
	/**
	 * @return the fileName
	 */
	public String getFileName() {
		return fileName;
	}
	/**
	 * @param fileName the fileName to set
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	/**
	 * @return the recNum
	 */
	public BigDecimal getRecNum() {
		return recNum;
	}
	/**
	 * @param recNum the recNum to set
	 */
	public void setRecNum(BigDecimal recNum) {
		this.recNum = recNum;
	}
	/**
	 * @return the loadResultUUID
	 */
	public String getLoadResultUUID() {
		return loadResultUUID;
	}
	/**
	 * @param loadResultUUID the loadResultUUID to set
	 */
	public void setLoadResultUUID(String loadResultUUID) {
		this.loadResultUUID = loadResultUUID;
	}
	
	
}
