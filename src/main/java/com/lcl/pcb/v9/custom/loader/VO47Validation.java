package com.lcl.pcb.v9.custom.loader;

import java.util.ArrayList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class VO47Validation extends DNSVendorFileBaseValidation
{
	Logger logger = null;

	public VO47Validation()
	{
		super.SourceFileId="V0-47";
		logger = LogManager.getLogger(VO47Validation.class.getName());
	}

	public boolean doValidation(ArrayList files)
			throws Exception
			{
		logger.debug("VO47Validation Called");
		return super.doValidation(files);
			}

	public void doFinalize(ArrayList files)
			throws Exception
			{
		logger.debug("VO47Validation doFinalize() Called ");
		super.doFinalize(files);
			}
}
