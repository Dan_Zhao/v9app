package com.lcl.pcb.v9.custom.loader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.lcl.pcb.v9.dao.V9DAOManager;
import com.lcl.pcb.v9.generic.loader.V9ValidationDaoManager;
import com.lcl.pcb.v9.utilities.fileReader.FileUtil;

/**
 * This class is used to validate the csv files generated for VO10C interface.
 * It updates SOURCE_END_DT_PREV and SOURCE_END_DT_CURR and SOURCE_FILE_ID
 * values with correct Values/format.
 */

public class VO10CValidation {

	ArrayList<String> csvFilesList = new ArrayList<String>();
	public String headerCsvFile = "";
	BufferedReader br;
	String line = "";
	String endDatePrevVal = "";
	String endDateCurVal = "";
	String seqVal = "";
	String vendorId = "";
	String fileId = "";
	String SourceFileId = "";
	String endDatePrev = "";
	String endDateCur = "";
	boolean headerResult = true;
	boolean trailerResult = true;
	boolean bodyResult = true;
	boolean doValidation = true;
	String[] headerVal = null;
	String fileEndDateCurr = "";
	String dateFormat = "YYYY-MM-DD HH24:MI:SS";
	HashMap<String, Integer> totRecParsed = new HashMap<String, Integer>();
	HashMap<String, Integer> trailerRecList = new HashMap<String, Integer>();
	public String trailerCsvFile = "";
	int totalRecTrailerCount = 0;
	int totalRecCount = 0;

	V9DAOManager db = new V9DAOManager();
	Logger logger = LogManager.getLogger(VO10CValidation.class.getName());
	V9ValidationDaoManager v9Validation = new V9ValidationDaoManager();

	/**
	 * This method will be invoked from V9Loader class which expects the csv
	 * files list for validating header, body and trailer.
	 * 
	 * @param files
	 *            This is the parameter of CSV files list
	 * @return boolean This returns true/false value.
	 */
	public boolean doValidation(ArrayList<String> files) throws Exception {
		logger.debug("VO10CValidation Called");
		this.csvFilesList = files;
		doValidation &= doHeader();
		doValidation &= doTrailer();
		doValidation &= doBody();
		return doValidation;
	}

	public boolean doHeader() throws Exception {
		logger.debug("VO10CValidation doHeader() Called");
		for (int i = 0; i < csvFilesList.size(); i++) {
			if (csvFilesList.get(i).contains("HEADER")) {
				headerCsvFile = csvFilesList.get(i);
				try {
					File f = new File(headerCsvFile);
					if(!(f.exists())){
						  logger.error("There is no Trailer Record/Type. Please check the Data file.");
						  db.setLevelWarning(3,"There is no Trailer Record/Type. Please check the Data file.",0);
				    }
					br = new BufferedReader(new FileReader(headerCsvFile));

					try {
						while ((line = br.readLine()) != null) {
							String[] headerVal = line.split("\\|");
							if (headerVal[0].equalsIgnoreCase("HEADER")
									&& headerVal.length > 5) {
								fileEndDateCurr = headerVal[2];
								String endDateCur1 = fileEndDateCurr.replace("T", " ");
								endDateCur = endDateCur1.substring(0, 19);
								logger.debug("FileEndDateCurr in HEADER CSV File:"
										+ endDateCur);
								v9Validation.getInstance();
								String sql = "SELECT TO_CHAR(File_End_Dtz_Curr, '"
										+ dateFormat
										+ "') FROM AGNT_RPT_DL_HEAD_TRAIL where TO_CHAR(File_End_Dtz_Curr, '"
										+ dateFormat + "')='" + endDateCur
										+ "'";
								try {
									v9Validation.open();
									ResultSet res = V9ValidationDaoManager
											.execQuery(sql);
									if (res.next()) {

										logger.error("File already loaded for the date < File_End_Dtz_Curr>:"
												+ endDateCur
												+ " "
												+ "To reload the file, delete all the existing records for this date from all the FINSCAN VO10C tables first and then restart the job.");
										db.setLevelWarning(4,"File already loaded for the date < File_End_Dtz_Curr>:"
												+ endDateCur
												+ " "
												+ "To reload the file, delete all the existing records for this date from all the FINSCAN VO10C tables first and then restart the job.",0);
										headerResult = false;
									} else {
										logger.debug("Header Date Validation Passed:A File with new fileEndDateCurr Date Found");
									}
									v9Validation.close();
								} catch (SQLException e) {
									throw e;
								} finally {
									v9Validation.con.close();
								}
							}
							
						}
						br.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				} catch (FileNotFoundException ex) {
					ex.printStackTrace();
				}
			}
			
		}
		return headerResult;
	}

	public boolean doTrailer() {
		logger.debug("VO10CValidation doTrailer() Called");
		totRecParsed = FileUtil.totalRecParsedList;
		for (int i = 0; i < csvFilesList.size(); i++) {
			if (csvFilesList.get(i).contains("TRAILER")) {
				trailerCsvFile = csvFilesList.get(i);
				try {
					br = new BufferedReader(new FileReader(trailerCsvFile));

					try {
						while ((line = br.readLine()) != null) {
							String[] trailerVal = line.split("\\|");
							if (trailerVal[5].equalsIgnoreCase("TRAILER")
									&& trailerVal.length > 6) {
								trailerRecList.put("PCCIF",
										Integer.parseInt(trailerVal[6]));
								trailerRecList.put("TRAILER", 1);
								
							}
						}
						br.close();
						
					} catch (IOException e) {
						e.printStackTrace();
					}
				} catch (FileNotFoundException ex) {
					ex.printStackTrace();
				}
			}
		}
		for (Entry<String, Integer> entry : trailerRecList.entrySet()) {
			totalRecTrailerCount += (entry.getValue());
			System.out.println("totalRecTrailerCount: "+totalRecTrailerCount);
		}

		for (Entry<String, Integer> entry : totRecParsed.entrySet()) {
			totalRecCount += (entry.getValue());
			System.out.println("totalRecCount: "+totalRecCount);
		}

		if (totRecParsed.keySet().equals(trailerRecList.keySet())) {
			if (totalRecCount == totalRecTrailerCount) {
				logger.debug("Total Record Count in File matches with Total Record Count in TRAILER.");
			} else {
				logger.error("Total Record Count in File Mismatches with Record Count in TRAILER.");
				trailerResult = false;
			}

		} else {
			logger.error("VO10C Record Segments"+totRecParsed.keySet()+" doesn't match/not available in the Processed Data file.");
			trailerResult = false;
		}
		return trailerResult;
	}

	public boolean doBody() throws Exception {
		logger.debug("VO10CValidation doBody() Called");
		return bodyResult;
	}

	public void doErrorLog() throws Exception {
		//logger.debug("VO10CValidation doerror() Called");

	}

	/**
	 * This method is used for updating SOURCE_END_DT_PREV,SOURCE_END_DT_CURR
	 * and SOURCE_FILE_ID values with correct Values/format.
	 * 
	 * @param files
	 *            This is the parameter of CSV files list
	 * @return boolean This returns true/false value.
	 */
	public void doFinalize(ArrayList<String> files) throws Exception {

		logger.debug("VO10CValidation doFinalize() Called ");
		for (int i = 0; i < files.size(); i++) {
			if (files.get(i).contains("HEADER")) {
				headerCsvFile = files.get(i);
				logger.debug("headerCsvFile: " + headerCsvFile);
				try {
					br = new BufferedReader(new FileReader(headerCsvFile));

					try {
						while ((line = br.readLine()) != null) {
							headerVal = line.split("\\|");
							if (headerVal[0].equalsIgnoreCase("HEADER")
									&& headerVal.length > 5) {
								endDatePrevVal = headerVal[1];
								endDateCurVal = headerVal[2];
								seqVal = headerVal[9];
								vendorId = headerVal[7];
								//fileId = headerVal[8];
								SourceFileId = vendorId;
								endDatePrev = endDatePrevVal.replace("T", " ");
								endDateCur = endDateCurVal.replace("T", " ");

								StringBuilder sb = new StringBuilder("");
								sb.append("UPDATE LOAD_RESULT SET SOURCE_END_DT_PREV = ");
								sb.append("timestamp '" + endDatePrev
										+ "'");
								sb.append(",");
								sb.append("SOURCE_END_DT_CURR = ");
								sb.append("timestamp '" + endDateCur
										+ "'");
								sb.append(",");
								sb.append("SOURCE_ID = '");
								sb.append(SourceFileId);
								sb.append("'");
								sb.append(" WHERE PROCESS_NUM = '");
								sb.append(seqVal);
								sb.append("'");
								V9ValidationDaoManager.execNonQuery(sb
										.toString());
								sb = null;

								StringBuilder sb1 = new StringBuilder("");
								sb1.append("UPDATE AGNT_RPT_DL_HEAD_TRAIL SET File_End_Dtz_Prev = ");
								sb1.append("timestamp '" + endDatePrev
										+ "'");
								sb1.append(",");
								sb1.append("File_End_Dtz_Curr = ");
								sb1.append("timestamp '" + endDateCur
										+ "'");
								sb1.append(" WHERE V9SEQ = '");
								sb1.append(seqVal);
								sb1.append("'");
								V9ValidationDaoManager.execNonQuery(sb1
										.toString());
								sb1 = null;

							}
						}
					} catch (IOException e) {
						logger.error("Error in VO10C inerface Finalize method.");
						e.printStackTrace();
					}
				} catch (FileNotFoundException ex) {
					ex.printStackTrace();
				}
			}
		}

	}

}
