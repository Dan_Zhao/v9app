package com.lcl.pcb.v9.custom.loader;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.lcl.pcb.v9.generic.loader.V9ValidationDaoManager;
import com.lcl.pcb.v9.idf.jobflow.config.IDFConfig;

public class II_PCMCValidation {
	boolean headerResult = true;
	boolean trailerResult = true;
	boolean bodyResult = true;
	boolean doValidation = true;
	Logger logger = LogManager.getLogger(HO70Validation.class.getName());

	public boolean doValidation(ArrayList<String> files) throws Exception {
		logger.debug("II_PCMCValidation Called");
		return doValidation;
	}

	public boolean doHeader() throws Exception {
		logger.debug("II_PCMCValidation doHeader() Called");
		return headerResult;
	}

	public boolean doTrailer() {
		logger.debug("II_PCMCValidation doTrailer() Called");
		return trailerResult;
	}

	public boolean doBody() {
		logger.debug("II_PCMCValidation doBody() Called");
		return bodyResult;
	}

	public void doErrorLog() throws Exception {

	}

	public void doFinalize(ArrayList<String> files) throws Exception {
		logger.debug("II_PCMCValidation doFinalize() Called ");
		
		String dataFile = IDFConfig.getInstance().getIdfData().getDatafileDatafileLocation();
		String fileName = null;
		String fileNameArray[] = null;
		String fileDate = null;
		String endDatePrevVal = null;
		String endDateCurVal = null;
		String SourceFileId = null;
		String schemaname = null;
		StringBuilder sb = new StringBuilder("");
		int seqVal;
		if (dataFile.lastIndexOf("\\") > 0) {
			fileName = dataFile
					.substring(dataFile.lastIndexOf("\\") + 1);
		}
		if (dataFile.lastIndexOf("/") > 0) {
			fileName = dataFile.substring(dataFile.lastIndexOf("/") + 1);
		}
		
		if (fileName.indexOf("_") > 0)
			fileDate=fileName.substring(0,fileName.indexOf("_"));
		/*
		fileNameArray=fileName.split("_");
		if (fileNameArray.length >= 5)
			fileDate=fileNameArray[4].substring(0,fileNameArray[4].indexOf('.'));
		else
		{
			logger.error("II_PCMCValidation doFinalize() Called: Could not get SOURCE_END_DT_CURR from  file name, so taking current date as SOURCE_END_DT_CURR");
			SimpleDateFormat gdf = new SimpleDateFormat("yyyyMMddHHmmss");
			fileDate = gdf.format(new Date());
			
		}	*/
		try{
					endDatePrevVal = fileDate;
					endDateCurVal = fileDate;
					seqVal = IDFConfig.getInstance().getSequenceNum();
					schemaname = IDFConfig.getInstance().getIdfData().getCsvloadDbSchema();
					SourceFileId = "II_PCMC";
					
					sb.append("UPDATE "+schemaname+".LOAD_RESULT SET SOURCE_END_DT_PREV = ");
					sb.append("TO_TIMESTAMP ('"+endDatePrevVal+"', 'YYYYMMDD')-1");
					sb.append(",");
					sb.append("SOURCE_END_DT_CURR = ");
					sb.append("TO_TIMESTAMP ('"+endDateCurVal+"', 'YYYYMMDD')");
					sb.append(",");
					sb.append("SOURCE_ID = '");
					sb.append(SourceFileId);
					sb.append("'");
					sb.append(" WHERE PROCESS_NUM = '");
					sb.append(seqVal);
					sb.append("'");
					V9ValidationDaoManager.execNonQuery(sb
							.toString());
					sb = null; 
			}catch(Exception e){
				logger.error("II_PCMCValidation doFinalize() Called: Loading successful, LOAD_RESULT update Failure Please check"+sb.toString());
				e.printStackTrace();
			}
	}

}
