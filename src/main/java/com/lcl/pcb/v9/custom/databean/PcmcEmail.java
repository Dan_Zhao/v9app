/**
 * 
 */
package com.lcl.pcb.v9.custom.databean;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
//import org.apache.logging.log4j.LogManager;

import com.lcl.pcb.v9.custom.loader.VO41BValidation;

//import ca.pcfinancial.pcmc.common.PcmcConfig;


/**
 * @author Dev
 *
 */
public class PcmcEmail {
	
	Logger logger = LogManager.getLogger(VO41BValidation.class.getName());

	public void sendEmail(String emailAddress, String subject, String content) throws MessagingException{
		logger.info("PcmcEmail: sendEmail: Execution Started");
		/*PcmcConfig pcmcConfig = PcmcConfig.getSingeltonInstance();
		String emailHostString = pcmcConfig
				.getProperty("pcmc.host.email.server");*/
		String emailHostString = "Lclsmtp.ngco.com";

		/*PcmcConfig pcmcConfigOne = PcmcConfig
				.getSingeltonInstance();*/
		/*String fromEmailAddress = pcmcConfigOne
				.getProperty("pcmc.from.email");*/
		String fromEmailAddress = "VO41BFailureAlert@loblaw.ca";

		JavaMailSenderImpl sender = new JavaMailSenderImpl();

		sender.setHost(emailHostString);
		sender.setPort(25);
		
		MimeMessage message = sender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(message);
		helper.setTo(emailAddress);

		helper.setFrom(fromEmailAddress);

		helper.setSubject(subject);
		
		helper.setText(content, true);

		try {
			logger.debug("Sending email message to: "+emailAddress);
			sender.send(message);
		} catch (MailException ex) {
			// simply log it and go on...
			logger.debug("msgEmail failed");
			logger.error("PcmcEmail: Exception is: ",ex);
		}
		logger.info("PcmcEmail: sendEmail: Execution Ended");
	
	}

}
