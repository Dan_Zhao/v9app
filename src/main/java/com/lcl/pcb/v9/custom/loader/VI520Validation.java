package com.lcl.pcb.v9.custom.loader;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.lcl.pcb.v9.generic.loader.LoaderUtil;
import com.lcl.pcb.v9.generic.loader.V9ValidationDaoManager;
import com.lcl.pcb.v9.idf.jobflow.config.IDFConfig;
import com.lcl.pcb.v9.utilities.audit.AuditTablePojo;
import com.lcl.pcb.v9.utilities.audit.AuditTableUtil;
import com.lcl.pcb.v9.utilities.fileReader.FileUtil;

/**
 * This class is used to validate the csv files generated for VI520 interface.
 * It validates and makes the file based on the layout for loading into
 * PCL_TEMP.CUST_DEMOGRAPHICS_TEMP
 * 
 */
public class VI520Validation {

	Logger logger = LogManager.getLogger(VI520Validation.class.getName());
	ArrayList<String> csvFilesList = new ArrayList<String>();
	ArrayList<String> bodyCsvFilesList = new ArrayList<String>();
	String line = "";
	boolean headerResult = true;
	boolean trailerResult = true;
	boolean bodyResult = true;
	boolean doValidation = true;
	HashMap<String, Integer> totRecParsed = new HashMap<String, Integer>();
	public String bodyCsvFile = "";
	int totalRecCount = 0;

	V9ValidationDaoManager v9Validation = new V9ValidationDaoManager();
	int seqValue = IDFConfig.getInstance().getSequenceNum();
	String interfaceName = IDFConfig.getInstance().getIdfData()
			.getFilePropName();
	String fileSizeVal = null;

	/**
	 * This method returns the result of boolean value based on
	 * header/trailer/body validation
	 * 
	 * @param files
	 * @return
	 * @throws Exception
	 */
	public boolean doValidation(ArrayList<String> files) throws Exception {
		logger.debug("VI520Validation Called");
		this.csvFilesList = files;
		doValidation = doHeader() && doTrailer() && doBody();
		return doValidation;
	}

	/**
	 * This method does validation for header file
	 * 
	 * @return
	 * @throws Exception
	 */
	public boolean doHeader() throws Exception {
		logger.debug("VI520Validation doHeader() Called");
		return headerResult;
	}

	/**
	 * This method does validation for trailer part of the file and fetches
	 * total count
	 * 
	 * @return
	 */
	public boolean doTrailer() {
		logger.debug("VI520Validation doTrailer() Called");
		totRecParsed = FileUtil.totalRecParsedList;
		for (Entry<String, Integer> entry : totRecParsed.entrySet()) {
			totalRecCount += (entry.getValue());
		}
		if (totalRecCount == 0) {

			logger.error("No record found in file\n");
			trailerResult = false;
		} else {
			totalRecCount -= 1;
		}
		return trailerResult;
	}

	/**
	 * This writes the body segment data into another file based on the table
	 * mapping
	 * 
	 * @return
	 * @throws IOException
	 * @throws SQLException
	 */
	public boolean doBody() throws IOException, SQLException {
		logger.debug("VI520Validation doBody() Called");

		String nameFile = IDFConfig.getInstance().getIdfData()
				.getDatafileCsvFileLocation()
				+ "VI520_BODY_520_FINAL_" + seqValue + ".csv";
		String[] nameFileVal;
		String pcf_cust_ID;
		for (int i = 0; i < csvFilesList.size(); i++) {
			if (csvFilesList.get(i).contains("BODY_520")) {

				try (BufferedWriter bw = new BufferedWriter(new FileWriter(
						nameFile));
						BufferedReader nameFileReader = new BufferedReader(
								new FileReader(csvFilesList.get(i)))) {

					while ((line = nameFileReader.readLine()) != null) {
						nameFileVal = line.split("\\|");
						pcf_cust_ID = nameFileVal[0].replaceAll("\\D+", "");

						line = pcf_cust_ID + "|" + nameFileVal[0]
								+ "|520|Customer Type|" + nameFileVal[4] + "|";
						bw.write(line);
						bw.newLine();
						line = pcf_cust_ID + "|" + nameFileVal[0]
								+ "|520|Job Title|" + nameFileVal[6] + "|";
						bw.write(line);
						bw.newLine();
						line = pcf_cust_ID + "|" + nameFileVal[0]
								+ "|520|Custmer ID|" + nameFileVal[8] + "|";
						bw.write(line);
						bw.newLine();
					}

					bw.flush();
				}

			}
		}
		for (int j = 0; j < FileUtil.totalCSVFileNamesList.size(); j++) {
			FileUtil.totalCSVFileNamesList.clear();
		}
		FileUtil.totalCSVFileNamesList.add(nameFile);
		insertLaodResult();
		return bodyResult;
	}

	/**
	 * This method inserts data in LOAD_RESULT table based on pojo's
	 * 
	 * @throws SQLException
	 */
	public void insertLaodResult() throws SQLException {
		AuditTablePojo auditPojo = new AuditTablePojo();
		auditPojo.setProcessNumber(seqValue + "");
		auditPojo.setSchemaName(IDFConfig.getInstance().getIdfData()
				.getCsvloadDbSchema());
		auditPojo.setSourceInterface(interfaceName);
		auditPojo
				.setSourceNM(IDFConfig
						.getInstance()
						.getIdfData()
						.getDatafileDatafileLocation()
						.substring(
								IDFConfig.getInstance().getIdfData()
										.getDatafileDatafileLocation()
										.lastIndexOf("/") + 1));
		fileSizeVal = IDFConfig.getInstance().getIdfData().getFileSize();
		if (fileSizeVal != null && !fileSizeVal.isEmpty()) {
			auditPojo.setSourceSize(fileSizeVal);
		} else {
			auditPojo.setSourceSize("-9");
		}

		auditPojo.setTableName("CUST_DEMOGRAPHICS_TEMP");
		String sourceDTLRec = Integer.toString(totalRecCount);
		auditPojo.setSourceDtlRec(sourceDTLRec);

		auditPojo = AuditTableUtil.insertAudit(auditPojo);
		auditPojo = null;
	}

	/**
	 * @throws Exception
	 */
	public void doErrorLog() throws Exception {

	}

	/**
	 * doFinalize method called from V9Loader class before exit of Job , to
	 * update the LOAD_RESULT table with record values
	 * 
	 * @param files
	 * @throws Exception
	 */
	public void doFinalize(ArrayList<String> files) throws Exception {

		logger.debug("VI520Validation doFinalize() Called ");
		int successCount = LoaderUtil.successLoadCount;
		int failureCount = LoaderUtil.errorLoadCount;
		String SourceFileId = "520";
		StringBuilder sb = new StringBuilder("");
		sb.append("UPDATE PCL_TEMP.LOAD_RESULT SET ");
		sb.append("SOURCE_ID = '");
		sb.append(SourceFileId);
		sb.append("'");
		sb.append(",");
		sb.append("TL_REC_LOAD = '");
		sb.append(successCount);
		sb.append("'");
		sb.append(",");
		sb.append("LOAD_ERRORS = '");
		sb.append(failureCount);
		sb.append("'");
		sb.append(" WHERE PROCESS_NUM = '");
		sb.append(seqValue);
		sb.append("'");
		V9ValidationDaoManager.execNonQuery(sb.toString());
		sb = null;
	}
}
