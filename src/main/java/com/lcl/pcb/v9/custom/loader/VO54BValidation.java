package com.lcl.pcb.v9.custom.loader;

import java.util.ArrayList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.lcl.pcb.v9.generic.loader.V9ValidationDaoManager;
import com.lcl.pcb.v9.idf.jobflow.config.IDFConfig;

public class VO54BValidation {
	boolean headerResult = true;
	boolean trailerResult = true;
	boolean bodyResult = true;
	boolean doValidation = true;
	Logger logger = LogManager.getLogger(VO54BValidation.class.getName());

	public boolean doValidation(ArrayList<String> files) throws Exception {
		logger.debug("VO54BValidation Called");
		return doValidation;
	}

	public boolean doHeader() throws Exception {
		logger.debug("VO54BValidation doHeader() Called");
		return headerResult;
	}

	public boolean doTrailer() {
		logger.debug("VO54BValidation doTrailer() Called");
		return trailerResult;
	}

	public boolean doBody() {
		logger.debug("VO54BValidation doBody() Called");
		return bodyResult;
	}

	public void doErrorLog() throws Exception {

	}

	public void doFinalize(ArrayList<String> files) throws Exception {
		logger.debug("VO54BValidation doFinalize() Called ");
		String dataFile = IDFConfig.getInstance().getIdfData().getDatafileDatafileLocation();
		String fileName = null;
		String fileNameArray[] = null;
		String fileDate = null;
		String endDatePrevVal = null;
		String endDateCurVal = null;
		String SourceFileId = null;
		String schemaname = null;
		StringBuilder sb = new StringBuilder("");
		int seqVal;
		if (dataFile.lastIndexOf("\\") > 0) {
			fileName = dataFile
					.substring(dataFile.lastIndexOf("\\") + 1);
		}
		if (dataFile.lastIndexOf("/") > 0) {
			fileName = dataFile.substring(dataFile.lastIndexOf("/") + 1);
		}
		
		if (fileName.indexOf("_") > 0)
			fileDate=fileName.substring(0,fileName.indexOf("_"));
		
		try{
					endDatePrevVal = fileDate;
					endDateCurVal = fileDate;
					seqVal = IDFConfig.getInstance().getSequenceNum();
					schemaname = IDFConfig.getInstance().getIdfData().getCsvloadDbSchema();
					SourceFileId = "FILE_ID";
					
					sb.append("UPDATE "+schemaname+".LOAD_RESULT SET SOURCE_END_DT_PREV = ");
					sb.append("SYSTIMESTAMP");
					//sb.append("TO_TIMESTAMP ('"+endDatePrevVal+"', 'YYYYMMDD')-1");
					//sb.append("null");
					sb.append(",");
					sb.append("SOURCE_END_DT_CURR = ");
					sb.append("SYSTIMESTAMP");
					//sb.append("TO_TIMESTAMP ('"+endDateCurVal+"', 'YYYYMMDD')");
					//sb.append("null");
					//sb.append("SYSDATE");
					sb.append(",");
					sb.append("SOURCE_ID = '");
					sb.append(SourceFileId);
					sb.append("'");
					sb.append(" WHERE PROCESS_NUM = '");
					sb.append(seqVal);
					sb.append("'");
					V9ValidationDaoManager.execNonQuery(sb
							.toString());
					sb = null; 
			}catch(Exception e){
				logger.error("VO54BValidation doFinalize() Called: Loading successful, LOAD_RESULT update Failure Please check the SQL: "+sb.toString());
				e.printStackTrace();
			}
	}

}
