/**
 * 
 */
package com.lcl.pcb.v9.custom.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import sun.jdbc.odbc.ee.DataSource;

/**
 * This V9MultipleStagesTrgDaoManager framework class is used to establish and manage the
 * database connections for interfaces It uses the factory pattern to return the
 * correct database table that is needed by the V9DAOManager
 */
public class V9MultipleStagesTrgDaoManager {
	static final Logger logger = LogManager
			.getLogger(V9MultipleStagesTrgDaoManager.class.getName());

	private DataSource src;
	public Connection con;
	public static String URL;
	public static String USER;
	public static String PASS;

	public V9MultipleStagesTrgDaoManager(){

		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
		} catch (ClassNotFoundException ex) {
			logger.debug("Error: unable to load driver class for Oracle");
			System.out.println("Error: unable to load driver class for Oracle");
			System.exit(1);
		}

		URL = "jdbc:oracle:thin:@"
				+ URL;

		try {
			this.con = DriverManager.getConnection(URL, USER, PASS);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private static class MultiStageValidationTrgDAOSingleton {

		public static final ThreadLocal<V9MultipleStagesTrgDaoManager> INSTANCE;
		static {
			ThreadLocal<V9MultipleStagesTrgDaoManager> dm;
			try {
				dm = new ThreadLocal<V9MultipleStagesTrgDaoManager>() {
					@Override
					protected V9MultipleStagesTrgDaoManager initialValue() {
						try {
							return new V9MultipleStagesTrgDaoManager();
						} catch (Exception e) {
							return null;
						}
					}
				};
			} catch (Exception e) {
				dm = null;
			}
			INSTANCE = dm;
		}

	}

	public static V9MultipleStagesTrgDaoManager getInstance() {
		return MultiStageValidationTrgDAOSingleton.INSTANCE.get();
	}

	public void open() throws SQLException {
		try {
			if (this.con == null || this.con.isClosed())
				this.con = src.getConnection();
		} catch (SQLException e) {
			throw e;
		}
	}

	public void close() throws SQLException {
		try {
			if (this.con != null && !this.con.isClosed())
				this.con.close();
		} catch (SQLException e) {
			throw e;
		}
	}

	public static ResultSet execQuery(String sql) throws SQLException {
		V9MultipleStagesTrgDaoManager x = V9MultipleStagesTrgDaoManager.getInstance();
		PreparedStatement p1 = x.con.prepareStatement(sql);
		ResultSet k = p1.executeQuery();
		return k;
	}

	public static int execNonQuery(String sql) throws SQLException {
		V9MultipleStagesTrgDaoManager x = V9MultipleStagesTrgDaoManager.getInstance();
		PreparedStatement p2 = x.con.prepareStatement(sql);
		int k = p2.executeUpdate();
		p2.close();
		return k;
	}
	
}
