package com.lcl.pcb.v9.custom.loader;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.lcl.pcb.v9.generic.loader.LoaderUtil;
import com.lcl.pcb.v9.generic.loader.V9ValidationDaoManager;
import com.lcl.pcb.v9.idf.jobflow.config.IDFConfig;
import com.lcl.pcb.v9.utilities.audit.AuditTablePojo;
import com.lcl.pcb.v9.utilities.audit.AuditTableUtil;
import com.lcl.pcb.v9.utilities.fileReader.FileUtil;

/**
 * This class is used to validate the csv files generated for VI506 interface.
 * It validates and makes the file based on the layout for loading into
 * PCL_TEMP.CUST_DEMOGRAPHICS_TEMP
 * 
 */
public class VI506Validation {

	private static final Logger logger = LogManager
			.getLogger(VI506Validation.class.getName());

	private List<String> csvFilesList;
	private String line;
	private boolean headerResult = true;
	private boolean trailerResult = true;
	private boolean bodyResult = true;
	private boolean doValidation = true;
	private int seqValue = IDFConfig.getInstance().getSequenceNum();
	private String fileSizeVal = null;

	HashMap<String, Integer> totRecParsed = new HashMap<String, Integer>();
	int totalRecCount = 0;
	String interfaceName = IDFConfig.getInstance().getIdfData()
			.getFilePropName();

	/**
	 * This method returns the result of boolean value based on
	 * header/trailer/body validation
	 * 
	 * @param files
	 * @return
	 * @throws Exception
	 */
	public boolean doValidation(ArrayList<String> files) throws Exception {
		logger.debug("VI506Validation : start");
		this.csvFilesList = files;
		doValidation = doHeader() && doTrailer() && doBody();
		logger.debug("VI506Validation : end");
		return doValidation;
	}

	/**
	 * This method does validation for header file
	 * 
	 * @return
	 * @throws Exception
	 */
	public boolean doHeader() throws Exception {
		logger.debug("VI506Validation doHeader() Called");
		return headerResult;
	}

	/**
	 * This method does validation for trailer part of the file and fetches
	 * total count
	 * 
	 * @return
	 */
	public boolean doTrailer() {
		logger.debug("VI506Validation doTrailer() Called");
		totRecParsed = FileUtil.totalRecParsedList;
		for (Entry<String, Integer> entry : totRecParsed.entrySet()) {
			totalRecCount += (entry.getValue());
		}
		if (totalRecCount == 0) {

			logger.error("No record found in file\n");
			trailerResult = false;
		} else {
			totalRecCount -= 1;
		}
		return trailerResult;
	}

	/**
	 * This writes the body segment data into another file based on the table
	 * mapping
	 * 
	 * @return
	 * @throws IOException
	 * @throws SQLException
	 */
	public boolean doBody() throws IOException, SQLException {

		logger.debug("VI506Validation doBody() Called");

		File bodyCsvFile;
		File convertedFile;

		final List<String> convertedFilesList = new ArrayList<>(
				csvFilesList.size());

		final String csvSeparator = IDFConfig.getInstance().getIdfData()
				.getCsvFileSeparator();

		for (int i = 0; bodyResult && i < csvFilesList.size(); i++) {
			if (csvFilesList.get(i).contains("BODY")) {

				bodyCsvFile = new File(csvFilesList.get(i));

				convertedFile = new File(bodyCsvFile.getParentFile(), "Conv_"
						+ bodyCsvFile.getName());

				convertBodyInputFile(bodyCsvFile, csvSeparator, convertedFile);
				convertedFilesList.add(convertedFile.getName());
			}
		}

		FileUtil.totalCSVFileNamesList.clear();
		FileUtil.totalCSVFileNamesList.addAll(convertedFilesList);
		insertLaodResult();
		return bodyResult;

	}

	/**
	 * @param bodyCsvFile
	 * @param csvSeparator
	 * @param convertedFile
	 */
	private void convertBodyInputFile(final File bodyCsvFile,
			final String csvSeparator, final File convertedFile) {
		try (BufferedReader br = new BufferedReader(new FileReader(bodyCsvFile));
				BufferedWriter bw = new BufferedWriter(new FileWriter(
						convertedFile))) {

			String[] bodyVal;
			String pcfCustId;
			String constantPreString;
			String recordType;
			String addrLine1;
			String addrLine2;
			String city;
			String provinceState;
			String postalZip;
			String countryCode;
			String addrValidate = "N";

			while (br.ready()) {

				line = br.readLine();
				bodyVal = line.split("\\|");

				if (bodyVal.length == 26) {

					if (bodyVal[0].contains("@")) {
						pcfCustId = bodyVal[0].substring(0, 13);
					} else if (bodyVal[0].contains("I")) {
						pcfCustId = bodyVal[0].substring(6, 17);
					} else {
						logger.error("Corrupted data: Column#1 identifier not recognized. Not consisting of either '@' or 'I'.");
						logger.error("Corrupted Body Row: " + line);
						bodyResult = false;
						break;
					}

					if (bodyVal[20] == "21") {
						recordType = "07";
					} else {
						recordType = "02";
					}

					constantPreString = pcfCustId + csvSeparator + bodyVal[0];
					if (bodyVal[6]
							.equals("9999999999999999999999999999999999999999")
							|| bodyVal[6]
									.equals("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$")) {
						addrLine1 = "$";
					} else if (bodyVal[6].contains("$")
							&& bodyVal[6].contains("@")) {
						logger.error("AddressLine 1 contains both $ and @ , so exiting : "
								+ bodyVal[6].toString());
						bodyResult = false;
						break;
					} else {
						addrLine1 = bodyVal[6].replaceAll("[^a-zA-Z0-9]", " ");
					}
					bw.write(constantPreString + csvSeparator + "ADDRLINE1"
							+ csvSeparator + addrLine1 + csvSeparator
							+ recordType + csvSeparator + "01");
					bw.newLine();
					if (bodyVal[8]
							.equals("9999999999999999999999999999999999999999")
							|| bodyVal[8]
									.equals("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$")) {
						addrLine2 = "$";
					} else if (bodyVal[8].contains("$")
							&& bodyVal[8].contains("@")) {
						logger.error("AddressLine 2 contains both $ and @ , so exiting : "
								+ bodyVal[8].toString());
						bodyResult = false;
						break;
					} else {
						addrLine2 = bodyVal[8].replaceAll("[^a-zA-Z0-9]", " ");
					}
					bw.write(constantPreString + csvSeparator + "ADDRLINE2"
							+ csvSeparator + addrLine2 + csvSeparator
							+ recordType + csvSeparator + "01");
					bw.newLine();
					if (bodyVal[10].equals("9999999999999999999")
							|| bodyVal[10].equals("$$$$$$$$$$$$$$$$$$$")) {
						city = "$";
					} else if (bodyVal[10].contains("$")
							&& bodyVal[10].contains("@")) {
						logger.error("CITY contains both $ and @ , so exiting : "
								+ bodyVal[10].toString());
						bodyResult = false;
						break;
					} else {
						city = bodyVal[10].replaceAll("[^a-zA-Z0-9]", " ");
					}
					bw.write(constantPreString + csvSeparator + "CITY"
							+ csvSeparator + city + csvSeparator + recordType
							+ csvSeparator + "01");
					bw.newLine();
					if (bodyVal[12].equals("999")
							|| bodyVal[12].equals("$$$")) {
						provinceState = "$";
					} else if (bodyVal[12].contains("$")
							&& bodyVal[12].contains("@")) {
						logger.error("PROVINCESTATE contains both $ and @ , so exiting : "
								+ bodyVal[12].toString());
						bodyResult = false;
						break;
					} else {
						provinceState = bodyVal[12].replaceAll("[^a-zA-Z0-9]",
								" ");
					}
					bw.write(constantPreString + csvSeparator + "PROVINCESTATE"
							+ csvSeparator + provinceState + csvSeparator
							+ recordType + csvSeparator + "01");
					bw.newLine();

					if (bodyVal[14].equals("999999")
							|| bodyVal[14].equals("$$$$$$") || bodyVal[14].equals("9999999999999") || bodyVal[14].equals("$$$$$$$$$$$$$")) {
						postalZip = "$";
					} else if (bodyVal[14].contains("$")
							&& bodyVal[14].contains("@")) {
						logger.error("POSTALZIP contains both $ and @ , so exiting : "
								+ bodyVal[14].toString());
						bodyResult = false;
						break;
					} else {
						postalZip = bodyVal[14].replaceAll("[^a-zA-Z0-9]", " ");
					}

					bw.write(constantPreString + csvSeparator + "POSTALZIP"
							+ csvSeparator + postalZip + csvSeparator
							+ recordType + csvSeparator + "01");
					bw.newLine();

					if (bodyVal[16].equals("999")
							|| bodyVal[16].equals("$$$")) {
						countryCode = "$";
					} else if (bodyVal[16].contains("$")
							&& bodyVal[16].contains("@")) {
						logger.error("COUNTRYCODE contains both $ and @ , so exiting : "
								+ bodyVal[16].toString());
						bodyResult = false;
						break;
					} else {
						countryCode = bodyVal[16].replaceAll("[^a-zA-Z0-9]", " ");
					}

					bw.write(constantPreString + csvSeparator + "COUNTRYCODE"
							+ csvSeparator + countryCode + csvSeparator
							+ recordType + csvSeparator + "01");
					bw.newLine();

					if (bodyVal[24].equals("Y")) {
						addrValidate = "Y";
					} else if (bodyVal[24].equals("N")
							|| bodyVal[24].equals("@")
							|| bodyVal[24].equals("")) {
						addrValidate = "N";
					}

					bw.write(constantPreString + csvSeparator
							+ "ADDRESSVALIDATE" + csvSeparator + addrValidate
							+ csvSeparator + recordType + csvSeparator + "01");
					bw.newLine();

				} else {
					logger.error("Corrupted data: not enough parts in body row.");
					logger.error("Corrupted Body Row: " + line);
					bodyResult = false;
					break;
				}

			}
			br.close();
			bw.close();
		} catch (IOException ex) {
			logger.error("Error while reading/wrting from/to file.", ex);
			bodyResult = false;
		}
	}

	/**
	 * This method inserts data in LOAD_RESULT table based on pojo's
	 * 
	 * @throws SQLException
	 */
	public void insertLaodResult() throws SQLException {
		AuditTablePojo auditPojo = new AuditTablePojo();
		auditPojo.setProcessNumber(seqValue + "");
		auditPojo.setSchemaName(IDFConfig.getInstance().getIdfData()
				.getCsvloadDbSchema());
		auditPojo.setSourceInterface(interfaceName);
		auditPojo.setSourceNM(new File(IDFConfig.getInstance().getIdfData()
				.getDatafileDatafileLocation()).getName());
		fileSizeVal = IDFConfig.getInstance().getIdfData().getFileSize();
		if (fileSizeVal != null && !fileSizeVal.isEmpty()) {
			auditPojo.setSourceSize(fileSizeVal);
		} else {
			auditPojo.setSourceSize("-9");
		}
		auditPojo.setTableName("CUST_DEMOGRAPHICS_TEMP");
		String sourceDTLRec = Integer.toString(totalRecCount);
		auditPojo.setSourceDtlRec(sourceDTLRec);
		auditPojo = AuditTableUtil.insertAudit(auditPojo);
		auditPojo = null;
	}

	/**
	 * @throws Exception
	 */
	public void doErrorLog() throws Exception {
		// no actions
	}

	/**
	 * doFinalize method called from V9Loader class before exit of Job , to
	 * update the LOAD_RESULT table with record values
	 * 
	 * @param files
	 * @throws Exception
	 */
	public void doFinalize(ArrayList<String> files) throws Exception {
		logger.info("VI506Validation doFinalize() : start ");
		int successCount = LoaderUtil.successLoadCount;
		int failureCount = LoaderUtil.errorLoadCount;
		String SourceFileId = "506";
		StringBuilder sb = new StringBuilder("");
		sb.append("UPDATE PCL_TEMP.LOAD_RESULT SET ");
		sb.append("SOURCE_ID = '");
		sb.append(SourceFileId);
		sb.append("'");
		sb.append(",");
		sb.append("TL_REC_LOAD = '");
		sb.append(successCount);
		sb.append("'");
		sb.append(",");
		sb.append("LOAD_ERRORS = '");
		sb.append(failureCount);
		sb.append("'");
		sb.append(" WHERE PROCESS_NUM = '");
		sb.append(seqValue);
		sb.append("'");
		V9ValidationDaoManager.execNonQuery(sb.toString());
		sb = null;
		logger.info("VI506Validation doFinalize() : start of updating PCF_CUST_ID ");
		if (failureCount == 0) {/*
			String updateQuery = new StringBuilder(280)
					.append("MERGE INTO PCL_TEMP.CUST_DEMOGRAPHICS_TEMP T USING (SELECT DISTINCT PCF_CUST_ID , ")
					.append("ACCOUNT_REF_ID ,rn from (SELECT   DISTINCT  a.ACCOUNT_REF_ID ,a.PCF_CUST_ID ,Row_number() OVER(PARTITION BY a.ACCOUNT_REF_ID ORDER BY a.REC_CREATE_TMS DESC) rn ")
					.append(" FROM PCL.PCF_PRODUCT a JOIN PCL_TEMP.CUST_DEMOGRAPHICS_TEMP C ")
					.append("ON (LTRIM(SUBSTR(C.ACC_ACCESS_NUM, 0, LENGTH(C.ACC_ACCESS_NUM) - 2), 'I ') = a.ACCOUNT_REF_ID) JOIN PCL.PCF_CUST b ")
					.append("ON a.PCF_CUST_ID = b.PCF_CUST_ID AND b.CUST_TYPE='0')where rn=1)P ")
					.append("ON (LTRIM(SUBSTR(T.ACC_ACCESS_NUM, 0, LENGTH(T.ACC_ACCESS_NUM) - 2), 'I ') = P.ACCOUNT_REF_ID AND T.RECORD_TYPE='02' AND T.ACC_ACCESS_NUM like 'I%') ")
					.append("WHEN MATCHED THEN UPDATE SET T.PCF_CUST_ID = P.PCF_CUST_ID")
					.toString();

			logger.info("Updating the PCL_TEMP.CUST_DEMOGRAPHICS_TEMP FOR PCF_CUST_ID column for Account Id :  "
					+ updateQuery);
			int k = V9ValidationDaoManager.execNonQuery(updateQuery);
			logger.info("Number of records updated : " + k);
			updateQuery = new StringBuilder(280)
					.append("MERGE INTO PCL_TEMP.CUST_DEMOGRAPHICS_TEMP T")
					.append(" USING PCL.PCF_CARD C ")
					.append(" ON (RTRIM(SUBSTR(T.ACC_ACCESS_NUM, 0, LENGTH(T.ACC_ACCESS_NUM) - 2), '@') = C.CARD_NUM ")
					.append(" AND T.RECORD_TYPE='02'")
					.append(" AND T.ACC_ACCESS_NUM like '%@' ) ")
					.append(" WHEN MATCHED THEN ")
					.append(" UPDATE SET T.PCF_CUST_ID = C.PCF_CUST_ID ")
					.toString();
			logger.info("Updating the PCL_TEMP.CUST_DEMOGRAPHICS_TEMP FOR PCF_CUST_ID column for Account Number :  "
					+ updateQuery);
			int l = V9ValidationDaoManager.execNonQuery(updateQuery);
			logger.info("Number of records updated : " + l);
			logger.info(" **** Total Number of records updated **** : "
					+ (k + l));
			if (successCount != (k + l)) {
				logger.info("Since total number of records updated in database do not match"
						+ successCount + "  !=  " + (k + l));
				logger.error("Since total number of records updated with PCF_CUST_ID in database do not match with the number of records loaded from file, so exiting the job");
				System.exit(1);
			}
		*/} else {
			logger.info("Bad records : "
					+ failureCount
					+ " Since bad records is not equal to 0, so not updating PCF_CUST_ID ");
		}
		logger.info("VI506Validation doFinalize() : end ");

	}
}
