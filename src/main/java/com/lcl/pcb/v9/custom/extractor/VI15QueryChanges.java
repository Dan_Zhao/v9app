package com.lcl.pcb.v9.custom.extractor;

import java.util.Arrays;

import com.lcl.pcb.v9.idf.jobflow.config.ExtractorConfig;


/**
 * This class is used if additional changes are needed to the original
 * interface sql query in the extractor xml
 * This class is specific to this interface, other custom classes
 * need to be coded for other interfaces if the original query is
 * not the final layout of the extraction flow for that interface.
 */
public class VI15QueryChanges extends QueryChanges {

	String[] prefixF = {
        "MISS",
        "MRS",
        "MRS.",
        "MS",
        "MS.",
        "MME",
        "PRINCESS",
        "LADY",
        "MLLE",
        "MIS",
        "MI"      
    };	
	
	String[] prefixM = {
        "PRINCE",
        "EARL",
        "JR",
        "JR.",
        "SIR",
        "LORD",
        "MR"
    };		

	/**
	 * Constructor sets up originalRsArray 
	 * to be the original data that was extracted
	 * from the database in the Extractor process flow
	 * Use parent class's constructor
	 *
	 * @param  String[][] originalRsArray
	 * @return void
	 */		
	public VI15QueryChanges(String[][] originalRsArray) {
		super(originalRsArray);
	}
	
	/**
	 * override execute method
	 * this method will do the necessary custom
	 * data modifications for the Extractor process flow
	 * for this interface 
	 *
	 * @param  void
	 * @return String[][] new array after modifications
	 */	
	public String[][] execute() throws Exception {

		int rowCount=originalRsArray.length;
		//int colCount=originalRsArray[0].length;

		//Decode prefix to serve as proxy for Gender
		//MAST_ACCOUNT_ID|CIFP_CUSTOMER_ID_1|CIFP_CUSTOMER_TYPE|CIFP_DATE_OPEN|CIFP_PREFIX|CIFP_SUFFIX|FIRST_NAME|MIDDLE_INITIAL|LAST_NAME|CIFP_DATE_OF_BIRTH|ADDRESS_LINE1|ADDRESS_LINE2|ADDRESS_LINE3|CITY|PROVINCE|COUNTRY|ZIPCODE|AM00_STATC_CHARGEOFF|AM00_STATC_CLOSED|AM00_STATC_CREDIT_REVOKED|AM00_STATC_SECURITY_FRAUD//
		int AM00_STATC_CHARGEOFF=Arrays.asList(originalRsArray[0]).indexOf("AM00_STATC_CHARGEOFF");
		int AM00_STATC_CLOSED=Arrays.asList(originalRsArray[0]).indexOf("AM00_STATC_CLOSED");
		int AM00_STATC_CREDIT_REVOKED=Arrays.asList(originalRsArray[0]).indexOf("AM00_STATC_CREDIT_REVOKED");
		int AM00_STATC_SECURITY_FRAUD=Arrays.asList(originalRsArray[0]).indexOf("AM00_STATC_SECURITY_FRAUD");	
		int CIFP_PREFIX=Arrays.asList(originalRsArray[0]).indexOf("CIFP_PREFIX");
	    String[] gender = new String[rowCount];    
		
		// loop through each row checking condition to determine gender
		int newRowCount=0;	    
		for (int i = 0; i < rowCount; i++) {
			if (originalRsArray[i][AM00_STATC_CHARGEOFF].equals("") &&
				originalRsArray[i][AM00_STATC_CLOSED].equals("") &&
				originalRsArray[i][AM00_STATC_CREDIT_REVOKED].equals("") &&
				originalRsArray[i][AM00_STATC_SECURITY_FRAUD].equals("")) {
					if (Arrays.asList(prefixF).indexOf(originalRsArray[i][CIFP_PREFIX])!=-1) {
						gender[i]="F";
					} else if (Arrays.asList(prefixM).indexOf(originalRsArray[i][CIFP_PREFIX])!=-1) {
						gender[i]="M";					
					} else {
						gender[i]="U";				
					}
				newRowCount++;					
			} else {
				gender[i]="";
			}
		}
		
		// SAS code gets frequency count, not needed to be translated to Java
				
		//		proc freq data = cifa1;
		//			table cifp_prefix /out= prefix missing norow nocol nocum nopercent;
		//			table cifp_suffix/out= Suffix missing norow nocol nocum nopercent;
		//			table gender/out= Gender missing norow nocol nocum nopercent;
		//		run;
		//		endrsubmit;
		
		// loop through and store layout header from config file in newRsArray
		int numFields=ExtractorConfig.getInstance().getExtractorData().getLayout().getLField().size();
		newRsArray=new String[newRowCount+1][numFields];  //count is number of rows with Gender value, need to add 1 row for the header
		
	    String[] header = new String[numFields];    
		for (int i = 0; i < numFields; i++) {
			String fieldName = ExtractorConfig.getInstance().getExtractorData().getLayout().getLField().get(i).getFieldName();	
			header[i]=fieldName;
		}
		newRsArray[0]=header;
		
		//  SAS code:
		
		//	rsubmit;
		//	data template;
		//	input 	source_code $ client_id last_modified_date date9. status_indicator $ 
		//			record_type $ Gender $ first_name $75. middle_initial $75. last_name $75. suffix $75.
		//			address_line1 $200. address_line2 $200. address_line3 $200. 
		//			city $95. province $15. postal_code $13. country $15.
		//		  	date_of_birth national_id display_field1 display_field2 display_field3 comment_1 $100. comment_2 $100.;
		//	datalines;
		//	run;
		//	data finscanSummary;
		//	set template finscan;
		//	run;
		//	endrsubmit;			
		
		// reformat the data to the columns that are specified in vi15extractor.xml
		// and remove any that don't have Gender value
	    String[] row = null;
	    int j=1;
		for (int i = 1; i < rowCount; i++) {
			
			//remove rows without Gender value
			if (!gender[i].equals("")) {			
			
				row = new String[numFields];
				
				row[0]="PCCIF"; // source code
				
				int CIFP_CUSTOMER_ID_1=Arrays.asList(originalRsArray[0]).indexOf("CIFP_CUSTOMER_ID_1");			
				row[1]=originalRsArray[i][CIFP_CUSTOMER_ID_1]; //  client id
				
				row[2]=""; // last modified date		
				
				row[3]=""; // status indicator
				
				row[4]="I"; // record type
				
				row[5]=gender[i]; // gender
				
				int FIRST_NAME=Arrays.asList(originalRsArray[0]).indexOf("FIRST_NAME");	
				int MIDDLE_INITIAL=Arrays.asList(originalRsArray[0]).indexOf("MIDDLE_INITIAL");	
				int LAST_NAME=Arrays.asList(originalRsArray[0]).indexOf("LAST_NAME");				
				row[6]=originalRsArray[i][FIRST_NAME]+" ";
				if (originalRsArray[i][MIDDLE_INITIAL].length()>0) {
					row[6]=row[6]+originalRsArray[i][MIDDLE_INITIAL].substring(0,0);
				}
				row[6]=row[6]+originalRsArray[i][MIDDLE_INITIAL]+" "+originalRsArray[i][LAST_NAME]; // fullname
				
				int ADDRESS_LINE1=Arrays.asList(originalRsArray[0]).indexOf("ADDRESS_LINE1");					
				row[7]=originalRsArray[i][ADDRESS_LINE1];  // address line1
				
				int ADDRESS_LINE2=Arrays.asList(originalRsArray[0]).indexOf("ADDRESS_LINE2");				
				row[8]=originalRsArray[i][ADDRESS_LINE2]; // address line2
				
				int ADDRESS_LINE3=Arrays.asList(originalRsArray[0]).indexOf("ADDRESS_LINE3");				
				row[9]=originalRsArray[i][ADDRESS_LINE3]; // address line 3
				
				int CITY=Arrays.asList(originalRsArray[0]).indexOf("CITY");				
				row[10]=originalRsArray[i][CITY]; // city
				
				int PROVINCE=Arrays.asList(originalRsArray[0]).indexOf("PROVINCE");				
				row[11]=originalRsArray[i][PROVINCE]; // province
				
				int ZIPCCODE=Arrays.asList(originalRsArray[0]).indexOf("ZIPCODE");				
				row[12]=originalRsArray[i][ZIPCCODE]; // postal code	
				
				int COUNTRY=Arrays.asList(originalRsArray[0]).indexOf("COUNTRY");				
				row[13]=originalRsArray[i][COUNTRY]; // country
				
				int CIFP_DATE_OF_BIRTH=Arrays.asList(originalRsArray[0]).indexOf("CIFP_DATE_OF_BIRTH");	
				String dateOfBirth=originalRsArray[i][CIFP_DATE_OF_BIRTH];
				if (!dateOfBirth.equals("")) {
					dateOfBirth=dateOfBirth.replaceAll("[\\-]", "");
					dateOfBirth=dateOfBirth.substring(0,8);
				}
				row[14]=dateOfBirth; // date of birth
				
				row[15]=""; // national id
				
				int MAST_ACCOUNT_ID=Arrays.asList(originalRsArray[0]).indexOf("MAST_ACCOUNT_ID");			
				row[16]=originalRsArray[i][MAST_ACCOUNT_ID]; // display field 1
				
				int CIFP_CUSTOMER_TYPE=Arrays.asList(originalRsArray[0]).indexOf("CIFP_CUSTOMER_TYPE");
				row[17]=originalRsArray[i][CIFP_CUSTOMER_TYPE];  // display field 2;
				
				row[18]="CIF"; // display field 3
				
				//row[19]=""; // comment 1;
				
				int CIFPCUSTOMDATA2=Arrays.asList(originalRsArray[0]).indexOf("CIFPCUSTOMDATA2");				
				row[19]=originalRsArray[i][CIFPCUSTOMDATA2]; // comment 1
				
				row[20]=""; //comment 2			
			

				newRsArray[j]=row;
				j++;
			}
		}		
		int newRsArrayLength=newRsArray.length;
		return newRsArray;
	}
	

}


