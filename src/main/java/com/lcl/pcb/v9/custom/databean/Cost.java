//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.2-147 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2018.05.16 at 09:57:56 PM EDT 
//


package com.lcl.pcb.v9.custom.databean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 *  
 * 
 * 				 2  PackageCost  Dollar amout
 * 				 3  CancellationCost Dollar amout
 * 				 4  GrandTotal Dollar amout
 * 				 5  CreditCardPayment Dollar amout
 * 				 6  TotalGstHst Dollar amout
 * 				 7  BalanceDue Dollar amout
 * 				 8  TotalBalanceDue Dollar amout
 * 
 * 
 * 
 * 			
 * 
 * <p>Java class for Cost complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Cost">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PackageCost" type="{}ItemCost" minOccurs="0"/>
 *         &lt;element name="CancellationCost" type="{}ItemCost" minOccurs="0"/>
 *         &lt;element name="GrandTotal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CreditCardPayment" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TotalGstHst" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BalanceDueToInvoice" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TotalBalanceDue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Cost", propOrder = {
    "packageCost",
    "cancellationCost",
    "grandTotal",
    "creditCardPayment",
    "totalGstHst",
    "balanceDueToInvoice",
    "totalBalanceDue"
})
public class Cost {

    @XmlElement(name = "PackageCost")
    protected ItemCost packageCost;
    @XmlElement(name = "CancellationCost")
    protected ItemCost cancellationCost;
    @XmlElement(name = "GrandTotal")
    protected String grandTotal;
    @XmlElement(name = "CreditCardPayment")
    protected String creditCardPayment;
    @XmlElement(name = "TotalGstHst")
    protected String totalGstHst;
    @XmlElement(name = "BalanceDueToInvoice")
    protected String balanceDueToInvoice;
    @XmlElement(name = "TotalBalanceDue")
    protected String totalBalanceDue;

    /**
     * Gets the value of the packageCost property.
     * 
     * @return
     *     possible object is
     *     {@link ItemCost }
     *     
     */
    public ItemCost getPackageCost() {
        return packageCost;
    }

    /**
     * Sets the value of the packageCost property.
     * 
     * @param value
     *     allowed object is
     *     {@link ItemCost }
     *     
     */
    public void setPackageCost(ItemCost value) {
        this.packageCost = value;
    }

    /**
     * Gets the value of the cancellationCost property.
     * 
     * @return
     *     possible object is
     *     {@link ItemCost }
     *     
     */
    public ItemCost getCancellationCost() {
        return cancellationCost;
    }

    /**
     * Sets the value of the cancellationCost property.
     * 
     * @param value
     *     allowed object is
     *     {@link ItemCost }
     *     
     */
    public void setCancellationCost(ItemCost value) {
        this.cancellationCost = value;
    }

    /**
     * Gets the value of the grandTotal property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGrandTotal() {
        return grandTotal;
    }

    /**
     * Sets the value of the grandTotal property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGrandTotal(String value) {
        this.grandTotal = value;
    }

    /**
     * Gets the value of the creditCardPayment property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCreditCardPayment() {
        return creditCardPayment;
    }

    /**
     * Sets the value of the creditCardPayment property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreditCardPayment(String value) {
        this.creditCardPayment = value;
    }

    /**
     * Gets the value of the totalGstHst property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTotalGstHst() {
        return totalGstHst;
    }

    /**
     * Sets the value of the totalGstHst property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTotalGstHst(String value) {
        this.totalGstHst = value;
    }

    /**
     * Gets the value of the balanceDueToInvoice property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBalanceDueToInvoice() {
        return balanceDueToInvoice;
    }

    /**
     * Sets the value of the balanceDueToInvoice property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBalanceDueToInvoice(String value) {
        this.balanceDueToInvoice = value;
    }

    /**
     * Gets the value of the totalBalanceDue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTotalBalanceDue() {
        return totalBalanceDue;
    }

    /**
     * Sets the value of the totalBalanceDue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTotalBalanceDue(String value) {
        this.totalBalanceDue = value;
    }

}
