package com.lcl.pcb.v9.generic.security;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import sun.jdbc.odbc.ee.DataSource;

import com.lcl.pcb.v9.idf.jobflow.config.IDFConfig;

/**
 * This V9PgpDecryptDaoManager framework class is used to establish and manage the
 * database connections for Pgp Decryption Logic.
 */
public class V9PgpDecryptDaoManager {
	static final Logger logger = LogManager
			.getLogger(V9PgpDecryptDaoManager.class.getName());

	private DataSource src;
	public Connection con;

	public V9PgpDecryptDaoManager(){

		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
		} catch (ClassNotFoundException ex) {
			logger.debug("Error: unable to load driver class for Oracle");
			System.out.println("Error: unable to load driver class for Oracle");
			System.exit(1);
		}

		IDFConfig idfConfig = IDFConfig.getInstance();

		String URL = "jdbc:oracle:thin:@"
				+ idfConfig.getIdfData().getDecryptDbUrl();
		logger.debug("V9PgpDecryptDaoManager: DB URL: " + URL);
		String USER = idfConfig.getIdfData().getDecryptDbUser();
		String PASS = idfConfig.getIdfData().getDecryptDbPass();

		try {
			this.con = DriverManager.getConnection(URL, USER, PASS);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			logger.error("V9PgpDecryptDaoManager: Exception occurred while connecting to DB: " + idfConfig.getIdfData().getDecryptDbUrl(),e);
			//e.printStackTrace();
		}

	}

	private static class ValidationPgpDAOSingleton {

		public static final ThreadLocal<V9PgpDecryptDaoManager> INSTANCE;
		static {
			ThreadLocal<V9PgpDecryptDaoManager> dm;
			try {
				dm = new ThreadLocal<V9PgpDecryptDaoManager>() {
					@Override
					protected V9PgpDecryptDaoManager initialValue() {
						try {
							return new V9PgpDecryptDaoManager();
						} catch (Exception e) {
							return null;
						}
					}
				};
			} catch (Exception e) {
				dm = null;
			}
			INSTANCE = dm;
		}

	}

	public static V9PgpDecryptDaoManager getInstance() {
		return ValidationPgpDAOSingleton.INSTANCE.get();
	}

	public void open() throws SQLException {
		try {
			if (this.con == null || this.con.isClosed())
				this.con = src.getConnection();
		} catch (SQLException e) {
			throw e;
		}
	}

	public void close() throws SQLException {
		try {
			if (this.con != null && !this.con.isClosed())
				this.con.close();
		} catch (SQLException e) {
			throw e;
		}
	}
	
}
