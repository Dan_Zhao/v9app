package com.lcl.pcb.v9.generic.exception;

import java.io.PrintStream;
import java.io.PrintWriter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * 
 * This class uses for Exceptions.
 *
 */
public class FileException extends java.lang.Exception{
	  
	  /** Creates a new instance of FileFieldException */
	  private Throwable rootCause;
	  protected String SourceName;
	  protected String parameters;
	  
	  Logger log = LogManager.getLogger(FileException.class); 
	  
	  public FileException() {
	    SourceName = "";
	    parameters = "";
	  }
	  
	  public FileException(String msg) {
	    super(msg);
	    SourceName = "";
	    parameters = "";
	    log.error("Exception : " + msg);
	  }
	  
	  public FileException(Throwable e) {
	    SourceName = "";
	    parameters = "";
	    rootCause = e;
	    log.error("Exception : " + e.getMessage());
	    printStackTrace();
	  }
	  
	  public FileException(String msg, Throwable ex) {
	    super(msg);
	    SourceName = "";
	    parameters = "";
	    rootCause = ex;
	    log.error("Exception : " + msg + "\nRoot cause : " + ex.getMessage());
	    printStackTrace();
	  }
	  
	//  public FileFieldException(String SourceName, String parameters) {
//	    super(SourceName + " " + parameters);
//	    this.SourceName = SourceName;
//	    this.parameters = parameters;
	//  }
	  
	  public Throwable getRootCause() {
	    return rootCause;
	  }
	  
	  public String getMessage() {
	    if(rootCause == null) return super.getMessage();
	    return super.getMessage() + "; nested exception is \n\t" + rootCause.toString();
	  }
	  
	  public void printStackTrace(PrintStream ps) {
	    if(rootCause == null) {
	      super.printStackTrace(ps);
	      ps.println("source(" + SourceName + "); parameters(" + parameters + ")");
	    } else {
	      ps.println(this);
	      ps.println("source(" + SourceName + "); parameters(" + parameters + ")");
	      rootCause.printStackTrace(ps);
	    }
	  }
	  
	  public void printStackTrace(PrintWriter pw) {
	    if(rootCause == null) {
	      super.printStackTrace(pw);
	      pw.println("source(" + SourceName + "); parameters(" + parameters + ")");
	    } else {
	      pw.println(this);
	      pw.println("source(" + SourceName + "); parameters(" + parameters + ")");
	      rootCause.printStackTrace(pw);
	    }
	  }

	  public String getStackString() {
	       StackTraceElement[] s = getStackTrace();
	       String x = this.getMessage(); 
	       for(int k = 0; k < s.length; k++) {
	          x += "\r\n\tat " + s[k].getClassName() + "." + s[k].getMethodName() +" (" + s[k].getFileName() + ":" + s[k].getLineNumber() + ")";
	       }
	       return x;
	  }
	  public void printStackTrace() {
	    printStackTrace(System.err);
	    log.error(getStackString());
	  }
	  
	  public String getSourceName() {
	    return SourceName;
	  }
	  
	  public void setSourceName(String SourceName) {
	    this.SourceName = SourceName;
	  }
	  
	  public String getParameters() {
	    return parameters;
	  }
	  
	  public void setParameters(String parameters) {
	    this.parameters = parameters;
	  }
	  
}
