package com.lcl.pcb.v9.generic.loader;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import sun.jdbc.odbc.ee.DataSource;

import com.lcl.pcb.v9.idf.jobflow.config.IDFConfig;

/**
 * This V9ValidationDAO framework class is used to establish and manage the
 * database connections for interfaces It uses the factory pattern to return the
 * correct database table that is needed by the V9DAOManager
 */
public class V9ValidationDaoManager {
	static final Logger logger = LogManager
			.getLogger(V9ValidationDaoManager.class.getName());

	private DataSource src;
	public Connection con;

	public V9ValidationDaoManager(){

		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
		} catch (ClassNotFoundException ex) {
			logger.debug("Error: unable to load driver class for Oracle");
			System.out.println("Error: unable to load driver class for Oracle");
			System.exit(1);
		}

		IDFConfig idfConfig = IDFConfig.getInstance();

		String URL = "jdbc:oracle:thin:@"
				+ idfConfig.getIdfData().getCsvloadDbUrl();
		String USER = idfConfig.getIdfData().getCsvloadDbUser();
		String PASS = idfConfig.getIdfData().getCsvloadDbPass();

		try {
			this.con = DriverManager.getConnection(URL, USER, PASS);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private static class ValidationDAOSingleton {

		public static final ThreadLocal<V9ValidationDaoManager> INSTANCE;
		static {
			ThreadLocal<V9ValidationDaoManager> dm;
			try {
				dm = new ThreadLocal<V9ValidationDaoManager>() {
					@Override
					protected V9ValidationDaoManager initialValue() {
						try {
							return new V9ValidationDaoManager();
						} catch (Exception e) {
							return null;
						}
					}
				};
			} catch (Exception e) {
				dm = null;
			}
			INSTANCE = dm;
		}

	}

	public static V9ValidationDaoManager getInstance() {
		return ValidationDAOSingleton.INSTANCE.get();
	}

	public void open() throws SQLException {
		try {
			if (this.con == null || this.con.isClosed())
				this.con = src.getConnection();
		} catch (SQLException e) {
			throw e;
		}
	}

	public void close() throws SQLException {
		try {
			if (this.con != null && !this.con.isClosed())
				this.con.close();
		} catch (SQLException e) {
			throw e;
		}
	}

	public static ResultSet execQuery(String sql) throws SQLException {
		V9ValidationDaoManager x = V9ValidationDaoManager.getInstance();
		PreparedStatement p1 = x.con.prepareStatement(sql);
		ResultSet k = p1.executeQuery();
		return k;
	}

	public static int execNonQuery(String sql) throws SQLException {
		V9ValidationDaoManager x = V9ValidationDaoManager.getInstance();
		PreparedStatement p2 = x.con.prepareStatement(sql);
		int k = p2.executeUpdate();
		p2.close();
		return k;
	}
	
}
