package com.lcl.pcb.v9.generic.loader;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.lcl.pcb.v9.idf.jobflow.config.IDFConfig;

/**
 * This utility class is used to get all loaded and error record count
 */
public class LoaderUtil {
	public static Integer successLoadCount = 0;
	public static Integer errorLoadCount = 0;
	public static HashMap<String, String> loadCount = new HashMap<String, String>();
	List<String> csvfile = new ArrayList<String>();
	List<String> logfile = new ArrayList<String>();
	public static ArrayList<String> totalbadFileNamesList = new ArrayList<String>();
	public static ArrayList<String> ErrorlogFileNamesList = new ArrayList<String>();
	String logName = "";
	String csvName = "";
	String badFileName = "";
	String logFileName = "";
	String[] logFileNamePattern ;
	Logger logger = LogManager.getLogger(LoaderUtil.class.getName());

	public LoaderUtil(List<String> logfilename, List<String> csvfilename) {
		logger.debug("LoaderUtil Called");
		csvfile = csvfilename;
		logfile = logfilename;
		loadCount();

	}

	/**
	 * This method returns number of total records loaded into table
	 * 
	 * @param no param
	 * @return Integer This returns the load count value.
	 */
	private Integer loadCount() {
		
		BufferedReader br;
		String successSearch = "Rows successfully loaded.";
		String successSearch1 = "Row successfully loaded.";
		String errorSearch = "Rows not loaded due to data errors.";
		String errorSearch1 = "Row not loaded due to data errors.";
		String logVal;
		String errorLogVal;
		String line = "";
		String[] lineArray = null;
		String tableName = null;
		String segSucCount="0";
		String segFailCount="0";
		//boolean tableFound= false;
		if (null != logfile && logfile.size() != 0) {
			for (int i = 0; i < logfile.size(); i++) {
				logName = logfile.get(i);
				try {
					br = new BufferedReader(new FileReader(logName));
					boolean tableFound= false;
					try {
						while ((line = br.readLine()) != null) {
							/*if(line.toUpperCase().startsWith("Table".toUpperCase())){*/
							if(!tableFound && line.toUpperCase().startsWith("Table".toUpperCase())){
								logger.debug("Table Found :"+line);
								lineArray = line.split(" ");
								logger.debug("Table Found Line Array:"+lineArray[1]);
								tableName=lineArray[1].substring(0, lineArray[1].length()-1);
								logger.debug("Table Name Substring:"+tableName);
								tableName = tableName.replaceAll("\"", "");
								if (tableName.indexOf(".") > 0)
								{
									tableName = tableName.substring(tableName.indexOf(".")+1);
									logger.debug("Table Name Removing Schema:"+tableName);
								}
							 tableFound= true;
							}							
							else if (line.contains(successSearch)
									|| line.contains(successSearch1)) {
								logVal = line.substring(line.indexOf(" "),
										line.lastIndexOf("Row"));
								successLoadCount = successLoadCount
										+ Integer.valueOf(logVal.trim());
								segSucCount=logVal.trim();
							}
							else if(line.contains(errorSearch)
									|| line.contains(errorSearch1)){
								errorLogVal = line.substring(line.indexOf(" "),
										line.lastIndexOf("Row"));
								errorLoadCount = errorLoadCount + Integer.valueOf(errorLogVal.trim());
 							    segFailCount=errorLogVal.trim();
								
								Integer errorCount=Integer.valueOf(line.substring(line.indexOf(" "),
										line.lastIndexOf("Row")).trim());
								if (errorCount != 0) {
									logFileName=logName.substring(logName.lastIndexOf("/")+1, logName.lastIndexOf("."));
									logFileNamePattern=logFileName.split("_");
									csvName = IDFConfig.getInstance()
											.getIdfData()
											.getDatafileCsvFileLocation()
											+ logFileNamePattern[0]+"_"+logFileNamePattern[2]+"_"+logFileNamePattern[3]+"_"+logFileNamePattern[1]
											+ IDFConfig
													.getInstance()
													.getIdfData()
													.getDatafileCsvNamePattern();
									badFileName = IDFConfig.getInstance()
											.getIdfData()
											.getDatafileBadFileLocation()
											+ "/"
											+ csvName
													.substring(
															csvName.lastIndexOf("/") + 1,
															csvName.lastIndexOf("."))
											+ IDFConfig
													.getInstance()
													.getIdfData()
													.getDatafileBadNamePattern();
									logger.error("Bad record Found in CSV File:"
											+ csvName);
									totalbadFileNamesList.add(badFileName);
									ErrorlogFileNamesList.add(logName);

								}
								errorCount=0;
							}
							

						}
						br.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				} catch (FileNotFoundException ex) {
					ex.printStackTrace();
				}
				logger.debug("TableName :"+tableName);
				logger.debug("Segment SuccessCount | Failcount :"+segSucCount+"|"+segFailCount);
				loadCount.put(tableName, segSucCount+"|"+segFailCount);
				tableName = "";
				segSucCount="0";
				segFailCount = "0";
				
			}
		
		}
		return successLoadCount;
	}
}
