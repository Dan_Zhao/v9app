package com.lcl.pcb.v9.generic.loader;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.lcl.pcb.v9.idf.jobflow.config.IDFConfig;

/**
 * Implements Runnable This class is used to execute the sql loader command with
 * input as csv file, ctl file, log file and bad file. Sql Loader Connection
 * values will be taken from IDF.xml of particular interface
 */
public class V9SqlLoader implements Runnable {
	private String lCtl;
	private String lLog;
	private String lCsv;
	private String lBad;
	ProcessHandler inputStream = null;
	ProcessHandler errorStream = null;
	

	public V9SqlLoader(String ctlfilename, String logfilename,
			String csvfilename, String badcsvfilename) {
		this.lCtl = ctlfilename;
		this.lLog = logfilename;
		this.lCsv = csvfilename;
		this.lBad = badcsvfilename;
	}

	Logger logger = LogManager.getLogger(V9SqlLoader.class.getName());
	IDFConfig idfConfig = IDFConfig.getInstance();
	String LoadUSER = idfConfig.getIdfData().getCsvloadDbUser();
	String LoadPASS = idfConfig.getIdfData().getCsvloadDbPass();
	String LoadDB = idfConfig.getIdfData().getCsvloadDb();
	String errorsAllowed = idfConfig.getIdfData().getSqlErrorsAllowed();
	String sqlLoaderDirect = idfConfig.getIdfData().getSqlLoaderDirect();
	StringBuilder sqlldrCmd = new StringBuilder();

	public void run() {
		try {
			logger.debug("Start of SQL Loader to process csv file:" + lCsv);
			/*
			 * String sqlldrCmd = "sqlldr " + LoadUSER + "/" + LoadPASS + "@" +
			 * LoadDB + ", control=" + lCtl + " direct=true  bad=" + lBad +
			 * " log=" + lLog + " DATA=" + lCsv + "";
			 */

			sqlldrCmd.append("sqlldr ");
			sqlldrCmd.append(LoadUSER);
			sqlldrCmd.append("/");
			sqlldrCmd.append(LoadPASS);
			sqlldrCmd.append("@");
			sqlldrCmd.append(LoadDB);
			sqlldrCmd.append(" , control=");
			sqlldrCmd.append(lCtl);
			// sqlldrCmd.append(" direct=true");
			sqlldrCmd.append(" direct=" + sqlLoaderDirect);
			sqlldrCmd.append(" bad=");
			sqlldrCmd.append(lBad);
			sqlldrCmd.append(" log=");
			sqlldrCmd.append(lLog);
			sqlldrCmd.append(" DATA=");
			sqlldrCmd.append(lCsv);
			sqlldrCmd.append(" ERRORS=");
			sqlldrCmd.append(errorsAllowed);
			Runtime rt = Runtime.getRuntime();
			//logger.debug("sqlldrCmd value" + sqlldrCmd.toString());
			logger.debug("sqlldrCmd value=sqlldr "+LoadUSER+"/XXXXXXXX@"+LoadDB+" , control="+lCtl+" direct="+sqlLoaderDirect+" bad="+lBad+" log="+lLog+" DATA="+lCsv+" ERRORS="+errorsAllowed+"");
			Process proc = rt.exec(sqlldrCmd.toString());
			logger.debug("Executing SQL Loader Command for File:" + lCsv);
			/* handling the streams so that dead lock situation never occurs. */
			inputStream = new ProcessHandler(
					proc.getInputStream(), "INPUT");
			errorStream = new ProcessHandler(
					proc.getErrorStream(), "ERROR");
			/* start the stream threads */
			inputStream.start();
			errorStream.start();
			sqlldrCmd = null;
			proc.waitFor();
		}catch (Exception e) {
			e.printStackTrace();
		} 
	}

}
