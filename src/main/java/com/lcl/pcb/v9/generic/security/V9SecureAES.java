package com.lcl.pcb.v9.generic.security;

import java.security.Key;
import java.security.MessageDigest;
import java.util.Arrays;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;

/**
 * Class to encrypt/decrypt sensitive xml fields such as passswords
 * Uses standard Java Security and JavaX Crypto libraries
 * User defined key for SecureAES encryption/decryption and Base64 encoding/decoding
 */
public class V9SecureAES {
	
	private static final String keyString = "V9SecureKey"; // PROJECT DEFINED KEYWORD KEY
	
	private byte[] key; // first 128 bits of keyString will be actual utf-8 128 bit key
	
	/**
	 * Constructor for in application decryption 
	 * using keyString constant
	 */		
	public V9SecureAES() { 
		try {
			// generate key	
			key = keyString.getBytes("UTF-8");
			MessageDigest sha = MessageDigest.getInstance("SHA-1");
			key = sha.digest(key);
			key = Arrays.copyOf(key, 16); // use only first 128 bits of keyString
		} catch (Exception e) {
			e.printStackTrace();
		}
	}	
	
	/**
	 * Constructor for stand alone mode encrpyt or decript
	 * using keyArg parameter
	 *
	 * @param keyArg the key
	 */		
	public V9SecureAES(String keyArg) { 
		try {
			// generate key	
			key = keyArg.getBytes("UTF-8");
			MessageDigest sha = MessageDigest.getInstance("SHA-1");
			key = sha.digest(key);
			key = Arrays.copyOf(key, 16); // use only first 128 bits of keyArg
		} catch (Exception e) {
			e.printStackTrace();
		}
	}		
	
	/**
	 * This method Encrypts the given text
	 * and encodes to Base64 String
	 * only used in stand alone mode
	 * key parameter when running in stand alone mode with parameters
	 *
	 * @param text the String you wish to encrypt
	 * @return String encrypted text in Base64
	 */	
	public void encrypt(String text) { 
		try {
			// encrypt the text				
			Key aesKey = new SecretKeySpec(key, "AES");		
	        Cipher cipher = Cipher.getInstance("AES"); 			
			cipher.init(Cipher.ENCRYPT_MODE, aesKey);
			byte[] encrypted = cipher.doFinal(text.getBytes());
			
			// take encrypted binary (byte[]) encode to Base64 to store as String 
			String encryptedText = DatatypeConverter.printBase64Binary(encrypted);
			System.err.println("\n\t "+text+" encrypted to "+encryptedText);	

		} catch (Exception e) {
			System.err.println("\n\t ERROR: Could not encrypt");			
			e.printStackTrace();
		}
	}
	
	/**
	 * This method performs reading Base64 String encoding  
	 * Decrypts given text with either the
	 * key constant defined in this class when using in application mode or
	 * key parameter when running in stand alone mode with parameters
	 *
	 * @param  encryptedText the String you wish to decrypt
	 * @return String unencrypted text
	 */	
	public String decrypt(String encryptedText) {
		String decrypted="";
		try {
			// take Base64 String decode to binary (byte[])
			byte[] encrypted=DatatypeConverter.parseBase64Binary(encryptedText);			
			
			// decrypt the text
			Key aesKey = new SecretKeySpec(key, "AES");		
	        Cipher cipher = Cipher.getInstance("AES"); 
			cipher.init(Cipher.DECRYPT_MODE, aesKey);
			decrypted = new String(cipher.doFinal(encrypted));
		} catch (Exception e) {
			System.err.println("\n\t ERROR: Could not decrypt");
			e.printStackTrace();
		}
		return decrypted;
	}	


	/**
	 * This main method allows encrption to be  
	 * Run as Stand Alone Application with parameters to
	 * [encrypt/decrypt] [key] [text]
	 *
	 * @param  args[] the String values to encrpyt/decrypt with specified key
	 * @return output with encrpyted or decrypted text
	 */	
	public static void main(String[] args) {
		
//		args = new String[]{"encrypt", keyString, "Pafe_764"};
		args = new String[]{"decrypt", keyString, "4hUhv44ArBCoVB9ZExGJsg=="};
		
		if (args.length != 3) {
			System.err.println("V9SecureAES \n\t Syntax is: V9SecureAES [encrypt/decrypt] [key] [text]");
			System.exit(1);
		}		
		
		String type = args[0];
		String keyArg = args[1];	
		String text = args[2];			
		
		V9SecureAES security = new V9SecureAES(keyArg);	
		System.err.println("V9SecureAES using key: "+keyArg);
		
		if (type.toUpperCase().equals("ENCRYPT")) {
			security.encrypt(text);
		} else if (type.toUpperCase().equals("DECRYPT")) {
			System.err.println("\n\t"+text+" decrypted to "+security.decrypt(text));
		} else {
			System.err.println("\n\t ERROR: Must specify if you wish to encrypt or decrypt");
			System.exit(1);			
		}
	}

}
