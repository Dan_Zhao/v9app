package com.lcl.pcb.v9.generic.security;

import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.lcl.pcb.v9.dao.V9DAOManager;
import com.lcl.pcb.v9.idf.jobflow.config.IDFConfig;
import com.lcl.pcb.v9.idf.jobflow.factory.V9Interface;

/**
 * @author Dev
 *
 */
public class QuantumProcessDecrypt implements V9Interface{

	static final Logger logger = LogManager.getLogger(QuantumProcessDecrypt.class.getName());	

	/**
	 * @param args
	 */

	/* (non-Javadoc)
	 * @see com.lcl.pcb.v9.idf.jobflow.factory.V9Interface#execute()
	 */
	@Override
	public void execute() throws Exception {
		// TODO Auto-generated method stub
		logger.info("QuantumProcessDecrypt: execute() method execution - Started");
		V9PgpDecryptDaoManager v9PgpDecryptDaoManager = new V9PgpDecryptDaoManager();
		V9DAOManager db = new V9DAOManager();
		IDFConfig idfConfig = IDFConfig.getInstance(); // singleton to store
		ExecuteShellCommand executeShellCommand = new ExecuteShellCommand();
		int decryptStatus = 1;

		v9PgpDecryptDaoManager.open();
		String pgpFullFilePath = idfConfig.getIdfData().getDatafileDatafileLocation();
		logger.debug("QuantumProcessDecrypt: execute() method: pgpFullFilePath: " + pgpFullFilePath);
		String pgpFileName = pgpFullFilePath.substring(pgpFullFilePath.lastIndexOf("/")+1);
		logger.debug("QuantumProcessDecrypt: execute() method: pgpFileName: " + pgpFileName);
		String pgpFilePath = pgpFullFilePath.substring(0, pgpFullFilePath.lastIndexOf("/"));
		logger.debug("QuantumProcessDecrypt: execute() method: pgpFilePath: " + pgpFilePath);
		decryptStatus = executeShellCommand.v9PgpDecrypt(v9PgpDecryptDaoManager.con, idfConfig.getIdfData().getDecryptPgpProgramDirectory(), pgpFileName, pgpFilePath, idfConfig.getIdfData().getDecryptFileLocation() ,idfConfig.getIdfData().getDecryptTempFileLocation());
		
		if(decryptStatus == 0){
			logger.debug("QuantumProcessDecrypt: execute() method: PGP Decryption successful for file: " + pgpFullFilePath);
		}else{
			logger.error("QuantumProcessDecrypt: execute() method: PGP Decryption FAILED for file: " + pgpFullFilePath);
			db.setLevelWarning(1, "Failed to decrypt data file - " + pgpFullFilePath + " - Resp Code: 0001", 0);		//NEED TO VERIFY THIS LOGIC TO GRACEFULLY END THE PROCESS IN CASE OF A FAILURE
			//System.exit(1);
		}
		logger.info("QuantumProcessDecrypt: execute() method execution - Ended");
	}
	
	@Override
	public void addProperties(Map<String, String> properties) throws Exception {
		// TODO Auto-generated method stub
		
	}
}
