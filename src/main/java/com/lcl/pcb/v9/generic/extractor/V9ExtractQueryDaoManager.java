package com.lcl.pcb.v9.generic.extractor;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import sun.jdbc.odbc.ee.DataSource;

import com.lcl.pcb.v9.generic.loader.V9ValidationDaoManager;
import com.lcl.pcb.v9.idf.jobflow.config.ExtractorConfig;
import com.lcl.pcb.v9.idf.jobflow.config.IDFConfig;

/**
 * This V9ExtractQueryDaoManager class is used to establish and manage the
 * database connections for the Extractor process flow.  This maintains
 * the connection to the database which contains the data to be extracted.
 */
public class V9ExtractQueryDaoManager {
	static final Logger logger = LogManager
			.getLogger(V9ExtractQueryDaoManager.class.getName());

	private DataSource src;
	public Connection con;

	public V9ExtractQueryDaoManager(){

		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
		} catch (ClassNotFoundException ex) {
			logger.debug("Error: unable to load driver class for Oracle");
			System.out.println("Error: unable to load driver class for Oracle");
			System.exit(1);
		}

		IDFConfig idfConfig = IDFConfig.getInstance();

		String URL = "jdbc:oracle:thin:@"
				+ idfConfig.getIdfData().getCsvloadDbUrl();
		String USER = idfConfig.getIdfData().getCsvloadDbUser();
		String PASS = idfConfig.getIdfData().getCsvloadDbPass();

		try {
			this.con = DriverManager.getConnection(URL, USER, PASS);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private static class ValidationDAOSingleton {

		public static final ThreadLocal<V9ExtractQueryDaoManager> INSTANCE;
		static {
			ThreadLocal<V9ExtractQueryDaoManager> dm;
			try {
				dm = new ThreadLocal<V9ExtractQueryDaoManager>() {
					@Override
					protected V9ExtractQueryDaoManager initialValue() {
						try {
							return new V9ExtractQueryDaoManager();
						} catch (Exception e) {
							return null;
						}
					}
				};
			} catch (Exception e) {
				dm = null;
			}
			INSTANCE = dm;
		}

	}

	public static V9ExtractQueryDaoManager getInstance() {
		return ValidationDAOSingleton.INSTANCE.get();
	}

	public void open() throws SQLException {
		try {
			if (this.con == null || this.con.isClosed())
				this.con = src.getConnection();
		} catch (SQLException e) {
			throw e;
		}
	}

	public void close() throws SQLException {
		try {
			if (this.con != null && !this.con.isClosed())
				this.con.close();
		} catch (SQLException e) {
			throw e;
		}
	}

	public static ResultSet execQuery(String sql) throws SQLException {
		V9ExtractQueryDaoManager x = V9ExtractQueryDaoManager.getInstance();
		PreparedStatement p1 = x.con.prepareStatement(sql);
		ResultSet k = p1.executeQuery();
		return k;
	}

	public static int execNonQuery(String sql) throws SQLException {
		V9ExtractQueryDaoManager x = V9ExtractQueryDaoManager.getInstance();
		PreparedStatement p2 = x.con.prepareStatement(sql);
		int k = p2.executeUpdate();
		p2.close();
		return k;
	}
	
	
	
	// Test Query ---------------------------------------------------------
		
    public ResultSet extractSQL() throws SQLException {
		String query=ExtractorConfig.getInstance().getExtractorData().getQuery();
		ResultSet rs=null; 	
		String dateFormat = "DD-Mon-YYYY HH24:MI:SS";
		String admExtractDate = "25-Nov-2014 00:00:00";
//		String query = "SELECT * FROM ADM_STD_DISP_HEAD_TRAIL";
//				+ "where TO_CHAR(ADM_EXTRACT_DT, '"
//				+ dateFormat + "')='" + admExtractDate
//				+ "'";
		try {
			rs = execQuery(query);
		} catch (SQLException e) {	
			throw e;
		}
		return rs;	    

    }   			
	
	
	
}
