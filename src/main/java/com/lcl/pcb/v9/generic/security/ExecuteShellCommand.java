package com.lcl.pcb.v9.generic.security;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.lcl.pcb.v9.idf.jobflow.config.IDFConfig;


/**
 * @author Dev
 *
 */
public class ExecuteShellCommand {
	
	static final Logger logger = LogManager.getLogger(ExecuteShellCommand.class.getName());	
	IDFConfig idfConfig = IDFConfig.getInstance();

	public static void main(String[] args) {

		try{

			ExecuteShellCommand obj = new ExecuteShellCommand();

			String pdpProgramDirectory = "/usr/bin";
			String pgpFileName = "TS2_ADM_dispo.20160204014124.uatv.pgp";
			String pgpFilePath = "/y/app/pcb/inbound/tsys/process";
			String outputDirectory = "/y/app/pcb/inbound/tsys/process";
			String temporaryDirectory = "/y/app/pcb/inbound/tsys/process/temp";
			Connection con = DriverManager.getConnection("jdbc:oracle:thin:@LAHEMWAIT01.ngco.com:1521:DV724", "PCF_CONTROL", "XXXXXXX"); 

			obj.v9PgpDecrypt(con,pdpProgramDirectory, pgpFileName, pgpFilePath, outputDirectory, temporaryDirectory);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * @param con
	 * @param pdpProgramDirectory
	 * @param pgpFileName
	 * @param pgpFilePath
	 * @param outputDirectory
	 * @return
	 */
	public int v9PgpDecrypt(Connection con, String pdpProgramDirectory,
			String pgpFileName, String pgpFilePath, String outputDirectory, String temporaryDirectory) {
		logger.info("ExecuteShellCommand: v9PgpDecrypt: Execution Started");
		String passphrase = getPassphrase(con, pgpFileName);
		// String passphrase = "UsWd173bYKPAjSl3HP4q";
		logger.info("ExecuteShellCommand: v9PgpDecrypt: Execution Ended");
		return decryptPgpFile(pdpProgramDirectory, pgpFilePath, pgpFileName,
				passphrase, outputDirectory, temporaryDirectory);
		// return 0;
	}

	/**
	 * @param pgpPath
	 * @param pgpFilePath
	 * @param pgpFileName
	 * @param passphrase
	 * @param outputDirectory
	 * @return
	 */
	private int decryptPgpFile(String pgpPath, String pgpFilePath, String pgpFileName, String passphrase, String outputDirectory, String temporaryDirectory) {
		logger.info("ExecuteShellCommand: decryptPgpFile: Execution Started");

		List<String> command = new ArrayList<String>();
		int decryptStatus=-1;
		String pgpFilename = pgpFilePath+"/"+pgpFileName; 
		//command.add(pgpPath+"/pgp");			//NW: Changed to below on 15th September
		command.add(pgpPath);
		command.add("--decrypt");
		command.add(pgpFilename);
		command.add("--passphrase");
		command.add(passphrase);
		command.add("--temp-dir");				//NW: Added on 15th September
		command.add(temporaryDirectory);		//NW: Added on 15th September
		command.add("--overwrite");				
		command.add("remove");
		command.add("--output");
		command.add(outputDirectory);

		logger.debug("decryptPgpFile command= "+pgpPath+" --decrypt "+pgpFilename+ " --passphrase XXXXXXX" + " --overwrite --temp-dir "+temporaryDirectory+" remove --output "+outputDirectory);
		//logger.debug("decryptPgpFile command= " + command);
		/*String responseMessage = executeProcessBuilder(command);
		if (responseMessage.indexOf("(0:output file ")>0) {
			return 0;
		} else {
			return -1;			
		}*/
		HashMap<String, String> processOutput = executeProcessBuilder(command);
		String processOutputText = processOutput.get("OUTPUT");
		logger.debug("ExecuteShellCommand: decryptPgpFile: processOutputText: " + processOutputText);
		String processErrorText = processOutput.get("ERROR");
		logger.debug("ExecuteShellCommand: decryptPgpFile: processErrorText: " + processErrorText);
		String successPattern = "(0:output file ";
		String decryptedFileLocation="";
		String[] pgpTargetFile;            //SR: Added on 20th December
		String pgpTargetFileName=""; //SR: Added on 20th December
		int moveFileStatus=-1;
		if(null!=processOutputText && processOutputText.indexOf(successPattern)>0){
			logger.debug("ExecuteShellCommand: decryptPgpFile: Successful output received from PGP command line execution");
			decryptStatus = 0;
//			idfConfig.getIdfData().setDecryptedFileName(processOutputText.substring(processOutputText.lastIndexOf(successPattern)+successPattern.length(), processOutputText.lastIndexOf(")")));
			decryptedFileLocation = processOutputText.substring(processOutputText.lastIndexOf(successPattern)+successPattern.length(), processOutputText.lastIndexOf(")"));
			//Added on 20th December
			if(null!=idfConfig.getIdfData().getDecryptFileNameChange() && "Y".equalsIgnoreCase(idfConfig.getIdfData().getDecryptFileNameChange())){
				pgpTargetFile=pgpFilename.split(".pgp");
				if(pgpTargetFile.length>0){
				pgpTargetFileName=pgpTargetFile[0];
				}
				moveFileStatus = moveFile(decryptedFileLocation, pgpTargetFileName);	
				logger.debug("ExecuteShellCommand: decryptPgpFile: Renamed Decrypted File placed at: " + pgpTargetFileName);
				idfConfig.getIdfData().setDatafileDatafileLocation(pgpTargetFileName);
				
			}
			else{
			logger.debug("ExecuteShellCommand: decryptPgpFile: Decrypted File placed at: " + decryptedFileLocation);
			idfConfig.getIdfData().setDatafileDatafileLocation(decryptedFileLocation);
			}
		}
		else if(null!=processErrorText && processErrorText.indexOf(successPattern)>0){
			logger.debug("ExecuteShellCommand: decryptPgpFile: Successful output received from PGP command line execution");
			decryptStatus = 0;
			decryptedFileLocation = processErrorText.substring(processErrorText.lastIndexOf(successPattern)+successPattern.length(), processErrorText.lastIndexOf(")"));
			//Added on 20th December
			if(null!=idfConfig.getIdfData().getDecryptFileNameChange() && "Y".equalsIgnoreCase(idfConfig.getIdfData().getDecryptFileNameChange())){
				//decryptedFileLocation = processErrorText.substring(processErrorText.lastIndexOf(successPattern)+successPattern.length(), processErrorText.indexOf(")"));
				pgpTargetFile=pgpFilename.split(".pgp");
				if(pgpTargetFile.length>0){
				pgpTargetFileName=pgpTargetFile[0];
				}
				moveFileStatus = moveFile(decryptedFileLocation, pgpTargetFileName);	
				logger.debug("ExecuteShellCommand: decryptPgpFile: Renamed Decrypted File placed at: " + pgpTargetFileName);
				idfConfig.getIdfData().setDatafileDatafileLocation(pgpTargetFileName);
				
			}
			else{
			logger.debug("ExecuteShellCommand: decryptPgpFile: Decrypted File placed at: " + decryptedFileLocation);
			idfConfig.getIdfData().setDatafileDatafileLocation(decryptedFileLocation);		
			}
		}else{
			logger.error("Error in ExecuteShellCommand: decryptPgpFile: Exception has occurred while executing PGP command line decryption: "+processOutputText + processErrorText);
			moveFileStatus = moveFile(pgpFilename, pgpFilePath + "/with_error_" + pgpFileName);
			decryptStatus = -1;	
		}
		logger.info("ExecuteShellCommand: decryptPgpFile: Execution Ended");
		return decryptStatus;
	}
	
	/**
	 * @param sourceFilePath
	 * @param targetFilePath
	 * @return
	 */
	private int moveFile(String sourceFilePath, String targetFilePath) {
		logger.info("ExecuteShellCommand: moveFile: Execution Started");

		List<String> command = new ArrayList<String>();
		int moveFileStatus=-1;
		command.add("mv");
		command.add(sourceFilePath);
		command.add(targetFilePath);

		logger.debug("moveFile command="+command);

		HashMap<String, String> processOutput = executeProcessBuilder(command);
		String processOutputText = processOutput.get("OUTPUT");
		if(null!=processOutputText){
			logger.debug("ExecuteShellCommand: moveFile: File moved successfully from " + sourceFilePath + " to " + targetFilePath);
			moveFileStatus = 0;
		}
		if(null!=processOutput.get("ERROR")){
			logger.error("Error in ExecuteShellCommand: moveFile: Exception has occurred while executing command line Move File Command: "+processOutput.get("ERROR"));
			moveFileStatus = -1;			
		}
		logger.info("ExecuteShellCommand: moveFile: Execution Ended");
		return moveFileStatus;
	}

	/**
	 * @param con
	 * @param pgpFileName
	 * @return
	 */
	private String getPassphrase(Connection con, String pgpFileName) {
		logger.info("ExecuteShellCommand: getPassphrase: Execution Started");

		String passphrase = ""; 
		String respCode = "";
		CallableStatement cs = null;

		try {
			cs = con.prepareCall("{call PCF_CONTROL.PCF_PROC_FILE_DECRYPT_LOOKUP(?, ? ,?, ?, ?)}");
			cs.setString(1, pgpFileName);
			java.util.Date currentDate = new java.util.Date();
			cs.setTimestamp(2, new Timestamp(currentDate.getTime()) );
			//cs.setString(3, "");		//Changed to below on 29th Feb 2016
			cs.setString(3, String.valueOf(idfConfig.getSequenceNum()));

			cs.registerOutParameter(4, Types.VARCHAR);
			cs.registerOutParameter(5, Types.VARCHAR);
			cs.executeQuery();

			passphrase = cs.getString(4);
			respCode = cs.getString(5);

		} catch (Exception e) {
			//e.printStackTrace();
			logger.error("ExecuteShellCommand: getPassphrase: Exception has occurred while calling the Decrypt Stored Procedure", e);
		}

		if (!respCode.trim().equals("0000")) {
			logger.debug("Get passphrase failed. RespCode="+respCode);
			passphrase = "";
		}
		logger.info("ExecuteShellCommand: getPassphrase: Execution Ended");
		return passphrase;
	}

	/**
	 * @param command
	 * @return
	 */
	private HashMap<String, String> executeProcessBuilder(List<String> command) {
		logger.info("ExecuteShellCommand: executeProcessBuilder: Execution Started");

		HashMap<String, String> processOutput = new HashMap<String, String>();
		try {
			StringBuffer output = new StringBuffer();
			StringBuffer error = new StringBuffer();

			ProcessBuilder builder = new ProcessBuilder(command);
			Map<String, String> environ = builder.environment();

			logger.debug("ExecuteShellCommand: executeProcessBuilder: Going to execute the ProcessBuilder Command");
			final Process process = builder.start();

			InputStream inputStream = process.getInputStream();
			InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
			BufferedReader inputBufferReader = new BufferedReader(inputStreamReader);
			String inputLine="";
			while ((inputLine = inputBufferReader.readLine()) != null) {
				logger.debug("ExecuteShellCommand: executeProcessBuilder: Input Stream Read and not null: " + inputLine);
				output.append(inputLine + "\n");
			}

			InputStream errorStream = process.getErrorStream();
			InputStreamReader errorStreamReader = new InputStreamReader(errorStream);
			BufferedReader errorBufferReader = new BufferedReader(errorStreamReader);
			String errorLine="";
			while ((errorLine = errorBufferReader.readLine()) != null) {
				logger.debug("ExecuteShellCommand: executeProcessBuilder: Error Stream Read and not null: " + errorLine);
				error.append(errorLine + "\n");
			}
			
			processOutput.put("OUTPUT", output.toString());
			processOutput.put("ERROR", error.toString());
			
//			return output +"\n"+error;
			logger.info("ExecuteShellCommand: executeProcessBuilder: Execution Ended");
			return processOutput;

		} catch (Exception e) {
			//e.printStackTrace();
			logger.error("Error in ExecuteShellCommand: executeProcessBuilder: Exception is: ",e);
			return processOutput;
			//return e.getMessage();
		}

	}
	
	/**
	 * @param con
	 * @param pdpProgramDirectory
	 * @param pgpFileName
	 * @param pgpFilePath
	 * @param outputDirectory
	 * @return
	 */
	public int v9PgpEncrypt(Connection con, String pdpProgramDirectory,
			String pgpFileName, String pgpFilePath, String outputDirectory, String temporaryLocation) {
		logger.info("ExecuteShellCommand: v9PgpEncrypt: Execution Started");
		String keyList = getKeyList(con, pgpFileName);
		// String keylist = "0xCD99FCF5";
		if(null!=keyList && !"".equalsIgnoreCase(keyList)){
			logger.info("ExecuteShellCommand: v9PgpEncrypt: Execution Ended");
			return encryptPgpFile(pdpProgramDirectory, pgpFilePath, pgpFileName,
					keyList, outputDirectory, temporaryLocation);
		}else{
			logger.info("ExecuteShellCommand: v9PgpEncrypt: Execution Ended with Error: KeyList is empty or null");
			return -1;
		}
	}

	/**
	 * @param con
	 * @param pgpFileName
	 * @return
	 */
	private String getKeyList(Connection con, String pgpFileName) {
		logger.info("ExecuteShellCommand: getKeyList: Execution Started");

		String keyList = ""; 
		String respCode = "";
		CallableStatement cs = null;

		try {
			cs = con.prepareCall("{call PCF_CONTROL.PCF_FILE_PUBENCRYPT_KEY_LOOKUP(? ,?, ?, ?, ?, ?)}");
			cs.setString(1, pgpFileName);
			java.util.Date currentDate = new java.util.Date();
			cs.setTimestamp(2, new Timestamp(currentDate.getTime()) );
			//cs.setString(3, "");		//Changed to below on 29th Feb 2016
			cs.setString(3, String.valueOf(idfConfig.getSequenceNum()));
			cs.setString(4, "PROCESS");

			cs.registerOutParameter(5, Types.VARCHAR);
			cs.registerOutParameter(6, Types.VARCHAR);
			cs.executeQuery();

			keyList = cs.getString(5);
			respCode = cs.getString(6);

		} catch (Exception e) {
			//e.printStackTrace();
			logger.error("ExecuteShellCommand: getPassphrase: Exception has occurred while calling the Decrypt Stored Procedure", e);
		}

		if (!respCode.trim().equals("0000")) {
			logger.debug("Get KeyList failed. RespCode="+respCode);
			keyList = "";
		}
		logger.info("ExecuteShellCommand: getKeyList: Execution Ended");
		return keyList;
	}


	/**
	 * @param pgpPath
	 * @param pgpFilePath
	 * @param pgpFileName
	 * @param passphrase
	 * @param outputDirectory
	 * @return
	 */
	private int encryptPgpFile(String pgpPath, String pgpFilePath, String pgpFileName, String keyList, String outputDirectory, String temporaryLocation) {
		logger.info("ExecuteShellCommand: encryptPgpFile: Execution Started");

		List<String> command = new ArrayList<String>();
		int encryptStatus=-1;
		String[] keyEntries = keyList.split(",");

		//command.add(pgpPath+"/pgp");			//NW: Changed to below on 15th September
		command.add(pgpPath);
		command.add("--encrypt");
		command.add(pgpFilePath+"/"+pgpFileName);
		for (String keyEntry: keyEntries) {
			command.add("--recipient");
			command.add(keyEntry);
		}
		command.add("--temp-dir");				//NW: Added on 15th September
		command.add(temporaryLocation);			//NW: Added on 15th September
		command.add("--overwrite");
		command.add("remove");
		command.add("--output");
		command.add(outputDirectory);

		logger.debug("encryptPgpFile command="+command);

		HashMap<String, String> processOutput = executeProcessBuilder(command);
		String processOutputText = processOutput.get("OUTPUT");
		logger.debug("ExecuteShellCommand: encryptPgpFile: processOutputText: " + processOutputText);
		String processErrorText = processOutput.get("ERROR");
		logger.debug("ExecuteShellCommand: encryptPgpFile: processErrorText: " + processErrorText);
		String successPattern = "(0:output file ";
		String encryptedFileLocation="";
		int moveFileStatus=-1;
		if(null!=processOutputText && processOutputText.indexOf(successPattern)>0){
			logger.debug("ExecuteShellCommand: encryptPgpFile: Successful output received from PGP command line execution");
			encryptStatus = 0;
			encryptedFileLocation = processOutputText.substring(processOutputText.lastIndexOf(successPattern)+successPattern.length(), processOutputText.lastIndexOf(")"));
			logger.debug("ExecuteShellCommand: encryptPgpFile: Encrypted File placed at: " + encryptedFileLocation);
			idfConfig.getIdfData().setDatafileDatafileLocation(encryptedFileLocation);
		}
		else if(null!=processErrorText && processErrorText.indexOf(successPattern)>0){
			logger.debug("ExecuteShellCommand: encryptPgpFile: Successful output received from PGP command line execution");
			encryptStatus = 0;
			encryptedFileLocation = processErrorText.substring(processErrorText.lastIndexOf(successPattern)+successPattern.length(), processErrorText.lastIndexOf(")"));
			logger.debug("ExecuteShellCommand: encryptPgpFile: Encrypted File placed at: " + encryptedFileLocation);
			idfConfig.getIdfData().setDatafileDatafileLocation(encryptedFileLocation);		
		}else{
			logger.error("Error in ExecuteShellCommand: encryptPgpFile: Exception has occurred while executing PGP command line encryption: "+processOutputText + processErrorText);
			//moveFileStatus = moveFile(pgpFileName, pgpFilePath + "/with_error_" + pgpFileName);
			moveFileStatus = moveFile(pgpFilePath+"/"+pgpFileName, pgpFilePath + "/with_error_" + pgpFileName);		//Changed on 25th Feb 2016 - Correcting the source file path
			encryptStatus = -1;	
		}
		logger.info("ExecuteShellCommand: encryptPgpFile: Execution Ended");
		return encryptStatus;
	}		

}