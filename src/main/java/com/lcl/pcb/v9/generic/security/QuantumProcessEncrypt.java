/**
 * 
 */
package com.lcl.pcb.v9.generic.security;

import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.lcl.pcb.v9.dao.V9DAOManager;
import com.lcl.pcb.v9.idf.jobflow.config.IDFConfig;
import com.lcl.pcb.v9.idf.jobflow.factory.V9Interface;

/**
 * @author Dev
 *
 */
public class QuantumProcessEncrypt implements V9Interface {
	
	static final Logger logger = LogManager.getLogger(QuantumProcessEncrypt.class.getName());	

	/* (non-Javadoc)
	 * @see com.lcl.pcb.v9.idf.jobflow.factory.V9Interface#execute()
	 */
	@Override
	public void execute() throws Exception {
		// TODO Auto-generated method stub
		logger.info("QuantumProcessEncrypt: execute() method execution - Started");
		V9PgpEncryptDaoManager v9PgpEncryptDaoManager = new V9PgpEncryptDaoManager();
		V9DAOManager db = new V9DAOManager();
		IDFConfig idfConfig = IDFConfig.getInstance(); // singleton to store
		ExecuteShellCommand executeShellCommand = new ExecuteShellCommand();
		int encryptStatus = 1;

		v9PgpEncryptDaoManager.open();
		
		String pgpFullFilePath = idfConfig.getIdfData().getDatafileDatafileLocation();
		logger.debug("QuantumProcessEncrypt: execute() method: pgpFullFilePath: " + pgpFullFilePath);
		String pgpFileName = pgpFullFilePath.substring(pgpFullFilePath.lastIndexOf("/")+1);
		logger.debug("QuantumProcessEncrypt: execute() method: pgpFileName: " + pgpFileName);
		String pgpFilePath = pgpFullFilePath.substring(0, pgpFullFilePath.lastIndexOf("/"));
		logger.debug("QuantumProcessEncrypt: execute() method: pgpFilePath: " + pgpFilePath);
		//encryptStatus = executeShellCommand.v9PgpDecrypt(v9PgpDecryptDaoManager.con, idfConfig.getIdfData().getDecryptPgpProgramDirectory(), pgpFileName, pgpFilePath, idfConfig.getIdfData().getDecryptTempFileLocation());
		encryptStatus = executeShellCommand.v9PgpEncrypt(v9PgpEncryptDaoManager.con, idfConfig.getIdfData().getEncryptPgpProgramDirectory(), pgpFileName, pgpFilePath, idfConfig.getIdfData().getEncryptFileLocation(), idfConfig.getIdfData().getEncryptTempFileLocation());
		
		if(encryptStatus == 0){
			logger.debug("QuantumProcessEncrypt: execute() method: PGP Encryption successful for file: " + pgpFullFilePath);
		}else{
			logger.error("QuantumProcessEncrypt: execute() method: PGP Encryption FAILED for file: " + pgpFullFilePath);
			db.setLevelWarning(1, "Failed to Encrypt data file - " + pgpFullFilePath + " - Resp Code: 0001", 0);		//NEED TO VERIFY THIS LOGIC TO GRACEFULLY END THE PROCESS IN CASE OF A FAILURE
			//System.exit(1);
		}
		logger.info("QuantumProcessEncrypt: execute() method execution - Ended");
	}
	
	@Override
	public void addProperties(Map<String, String> properties) throws Exception {
		// TODO Auto-generated method stub
		
	}
}
