package com.lcl.pcb.v9.generic.loader;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

public class ProcessHandler extends Thread {
	InputStream inputStream;
	String streamType;

	public ProcessHandler(InputStream inputStream, String streamType) {
		this.inputStream = inputStream;
		this.streamType = streamType;
	}

	public void run() {
		try {
			InputStreamReader inpStrd = new InputStreamReader(inputStream);
			BufferedReader buffRd = new BufferedReader(inpStrd);
			String line = null;
			while ((line = buffRd.readLine()) != null) {
				System.out.println(streamType + "::" + line);
			}
			buffRd.close();
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public static void main(String args[]) throws Exception {
		/* For windows setting to cmd.exe */
		String command[] = { "", args.toString() };

		/* executing the command with environments set. */
		Process proc = Runtime.getRuntime().exec(command);
		/* handling the streams so that dead lock situation never occurs. */
		ProcessHandler inputStream = new ProcessHandler(proc.getInputStream(),
				"INPUT");
		ProcessHandler errorStream = new ProcessHandler(proc.getErrorStream(),
				"ERROR");
		/* start the stream threads */
		inputStream.start();
		errorStream.start();
	}
}
