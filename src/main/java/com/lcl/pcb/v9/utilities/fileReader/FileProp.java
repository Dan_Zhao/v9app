/*
 * FileProp.java
 *
 * Created on January 22, 2007, 2:01 PM
 */

package com.lcl.pcb.v9.utilities.fileReader;

import java.util.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.lcl.pcb.v9.dao.V9DAOManager;
import com.lcl.pcb.v9.idf.jobflow.config.Property;
import com.lcl.pcb.v9.utilities.fileReader.segments.BodySegment;
import com.lcl.pcb.v9.utilities.fileReader.segments.FileSegmentBase;
import com.lcl.pcb.v9.utilities.fileReader.segments.HeaderSegment;
import com.lcl.pcb.v9.utilities.fileReader.segments.TrailerSegment;

/**
 * This class Reads the CopyBook.xml/InterfaceLoader.xml file
 * and set the property and values for all segments(Header,Body and Trailer).
 * @author  fasghed
 */
public class FileProp {
	
    private String         segName      = null;
    
	
	private String         fileName     = "";
    private boolean        ebcdic       = false;
    private boolean        binary       = false;
    private boolean        variableSize = false;
    private boolean        delimited    = false;
    //private char           delimiter    = ' ';
    private String           delimiter    = "";
    
    private BodySegment[]  body         = null;
    private HeaderSegment  header       = null;
    private TrailerSegment trailer      = null;
   // private DatabaseProp   dBase        = null;
    
    
    private String dbUrl = "";
    private String dbUser = "";
    private String dbPass = "";
    
    private int    commitPoint = 0;
    private int    batchSize   = 100;
    
    private int loadThreshold=99;// expressed in percentage 
    							 //for comparing current load with previous load, issue an alert if the difference is too high!
    
    public int getLoadThreshold() {
		return loadThreshold;
	}

	public void setLoadThreshold(int loadThreshold) {
		this.loadThreshold = loadThreshold;
	}

    V9DAOManager db = new V9DAOManager();
    Logger log = LogManager.getLogger(FileProp.class);
    /** Creates a new instance of FileProp */
    public FileProp(String fnm) {
        fileName = fnm;
        
    }
    
    public int getCommitPoint() {
        return commitPoint;
    }
    public void setCommitPoint(int n) throws Exception {
        if(n < 0){
        	throw new Exception("CommitPoint should be >= 0");
        	//throw new FileException("CommitPoint should be >= 0");
        }
        commitPoint = n * batchSize;
    }
    public int getBatchsize() {
        return batchSize;
    }
    public void setBatchSize(int n) throws Exception {
        if(n < 0){
        	 throw new Exception("Batch Size should be > 0");
        	//throw new FileException("Batch Size should be > 0");
        }
        commitPoint = commitPoint / batchSize;
        batchSize = n;
        commitPoint = n * batchSize;
    }
    public String getDbUrl() {
        return dbUrl;
    }
    public void setDbUrl(String dburl) {
        dbUrl = "jdbc:oracle:thin:@" + dburl;
    }
    
    public String getDbUser() {
        return dbUser;
    }
    public void setDbUser(String dbuser) {
        dbUser = dbuser;
    }
    public String getDbPass() {
        return dbPass;
    }
    public void setDbPass(String dbpass) {
        dbPass = dbpass;
    }
    public HeaderSegment getHeader() {
        return header;
    }
    public TrailerSegment getTrailer() {
        return trailer;
    }
    public BodySegment[] getBody() {
        return body;
    }
    public BodySegment getBodySegment(int index) {
        if(index < 0 || index >= body.length) return null;
        return body[index];
    }
    public BodySegment getBodySegment(String nm) {
        if(this.getBody() != null) {
            for(int index = 0; index < body.length; index++) {
                if(body[index].getName().equalsIgnoreCase(nm))
                    return body[index];
            }
        }
        return null;
    }
    public FileSegmentBase getFileSegment(String nm) {
        if(this.getHeader() != null && this.getHeader().getName().equalsIgnoreCase(nm)) return this.getHeader();
        FileSegmentBase fs = this.getBodySegment(nm);
        if(fs != null) return fs;
        if(this.getTrailer() != null && this.getTrailer().getName().equalsIgnoreCase(nm)) return this.getTrailer();
        return null;
    }
    public boolean isBinary() {
        return binary;
    }
    public boolean isBinary(boolean b) {
        binary = b;
        return b;
    }
    
    public boolean isVariableSize() {
        return variableSize;
    }
    public boolean isVariableSize(boolean b) {
        variableSize = b;
        return b;
    }
    public String getFileName() {
        return fileName;
    }
    public String getSegName(){ return segName;}
    public boolean isEbcdic() {
        return ebcdic;
    }
    public boolean isEbcdic(boolean b) {
        ebcdic = b;
        return b;
    }
    /*public char getDelimiter() {
        return delimiter;
    }
    public void setDelimiter(char delmtr) {
        if(delmtr == ' ') {
            delimited = false;
            return;
        }
        delimiter = delmtr;
        delimited = true;
    }*/
    public String getDelimiter() {
        return delimiter;
    }
    public void setDelimiter(String delmtr) {
        if("".equalsIgnoreCase(delmtr)) {
            delimited = false;
            return;
        }
        delimiter = delmtr;
        delimited = true;
    }

    public boolean isDelimited() {
        return delimited;
    }
    
    public FileField getField(FileSegmentBase fb, String sName, String fieldName) {
        if(fb != null && fb.getName().equalsIgnoreCase(sName)) {
            FileField ff = fb.getFieldByName(fieldName);
            if(ff != null) return ff;
        }
        return null;
    }
    public FileField getFieldByName(String sName, String fieldName) {
        FileField ff = getField(header, sName, fieldName);
        if(ff != null) return ff;
        for(int k = 0; k < body.length; k++) {
            if((ff = getField(body[k], sName, fieldName)) != null) return ff;
        }
        if((ff = getField(trailer, sName, fieldName)) != null) return ff;
        return null;
    }
    boolean processFileSegment(ArrayList al) throws Exception {
        boolean b = false;
        for(int k = 0; k < al.size(); k++) {
            Property pr = (Property)al.get(k);
            String v = pr.getValue();
            String key = pr.getKey();
            if( key.toUpperCase().startsWith("FILE.") && key.length() > "FILE.".length() && v == null) {
                if(b){
                	log.error("Multiple FILE definition : " + key);
    			    db.setLevelWarning(2, "Multiple FILE definition : " + key , 0);
                	//throw new FileException("Multiple FILE definition : " + key);
                }
                segName = key.substring(5);
                b = true;
            }
        }
        return b;
    }
    
    void AddToBody(BodySegment b) {
        ArrayList a = new ArrayList();
        if(body != null) {
            for(int k = 0; k < body.length; k++) {
                a.add(body[k]);
            }
        }
        a.add(b);
        body = new BodySegment[a.size()];
        for(int k = 0; k < body.length; k++) {
            body[k] = (BodySegment)a.get(k);
        }
    }
    public boolean processFileDefinition(ArrayList al) throws Exception {
        String Prefix = segName + ".";
        ArrayList bodyList = new ArrayList();
        for(int k = 0; k < al.size(); k++) {
        	Property pr = (Property)al.get(k);
            String v = pr.getValue();
            String key = pr.getKey();
            if(key.toUpperCase().startsWith(Prefix.toUpperCase()) && key.length() > Prefix.length()) {
                StringTokenizer stringtokenizer = new StringTokenizer(key, ".");
                int i = stringtokenizer.countTokens();
                if(i != 3) 
                {
                	log.error("Invalid property found."+key);
    			    db.setLevelWarning(2, "Invalid property found."+key, 0);
                	//throw new FileException("Invalid property found " + key);
                }
                String s1 = stringtokenizer.nextToken();
                String s2 = stringtokenizer.nextToken();
                String s3 = stringtokenizer.nextToken();
                if(s2.equalsIgnoreCase("Field")) {
                    if(s3.equalsIgnoreCase("EBCDIC")) this.isEbcdic(v.toUpperCase().startsWith("Y"));
                    else if(s3.equalsIgnoreCase("VariableSize")) this.isVariableSize(v.toUpperCase().startsWith("Y"));
                    //else if(s3.equalsIgnoreCase("DelimiterChar")) this.setDelimiter(FileSegmentBase.ConvertControlChars(v).charAt(0));
                    else if(s3.equalsIgnoreCase("DelimiterChar")) this.setDelimiter(FileSegmentBase.ConvertControlChars(v));
                    else if(s3.equalsIgnoreCase("Binary")) this.isBinary(v.toUpperCase().startsWith("Y"));
                    else 
                    {
                    	log.error("Invalid property " + s3 + " found in " + key);
        			    db.setLevelWarning(2, "Invalid property " + s3 + " found in " + key ,0);
                    	//throw new FileException("Invalid property " + s3 + " found in " + key);
                    }
                }else if(s2.equalsIgnoreCase("Segment")) {
                    if(v.equalsIgnoreCase("HEADER")) {
                        if(header != null){
                        	log.error("Duplicate Header definition : " + v);
            			    db.setLevelWarning(2, "Duplicate Header definition : " + v ,0);
                        	//throw new FileException("Duplicate Header definition : " + v);
                        }
                        header = new HeaderSegment(s3, this);
                        header.processFieldDefinition(al);
                    }
                    else if(v.equalsIgnoreCase("TRAILER")) {
                        if(trailer != null){
                        	log.error("Duplicate Trailer definition : " + v);
            			    db.setLevelWarning(2, "Duplicate Trailer definition : " + v ,0);
                        	//throw new FileException("Duplicate Trailer definition : " + v);
                        }
                        trailer = new TrailerSegment(s3, this);
                        trailer.processFieldDefinition(al);
                        
                    }
                    else if(v.equalsIgnoreCase("BODY")) {
                        BodySegment bd = new BodySegment(s3, this);
                        bodyList.add(bd);
                        AddToBody(bd);
                        bd.processFieldDefinition(al);
                       
                    }
                    else{
                    	log.error("Invalid file property value: " + v + " in " + key);
        			    db.setLevelWarning(2, "Invalid file property value: " + v + " in " + key, 0);
                    	//throw new FileException("Invalid file property value: " + v + " in " + key);
                    }
                }
                else{
                	log.error("Invalid property " + s2 + " found in : " + key);
    			    db.setLevelWarning(2, "Invalid property " + s2 + " found in : " + key, 0);
                	//throw new FileException("Invalid property " + s2 + " found in : " + key );
                }
            }
       }
        if(bodyList.size() == 0){
        	log.error("No definition is found for BODY fields");
		    db.setLevelWarning(2, "No definition is found for BODY fields", 0);
        	//throw new FileException("No definition is found for BODY fields");
        }
        return true;
    }
    
    
    
    public void doLoad() throws Exception {
        try {
        	
            ArrayList<Property> pList = FileUtil.ReadPropsFile(fileName);
            if(!processFileSegment(pList)){
                //throw new FileException("No File definition found");
            	log.error("No File definition found.");
			    db.setLevelWarning(2, "No File definition found.", 0);
            }
            processFileDefinition(pList);
            boolean b = true;
            if(header != null && !header.isValid()) b = false;
            if(trailer != null && !trailer.isValid()) b = false;
            if(body == null) b = false;
            
            else {
                for(int k = 0; k < body.length; k++) {
                    if(!body[k].isValid()) {
                        b = false;
                        break;
                    }
                }
            }
           if(!b) {
            	//throw new FileException("Validation of field definitions failed");
            	log.error("Validation of field definitions failed.");
			    db.setLevelWarning(2, "Validation of field definitions failed.", 0);
            	
            }
        } catch (Exception e) {
            throw new Exception(e);
        }
        
    }
    
}

