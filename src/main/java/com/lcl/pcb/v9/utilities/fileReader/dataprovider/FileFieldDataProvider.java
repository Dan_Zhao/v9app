/*
 * FileFieldDataProvider.java
 *
 * Created on January 27, 2007, 11:14 PM
 */

package com.lcl.pcb.v9.utilities.fileReader.dataprovider;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.io.LineNumberReader;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.lcl.pcb.v9.dao.V9DAOManager;
import com.lcl.pcb.v9.idf.jobflow.config.IDFConfig;
import com.lcl.pcb.v9.idf.jobflow.config.IDFData;

/**
 * This class uses for Reading Data file.
 * @author  fasghed
 */
public class FileFieldDataProvider {
    
    public static final int FIRST_RECORD = 1;
    public static final int BODY_RECORD  = 2;
    public static final int LAST_RECORD  = 3;
    
    private String              dataFileName = "";
    //private BufferedReader      fr           = null;
//    private LineNumberReader      fr           = null;
    private V9LineNumberFileReader      fr           = null;   
    
    private int linesCount = 0;
    int skipTrailer = 0;
    private BufferedReader      frSkipped           = null;
    private BufferedInputStream bis          = null;
    //private long                lineCount    = 0;
    private boolean             isBinary     = false;
    //V9
    private FileLine            lastLine     = new FileLine();
    V9DAOManager db = new V9DAOManager();
    Logger logger = LogManager.getLogger(FileFieldDataProvider.class);
    
    static char[] ByteChar = new char[256];
    static {
        for(int k = 0; k < 256; k++)
            ByteChar[k] = (char)k;
    }
    
    //Logger log = LogManager.getLogger(FileFieldDataProvider.class);
    /** Creates a new instance of FileFieldDataProvider */
    public FileFieldDataProvider(String fname, boolean binary) throws Exception {
        dataFileName = fname;
        isBinary = binary;
        int skipHeader = 0;
        String skippedRecord;
        IDFData idfData = IDFConfig.getInstance().getIdfData();
		String fileParsingCharset=idfData.getFileParsingCharset();
        if(null!=idfData.getSkipHeadRow() && !"".equalsIgnoreCase(idfData.getSkipHeadRow())){
            skipHeader = Integer.parseInt(idfData.getSkipHeadRow());
        }
        if(null!=idfData.getSkipTrailRow() && !"".equalsIgnoreCase(idfData.getSkipTrailRow())){
        	skipTrailer = Integer.parseInt(idfData.getSkipTrailRow());
        }
        try {
            if(isBinary) {
                bis = new BufferedInputStream(new FileInputStream(dataFileName));
            }
            else {
            	
//              LineNumberReader lineNumberReader = new LineNumberReader(new FileReader(fname));    
            	String idfType = idfData.getIdfType();
            	V9LineNumberFileReader lineNumberReader = LineNumberFileReaderFactory.getReader(new FileReader(fname), idfType);
            	
                lineNumberReader.skip(Long.MAX_VALUE);
                linesCount = lineNumberReader.getLineNumber()+1;
                lineNumberReader.close();
                logger.debug("Total No.of Records in file: " + fname + " is: " + linesCount);
                //fr = new BufferedReader(new FileReader(fname));		//Changed to LineNumberReader for skip trailer logic
                //Changed above to below for getting the Record count and using it for skipping trailer rows
                if(null!=fileParsingCharset && !"".equalsIgnoreCase(fileParsingCharset)){			//Secure Code Change
                	
//                    fr = new LineNumberReader(new InputStreamReader(new FileInputStream(fname), fileParsingCharset));	//Charset Change
                    fr = LineNumberFileReaderFactory.getReader(new InputStreamReader(new FileInputStream(fname), fileParsingCharset), idfType);	//Charset Change
                    
    			}else{
    				
//                    fr = new LineNumberReader(new FileReader(fname));		
    				fr = LineNumberFileReaderFactory.getReader(new FileReader(fname), idfType);	
    				
    			}
                //Below Logic Added For Secure Code Skip Header Rows 
                for(int i=0; i<skipHeader; i++){
                	skippedRecord=fr.readLine();
                    logger.debug("Skipping Header Record No.: " + i + " Record is: " + skippedRecord);
        		}
                if(ReadFile() == false){
                	logger.error("Error reading from data file " + fname);
        		    db.setLevelWarning(2, "Error reading from data file " + fname, 0);
                	//throw new FileException("Error reading from data file " + fname);
                }
                lastLine.recordId = FIRST_RECORD;
            }
        }
        catch (Exception e) {
        	e.printStackTrace();
            throw new Exception(e);
        }
        logger.info("Data provider created for file " + fname);
        
    }
    
    public synchronized boolean InError() {
        return lastLine.inError;
    }
    public synchronized boolean AtEnd() {
        return lastLine.atEnd;
    }
    public synchronized String getErrorMsg() {
        return lastLine.erMsg;
    }
    public synchronized long getLineCount() {
        return lastLine.lineNum;
    }
    public synchronized void setLineNum(long n) {
        if(isBinary) lastLine.lineNum = n;
    }
    
    public synchronized boolean getNextLine(FileLine fl, int begin, int len ) throws Exception {
        boolean b = false;
        // if(!fl.dirty) return true;
        if(isBinary) b = ReadFile(fl, begin, len);
        else b = getNextLineAscii(fl);
        fl.dirty = !b;
        return b;
    }
    public synchronized boolean getNextLine(FileLine fl, int len ) throws Exception {
        boolean b = false;
        if(!fl.dirty) return true;
        if(isBinary) b = ReadFile(fl, 0, len);
        else b = getNextLineAscii(fl);
        fl.dirty = !b;
        return b;
    }
    
    private synchronized boolean ReadFile(FileLine fl,  int begin, int end ) throws Exception {
        try {
            if(!isBinary) {
                lastLine.inError = true;
                lastLine.erMsg = "File was opened in TEXT mode";
                return false;
            }
            if(lastLine.inError) {
                fl.lineStr = null;
                fl.atEnd   = false;
                fl.inError = true;
                fl.erMsg = lastLine.erMsg;
                return false;
            }
            if(lastLine.atEnd) {
                fl.lineStr = null;
                fl.atEnd   = true;
                fl.inError = false;
                fl.erMsg   = "";
                return false;
            }
            fl.atEnd     = lastLine.atEnd;
            fl.inError   = lastLine.inError;
            fl.recordId  = lastLine.recordId;
            fl.lineNum   = lastLine.lineNum;
            // fl.lineStr   = lastLine.lineStr;
            int n = end - begin;
            fl.bytesInLineByte = begin;
            if(n > 0) {
                int m = 0;
                if(fl.lineByte.length < end) {
                    lastLine.inError = true;
                    lastLine.erMsg = "Byte buffer overflow";
                    fl.inError = lastLine.inError;
                    fl.erMsg = lastLine.erMsg;
                    return false;
                }
                m = bis.read(fl.lineByte, begin, n);
                if(m == -1) {
                    lastLine.atEnd = true;
                    fl.atEnd = true;
                    return true;
                }
                fl.lineStr = "";
                for(int t = 0; t < begin + m; t++) fl.lineStr += (char)(fl.lineByte[t]);
                if(m != n){
                    logger.error("Early end of file");
        		    db.setLevelWarning(2, "Early end of file", 0);
        		    //throw new FileException("Early end of file");
                }
                return true;
            }
            return true;
        } catch (Exception e) {
            throw new Exception(e);
        }
    }
    
    private synchronized boolean ReadFile() {
        if(isBinary) {
            lastLine.inError = true;
            lastLine.erMsg   = "File is opened in BINARY mode";
            return false;
        }
        while(true) {
            try {
                lastLine.lineStr = fr.readLine();
                //Added below linesCount count logic for skipping Trailer rows
                if(lastLine.lineStr == null || fr.getLineNumber()==(linesCount+1-skipTrailer)) {
                    lastLine.atEnd = true;
                    return true;
                }
                else {
                    if(lastLine.lineStr.trim().length() != 0 ) {
                        lastLine.lineNum += 1L;
                        return true;
                    }
                }
            }
            catch (Exception e) {
                lastLine.inError = true;
                lastLine.erMsg = e.getMessage();
                logger.error("Exception : " + lastLine.erMsg);
                e.printStackTrace();
                return false;
            }
        }
    }
    private synchronized boolean getNextLineAscii(FileLine fl) {
        if(isBinary) {
            fl.inError = true;
            fl.erMsg   = "File is opened in BINARY mode";
            return false;
        }
        if(lastLine.inError) {
            fl.lineStr = null;
            fl.atEnd   = false;
            fl.inError = true;
            fl.erMsg = lastLine.erMsg;
            return false;
        }
        if(lastLine.atEnd) {
            fl.lineStr = null;
            fl.atEnd   = true;
            fl.inError = false;
            fl.erMsg   = "";
            return true;
        }
        fl.atEnd     = lastLine.atEnd;
        fl.inError   = lastLine.inError;
        fl.recordId  = lastLine.recordId;
        fl.lineNum   = lastLine.lineNum;
        fl.lineStr   = lastLine.lineStr;
        if(!ReadFile()) {
            fl.lineStr = null;
            fl.atEnd   = lastLine.atEnd;
            fl.inError = lastLine.inError;
            fl.erMsg   = lastLine.erMsg;
            return false;
        }
        if(lastLine.atEnd) {
            lastLine.recordId = LAST_RECORD;
            fl.recordId = lastLine.recordId;
            return true;
        }
        lastLine.recordId = BODY_RECORD;
        fl.clear();
        return true;
    }

    
    public void Close() {
        try {
            if(isBinary) bis.close();
            else fr.close();
        } catch (Exception ex) {
        	logger.info("Exception " + ex.getMessage());
            return;
        }
    }
}
