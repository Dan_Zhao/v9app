package com.lcl.pcb.v9.utilities;

import java.io.PrintStream;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.Logger;

public class LoggerPrintStream extends PrintStream {

	private LoggerPrintStream(final Logger logger, final Level level) {
		super(new LoggerOutputStream(logger, level), false);
	}

	public static LoggerPrintStream of(final Logger logger, final Level level) {
		return new LoggerPrintStream(logger, level);
	}
	public static LoggerPrintStream ofAll(final Logger logger) {
		return of(logger, Level.ALL);
	}
	public static LoggerPrintStream ofTrace(final Logger logger) {
		return of(logger, Level.TRACE);
	}
	public static LoggerPrintStream ofDebug(final Logger logger) {
		return of(logger, Level.DEBUG);
	}
	public static LoggerPrintStream ofInfo(final Logger logger) {
		return of(logger, Level.INFO);
	}
	public static LoggerPrintStream ofWarn(final Logger logger) {
		return of(logger, Level.WARN);
	}
	public static LoggerPrintStream ofError(final Logger logger) {
		return of(logger, Level.ERROR);
	}
	public static LoggerPrintStream ofFatal(final Logger logger) {
		return of(logger, Level.FATAL);
	}
	public static LoggerPrintStream ofNone(final Logger logger) {
		return of(logger, Level.OFF);
	}

}
