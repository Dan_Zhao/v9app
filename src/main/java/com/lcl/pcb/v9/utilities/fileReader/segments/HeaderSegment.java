package com.lcl.pcb.v9.utilities.fileReader.segments;

import com.lcl.pcb.v9.utilities.fileReader.FileProp;

/**
 * This class uses for HeaderSegment creation.
 * Inherits from FileSegmentBase 
 * 
 */
public class HeaderSegment extends FileSegmentBase {
    
    /** Creates a new instance of HeaderSegment */
    public HeaderSegment(String nm, FileProp p) throws Exception {
        super(nm, HEADER_SEGMENT, p);
    }
    
}
