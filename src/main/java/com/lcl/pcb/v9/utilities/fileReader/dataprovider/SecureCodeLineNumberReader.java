package com.lcl.pcb.v9.utilities.fileReader.dataprovider;

import java.io.IOException;
import java.io.Reader;
import java.util.Scanner;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class SecureCodeLineNumberReader implements V9LineNumberFileReader {
    Logger logger = LogManager.getLogger(SecureCodeLineNumberReader.class);
    
	protected String lock="";
	Scanner scanner = null;
	
	/** The current line number */
	private int lineNumber = 0;

	public SecureCodeLineNumberReader(Reader in) {
		/**
		 * 
		 */
		try {
			scanner = new Scanner(in);
			scanner.useDelimiter("(?<!\r)\n");
		
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Set the current line number.
	 *
	 * @param  lineNumber
	 *         An int specifying the line number
	 *
	 * @see #getLineNumber
	 */
	public void setLineNumber(int lineNumber) {
		this.lineNumber = lineNumber;
	}

	/**
	 * Get the current line number.
	 *
	 * @return  The current line number
	 *
	 * @see #setLineNumber
	 */
	public int getLineNumber() {
		return lineNumber;
	}


	public String readLine() throws IOException {
		synchronized (lock) {
			String l = null;
			if	(scanner.hasNext()) l = scanner.next();
			if (l != null)
				lineNumber++;

			return l;
		}
	}

	public long skip(long n) throws IOException {

		if (n < 0) 
			throw new IllegalArgumentException("skip() value is negative");
		
		String lineRead = "";
		long lineSkipped = 0;
		
		synchronized (lock) {
			while (lineSkipped < n && scanner.hasNext()) {

				lineRead =	scanner.next();
				if (lineRead!=null) {
					lineNumber++;
				}
			}
		}

		logger.debug("requestedNumberOfLinesSkipped="+n);
		logger.debug("lineNumber="+lineNumber);
		
		return lineNumber;
	}

	public void close() throws IOException {
		scanner.close();
	}

}
