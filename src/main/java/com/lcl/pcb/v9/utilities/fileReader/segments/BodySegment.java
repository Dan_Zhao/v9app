/*
 * BodySegment.java
 *
 * Created on January 26, 2007, 3:08 PM
 */

package com.lcl.pcb.v9.utilities.fileReader.segments;

import com.lcl.pcb.v9.utilities.fileReader.FileProp;


/**
 * This class uses for BodySegments creation.
 * Inherits from FileSegmentBase 
 * @author  fasghed
 */
public class BodySegment extends FileSegmentBase {
    
    /** Creates a new instance of BodySegment */
    public BodySegment(String nm, FileProp p) throws Exception {
        super(nm, BODY_SEGMENT, p);
    }
    
}
