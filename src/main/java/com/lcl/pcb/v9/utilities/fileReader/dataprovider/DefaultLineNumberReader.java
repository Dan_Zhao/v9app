package com.lcl.pcb.v9.utilities.fileReader.dataprovider;

import java.io.IOException;
import java.io.LineNumberReader;
import java.io.Reader;

public class DefaultLineNumberReader extends LineNumberReader implements V9LineNumberFileReader {

	public DefaultLineNumberReader(Reader in) {
		super(in);
	}
	
	public long skip(long n) throws IOException {
		return super.skip(n);
	}
    
    public int getLineNumber() {
    	return super.getLineNumber();
    }

    public void close() throws IOException {
    	super.close();
    }
    
    public String readLine() throws IOException {
    	return super.readLine();
    }
}
