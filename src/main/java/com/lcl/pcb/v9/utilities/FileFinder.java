package com.lcl.pcb.v9.utilities;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class FileFinder {
	
	private FileFinder() {
		throw new UnsupportedOperationException("instantiation is disabled");
	}

	public static Path findUnique(final Path directory, final String pattern) throws IOException {
		boolean found = false;
		Path foundFile = null;
		try ( DirectoryStream<Path> files = Files.newDirectoryStream(directory, pattern) ) {
			for( Path file : files ) {
				if( found ) {
					throw new IllegalStateException("Multiple files exist. Provide unique file pattern");
				}
				foundFile = file;
				found = true;
			}
		}
		
		if( !found ) {
			throw new IllegalStateException("No file exists. Provide unique file pattern");
		} else {
			System.out.printf("Pattern: %s, Found File: %s%n", pattern, foundFile); // NOSONAR logger might not have been initiated yet.
			return foundFile;
		}
	}
	public static List<Path> findAll(final Path dir, final String pattern) throws IOException {
		final List<Path> foundFiles = new ArrayList<>();
		
		try ( DirectoryStream<Path> files = Files.newDirectoryStream(dir, pattern) ) {
			for( Path file : files ) {
				foundFiles.add(file);
			}
		}
		
		return foundFiles;
	}
}
