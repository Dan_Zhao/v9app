package com.lcl.pcb.v9.utilities;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.logging.log4j.Logger;

public class CommandUtils {
	private static final AtomicInteger THREAD_COUNTER = new AtomicInteger(0);
	
	private CommandUtils() {
		throw new UnsupportedOperationException("instantiation is disabled");
	}

	/**
	 * Runs the command on console.
	 * 
	 * @param command
	 *            the command to execute.
	 * @return the return_code returned by the command/process.
	 * @throws IOException
	 *             as thrown by the process API while running the program.
	 * @throws InterruptedException
	 *             if current thread is interrupted while we're waiting for the
	 *             process in the command to end and return the return value.
	 */
	public static int run(final String command) throws IOException, InterruptedException { //NOSONAR intentional
		return run(command, System.out, System.err); // NOSONAR logger may not have been initialized.
	}
	
	public static int run(final String command, final Logger logger) throws IOException, InterruptedException { //NOSONAR intentional
		return run(command, LoggerPrintStream.ofInfo(logger), LoggerPrintStream.ofError(logger));
	}
	
	public static int run(final String command, final PrintStream outErr) throws IOException, InterruptedException { //NOSONAR intentional
		return run(command, outErr, outErr);
	}
	
	public static int run(final String command, final PrintStream out, final PrintStream err) throws IOException, InterruptedException { //NOSONAR intentional
		final List<Thread> streamListeners = new LinkedList<>();
		final int returnCode;
		try {
			Process process = Runtime.getRuntime().exec(command);
			streamListeners.add(listen(process.getInputStream(), out));
			streamListeners.add(listen(process.getErrorStream(), err));
			returnCode = process.waitFor();
		} finally {
			tryWait(streamListeners, err);
		}
		return returnCode;
	}

	public static boolean tryWait(final List<Thread> threads, final PrintStream err) {
		boolean noErrors = true;
		if( threads != null ) {
			for( Thread thread : threads ) {
				noErrors = tryWait(thread, err) && noErrors;
			}
		}
		return noErrors;
	}
	public static boolean tryWait(final Thread thread, final PrintStream err) {
		boolean noError;
		
		if( thread != null ) {
			try {
				thread.join();
				noError = true;
			} catch( InterruptedException e ) { // NOSONAR the interrupted thread not being used anymore. we're out of it.
				err.println("Unable to wait for the thread: " + thread.getName());
				e.printStackTrace(err);
				noError = false;
			}
		} else {
			noError = true;
		}
		
		return noError;
	}
	
	/**
	 * Initially created to be used by {@link #run(String)} method to create a
	 * listener on the console for both standard output and error outputs.
	 * standard outputs are written to the provided stream. However, it can be
	 * utilized in different manners as well.
	 * 
	 * @param stream
	 *            the {@link InputStream} to read from.
	 * @param printTo
	 *            the {@link PrintStream} level to use.
	 */
	private static Thread listen(final InputStream stream, final PrintStream printTo) {
		Thread listener = new Thread(new Runnable() {
			@Override
			public void run() {
				try( BufferedReader input = new BufferedReader(new InputStreamReader(stream)) ) {
					String line;
					while( (line = input.readLine()) != null ) {
						printTo.println(line);
						printTo.flush();
					}
				} catch (IOException e) { // NOSONAR logging through the provided print stream
					printTo.println("Unable to read the console stream!");
					e.printStackTrace(printTo);
				}
			}
		}, String.format("Console::Logger.%d", THREAD_COUNTER.incrementAndGet()));
		listener.start();
		return listener;
	}
}
