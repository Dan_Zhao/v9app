/*
 * FileField.java
 *
 * Created on January 20, 2007, 9:30 PM
 */

package com.lcl.pcb.v9.utilities.fileReader;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.lcl.pcb.v9.dao.V9DAOManager;
import com.lcl.pcb.v9.utilities.fileReader.segments.FileSegmentBase;

import java.sql.Types;


/**
 * This class stores the CopyBook.xml/InterfaceLoder.xml file values. 
 * @author  fasghed
 */
public class FileField {
	public static boolean isFromVO_80ALoader=false;

	private String  name           = "";
    private int     type           = -1;
    private int     totSize        = 0;
    private int     decimal        = 0;
    private boolean signed         = false;
    private boolean imported       = true;
    private String  format         = "";
    private boolean ignoreSpaces   = false;
    private boolean mandatory      = false;
    private String constant       = "";
    
    private String dateFormat     = "";
    
    private boolean  saveInDB       = false;
    private String   dbColumnName   = "";
    private int      dbColumnType   = -1;
    private String   dbColumnFormat = "";
    private String   dbTableName    = "";
    
    // Calculated Fields
    private int sizeInFile       = 0;
    
    // Data Fields
    private String rawData       = "";   // Data as read from file
    private String value         = "";   // Converted Data
    private static String schema        = "";	 // Schema Name
    private FileSegmentBase owner = null;
    private boolean isValidated  = false;
    
    static int[] CompSizes = new int[] {0, 1, 1, 2, 2, 4, 4, 4, 4, 4, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8};
    
    V9DAOManager db = new V9DAOManager();
    Logger log = LogManager.getLogger(FileField.class);
    
    /*public static void schemaCheck(TableDef dbt){
    	
		if (dbt.getParent().getSchema().length() != 0)
			schema = dbt.getParent().getSchema();
		 }*/
    public FileField(String nm, FileSegmentBase o) throws Exception {
        setName(nm);
        owner = o;
    }
    
    public boolean CanIgnoreSpaces() {
        return ignoreSpaces;
    }
    
    public boolean CanIgnoreSpaces(boolean b) {
        ignoreSpaces = b;
        return ignoreSpaces;
    }
    public FileSegmentBase getOwner() {
        return owner;
    }
    public String getName() {
        return name;
    }
    private void setName(String nm)  throws Exception {
        if(nm != null) name = nm.toUpperCase();
        else{
        	log.error("Name cannot be null");
		    db.setLevelWarning(2, "Name cannot be null", 0);
        	//throw new FileException("Name cannot be null");
        }
        isValidated = false;
    }
    
    public int getType() {
        return type;
    }
    public void setType(String typenm) throws Exception {
        if(typenm != null) {
            int k = FileUtil.getTypeValue(typenm.toUpperCase());
            if(k == -1){
            	log.error("Undefined type " + typenm);
            	db.setLevelWarning(2, "Undefined type " + typenm, 0);
            	//throw new FileException("Undefined type " + typenm);
            }
            type = k;
        }
        else{
        	log.error("Column type cannot be null");
        	db.setLevelWarning(2, "Column type cannot be null", 0);
        	//throw new FileException("Column type cannot be null");
        }
        isValidated = false;
        //if(isValidated) Validate();
    }
    public int getTotSize() {
        return totSize;
    }
    public void setTotSize(int n) throws Exception {
        if(n <= 0){
        	log.error("Invalid total size : " + n);
        	db.setLevelWarning(2, "Invalid total size : " + n, 0);
        	//throw new FileException("Invalid total size : " + n);
        }
        totSize = n;
        isValidated = false;
    }
    
    public int getDecimal() {
        return decimal;
    }
    public void setDecimal(int dec)  throws Exception {
        if(dec < 0 || dec > totSize){
        	log.error("Invalild decimal size " + dec);
        	db.setLevelWarning(2, "Invalild decimal size " + dec, 0);
        	//throw new FileException("Invalild decimal size " + dec);
        }
        decimal = dec;
        isValidated = false;
    }
    
    public boolean getSigned() {
        return signed;
    }
    
    public void setSigned(boolean sign) {
        signed = sign;
    }
    public boolean getImported() {
        return imported;
    }
    public void setImported(boolean imprt) {
        imported = imprt;
    }
    public boolean isMandatory() {
        return mandatory;
    }
    public boolean isMandatory(boolean m) {
        mandatory = m;
        return m;
    }
    public String getConstant() {
        return constant;
    }
    public void setConstant(String s) {
        if(s == null) constant = "";
        else constant = s;
    }
    public String getFormat() {
        return format;
    }
    public void setFormat(String fmt) throws Exception {
        if(fmt == null) format = "";
        else {
            format = fmt;
            dateFormat = FileUtil.ValidateDateFormat(fmt);
        }
        isValidated = false;
    }
    public String getDateFormat() {
        return dateFormat;
    }
    
    public int getSizeInFile() {
        if(!isValidated) Validate();
        return sizeInFile;
    }
    
    private void setSizeInFile() throws Exception {
        if(type == FileUtil.COBOL_COMP3_TYPE) sizeInFile = (totSize >> 1) + 1;
        else if(type == FileUtil.COBOL_COMP_TYPE) {
            if(totSize > 0 && totSize < CompSizes.length - 1) sizeInFile = CompSizes[totSize];
            else{
            	log.error("Invalid size '" + totSize + " 'for field " + name);
            	db.setLevelWarning(2, "Invalid size '" + totSize + " 'for field " + name, 0);
            	//throw new FileException("Invalid size '" + totSize + " 'for field " + name);
            }
        }
        else sizeInFile = totSize;
    }
    
    public boolean getSaveInDB() {
        return saveInDB;
    }
    public void setSaveInDB(boolean b) {
        saveInDB = b;
        isValidated = false;
    }
    public String  getDbColumnName() {
        return dbColumnName;
    }
    public void setDbColumnName(String colName) {
        if(colName != null) dbColumnName = colName.toUpperCase();
        else dbColumnName = "";
        isValidated = false;
    }
    
    public int getDbColumnType() {
        return dbColumnType;
    }
    public void setDbColumnType(String s) throws Exception {
        if(s != null) {
            int k = FileUtil.getTypeValue(s.toUpperCase());
            if(k == -1){
            	throw new Exception("Undefined type " + s);
            	//throw new FileException("Undefined type " + s);
            }
            dbColumnType = k;
        }
        else dbColumnType = -1;
        isValidated = false;
    }
    public String getDbColumnFormat() {
        return dbColumnFormat;
    }
    public void setDbColumnFormat(String fmt) {
        if(fmt != null) dbColumnFormat = fmt.toUpperCase();
        isValidated = false;
    }
    public String getDbTableName() {
        return dbTableName;
    }
    public void setTableName(String tblNm) {
        if(tblNm != null) dbTableName = tblNm.toUpperCase();
        isValidated = false;
    }
    
    public String getRawData() {
        return rawData;
    }
    
    public void setRawData(String data) throws Exception {
        rawData = data;
        value = FileUtil.ConvertRawData(data, this);
        //System.out.println("value: "+value);
        if(isFromVO_80ALoader)
        	value=value.trim();
    }

    public void clearRawData() {
        rawData = "";
        value = "";
    }
    public void setRawData(byte[] b) throws Exception {
        String data = "";
        if(b != null && b.length != 0) {
            for(int k = 0; k < b.length; k++) {
                data += (char)b[k];
                
            }
        }
        setRawData(data);
    }
    
    public boolean validateData(String b) throws Exception {
        setRawData(b);
        if(isMandatory() && getValue().length() == 0) return false;
        if(getConstant().length() != 0) {
            if(!getValue().equals(getConstant())) return false;
        }
        return true;
    }
    
    public boolean validateData(byte[] b) throws Exception {
        setRawData(b);
        if(isMandatory() && getValue().length() == 0) return false;
        if(getConstant().length() != 0) {
            if(!getValue().equals(getConstant())) return false;
        }
        return true;
    }
    
    public String getValue() {
        return value;
    }
    
     
    
    public Object getDbValue() {
    	
    	if(schema.equalsIgnoreCase("ts2")){
    		return value;
    	}else{ 
    		if(value.trim().length() == 0)
        	{
        	return null;
        	}
        else
        	return value;
    	}
    }
    public int getDbType() {
        if(type == FileUtil.STRING_TYPE || type == FileUtil.CONSTANT_STRING_TYPE) return Types.VARCHAR;
        if(type == FileUtil.DATE_TYPE) return Types.TIMESTAMP;
        return Types.DECIMAL;
    }
    
    public boolean IsValidated() {
        return isValidated;
    }
    
    public int Validate() {
        int b = 0;
        isValidated = false;
        try {
            setSizeInFile();
        }
        catch (Exception e) {
            b |= 0x0200;
        }
        if(getName() == null) b |= 0x0001;
        if(getType() == -1)
            b |= 0x0002;
        if(getTotSize() <= 0)
            b |= 0x0004;
        if(getDecimal() < 0 || getDecimal() > getTotSize()) b |= 0x0008;
        int t = getType();
        if((t == FileUtil.NUMBER_TYPE || t == FileUtil.COBOL_COMP_TYPE || t == FileUtil.COBOL_COMP3_TYPE || t == FileUtil.COBOL_NUMBER_TYPE) && getTotSize() > CompSizes.length)  b |= 0x0010;
        if(t == FileUtil.DATE_TYPE && getDateFormat().length() == 0 ) b |= 0x0020;
        if(getSaveInDB()) {
            if(getDbColumnName().length() == 0) b |= 0x0040;
            if(getDbColumnType() == -1)   b |= 0x0080;
            if(getDbColumnFormat().length() == 0 && getDbColumnType() == FileUtil.DATE_TYPE) b |= 0x0100;
            if(getDbTableName().length() == 0)   b |= 0x0400;
        }
        if(b == 0) isValidated = true;
        return b;
    }
}
