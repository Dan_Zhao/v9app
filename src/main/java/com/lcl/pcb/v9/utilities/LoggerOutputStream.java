package com.lcl.pcb.v9.utilities;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.Logger;

public class LoggerOutputStream extends ByteArrayOutputStream {

	private static final String EMPTY_STRING = "";
	private static final char CR = '\r';
	private static final char LF = '\n';
	
	private final Logger logger;
	private final Level level;

	public LoggerOutputStream(final Logger logger, final Level level) {
		this(logger, level, 256);
	}
	public LoggerOutputStream(final Logger logger, final Level level, final int size) {
		super(size);
		this.logger = requireNotNull(logger, "Logger");
		this.level = requireNotNull(level, "Logging Level");
	}
	
	@Override
	public void flush() throws IOException {
		logger.log(level, trimEndingCRLF(this.toString()));
		super.flush();
		super.reset();
	}
	
	private static String trimEndingCRLF(final String line) {
		// return if no contents
		if( line == null || line.isEmpty() ) {
			return line;
		}
		
		// find the last index that excludes all CR & LF characters.
		int lastIndex = line.length();
		char charAtIndex;
		do {
			charAtIndex = line.charAt(--lastIndex);
		} while( (charAtIndex == CR || charAtIndex == LF) && lastIndex > 0 );
		
		// fill the excess deficiency
		if( charAtIndex != CR && charAtIndex != LF ) {
			lastIndex++;
		}
		
		// return based on output
		return lastIndex == 0 ? EMPTY_STRING :
			(lastIndex == line.length() ? line :
				line.substring(0, lastIndex));
	}
	
	private static <T> T requireNotNull(final T object, final String message) {
		if( object == null ) {
			throw new NullPointerException(message);
		}
		return object;
	}

}