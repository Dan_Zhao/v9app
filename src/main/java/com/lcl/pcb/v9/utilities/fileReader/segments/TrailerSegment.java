/*
 * TrailerSegment.java
 *
 * Created on January 26, 2007, 3:10 PM
 */

package com.lcl.pcb.v9.utilities.fileReader.segments;

import com.lcl.pcb.v9.utilities.fileReader.FileProp;

/**
 * This class uses for HeaderSegment creation.
 * Inherits from FileSegmentBase
 * @author  fasghed
 */
public class TrailerSegment extends FileSegmentBase {
    
    /** Creates a new instance of TrailerSegment */
    public TrailerSegment(String nm, FileProp p) throws Exception {
        super(nm, TRAILER_SEGMENT, p);
    }
    
}
