/*
 * FileLine.java
 *
 * Created on January 27, 2007, 11:22 PM
 */

package com.lcl.pcb.v9.utilities.fileReader.dataprovider;

/**
 *
 * @author  fasghed
 */
public class FileLine {
    
    public String lineStr  = "";
    public byte[] lineByte = null;
    public boolean atEnd   = false;
    public boolean inError = false;;
    public long lineNum    = 0;
    public long lines      = 0;
    public String erMsg    = "";
    public int status      = 0;
    public int recordId    = 0;
    public boolean dirty   = false;
    
    public int maxLineByteSize = 0;
    public int bytesInLineByte = 0;
    /** Creates a new instance of FileLine */
    public FileLine() {
    }

    public void clear() {
        atEnd   = false;
        inError = false;
    }
}
