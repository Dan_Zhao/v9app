package com.lcl.pcb.v9.utilities.audit;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.lcl.pcb.v9.generic.loader.V9ValidationDaoManager;
import com.lcl.pcb.v9.idf.jobflow.config.IDFConfig;

/**
 * 
 * This class implements the getting the UUID and inserting and updating the
 * Audit record values.
 * 
 */

public class AuditTableUtil {

	static Logger logger = LogManager.getLogger(AuditTableUtil.class);
	static IDFConfig idfConfig = IDFConfig.getInstance();
	public static String schemaname= idfConfig.getIdfData().getCsvloadDbSchema();

	/**
	 * This method is used to insert the Audit record.
	 * 
	 * @param auditPojo
	 *            This is the parameter of AuditTablePojo
	 * @return AuditTablePojo This returns AuditTablePojo.
	 */
	public static AuditTablePojo insertAudit(AuditTablePojo auditPojo)
			throws SQLException {

		int insertCount = 0;

		StringBuilder sb = new StringBuilder("");

		sb.append("INSERT INTO "+schemaname+".LOAD_RESULT ");
		sb.append("(");
		sb.append("SOURCE_INTERFACE");
		sb.append(",");
		sb.append("SOURCE_NM");
		sb.append(",");
		sb.append("SOURCE_SIZE");
		sb.append(",");
		sb.append("SOURCE_END_DT_PREV");
		sb.append(",");
		sb.append("SOURCE_END_DT_CURR");
		sb.append(",");
		sb.append("SOURCE_ID");
		sb.append(",");
		sb.append("SOURCE_DTL_REC");
		sb.append(",");
		sb.append("SCHEMA_NM");
		sb.append(",");
		sb.append("TABLE_NM");
		sb.append(",");
		sb.append("PROCESS_NM");
		sb.append(",");
		sb.append("PROCESS_NUM");
		sb.append(",");
		sb.append("TL_REC_LOAD");
		sb.append(")");
		sb.append(" ");
		sb.append("VALUES");
		sb.append("('");
		sb.append(auditPojo.getSourceInterface().trim());
		sb.append("','");
		sb.append(auditPojo.getSourceNM().trim());
		sb.append("','");
		sb.append(auditPojo.getSourceSize().trim());
		sb.append("',");
		sb.append(auditPojo.getSourceEndDtPrev());
		sb.append(",");
		sb.append(auditPojo.getSourceEndDtCurr());
		sb.append(",'");
		sb.append(auditPojo.getSourceFileId().trim());
		sb.append("','");
		sb.append(auditPojo.getSourceDtlRec().trim());
		sb.append("','");
		sb.append(auditPojo.getSchemaName().trim());
		sb.append("','");
		sb.append(auditPojo.getTableName().trim());
		sb.append("','");
		sb.append(auditPojo.getProcessName().trim());
		sb.append("','");
		sb.append(auditPojo.getProcessNumber().trim());
		sb.append("','");
		sb.append(auditPojo.getTotalRecordLoad().trim());
		sb.append("')");
		insertCount = V9ValidationDaoManager.execNonQuery(sb.toString());
		sb = null;

		if (insertCount == 1)
			auditPojo = getUUID(auditPojo);

		return auditPojo;

	}

	/**
	 * This method is used to get the UUID value.
	 * 
	 * @param auditPojo
	 *            This is the parameter of AuditTablePojo
	 * @return AuditTablePojo This returns AuditTablePojo.
	 */
	public static AuditTablePojo getUUID(AuditTablePojo auditPojo)
			throws SQLException {

		StringBuilder sb = new StringBuilder("");
		sb.append("SELECT LOAD_RESULT_UUID FROM "+schemaname+".LOAD_RESULT WHERE ");
		sb.append("TABLE_NM = '");
		sb.append(auditPojo.getTableName().trim());
		sb.append("' AND PROCESS_NUM = ");
		sb.append(auditPojo.getProcessNumber().trim());
		ResultSet rs = V9ValidationDaoManager.execQuery(sb.toString());
		if (rs.next())
			auditPojo.setLoadResultUUID(rs.getString("LOAD_RESULT_UUID"));
		else
			logger.debug("UUID not found");
			
		rs = null;
		sb = null;

		return auditPojo;
	}

	/**
	 * This method is used to update Audit record.
	 * 
	 * @param auditPojo
	 *            This is the parameter of AuditTablePojo
	 * @return int This returns the updated rows.
	 */
	public static int updateAudit(AuditTablePojo auditPojo) throws SQLException {

		int rowsUpdated = 0;

		StringBuilder sb = new StringBuilder("");
		sb.append("UPDATE "+schemaname+".LOAD_RESULT SET TL_REC_LOAD = ");
		sb.append(auditPojo.getTotalRecordLoad());
		sb.append(",");
		sb.append("LOAD_ERRORS = ");
		sb.append(auditPojo.getLoadError());
		sb.append(" WHERE LOAD_RESULT_UUID = '");
		sb.append(auditPojo.getLoadResultUUID());
		sb.append("'");
		
		rowsUpdated = V9ValidationDaoManager.execNonQuery(sb.toString());

		return rowsUpdated;

	}

	/**
	 * This method is used to get the Table Name.
	 * 
	 * @param ctlFileName
	 *            This is the parameter of ctlFileName
	 * @return String This returns the table name.
	 */
	public static String getTableName(String ctlFileName) {
		String line = null;
		String lineArray[] = null;
		String tableName = null;
		try {
			File srcFile = new File(ctlFileName);
			FileReader fr = new FileReader(srcFile);

			BufferedReader br = new BufferedReader(fr);
			while ((line = br.readLine()) != null) {

				if (line.trim().startsWith("-"))
					continue;

				if (line.trim().toUpperCase().contains("TABLE")) {
					lineArray = line.split(" ");
					tableName = lineArray[2];
					if (tableName.indexOf(".") > 0)
					{
						tableName = tableName.substring(tableName.indexOf(".")+1);
					}
					break;
				}
			}

			fr.close();
			br.close();

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		return tableName;
	}

	/**
	 * This method is used to create dynamic ctl file for LOAD_RESULT_UUID.
	 * 
	 * @param auditTableMap
	 *            This is the first parameter to create dynamic ctl file.
	 * @param ctlFileName
	 *            This is the second parameter to create dynamic ctl file.
	 * @return String This returns the ctl file name.
	 */
	public static String rewriteCtlFile(
			HashMap<String, AuditTablePojo> auditTableMap, String ctlFileName)
			throws Exception {

		String tempCtlFile = ctlFileName;
		/*String tempCtlFileName = tempCtlFile.substring(0,
				tempCtlFile.lastIndexOf(".") - 1)
				+ "_temp.ctl";*/
		String tempCtlFileName = tempCtlFile.substring(0,
				tempCtlFile.lastIndexOf("."))
				+ "_temp.ctl";
		String tableName = getTableName(ctlFileName);
		String uuid = auditTableMap.get(tableName).getLoadResultUUID();
		String replaceValue = "LOAD_RESULT_UUID CONSTANT \"" + uuid + "\"";
		String replaceString = "LOAD_RESULT_UUID";

		String line = null;

		try {
			File srcFile = new File(ctlFileName);
			File tempFile = new File(tempCtlFileName);
			FileReader fr = new FileReader(srcFile);
			FileWriter fw = new FileWriter(tempFile);

			BufferedReader br = new BufferedReader(fr);
			BufferedWriter bw = new BufferedWriter(fw);
			while ((line = br.readLine()) != null) {

				if (line.trim().equalsIgnoreCase(replaceString)) {
					line = line.replace(replaceString, replaceValue);
				}
				bw.write(line);
				bw.newLine();
				bw.flush();

			}
			fr.close();
			br.close();
			fw.close();
			bw.close();

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return tempCtlFileName;
	}
}
