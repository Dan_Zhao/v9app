package com.lcl.pcb.v9.utilities.audit;

/**
 * 
 * This class contains getter and setter methods of AuditTable.
 * 
 */
public class AuditTablePojo {

	private String loadResultUUID;
	private String sourceInterface;
	private String sourceNM;
	private String sourceSize;
	//private String sourceEndDtPrev = "SYSDATE";//Added for Instant Insurance
	private String sourceEndDtPrev;
	private String sourceEndDtCurr = "SYSDATE";
	private String sourceFileId = "FILE_ID";
	private String sourceDtlRec;
	private String loadDbName;
	private String schemaName;
	private String tableName;
	private String processName = "V9APP";
	private String processNumber;
	private String totalRecordLoad = "0";
	private String loadError;
	private String recordLoadDt;
	private String recordLoadChangeId;

	public String getLoadResultUUID() {
		return loadResultUUID;
	}

	public void setLoadResultUUID(String loadResultUUID) {
		this.loadResultUUID = loadResultUUID;
	}

	public String getSourceInterface() {
		return sourceInterface;
	}

	public void setSourceInterface(String sourceInterface) {
		this.sourceInterface = sourceInterface;
	}

	public String getSourceNM() {
		return sourceNM;
	}

	public void setSourceNM(String sourceNM) {
		this.sourceNM = sourceNM;
	}

	public String getSourceSize() {
		return sourceSize;
	}

	public void setSourceSize(String sourceSize) {
		this.sourceSize = sourceSize;
	}

	public String getSourceEndDtPrev() {
		return sourceEndDtPrev;
	}

	public void setSourceEndDtPrev(String sourceEndDtPrev) {
		this.sourceEndDtPrev = sourceEndDtPrev;
	}

	public String getSourceEndDtCurr() {
		return sourceEndDtCurr;
	}

	public void setSourceEndDtCurr(String sourceEndDtCurr) {
		this.sourceEndDtCurr = sourceEndDtCurr;
	}

	public String getSourceFileId() {
		return sourceFileId;
	}

	public void setSourceFileId(String sourceFileId) {
		this.sourceFileId = sourceFileId;
	}

	public String getSourceDtlRec() {
		return sourceDtlRec;
	}

	public void setSourceDtlRec(String sourceDtlRec) {
		this.sourceDtlRec = sourceDtlRec;
	}

	public String getLoadDbName() {
		return loadDbName;
	}

	public void setLoadDbName(String loadDbName) {
		this.loadDbName = loadDbName;
	}

	public String getSchemaName() {
		return schemaName;
	}

	public void setSchemaName(String schemaName) {
		this.schemaName = schemaName;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public String getProcessName() {
		return processName;
	}

	public void setProcessName(String processName) {
		this.processName = processName;
	}

	public String getProcessNumber() {
		return processNumber;
	}

	public void setProcessNumber(String processNumber) {
		this.processNumber = processNumber;
	}

	public String getTotalRecordLoad() {
		return totalRecordLoad;
	}

	public void setTotalRecordLoad(String totalRecordLoad) {
		this.totalRecordLoad = totalRecordLoad;
	}

	public String getLoadError() {
		return loadError;
	}

	public void setLoadError(String loadError) {
		this.loadError = loadError;
	}

	public String getRecordLoadDt() {
		return recordLoadDt;
	}

	public void setRecordLoadDt(String recordLoadDt) {
		this.recordLoadDt = recordLoadDt;
	}

	public String getRecordLoadChangeId() {
		return recordLoadChangeId;
	}

	public void setRecordLoadChangeId(String recordLoadChangeId) {
		this.recordLoadChangeId = recordLoadChangeId;
	}
}
