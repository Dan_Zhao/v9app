package com.lcl.pcb.v9.utilities.fileReader.dataprovider;

import java.io.IOException;

public interface V9LineNumberFileReader {
    /**
     * Skip characters.
     *
     * @param  n
     *         The number of characters for DefaultReader or lines for CustomReader to skip
     *
     * @return  The number of characters actually skipped
     *
     * @throws  IOException
     *          If an I/O error occurs
     *
     * @throws  IllegalArgumentException
     *          If <tt>n</tt> is negative
     */
    public long skip(long n) throws IOException;
    
    /**
     * Get the current line number.
     *
     * @return  The current line number
     *
     * @see #setLineNumber
     */
    public int getLineNumber() ;
    
    /**
     * close file
     */
    public void close() throws IOException;
    
    /**
     * Read a line of text.  Whenever a <a href="#lt">line terminator</a> is
     * read the current line number is incremented.
     *
     * @return  A String containing the contents of the line, not including
     *          any <a href="#lt">line termination characters</a>, or 
     *          <tt>null</tt> if the end of the stream has been reached
     *
     * @throws  IOException
     *          If an I/O error occurs
     */
    public String readLine() throws IOException ;

}
