package com.lcl.pcb.v9.utilities.fileReader;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.regex.Pattern;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


import com.lcl.pcb.v9.dao.V9DAOManager;
import com.lcl.pcb.v9.generic.exception.FileException;
import com.lcl.pcb.v9.idf.jobflow.V9Client;
import com.lcl.pcb.v9.idf.jobflow.config.BField;
import com.lcl.pcb.v9.idf.jobflow.config.CopyBookData;
import com.lcl.pcb.v9.idf.jobflow.config.CopyBookData.Body.BSegmentField;
import com.lcl.pcb.v9.idf.jobflow.config.HField;
import com.lcl.pcb.v9.idf.jobflow.config.IDFConfig;
import com.lcl.pcb.v9.idf.jobflow.config.Property;
import com.lcl.pcb.v9.idf.jobflow.config.TField;
import com.lcl.pcb.v9.utilities.fileReader.dataprovider.FileFieldDataProvider;
import com.lcl.pcb.v9.utilities.fileReader.dataprovider.FileLine;
import com.lcl.pcb.v9.utilities.fileReader.segments.FileSegmentBase;

/**
 * This utility class is used to store all ebcdic related operations. It parse
 * the EBCDIC data and converted to ASCII. It will create the all segments CSV
 * files.
 */
public class FileUtil {

	String copyBookFile = "";
	String dataFile = "";
	// private boolean validated = false;
	// private boolean interfaceValidation = false;
	// private int segTotalSize = 0;
	// private int segSizeInFile = 0;

	public static final Charset IBM037 = Charset.forName("IBM037");

	public static final int NO_SEGMENT_LOGIC = 1;

	public static final int NOT_RIGHT_SEGMENT = 0;
	public static final int RIGHT_SEGMENT = 2;

	public static final int NOT_END_OF_RECORD = 0;
	public static final int END_OF_RECORD = 4;

	public static final int DISCARD_DATA = 8;
	public static final int DISCARD_DATA_NOT_END = NOT_END_OF_RECORD
			+ DISCARD_DATA;
	public static final int DISCARD_DATA_END = END_OF_RECORD + DISCARD_DATA;

	public boolean Debug = false;
	
	public static int headerTrailerCount=0;

	ArrayList headerTrailerCsvList = new ArrayList();
	// ArrayList headerList = new ArrayList();
	ArrayList bodyList = new ArrayList();
	// ArrayList trailerList = new ArrayList();
	String delimeterPattern = "";
	String seqNameVal = "";
	String csvNamePattern = "";
	String csvFileLocationVal = "";
	String interfaceName = "";
	String jobseqflag ="";
	String seqNum = "";

	String bCSVNameVal = "";
	String bCSVName = "";
	String bSegCSVName = "";

	String htCSVNameVal = "";
	String htCSVName = "";
	String headerTrailerCSVName = "";
	String multiHeaderTrailerFlag ="";

	static V9DAOManager db = new V9DAOManager();

	ArrayList<String> csvFileNamesList = new ArrayList<String>();
	public static ArrayList<String> totalCSVFileNamesList = new ArrayList<String>();

	public static ArrayList<String> getTotalCSVFileNamesList() {
		return totalCSVFileNamesList;
	}

	public static void setTotalCSVFileNamesList(
			ArrayList<String> totalCSVFileNamesList) {
		FileUtil.totalCSVFileNamesList = totalCSVFileNamesList;
	}

	public static HashMap<String, Integer> totalRecParsedList = new HashMap<String, Integer>();

	public static HashMap<String, Integer> getTotalRecParsedList() {
		return totalRecParsedList;
	}

	public static void setTotalRecParsedList(
			HashMap<String, Integer> totalRecParsedList) {
		FileUtil.totalRecParsedList = totalRecParsedList;
	}

	public static int bodySegLength = 0;

	public static int getBodySegLength() {
		return bodySegLength;
	}

	public static void setBodySegLength(int bodySegLength) {
		FileUtil.bodySegLength = bodySegLength;
	}

	String configFile = "";
	// String dataFile = "";

	String dbUser = "";
	String dbPass = "";
	String dbUrl = ""; // in the form LCLNTD35:1521:X049
	int maxError = 0; // 0 means abort on error
	int commitPoint = 0; // 0 means commit at the end
	int batchSize = 1;
	String badDir = "";
	String logDir = "";
	String propertyFile = "";
	// SegmentLogic segLogic = null;

	int loadThreshold = 99;

	int minReadSize = 0;

	String[] configPara = new String[] { "DBUSER", "DBPASS", "DBURL",
			"COMMITPOINT", "MAXERROR", "BADDIR", "LOGDIR", "PROPERTYFILE",
			"SEGMENTLOGIC", "LOADTHRESHOLD" };

	boolean autoAdvance = true;
	boolean needCleanup = true;
	boolean clearHeader = false;

	public static final int STRING_TYPE = 0;
	public static final int NUMBER_TYPE = 1;
	public static final int COBOL_COMP_TYPE = 2;
	public static final int COBOL_COMP3_TYPE = 3;
	public static final int COBOL_NUMBER_TYPE = 4;
	public static final int DATE_TYPE = 5;
	public static final int CONSTANT_STRING_TYPE = 6;
	// public static final int SEGMENT_TYPE = 9;

	static Pattern fieldNamePattern = Pattern
			.compile("^[a-zA-Z][a-zA-z0-9_]*$");
	static HashMap typeNames = new HashMap();
	static HashMap typeValues = new HashMap();
	static {
		typeNames.put(new Integer(STRING_TYPE), "STRING");
		typeNames.put(new Integer(NUMBER_TYPE), "NUMBER");
		typeNames.put(new Integer(COBOL_COMP_TYPE), "COMP");
		typeNames.put(new Integer(COBOL_COMP3_TYPE), "COMP3");
		typeNames.put(new Integer(COBOL_NUMBER_TYPE), "COBOL_NUMBER");
		typeNames.put(new Integer(DATE_TYPE), "DATE");
		// typeNames.put(new Integer(SEGMENT_TYPE), "SEGMENT");
		typeNames.put(new Integer(CONSTANT_STRING_TYPE), "CONSTANT");

		typeValues.put("STRING", new Integer(STRING_TYPE));
		typeValues.put("NUMBER", new Integer(NUMBER_TYPE));
		typeValues.put("COMP", new Integer(COBOL_COMP_TYPE));
		typeValues.put("COMP3", new Integer(COBOL_COMP3_TYPE));
		typeValues.put("COBOL_NUMBER", new Integer(COBOL_NUMBER_TYPE));
		typeValues.put("DATE", new Integer(DATE_TYPE));
		// typeValues.put("SEGMENT", new Integer(SEGMENT_TYPE));
		typeValues.put("CONSTANT", new Integer(CONSTANT_STRING_TYPE));

	};

	public static final String HexVals = "0123456789ABCDEF";

	static Logger log = LogManager.getLogger(V9Client.class.getName());

	public FileUtil(String copybook, String data) throws Exception {
		copyBookFile = copybook;
		setDataFile(data);
	}

	public void setDataFile(String datafile) throws Exception {
		if (datafile == null || datafile.trim().length() == 0) {
			log.error("Data file name cannot be blank");
			db.setLevelWarning(2, "Data file name cannot be blank", 0);
			// throw new FileException("Data file name cannot be blank");
		}
		File fl = new File(datafile);
		if (!fl.exists()) {
			log.error("Data file '" + datafile + "' does not exist");
			db.setLevelWarning(2,
					"Data file '" + datafile + "' does not exist", 0);
			// throw new FileException("Data file '" + datafile+
			// "' does not exist");
		}
		if (!fl.isFile()) {
			log.error("Data file '" + datafile + "' is not a normal file");
			db.setLevelWarning(2, "Data file '" + datafile
					+ "' is not a normal file", 0);
			// throw new FileException("Data file '" + datafile+
			// "' is not a normal file");
		}
		dataFile = datafile.trim();
	}

	public static ArrayList<Property> ReadPropsFile(String fileName)
			throws Exception {
		// BufferedReader br = null;
		// int lineNum = 0;
		ArrayList<Property> al = new ArrayList<Property>();
		// int flag = 0;
		JAXBContext jb;
		// String hSegmentName = "";
		CopyBookData copyBookAttributes = null;
		CopyBookData.Header hdr = null;
		CopyBookData.Header.HSegmentField hsf = null;
		ArrayList<HField> headerList = null;
		CopyBookData.Body bdr = null;
		ArrayList<BSegmentField> bodySegList = null;
		CopyBookData.Trailer tdr = null;
		CopyBookData.Trailer.TSegmentField tsf = null;
		ArrayList<TField> trailerList = null;
		try {

			jb = JAXBContext.newInstance(CopyBookData.class);
			Unmarshaller jaxBUnmarshaller = jb.createUnmarshaller();
			copyBookAttributes = (CopyBookData) jaxBUnmarshaller
					.unmarshal(new File(fileName));

			
			hdr = copyBookAttributes.getHeader();
			if(hdr != null){
				hsf = hdr.gethSegmentField();
				headerList = hsf.gethField();
			}

			bdr = copyBookAttributes.getBody();
			bodySegList = bdr.getbSegmentField();

			//There is no trailer in the xml file.
			tdr = copyBookAttributes.getTrailer();
			if(tdr != null){
				tsf = tdr.gettSegmentField();
				trailerList = tsf.gettField();
			}

			al.add(new Property(
					"File." + copyBookAttributes.getInterfaceName(), null));
			/*al.add(new Property(copyBookAttributes.getInterfaceName()
					+ ".Field." + "Ebcdic", copyBookAttributes.getEbcdic()));*/
			String filePropType = IDFConfig.getInstance().getIdfData().getFilePropType();

			if(!(filePropType.equalsIgnoreCase("")) && filePropType.equalsIgnoreCase("EBCDIC")){
				al.add(new Property(copyBookAttributes.getInterfaceName()
						+ ".Field." + "Ebcdic", copyBookAttributes.getEbcdic()));
				
			}/*else{
				al.add(new Property(copyBookAttributes.getInterfaceName()
						+ ".Field." + "DelimiterChar", copyBookAttributes.getDelimiterChar()));
			}*/
			if(copyBookAttributes.getDelimiterChar()!=null && !(copyBookAttributes.getDelimiterChar().isEmpty())){
				al.add(new Property(copyBookAttributes.getInterfaceName()
					+ ".Field." + "DelimiterChar", copyBookAttributes.getDelimiterChar()));
			}
			al.add(new Property(copyBookAttributes.getInterfaceName()
					+ ".Field." + "VariableSize", copyBookAttributes.getVariableSize()));
			al.add(new Property(copyBookAttributes.getInterfaceName()
					+ ".Field." + "Binary", copyBookAttributes.getBinary()));

			if (headerList != null) {
				al.add(new Property(copyBookAttributes.getInterfaceName()
						+ ".Segment." + "Header", copyBookAttributes
						.getHeader().gethName()));
				for (HField headerVal : headerList) {

					if (copyBookAttributes.getHeader().gethName() != null) {

						al.add(new Property(copyBookAttributes.getHeader()
								.gethName()
								+ ".Field."
								+ headerVal.getHFieldName() + ".mandatory",
								headerVal.getHFieldMandatory()));
						al.add(new Property(copyBookAttributes.getHeader()
								.gethName()
								+ ".Field."
								+ headerVal.getHFieldName() + ".type",
								headerVal.getHFieldType()));
						al.add(new Property(copyBookAttributes.getHeader()
								.gethName()
								+ ".Field."
								+ headerVal.getHFieldName() + ".totalSize",
								headerVal.getHFieldSize()));
						al.add(new Property(copyBookAttributes.getHeader()
								.gethName()
								+ ".Field."
								+ headerVal.getHFieldName() + ".decimal",
								headerVal.getHFieldDecimal()));
						al.add(new Property(copyBookAttributes.getHeader()
								.gethName()
								+ ".Field."
								+ headerVal.getHFieldName() + ".signed",
								headerVal.getHFieldSigned()));
						al.add(new Property(copyBookAttributes.getHeader()
								.gethName()
								+ ".Field."
								+ headerVal.getHFieldName() + ".import",
								headerVal.getHFieldImport()));
						al.add(new Property(copyBookAttributes.getHeader()
								.gethName()
								+ ".Field."
								+ headerVal.getHFieldName() + ".format",
								headerVal.getHFieldFormat()));

					}
				}
			}

			// body
			if (bodySegList != null) {

				for (BSegmentField bodySegVal : bodySegList) {
					al.add(new Property(copyBookAttributes.getInterfaceName()
							+ ".Segment." + bodySegVal.getbSegmentName(),
							copyBookAttributes.getBody().getbName()));

					for (BField bodyVal : bodySegVal.getbField()) {

						al.add(new Property(bodySegVal.getbSegmentName()
								+ ".Field." + bodyVal.getBFieldName()
								+ ".mandatory", bodyVal.getBFieldMandatory()));
						al.add(new Property(
								bodySegVal.getbSegmentName() + ".Field."
										+ bodyVal.getBFieldName() + ".type",
								bodyVal.getBFieldType()));
						al.add(new Property(bodySegVal.getbSegmentName()
								+ ".Field." + bodyVal.getBFieldName()
								+ ".totalSize", bodyVal.getBFieldSize()));
						al.add(new Property(bodySegVal.getbSegmentName()
								+ ".Field." + bodyVal.getBFieldName()
								+ ".decimal", bodyVal.getBFieldDecimal()));
						al.add(new Property(bodySegVal.getbSegmentName()
								+ ".Field." + bodyVal.getBFieldName()
								+ ".signed", bodyVal.getBFieldSigned()));
						al.add(new Property(bodySegVal.getbSegmentName()
								+ ".Field." + bodyVal.getBFieldName()
								+ ".import", bodyVal.getBFieldImport()));

					}

				}
			}
			// Trailer
			if (trailerList != null) {
				al.add(new Property(copyBookAttributes.getInterfaceName()
						+ ".Segment." + "Trailer", copyBookAttributes
						.getTrailer().gettName()));
				for (TField trailerVal : trailerList) {

					al.add(new Property(copyBookAttributes.getTrailer()
							.gettName()
							+ ".Field."
							+ trailerVal.getTFieldName() + ".mandatory",
							trailerVal.getTFieldMandatory()));
					al.add(new Property(copyBookAttributes.getTrailer()
							.gettName()
							+ ".Field."
							+ trailerVal.getTFieldName() + ".type", trailerVal
							.getTFieldType()));
					al.add(new Property(copyBookAttributes.getTrailer()
							.gettName()
							+ ".Field."
							+ trailerVal.getTFieldName() + ".totalSize",
							trailerVal.getTFieldSize()));
					al.add(new Property(copyBookAttributes.getTrailer()
							.gettName()
							+ ".Field."
							+ trailerVal.getTFieldName() + ".decimal",
							trailerVal.getTFieldDecimal()));
					al.add(new Property(copyBookAttributes.getTrailer()
							.gettName()
							+ ".Field."
							+ trailerVal.getTFieldName() + ".signed",
							trailerVal.getTFieldSigned()));
					al.add(new Property(copyBookAttributes.getTrailer()
							.gettName()
							+ ".Field."
							+ trailerVal.getTFieldName() + ".import",
							trailerVal.getTFieldImport()));

				}
			}
			return al;
		} catch (Exception e) {
			log.error("Error occurred, while preparing the header,body and trailer lists.",e);
			db.setLevelWarning(2, "Error occurred, while preparing the header,body and trailer lists.", 0);
			throw new Exception(e);
			
		}
	}

	public void process(String copyBookLocation, String dataFileLoacation)
			throws Exception {
		FileProp fp = null;
		FileFieldDataProvider ffdp = null;
		boolean error = false;
		String errorMsg = "";
		try {
			fp = new FileProp(copyBookLocation);
			fp.doLoad();
			/*
			 * if (this.getSegLogic() != null)
			 * this.getSegLogic().Initialize(fp);
			 */
			ffdp = new FileFieldDataProvider(dataFileLoacation, fp.isBinary());
			ProcessDataFile(fp, ffdp);
		} catch (Exception ex) {
			errorMsg = ex.getMessage();
			log.error(errorMsg);
			error = true;
			db.setLevelWarning(2, errorMsg,0);
		}
		String fmt = "0000000";
		if (fp.getHeader() != null) {
			log.info("Header records parsed    = "
					+ FileUtil.FormatNumber(fp.getHeader()
							.getParsedRecordCount(), fmt)
					+ " processed = "
					+ FileUtil.FormatNumber(fp.getHeader()
							.getProcessedRecordCount(), fmt)
					+ " error  = "
					+ FileUtil.FormatNumber(fp.getHeader()
							.getErrorRecordCount(), fmt));
		}
		for (int m = 0; m < fp.getBody().length; m++) {
			log.info(fp.getBodySegment(m).getName()
					+ " records parsed = "
					+ FileUtil.FormatNumber(fp.getBodySegment(m)
							.getParsedRecordCount(), fmt)
					+ " processed = "
					+ FileUtil.FormatNumber(fp.getBodySegment(m)
							.getProcessedRecordCount(), fmt)
					+ " error  = "
					+ FileUtil.FormatNumber(fp.getBodySegment(m)
							.getErrorRecordCount(), fmt));
			int parsedBodyCount = Integer.parseInt(FileUtil.FormatNumber(fp
					.getBodySegment(m).getParsedRecordCount(), fmt));
			totalRecParsedList.put(fp.getBodySegment(m).getName(),
					parsedBodyCount);
		}

		bodySegLength = fp.getBody().length;

		if (fp.getTrailer() != null) {
			log.info("Trailer records parsed    = "
					+ FileUtil.FormatNumber(fp.getTrailer()
							.getParsedRecordCount(), fmt)
					+ " processed = "
					+ FileUtil.FormatNumber(fp.getTrailer()
							.getProcessedRecordCount(), fmt)
					+ " error  = "
					+ FileUtil.FormatNumber(fp.getTrailer()
							.getErrorRecordCount(), fmt));

			int parsedTrailerCount = Integer.parseInt(FileUtil.FormatNumber(
					fp.getTrailer().getParsedRecordCount(), fmt));

			totalRecParsedList.put(fp.getTrailer().getName(),
					parsedTrailerCount);

		}
		if (error){
			log.error(errorMsg);
			db.setLevelWarning(2, errorMsg, 0);
			throw new Exception(errorMsg);
		}
	}

	public void ProcessDataFile(FileProp fp, FileFieldDataProvider ffdp)
			throws Exception {
		FileLine fl = new FileLine();
		try {
			minReadSize = setupFileLine(fp, fl);
			fl.dirty = true;
			/*
			 * if (fp.getDao() != null) fp.InitializeTables();
			 */
			if (fp.getHeader() != null) {
				if (!ffdp.getNextLine(fl, getMinReadSize()))
					throw new FileException(fl.erMsg);
				if (!SegmentBeforeParse(fp, fp.getHeader(), fl)) {
					log.error("Header sgement not found or has an error");
					db.setLevelWarning(2,
							"Header sgement not found or has an error", 0);
					// throw new
					// FileException("Header sgement not found or has an error");
				}
				if (!SegmentAfterParse(fp, fp.getHeader(), fl, ffdp)) {
					log.error("Header sgement After Parse logic has an error");
					db.setLevelWarning(2,
							"Header sgement After Parse logic has an error", 0);
					// throw new
					// FileException("Header sgement After Parse logic has an error");
				}
			}
			while (true) {
				boolean dataMapped = false;
				if (fp.getTrailer() != null)
					fp.getTrailer().ClearValues(); // hasData(false);
				if (getNeedCleanup()) {
					for (int m = 0; m < fp.getBody().length; m++)
						fp.getBodySegment(m).ClearValues(); // hasData(false);
				}
				if (fp.isBinary())
					ffdp.setLineNum(ffdp.getLineCount() + 1);
				if (!ffdp.getNextLine(fl, getMinReadSize())) {
					log.error(fl.erMsg);
					db.setLevelWarning(2, fl.erMsg, 0);
					// throw new FileException(fl.erMsg);
				}
				if (fl.atEnd)
					break;

				if (fp.getTrailer() != null && isTrailer(fl)) {
					dataMapped = true;
					if (!SegmentAfterParse(fp, fp.getTrailer(), fl, ffdp))
						break;
				} else {
					int m = 0;
					while (m < fp.getBody().length) {
						/*
						 * if (!SegmentBeforeParse(fp, fp.getBodySegment(m),
						 * fl)) { if (++m < fp.getBody().length) continue;
						 * break;
						 */
						
						if (!fp.getBodySegment(m).getName()
								.equalsIgnoreCase(getRecType(fl.lineStr))) {
							
							if (++m < fp.getBody().length)
								continue;
							break;

						}
						// }
						dataMapped = true;
						if (SegmentAfterParse(fp, fp.getBodySegment(m), fl,
								ffdp))
							break;
						if (getAutoAdvance())
							++m;
						if (!ffdp.getNextLine(fl, getMinReadSize())) {
							log.error(fl.erMsg);
							db.setLevelWarning(2, fl.erMsg, 0);
							// throw new FileException(fl.erMsg);
						}
						if (fl.atEnd)
							break;
						continue;
					}
				}

				if (!dataMapped) {
					log.error("Data read could not be mapped to any segment-Length mismatch between data file and Property file");
					db.setLevelWarning(2,
							"Data read could not be mapped to any segment-Length mismatch between data file and Property file", 0);
					// throw new
					// FileException("Data read could not be mapped to any segment");
				}
				/*
				 * if (fp.getDao() != null) fp.InsertInDb();
				 */
				if (getClearHeader() && fp.getHeader() != null)
					fp.getHeader().hasData(false);
			}
			/*
			 * if (this.getSegLogic() != null) this.getSegLogic().Finalize(fp);
			 * if (fp.getDao() != null) fp.getDao().commitAndcloseBatchInsert();
			 */
			log.info("LineNum = " + fl.lineNum);
		} catch (Exception ex) {
			log.error(ex.getMessage(),ex);
			db.setLevelWarning(2, "EBCDIC Parsing Failed.", 0);
			try {
				/*
				 * if (fp.getDao() != null)
				 * fp.getDao().rollbackAndcloseBatchInsert();
				 */
			} catch (Exception ex2) {
			}
			throw new Exception(ex);
		} finally {
			if (ffdp != null) {
				try {
					ffdp.Close();
				} catch (Exception e) {
				}
			}
		}
	}

	public boolean SegmentBeforeParse(FileProp fp, FileSegmentBase fs,
			FileLine fl) throws Exception {
		/*
		 * if (this.getSegLogic() == null) return true;
		 */
		// int segResult = this.getSegLogic().BeforeParsing(fp, fs, fl);
		int segResult = BeforeParsing(fp, fs, fl);
		return ((RIGHT_SEGMENT & segResult) != 0);
	}

	public int BeforeParsing(FileProp fp, FileSegmentBase fs, FileLine fl) {
		String headerRecordType=IDFConfig.getInstance().getIdfData().getHeaderRecordType();
		String hRecordType="";
		int hRecStartPositionVal=0;
		int hRecEndPositionVal=0;
		String tRecordType="";
		int tRecStartPositionVal=0;
		int tRecEndPositionVal=0;
		String filePropType = IDFConfig.getInstance().getIdfData().getFilePropType();
		String hRecStartPosition = IDFConfig.getInstance().getIdfData().getHeaderRecTypeStartPosition();
		String hRecEndPosition = IDFConfig.getInstance().getIdfData().getHeaderRecTypeEndPosition();
		String trailerRecordType=IDFConfig.getInstance().getIdfData().getTrailerRecordType();
		String tRecStartPosition = IDFConfig.getInstance().getIdfData().getTrailerRecTypeStartPosition();
		String tRecEndPosition = IDFConfig.getInstance().getIdfData().getTrailerRecTypeEndPosition();
		String hName = IDFConfig.getInstance().getIdfData().gethName();
		String tName = IDFConfig.getInstance().getIdfData().gettName();
		if(!(hRecStartPosition.equalsIgnoreCase("")) && !(hRecEndPosition.equalsIgnoreCase(""))){
			hRecStartPositionVal = Integer.parseInt(hRecStartPosition);
			hRecEndPositionVal = Integer.parseInt(hRecEndPosition);
		}
			
		if(!(filePropType.equalsIgnoreCase("")) && filePropType.equalsIgnoreCase("EBCDIC")){
			hRecordType = "" + fl.lineStr.substring(hRecStartPositionVal, hRecEndPositionVal);
			hRecordType = FileUtil.GetAscii(hRecordType, FileUtil.STRING_TYPE);
		}
		else{
			hRecordType = "" + fl.lineStr.substring(hRecStartPositionVal, hRecEndPositionVal);
		}
				
		if(!(tRecStartPosition.equalsIgnoreCase("")) && !(tRecEndPosition.equalsIgnoreCase(""))){
			tRecStartPositionVal = Integer.parseInt(tRecStartPosition);
			tRecEndPositionVal = Integer.parseInt(tRecEndPosition);
		}
				
		if(!(filePropType.equalsIgnoreCase("")) && filePropType.equalsIgnoreCase("EBCDIC")){
			tRecordType = "" + fl.lineStr.substring(tRecStartPositionVal, tRecEndPositionVal);
			tRecordType = FileUtil.GetAscii(tRecordType, FileUtil.STRING_TYPE);
		}
		else{
			tRecordType = "" + fl.lineStr.substring(tRecStartPositionVal, tRecEndPositionVal);
		}
		
		if (fs.getName().equalsIgnoreCase(hName)
				&& hRecordType.equalsIgnoreCase(headerRecordType)) {
			return RIGHT_SEGMENT;
		}
		if (fs.getName().equalsIgnoreCase(tName)
				&& tRecordType.equalsIgnoreCase(trailerRecordType)) {
			return RIGHT_SEGMENT;
		}

		return NOT_RIGHT_SEGMENT;

	}

	public boolean SegmentAfterParse(FileProp fp, FileSegmentBase fs,
			FileLine fl, FileFieldDataProvider ffdp) throws Exception {
		if (fp.isBinary()) {
			if (!ffdp.getNextLine(fl, getMinReadSize(), fs.getSizeInFile()))
				throw new FileException(fl.erMsg);
		}

		if (!fs.Parse(fl)) {
			log.error("Error parsing " + fs.getName());
			db.setLevelWarning(2, "Error parsing " + fs.getName(), 0);
			// throw new FileException("Error parsing " + fs.getName());
		}

		int segResult = AfterParsing(fp, fs, fl);
		if ((segResult & DISCARD_DATA) == 0)
			fs.hasData(true);
		ShowData(fs);
		try {
			totalCSVFileNamesList = CSVData(fs, fp);
		} catch (Exception e) {
			log.error("Generating the CSV data Failed.");
			db.setLevelWarning(3, "Generating the CSV data Failed.", 0);
		}

		return ((segResult & END_OF_RECORD) != 0);
	}

	public int AfterParsing(FileProp fp, FileSegmentBase fs, FileLine fl) {

		return END_OF_RECORD;
	}

	public boolean isTrailer(FileLine fl) {
		String recType = "";
		int tRecStartPositionVal=0;
		int tRecEndPositionVal=0;
		String trailerRecType = "";
		String filePropType = IDFConfig.getInstance().getIdfData().getFilePropType();
		String tRecStartPosition = IDFConfig.getInstance().getIdfData().getTrailerRecTypeStartPosition();
		String tRecEndPosition = IDFConfig.getInstance().getIdfData().getTrailerRecTypeEndPosition();
		if(!(tRecStartPosition.equalsIgnoreCase("")) && !(tRecEndPosition.equalsIgnoreCase(""))){
			tRecStartPositionVal = Integer.parseInt(tRecStartPosition);
			tRecEndPositionVal = Integer.parseInt(tRecEndPosition);
		}
				
		if(!(filePropType.equalsIgnoreCase("")) && filePropType.equalsIgnoreCase("EBCDIC")){
			recType = "" + fl.lineStr.substring(tRecStartPositionVal, tRecEndPositionVal);
			recType = FileUtil.GetAscii(recType, FileUtil.STRING_TYPE);
		}
		else{
			recType = "" + fl.lineStr.substring(tRecStartPositionVal, tRecEndPositionVal);
		}
		
		trailerRecType = IDFConfig.getInstance().getIdfData().getTrailerRecordType();
		if (!(trailerRecType.equalsIgnoreCase("")) && recType.equalsIgnoreCase(trailerRecType)) {
			return true;
		}
		return false;
	}

	private String getRecType(String str) {
		String result = "";
		String typ = "";
		int bRecStartPositionVal=0;
		int bRecEndPositionVal=0;
		int bSingleRecStartPositionVal=0;
		int bSingleRecEndPositionVal=0;
		String bRecStartPosition = "";
		String bRecEndPosition = "";
		String bSingleRecStartPosition ="";
		String bSingleRecEndPosition = "";
		String multipleBodySegments = "";
		String filePropType = "";
		String bodySegmentName = "";
		String singleBodySegPosition ="";
		try {
			bRecStartPosition = IDFConfig.getInstance().getIdfData().getBodyRecTypeStartPosition();
			bRecEndPosition = IDFConfig.getInstance().getIdfData().getBodyRecTypeEndPosition();
			if(!(bRecStartPosition.equalsIgnoreCase("")) && !(bRecEndPosition.equalsIgnoreCase(""))){
				bRecStartPositionVal = Integer.parseInt(bRecStartPosition);
				bRecEndPositionVal = Integer.parseInt(bRecEndPosition);
			}
			
			bSingleRecStartPosition = IDFConfig.getInstance().getIdfData().getSingleBodySegStartPosition();
			bSingleRecEndPosition = IDFConfig.getInstance().getIdfData().getSingleBodySegEndPosition();
			if(!(bSingleRecStartPosition.equalsIgnoreCase("")) && !(bSingleRecEndPosition.equalsIgnoreCase(""))){
				bSingleRecStartPositionVal = Integer.parseInt(bSingleRecStartPosition);
				bSingleRecEndPositionVal = Integer.parseInt(bSingleRecEndPosition);
			}
			multipleBodySegments = IDFConfig.getInstance().getIdfData().getMultipleBodySegments();
			filePropType = IDFConfig.getInstance().getIdfData().getFilePropType();
			
			bodySegmentName = IDFConfig.getInstance().getIdfData().getBodySegmentName();
			singleBodySegPosition = IDFConfig.getInstance().getIdfData().getSingleBodySegPosition();
						
			if (!(multipleBodySegments.equalsIgnoreCase("")) && multipleBodySegments.equalsIgnoreCase("Y"))
			{
				typ = str.substring(bRecStartPositionVal, bRecEndPositionVal);
								
			}
			else {
				if(!(singleBodySegPosition.equalsIgnoreCase("")) && singleBodySegPosition.equalsIgnoreCase("Y")){
					typ = str.substring(bSingleRecStartPositionVal, bSingleRecEndPositionVal);
				}else if(bodySegmentName!=null){
					typ = bodySegmentName;
			     }
			}
			
			if(!(filePropType.equalsIgnoreCase("")) && filePropType.equalsIgnoreCase("EBCDIC"))
				typ = FileUtil.GetAscii(typ, FileUtil.STRING_TYPE);
			
			result = typ;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public int getMinReadSize() {
		return minReadSize;
	}

	public void setMinReadSize(int n) throws Exception {
		if (n <= 0) {
			log.error("Minimum read length cannot be <= 0");
			db.setLevelWarning(2, "Minimum read length cannot be <= 0", 0);
			// throw new FileException("Minimum read length cannot be <= 0");
		}
		minReadSize = n;
	}

	int getConfigParaIndex(String str) {
		for (int k = 0; k < configPara.length; k++) {
			if (str.equalsIgnoreCase(configPara[k]))
				return k;
		}
		return -1;
	}

	public String getDataFile() {
		return dataFile;
	}

	public String getDbUser() {
		return dbUser;
	}

	public void setDbUser(String dbuser) throws Exception {
		if (dbuser == null || dbuser.trim().length() == 0) {
			log.error("Database user id cannot be blank");
			db.setLevelWarning(2, "Database user id cannot be blank", 0);
			// throw new FileException("Database user id cannot be blank");
		}
		dbUser = dbuser.trim();
	}

	public String getDbPass() {
		return dbPass;
	}

	public void setDbPass(String dbpass) throws Exception {
		if (dbpass == null || dbpass.trim().length() == 0) {
			log.error("Database password cannot be blank");
			db.setLevelWarning(2, "Database password cannot be blank", 0);
			// throw new FileException("Database password cannot be blank");
		}
		dbPass = dbpass.trim();
	}

	public String getDbUrl() {
		return dbUrl;
	}

	public void setDbUrl(String dburl) throws Exception {
		if (dburl == null || dburl.trim().length() == 0) {
			log.error("Database dbUrl cannot be blank");
			db.setLevelWarning(2, "Database dbUrl cannot be blank", 0);
			// throw new FileException("Database dbUrl cannot be blank");
		}
		dbUrl = dburl.trim();
	}

	public int getMaxError() {
		return maxError;
	}

	public void setMaxError(int n) throws Exception {
		if (n < 0) {
			// throw new FileException("MaxError should be >= 0");
			log.error("MaxError should be >= 0");
			db.setLevelWarning(2, "MaxError should be >= 0", 0);
		}
		maxError = n;
	}

	public int getLoadThreshold() {
		return loadThreshold;
	}

	public void setLoadThreshold(int n) throws Exception {
		if (n < 0) {
			log.error("LoadThreshold should be >= 0");
			db.setLevelWarning(2, "LoadThreshold should be >= 0", 0);
			// throw new FileException("LoadThreshold should be >= 0");
		}
		loadThreshold = n;
	}

	public int getCommitPoint() {
		return commitPoint;
	}

	public void setCommitPoint(int n) throws Exception {
		if (n < 0) {
			log.error("");
			db.setLevelWarning(2, "CommitPoint should be >= 0", 0);
			// throw new FileException("CommitPoint should be >= 0");
		}
		commitPoint = n * batchSize;
	}

	public String getBadDir() {
		return badDir;
	}

	public void setBadDir(String baddir) throws Exception {
		if (baddir == null || baddir.trim().length() == 0) {
			log.error("Bad file directory cannot be blank");
			db.setLevelWarning(2, "Bad file directory cannot be blank", 0);
			// throw new FileException("Bad file directory cannot be blank");
		}
		File fl = new File(baddir);
		if (!fl.exists()) {
			log.error("Bad file directory '" + baddir + "' does not exist");
			db.setLevelWarning(2, "Bad file directory '" + baddir
					+ "' does not exist", 0);
			// throw new FileException("Bad file directory '" + baddir+
			// "' does not exist");
		}
		if (!fl.isDirectory()) {
			log.error("Bad file directory '" + baddir + "' is not a directory");
			db.setLevelWarning(2, "Bad file directory '" + baddir
					+ "' is not a directory", 0);
			// throw new FileException("Bad file directory '" + baddir+
			// "' is not a directory");
		}
		badDir = baddir.trim();
	}

	public String getLogDir() {
		return logDir;
	}

	public void setLogDir(String logdir) throws Exception {
		if (logdir == null || logdir.trim().length() == 0) {
			log.error("Log file directory cannot be blank");
			db.setLevelWarning(2, "Log file directory cannot be blank", 0);
			// throw new FileException("Log file directory cannot be blank");
		}
		File fl = new File(logdir);
		if (!fl.exists()) {
			log.error("Log file directory '" + logdir + "' does not exist");
			db.setLevelWarning(2, "Log file directory '" + logdir
					+ "' does not exist", 0);
			// throw new FileException("Log file directory '" + logdir+
			// "' does not exist");
		}
		if (!fl.isDirectory()) {
			log.error("Log file directory '" + logdir + "' is not a directory");
			db.setLevelWarning(2, "Log file directory '" + logdir
					+ "' is not a directory", 0);
			// throw new FileException("Log file directory '" + logdir+
			// "' is not a directory");
		}
		logDir = logdir.trim();
	}

	public String getPropertyFile() {
		return propertyFile;
	}

	public void setPropertyFile(String propFile) throws Exception {
		if (propFile == null || propFile.trim().length() == 0) {
			log.error("Property file cannot be blank");
			db.setLevelWarning(2, "Property file cannot be blank", 0);
			// throw new FileException("Property file cannot be blank");
		}
		File fl = new File(propFile);
		if (!fl.exists()) {
			log.error("Property file '" + propFile + "' does not exist");
			db.setLevelWarning(2, "Property file '" + propFile
					+ "' does not exist", 0);
			// throw new FileException("Property file '" + propFile+
			// "' does not exist");
		}
		if (!fl.isFile()) {
			log.error("Property file '" + propFile + "' is not a normal file");
			db.setLevelWarning(2, "Property file '" + propFile
					+ "' is not a normal file", 0);
			// throw new FileException("Property file '" + propFile+
			// "' is not a normal file");
		}
		propertyFile = propFile.trim();
	}

	/*
	 * public SegmentLogic getSegLogic() { return segLogic; }
	 * 
	 * public void setSegLogic(String id) throws FileException { try { Class<?>
	 * cls = Class.forName(id.trim()); if
	 * (cls.isAssignableFrom(SegmentLogic.class)) throw new
	 * FileException("Class " + id + " is not of " +
	 * SegmentLogic.class.getName()); segLogic = (SegmentLogic)
	 * cls.newInstance(); } catch (Exception e) { throw new FileException(e); }
	 * }
	 * 
	 * public void setSegLogic(SegmentLogic sg) throws FileException { segLogic
	 * = sg; }
	 */

	public int getBatchsize() {
		return batchSize;
	}

	/*
	 * private void ReadConfig() throws FileException { int flag = 0;
	 * ArrayList<Property> pList = EbcdicUtil.ReadPropsFile(configFile); if
	 * (pList.size() == 0) throw new FileException(
	 * "No configuration parameters were found in " + configFile);
	 * 
	 * }
	 */

	public int setupFileLine(FileProp fp, FileLine fl) {
		int mx = Integer.MIN_VALUE;
		int mn = Integer.MAX_VALUE;
		if (fp.getHeader() != null) {
			if (mn > fp.getHeader().getSizeInFile())
				mn = fp.getHeader().getSizeInFile();
			if (mx < fp.getHeader().getSizeInFile())
				mx = fp.getHeader().getSizeInFile();
		}
		for (int k = 0; k < fp.getBody().length; k++) {
			if (mn > fp.getBodySegment(k).getSizeInFile())
				mn = fp.getBodySegment(k).getSizeInFile();
			if (mx < fp.getBodySegment(k).getSizeInFile())
				mx = fp.getBodySegment(k).getSizeInFile();
		}
		if (fp.getTrailer() != null) {
			if (mn > fp.getTrailer().getSizeInFile())
				mn = fp.getTrailer().getSizeInFile();
			if (mx < fp.getTrailer().getSizeInFile())
				mx = fp.getTrailer().getSizeInFile();
		}
		fl.lineByte = new byte[mx];
		fl.maxLineByteSize = mx;
		fl.dirty = true;
		return mn;
	}

	private void ShowData(FileSegmentBase fg) {
		if (Debug) {
			for (int k = 0; k < fg.getFields().size(); k++) {
				FileField ff = fg.getField(k);
				if (ff.getImported()) {
					System.out.println(ff.getName() + " = '" + ff.getValue()
							+ "'");
				}
			}
		}
	}

	public ArrayList<String> CSVData(FileSegmentBase fg, FileProp fp)
			throws Exception {
		bodyList = new ArrayList();
		csvFileLocationVal = IDFConfig.getInstance().getIdfData()
				.getDatafileCsvFileLocation();
		interfaceName = IDFConfig.getInstance().getIdfData().getFilePropName();
		int seqName = IDFConfig.getInstance().getSequenceNum();
		seqNameVal = String.valueOf(seqName);
		csvNamePattern = IDFConfig.getInstance().getIdfData()
				.getDatafileCsvNamePattern();
		String headerFieldPos="";
		String trailerFieldPos="";
		int headerFieldPosVal;
		int trailerFieldPosVal;
		String headerRecType="";
		String trailerRecType="";
		//int headerTrailerCount=0;
		String hName = IDFConfig.getInstance().getIdfData().gethName();
		String tName = IDFConfig.getInstance().getIdfData().gettName();
		
		try {
			if(fg!=null){
				//multiHeaderTrailerFlag
				multiHeaderTrailerFlag = IDFConfig.getInstance().getIdfData().getMultiHeaderTrailerFlag();
				if (!(multiHeaderTrailerFlag.equalsIgnoreCase("")) && multiHeaderTrailerFlag.equalsIgnoreCase("Y"))
				{
					
				for (int k = 0; k < fg.getFields().size(); k++) {
					FileField ff = fg.getField(k);
					if (ff.getImported()) {
						bodyList.add(ff.getValue());

					}
				}
				headerFieldPos = IDFConfig.getInstance().getIdfData().getHeaderFieldPosition();
				trailerFieldPos = IDFConfig.getInstance().getIdfData().getTrailerFieldPosition();
				headerFieldPosVal = Integer.parseInt(headerFieldPos);
				trailerFieldPosVal = Integer.parseInt(trailerFieldPos);
				headerRecType= IDFConfig.getInstance().getIdfData().getHeaderRecordType();
				trailerRecType = IDFConfig.getInstance().getIdfData().getTrailerRecordType();
				
				if(bodyList.get(headerFieldPosVal).toString().contains(headerRecType)){
					
					headerTrailerCsvList.addAll(bodyList);
					//bodyList.clear();
					
					
				} else if(bodyList.get(headerFieldPosVal).toString().contains(trailerRecType)){
					
					headerTrailerCsvList.addAll(bodyList);
					headerTrailerCount++;
					//bodyList.clear();
					htCSVName = interfaceName + "_" + "HEADERTRAILER" + "_"
					+ "HEDR"+headerTrailerCount;
					headerTrailerCSVName = csvFileLocationVal + htCSVName + "_"
					+ seqNameVal + csvNamePattern;
					
					if (!(csvFileNamesList.contains(headerTrailerCSVName))) {
						csvFileNamesList.add(headerTrailerCSVName);
					}
					createCSVFile(headerTrailerCSVName, headerTrailerCsvList);
					headerTrailerCsvList.clear();
				} else {
				
					String multipleBodySegments = IDFConfig.getInstance().getIdfData().getMultipleBodySegments();

					if (!(multipleBodySegments.equalsIgnoreCase("")) && multipleBodySegments.equalsIgnoreCase("Y"))
					{
						//Get BodySegmentFieldPosition.
						bCSVNameVal = (String) bodyList.get(0);
								
					}
					else {
					//Note:- <BSegmentName> in "Loaderxml" file,"ctl" file name and "idf.xml" file <bodySegmentName> should equal.
						bCSVNameVal = IDFConfig.getInstance().getIdfData().getBodySegmentName();
					}
				
					if(!(bCSVNameVal.equalsIgnoreCase(""))){
						bCSVName = interfaceName + "_" + "BODY" + "_" + bCSVNameVal;
						bSegCSVName = csvFileLocationVal + bCSVName + "_" + seqNameVal
						+ csvNamePattern;
					}
				
				// log.debug("Body CSV File name: " + bSegCSVName);
					if (!(csvFileNamesList.contains(bSegCSVName))) {
						csvFileNamesList.add(bSegCSVName);
					}
					createCSVFile(bSegCSVName, bodyList);
					bodyList = null;
				}
				}//end of multiHeaderTrailerFlag
				else{
					
					if ((fg.getName().equalsIgnoreCase(hName) || fg.getName()
									.equalsIgnoreCase(tName))) {
						for (int k = 0; k < fg.getFields().size(); k++) {
							FileField ff = fg.getField(k);
							if (ff.getImported()) {
								headerTrailerCsvList.add(ff.getValue());

							}
						}

						htCSVNameVal = IDFConfig.getInstance().getIdfData().getHeaderRecordType();
						htCSVName = interfaceName + "_" + "HEADERTRAILER" + "_"
								+ htCSVNameVal;
						headerTrailerCSVName = csvFileLocationVal + htCSVName + "_"
								+ seqNameVal + csvNamePattern;
						
						if (!(csvFileNamesList.contains(headerTrailerCSVName))) {
							csvFileNamesList.add(headerTrailerCSVName);
						}
						if (fg.getName().equalsIgnoreCase(tName)) {
							createCSVFile(headerTrailerCSVName, headerTrailerCsvList);
							headerTrailerCsvList = null;
						}
					} 
					else {
						for (int k = 0; k < fg.getFields().size(); k++) {
							FileField ff = fg.getField(k);
							if (ff.getImported()) {
								bodyList.add(ff.getValue());

							}
						}
						
						String multipleBodySegments = IDFConfig.getInstance().getIdfData().getMultipleBodySegments();

						if (!(multipleBodySegments.equalsIgnoreCase("")) && multipleBodySegments.equalsIgnoreCase("Y"))
						{
							bCSVNameVal = (String) bodyList.get(0);
										
						}
						else{
							//Note:- <BSegmentName> in "Loaderxml" file,"ctl" file name and "idf.xml" file <bodySegmentName> should equal.
							bCSVNameVal = IDFConfig.getInstance().getIdfData().getBodySegmentName();
						}
						
						if(!(bCSVNameVal.equalsIgnoreCase(""))){
							bCSVName = interfaceName + "_" + "BODY" + "_" + bCSVNameVal;
							bSegCSVName = csvFileLocationVal + bCSVName + "_" + seqNameVal
							+ csvNamePattern;
						}
						
						// log.debug("Body CSV File name: " + bSegCSVName);
						if (!(csvFileNamesList.contains(bSegCSVName))) {
							csvFileNamesList.add(bSegCSVName);
						}
						createCSVFile(bSegCSVName, bodyList);
						bodyList = null;
					}
					
				}
			}
		} catch (Exception e) {
			log.error("Generating the CSV Data/CSV File Creation Failed.");
			db.setLevelWarning(3,
					"Generating the CSV Data/CSV File Creation Failed.", 0);
		}
		return csvFileNamesList;
	}

	public void createCSVFile(String fileName, ArrayList alist)
			throws Exception {
		delimeterPattern = IDFConfig.getInstance().getIdfData()
				.getCsvFileSeparator();
		jobseqflag = IDFConfig.getInstance().getIdfData().getInsertjobseqtodatatables();
		int seqNumVal = IDFConfig.getInstance().getSequenceNum();
		seqNum = String.valueOf(seqNumVal);
		String encloseDataBy=IDFConfig.getInstance().getIdfData().getEncloseDataBy();
		
		String idfType = IDFConfig.getInstance().getIdfData().getIdfType();
		boolean isCustomIdfType = false;
		if ("CUSTOM".equalsIgnoreCase(idfType)) isCustomIdfType = true;
		
		String fileParsingCharset=IDFConfig.getInstance().getIdfData().getFileParsingCharset();
		BufferedWriter bwriter;
		try {
			FileWriter fwriter = new FileWriter(fileName, true);
			if(null!=fileParsingCharset && !"".equalsIgnoreCase(fileParsingCharset)){
				bwriter = new BufferedWriter(new OutputStreamWriter(			//Secure Code Change
				        new FileOutputStream(fileName,true), fileParsingCharset)
				    );
			}else{
				bwriter = new BufferedWriter(fwriter);
			}
			
			StringBuilder sb = new StringBuilder();
			/*for (int i = 0; i < alist.size(); i++) {
				if (i == alist.size() - 1) {
					if(alist.get(i).toString().trim().contains("\"")){
						sb.append(alist.get(i).toString().trim().replace("\"", ""));
					}
					else{
						sb.append(alist.get(i).toString().trim());
					}
					if(jobseqflag.equalsIgnoreCase("Y")){
						//sb.append(alist.get(i).toString().trim());
						sb.append(delimeterPattern);
						sb.append(seqNum);
					}else{
						//sb.append(alist.get(i).toString().trim());
						sb.append(delimeterPattern);
						sb.append("");
					}
				} else {
					//If data file contains "\".
					if(alist.get(i).toString().trim().contains("\"")){
						sb.append(alist.get(i).toString().trim().replace("\"", ""));
						sb.append(delimeterPattern);
					}else{
						sb.append(alist.get(i).toString().trim());
						sb.append(delimeterPattern);
					}
				}
			}*/
			for (int i = 0; i < alist.size(); i++) {
				if (i == alist.size() - 1) {
					
					if(jobseqflag.equalsIgnoreCase("Y")){
						sb.append(encloseDataBy);		//Added for Secure Code
						sb.append(alist.get(i).toString().trim());
						sb.append(encloseDataBy);		//Added for Secure Code
						sb.append(delimeterPattern);
						sb.append(encloseDataBy);		//Added for Secure Code
						sb.append(seqNum);
						sb.append(encloseDataBy);		//Added for Secure Code
						if (isCustomIdfType) {
							sb.append("|");				//Added for Secure Code to form line delimiter as '|\n'
						}
					}else{
						sb.append(encloseDataBy);		//Added for Secure Code
						sb.append(alist.get(i).toString().trim());
						sb.append(encloseDataBy);		//Added for Secure Code
						sb.append(delimeterPattern);
						sb.append("");
					}
				} else {
						sb.append(encloseDataBy);		//Added for Secure Code
						sb.append(alist.get(i).toString().trim());
						sb.append(encloseDataBy);		//Added for Secure Code
						sb.append(delimeterPattern);
					
				}
			}
			//log.debug("sb: "+sb.toString());
			bwriter.write(sb.toString());
			bwriter.newLine();
			bwriter.flush();
			bwriter.close();
			fwriter.close();
		} catch (IOException e) {
			log.error("CSV File Creation Failed.");
			db.setLevelWarning(3, "CSV File Creation Failed.", 0);
			// e.printStackTrace();
		}

	}

	public boolean getAutoAdvance() {
		return autoAdvance;
	}

	public void setAutoAdvance(boolean b) {
		autoAdvance = b;
	}

	public boolean getClearHeader() {
		return clearHeader;
	}

	public void setClearHeader(boolean b) {
		clearHeader = b;
	}

	public boolean getNeedCleanup() {
		return needCleanup;
	}

	public void setNeedCleanup(boolean b) {
		needCleanup = b;
	}

	// FieldUtil

	public static boolean ValidateFieldName(String str) {
		return fieldNamePattern.matcher(str).matches();
	}

	public static String ValidateDateFormat(String s) throws Exception {
		String x = "";
		boolean yearFound = false;
		boolean monthFound = false;
		boolean dayFound = false;
		boolean hourFound = false;
		boolean minuteFound = false;
		boolean secondFound = false;
		s = s.toUpperCase();
		String sample = "";
		int k = 0;
		int n = 0;
		int l = s.length();
		while (k < l) {
			switch (s.charAt(k)) {
			case 'Y':
				if (yearFound) {
					log.error("Invalid Year format in " + s);
					db.setLevelWarning(2, "Invalid Year format in " + s, 0);
					throw new Exception("Invalid Year format in " + s);
				}
				if (s.substring(k).startsWith("YYYY")) {
					sample += "1999";
					x += "yyyy";
					k += 4;
					yearFound = true;
				} else if (s.substring(k).startsWith("YY")) {
					sample += "07";
					x += "yy";
					k += 2;
					yearFound = true;
				} else{
					log.error("Invalid year format in " + s);
					db.setLevelWarning(2, "Invalid year format in " + s, 0);
					throw new Exception("Invalid year format in " + s);
				}
				break;
			case 'M':
				if (s.substring(k).startsWith("MON")) {
					if (monthFound){
						log.error("Invalid month format in " + s);
						db.setLevelWarning(2, "Invalid month format in " + s, 0);
						throw new Exception("Invalid month format in " + s);
					}
					sample += "Aug";
					x += "MMM";
					k += 3;
					monthFound = true;
				} else if (s.substring(k).startsWith("MM")) {
					if (monthFound){
						log.error("Invalid month format in " + s);
						db.setLevelWarning(2, "Invalid month format in " + s, 0);
						throw new Exception("Invalid month format in " + s);
					}
					sample += "04";
					x += "MM";
					k += 2;
					monthFound = true;
				} else if (s.substring(k).startsWith("MI")) {
					if (minuteFound){
						log.error("Invalid minute format in " + s);
						db.setLevelWarning(2, "Invalid minute format in " + s, 0);
						throw new Exception("Invalid minute format in " + s);
					}
					sample += "43";
					x += "mm";
					k += 2;
					minuteFound = true;
				} else{
					log.error("Invalid month format in " + s);
					db.setLevelWarning(2, "Invalid month format in " + s, 0);
					throw new Exception("Invalid month format in " + s);
				}
				break;
			case 'D':
				if (dayFound){
					log.error("Invalid month format in " + s);
					db.setLevelWarning(2, "Invalid month format in " + s, 0);
					throw new Exception("Invalid month format in " + s);
				}
				if (s.substring(k).startsWith("DDD")) {
					sample += "127";
					x += "D";
					k += 3;
					dayFound = true;
				} else if (s.substring(k).startsWith("DD")) {
					sample += "27";
					x += "dd";
					k += 2;
					dayFound = true;
				} else{
					log.error("Invalid day format in " + s);
					db.setLevelWarning(2, "Invalid day format in " + s, 0);
					throw new Exception("Invalid day format in " + s);
				}
				break;
			case 'H':
				if (hourFound){
					log.error("Invalid hour format in " + s);
					db.setLevelWarning(2, "Invalid hour format in " + s, 0);
					throw new Exception("Invalid hour format in " + s);
				}
				if (s.substring(k).startsWith("HH24")) {
					sample += "21";
					x += "HH";
					k += 4;
					monthFound = true;
				} else if (s.substring(k).startsWith("HH")) {
					sample += "09";
					x += "hh";
					k += 2;
					monthFound = true;
				} else{
					log.error("Invalid hour format in " + s);
					db.setLevelWarning(2, "Invalid hour format in " + s, 0);
					throw new Exception("Invalid hour format in " + s);
				}
				break;
			case 'S':
				if (secondFound){
					log.error("Invalid second format in " + s);
					db.setLevelWarning(2, "Invalid second format in " + s, 0);
					throw new Exception("Invalid second format in " + s);
				}
				if (s.substring(k).startsWith("SS")) {
					sample += "51";
					x += "ss";
					k += 2;
					secondFound = true;
				} else{
					log.error("Invalid second format in " + s);
					db.setLevelWarning(2, "Invalid second format in " + s, 0);
					throw new Exception("Invalid second format in " + s);
				}
				break;
			case ' ':
			case '/':
			case '-':
			case ':':
				x += s.charAt(k);
				sample += s.charAt(k++);
				break;
			default:
				log.error("Invalid character in date format string : " + s);
				db.setLevelWarning(2, "Invalid character in date format string : " + s, 0);
				throw new Exception(
						"Invalid character in date format string : " + s);
			}
		}
		try {
			SimpleDateFormat sdf = new SimpleDateFormat(x);
			Timestamp tm = new Timestamp(sdf.parse(sample).getTime());
		} catch (Exception e) {
			log.error("Invalid date format : " + s, e);
			db.setLevelWarning(2, "Invalid date format : " + s, 0);
			throw new Exception("Invalid date format : " + s, e);
		}
		return x;
	}

	public static String getString(byte[] b) {
		int l = b.length;
		String s = "";
		for (int k = 0; k < l; k++) {
			s += (char) b[k];
		}
		return s;
	}

	public static String getHexVal(byte[] b) {
		int l = b.length;
		String s = "";
		for (int k = 0; k < l; k++) {
			s += HexVals.charAt(((b[k] & 0xF0) >> 4));
			s += HexVals.charAt(((b[k] & 0x0F)));
		}
		return s;
	}

	public static String getHexVal(String b) {
		int l = b.length();
		String s = "";
		for (int k = 0; k < l; k++) {
			s += HexVals.charAt(((b.charAt(k) & 0xF0) >> 4));
			s += HexVals.charAt(((b.charAt(k) & 0x0F)));
		}
		return s;
	}

	public static int getTypeValue(String nm) {
		if (typeValues.get(nm) != null)
			return ((Integer) (typeValues.get(nm))).intValue();
		return -1;
	}

	public static String getTypeName(int m) {
		return (String) typeNames.get(new Integer(m));
	}

	public static char[] ascii = new char[] { 0x00, 0x01, 0x02, 0x03, 0x04,
			0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F,
			0x10, 0x11, 0x12, 0x13, 0x00, 0x0A, 0x08, 0x00, 0x18, 0x19, 0x00,
			0x00, 0x1C, 0x1D, 0x1E, 0x1F, 0x00, 0x00, 0x1C, 0x2E, 0x2E, 0x0A,
			0x17, 0x1B, 0x2E, 0x2E, 0x2E, 0x2E, 0x2E, 0x05, 0x06, 0x07, 0x2E,
			0x2E, 0x16, 0x2E, 0x2E, 0x2E, 0x2E, 0x04, 0x2E, 0x2E, 0x2E, 0x2E,
			0x14, 0x15, 0x2E, 0x1A, 0x20, 0x2E, 0x2E, 0x2E, 0x2E, 0x2E, 0x2E,
			0x2E, 0x2E, 0x2E, 0x5B, 0x2E, 0x3C, 0x28, 0x2B, 0x21, 0x26, 0x2E,
			0x2E, 0x2E, 0x2E, 0x2E, 0x2E, 0x2E, 0x2E, 0x2E, 0x5D, 0x24, 0x2A,
			0x29, 0x3B, 0x5E, 0x2D, 0x2F, 0x2E, 0x2E, 0x2E, 0x2E, 0x2E, 0x2E,
			0x2E, 0x2E, 0x7C, 0x2C, 0x25, 0x5F, 0x3E, 0x3F, 0x97, 0x98, 0x99,
			0xA2, 0xA3, 0xA4, 0xA5, 0xA6, 0xA7, 0x60, 0x3A, 0x23, 0x40, 0x27,
			0x3D, 0x22, 0x2E, 0x61, 0x62, 0x63, 0x64, 0x65, 0x66, 0x67, 0x68,
			0x69, 0x2E, 0x2E, 0x2E, 0x2E, 0x2E, 0x2E, 0x2E, 0x6A, 0x6B, 0x6C,
			0x6D, 0x6E, 0x6F, 0x70, 0x71, 0x72, 0x2E, 0x2E, 0x2E, 0x2E, 0x2E,
			0x2E, 0x2E, 0x2E, 0x73, 0x74, 0x75, 0x76, 0x77, 0x78, 0x79, 0x7A,
			0x2E, 0x2E, 0x2E, 0x2E, 0x2E, 0x2E, 0x2E, 0x2E, 0x2E, 0x2E, 0x2E,
			0x2E, 0x2E, 0x2E, 0x2E, 0x2E, 0x2E, 0x2E, 0x2E, 0x2E, 0x2E, 0x2E,
			'{', 0x41, 0x42, 0x43, 0x44, 0x45, 0x46, 0x47, 0x48, 0x49, 0x2E,
			0x2E, 0x2E, 0x2E, 0x2E, 0x2E, '}', 0x4A, 0x4B, 0x4C, 0x4D, 0x4E,
			0x4F, 0x50, 0x51, 0x52, 0x2E, 0x2E, 0x2E, 0x2E, 0x2E, 0x2E, '\\',
			0x2E, 0x53, 0x54, 0x55, 0x56, 0x57, 0x58, 0x59, 0x5A, 0x2E, 0x2E,
			0x2E, 0x2E, 0x2E, 0x2E, 0x30, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36,
			0x37, 0x38, 0x39, '|', 0x2E, 0x2E, 0x2E, 0x2E, 0x7F };

	public static String GetAscii(String s, int type) {
		int l = s.length();
		int a = 0;
		String x = "";
		if (type == FileUtil.CONSTANT_STRING_TYPE
				|| type == FileUtil.STRING_TYPE
				|| type == FileUtil.COBOL_NUMBER_TYPE
				|| type == FileUtil.DATE_TYPE) {
			for (int k = 0; k < l; k++) {
				int b = (int) s.charAt(k);
				if (b > 255)
					b -= 65280;
				int c = (int) (ascii[(b)]);
				if (c > 255)
					c -= 65280;
				if (c != 0 && (c < 31 || c > 127))
					b = 0;
				x += "" + ascii[(b)];
			}
		} else {
			return s;
		}
		return x;
	}

	public static boolean CanWriteToFile(String x) {
		if (x == null || x.trim().length() == 0)
			return false;
		File fl = new File(x);
		if (!fl.exists())
			return false;
		if (!fl.isFile())
			return false;
		// if(!fl.canWrite()) return false;
		return true;
	}

	public static String ConvertRawData(String rawData, FileField smf)
			throws Exception {
		String z = rawData;

		if (smf.getOwner().isEbcdic())
			z = GetAscii(rawData, smf.getType());
		switch (smf.getType()) {
		case STRING_TYPE:
			return convertString(rawData, z, smf);
		case NUMBER_TYPE:
			return ConvertNumber(z, smf);
		case COBOL_NUMBER_TYPE:
			return ConvertCobolNumber(z, smf);
		case COBOL_COMP_TYPE:
			return ConvertComp(z, smf);
		case COBOL_COMP3_TYPE:
			return ConvertComp3(z, smf);
		case CONSTANT_STRING_TYPE:
			return z;
		case DATE_TYPE:
			return ConvertDate(z.trim(), smf);
		default:
			log.error("Invalid date " + z);
			db.setLevelWarning(2, "Invalid date " + z, 0);
			throw new Exception("Invalid date " + z);
		}
	}

	public static String convertString(String rawData, String str, FileField smf)  {
		if ("DTC_EMAIL_ADDRESS".equalsIgnoreCase(smf.getName())
				&& isHO70(smf)){
			// first, we need to revert the conversion from bytes to chars
			byte[] bytes = new byte[rawData.length()];

			for (int i=0; i<rawData.length(); ++i){
				bytes[i] = (byte)rawData.charAt(i);
			}

			// second, create IBM037 string
			return new String(bytes, 0, rawData.length(), IBM037);
		}

		return str;
	}

	private static boolean isHO70(FileField smf) {
		FileSegmentBase owner = smf.getOwner();

		if (null!=owner){
			try{
				FileProp fileProp = owner.getFilePropParent();

				if (null!=fileProp && null!=fileProp.getSegName()){
					return "HO70".equalsIgnoreCase(fileProp.getSegName());
				}
			}catch (Exception ex){
				log.error("Failed to check file source.", ex);
			}
		}

		return false;
	}

	public static String ConvertNumber(String str, FileField smf)
			throws Exception {
		if (str == null)
			return "";
		int len = smf.getTotSize();
		int dec = smf.getDecimal();
		int l = len, k, p;
		boolean decimalPt = false;
		StringBuffer res = new StringBuffer();
		String x = str.trim();
		p = 0;
		if (smf.getSigned() && x.charAt(0) == '-')
			res.append(x.charAt(p++));
		for (k = p; k < x.length(); k++) {
			if (x.charAt(k) < '0' || x.charAt(k) > '9') {
				if (x.charAt(k) == '.') {
					if (decimalPt) {
						log.error("Too many decimal points in "
								+ smf.getName() + " value = '" + x + "'.");
						db.setLevelWarning(2, "Too many decimal points in "
								+ smf.getName() + " value = '" + x + "'.", 0);
						throw new Exception("Too many decimal points in "
								+ smf.getName() + " value = '" + x + "'.");
					}
					decimalPt = true;
					// res.append(x.charAt(k));
				} else {
					System.out.println("number str = " + str);
					log.error("");
					db.setLevelWarning(2, "Invalid digit '" + x.charAt(k)
							+ "' in " + smf.getName() + ".", 0);
					throw new Exception("Invalid digit '" + x.charAt(k)
							+ "' in " + smf.getName() + ".");
				}
			}
			res.append(x.charAt(k));
		}
		if (res.length() == 0) {
			return res.toString();
		}
		try {
			double result = Double.parseDouble(res.toString());
			for (int i = 0; i < dec; i++) {
				result /= 10;
			}
			return result % 1 > 0 ? "" + result : "" + (long) result;
		} catch (Exception e) {
			System.out.println(smf.getName() + "->" + str);
			log.error(smf.getName() + "->" + str);
			db.setLevelWarning(2, smf.getName() + "->" + str, 0);
			// e.printStackTrace();
		}
		return res.toString();
	}

	public static String Positive = "{ABCDEFGHI";
	public static String NumericC = "0123456789";
	public static String Negative = "}JKLMNOPQR";
	public static String All80Spaces = "                                                                                ";
	public static String All80Zeros = "00000000000000000000000000000000000000000000000000000000000000000000000000000000";

	public static void IsNumeric(StringBuffer res, int startPos, FileField smf)
			throws Exception {
		if (smf.CanIgnoreSpaces()
				&& res.substring(startPos).equals(
						All80Spaces.substring(0, smf.getTotSize() - startPos))) {
			res.replace(startPos, smf.getTotSize(),
					All80Zeros.substring(0, smf.getTotSize() - startPos));
			return;
		}
		for (int p = startPos; p < smf.getTotSize(); p++) {
			if (res.charAt(p) < '0' || res.charAt(p) > '9') {
				System.out.println("IsNumeric -> " + res.toString());
				log.error("Invalid digit '" + res.charAt(p) + "' in "
						+ smf.getName() + ".");
				db.setLevelWarning(2, "Invalid digit '" + res.charAt(p) + "' in "
						+ smf.getName() + ".", 0);
				throw new Exception("Invalid digit '" + res.charAt(p) + "' in "
						+ smf.getName() + ".");
			}
		}
	}

	public static String ConvertCobolNumber(String str, FileField smf)
			throws Exception {
		int len = smf.getTotSize();
		int l = len, k, p, ix, ng = 0;
		StringBuffer res = new StringBuffer();
		if (smf.CanIgnoreSpaces()
				&& str.equals(All80Spaces.substring(0, smf.getSizeInFile()))) {
			res.insert(0, All80Zeros.substring(0, smf.getTotSize()));
			return res.toString();
		}
		if (smf.getSigned()) {
			ix = Negative.indexOf(str.charAt(len - 1));
			if (ix >= 0) {
				res.append('-');
				ng = 1;
			} else {
				ix = Positive.indexOf(str.charAt(len - 1));
				if (ix < 0) {
					ix = NumericC.indexOf(str.charAt(len - 1));
					if (ix < 0) {
						System.out.println("ConvertCobolNumber->" + str);
						log.error("Filed, "
								+ smf.getName()
								+ " is a COBOL signed number and does not have the sign bits set");
						db.setLevelWarning(2, "Filed, "
								+ smf.getName()
								+ " is a COBOL signed number and does not have the sign bits set", 0);
						throw new Exception(
								"Filed, "
										+ smf.getName()
										+ " is a COBOL signed number and does not have the sign bits set");
					}
				}
			}
			for (p = 0; p < len - 1; p++)
				res.append(str.charAt(p));
			res.append(NumericC.charAt(ix));
		} else {
			for (p = 0; p < len; p++)
				res.append(str.charAt(p));
		}
		IsNumeric(res, ng, smf);
		int dec = smf.getDecimal();
		if (dec > 0) {
			res.insert(res.length() - dec, '.');
		}
		try {
			Double.parseDouble(res.toString());
		} catch (Exception e) {
			System.out.println(smf.getName() + "->" + str);
			log.error(smf.getName() + "->" + str);
			db.setLevelWarning(2, smf.getName() + "->" + str, 0);
			e.printStackTrace();
		}
		return res.toString();
	}

	public static String ConvertComp(String str, FileField smf)
			throws Exception {
		StringBuffer res = new StringBuffer();
		int len = smf.getTotSize();
		int dec = smf.getDecimal();
		int m = len - dec;
		int l = smf.getSizeInFile();
		if (l > 8){
			log.error("COMP Size of more than 8 byte not implemented");
			db.setLevelWarning(2, "COMP Size of more than 8 byte not implemented", 0);
			throw new Exception("COMP Size of more than 8 byte not implemented");
		}

		long c = 0L;
		int sgn = 1;
		int extra = 0;
		byte a[] = new byte[8];

		for (int w = 0; w < l; w++)
			a[w] = (byte) str.charAt(w);
		if ((a[0] & (byte) 0x80) != 0) {
			extra = 1;
			sgn = -1;
			for (int w = 0; w < l; w++)
				a[w] = (byte) (((byte) (a[w] ^ (byte) 0xFF) & (byte) 0xFF));
		}
		c = 0;
		for (int w = 0; w < l; w++) {
			c = c * 256L | (a[w] & 0xFF);
		}
		c = sgn * (c + extra);
		long adj = 1l;
		if (c < 0) {
			adj = -1l;
		}
		res.append("" + c * adj);
		if (dec > 0) {
			if (dec >= res.length()) {
				String filler = "";
				for (int i = 0; i < (dec - res.length() + 1); i++) {
					filler += "0";
				}
				res.insert(0, filler);
			}
			if (res.length() > dec)
				res.insert(res.length() - dec, '.');
		}
		if (adj < 0) {
			res.insert(0, "-");
		}
		return res.toString();
	}

	public static Timestamp getTimeStamp(String str) throws Exception {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			Timestamp tm = new Timestamp(sdf.parse(str).getTime());
			return tm;
		} catch (Exception e) {
			log.error("Invalid date format : " + str);
			db.setLevelWarning(2, "Invalid date format : " + str, 0);
			throw new Exception("Invalid date format : " + str, e);
		}

	}

	public static String ConvertDate(Date dt, String fmt) throws Exception {
		String fmt2 = FileUtil.ValidateDateFormat(fmt);
		SimpleDateFormat sdf = new SimpleDateFormat(fmt2);
		return sdf.format(dt);
	}

	/*
	 * public static String ConvertDate(String str, FileField smf) throws
	 * Exception { // Always return in "yyyy-MM-dd hh:mm:ss" format if (str ==
	 * null || str.length() == 0) return ""; try { SimpleDateFormat sdf = new
	 * SimpleDateFormat(smf.getDateFormat()); Timestamp tm = new
	 * Timestamp(sdf.parse(str).getTime()); return tm.toString().substring(0,
	 * 19); } catch (Exception e) { throw new Exception("Invalid date format : "
	 * + str, e); } }
	 */

	public static String ConvertDate(String str, FileField smf)
			throws Exception {
		// Always return in "dd-MMM-yyyy HH:mm:ss" format
		String ddate = null;
		if (str == null || str.length() == 0)
			return "";
		try {
			isValidDate(str, smf);
			SimpleDateFormat sdf = new SimpleDateFormat(smf.getDateFormat());
			// output date format
			SimpleDateFormat dFormatFinal = new SimpleDateFormat(
					"dd-MMM-yyyy HH:mm:ss");
			Date dateVal = sdf.parse(str);
			ddate = dFormatFinal.format(dateVal);
			return dFormatFinal.format(dateVal);
		} catch (Exception e) {
			log.error("Invalid date format : " + str);
			db.setLevelWarning(2, "Invalid date format : " + str, 0);
			throw new Exception("Invalid date format : " + str, e);
		}
	}

	public static boolean isValidDate(String inDate, FileField smf1)
			throws Exception {
		//V9DAOManager db = new V9DAOManager();
		SimpleDateFormat dateFormat = new SimpleDateFormat(smf1.getDateFormat());
		dateFormat.setLenient(false);
		Date javaDate = null;
		try {
			javaDate = dateFormat.parse(inDate.trim());
		} catch (Exception pe) {
			log.debug("EBCDIC parsing failed due to invalid date: " + inDate);
			db.setLevelWarning(2, "EBCDIC parsing failed due to invalid date: " + inDate, 0);
			
		}
		return true;
	}

	public static String bConvertComp3(String str, FileField smf)
			throws Exception {
		StringBuffer res = new StringBuffer();
		int len = smf.getTotSize();
		int dec = smf.getDecimal();
		int m = len - dec, ng = 0;
		int l = smf.getSizeInFile();
		int k = 0;

		if ((str.charAt(l - 1) & 0x0F) == 0x0D) {
			m++;
			res.append('-');
			ng = 1;
		}
		if ((len % 2) == 1) {
			if (res.length() == m)
				res.append('.');
			res.append((char) ((((str.charAt(0) & 0xF0) >> 4) + '0')));
		}
		if (len > 1) {
			if (res.length() == m)
				res.append('.');
			res.append((char) ((str.charAt(0) & 0x0F) + '0'));
		}
		for (int p = 1; p < l - 1; p++) {
			if (res.length() == m)
				res.append('.');
			res.append((char) (((str.charAt(p) & 0xF0) >> 4) + '0'));
			if (res.length() == m)
				res.append('.');
			res.append((char) ((str.charAt(p) & 0x0F) + '0'));
		}
		if (res.length() == m && dec != 0)
			res.append('.');
		if (len > 1)
			res.append((char) (((str.charAt(l - 1) & 0xF0) >> 4) + '0'));
		String res2 = res.toString();
		String res1 = ConvertComp3(str, smf);

		// / CHANGE CHANGE CHANGE
		if (!res2.equals(res1)) {
			System.out.println("Str = " + getHexVal(str));
			dec = 1 * dec;
			return "0";
		}
		return res2;
	}

	public static String ConvertComp3(String str, FileField smf)
			throws Exception {
		StringBuffer res = new StringBuffer();
		int len = smf.getTotSize();
		int dec = smf.getDecimal();
		int m = len - dec, ng = 0;
		int l = smf.getSizeInFile();
		int k = 0;

		if ((str.charAt(l - 1) & 0x0F) == 0x0D) {
			m++;
			res.append('-');
			ng = 1;
		}
		if ((len % 2) == 1) {
			res.append((char) ((((str.charAt(0) & 0xF0) >> 4) + '0')));
		}
		if (len > 1) {
			res.append((char) ((str.charAt(0) & 0x0F) + '0'));
		}
		for (int p = 1; p < l - 1; p++) {
			res.append((char) (((str.charAt(p) & 0xF0) >> 4) + '0'));
			res.append((char) ((str.charAt(p) & 0x0F) + '0'));
		}
		if (len > 1)
			res.append((char) (((str.charAt(l - 1) & 0xF0) >> 4) + '0'));
		for (int p = ng; p < len; p++) {
			if (res.charAt(p) < '0' || res.charAt(p) > '9') {
				System.out.println("comp3 str = " + str);
				log.error("Invalid digit, " + res.charAt(p)
						+ ", in numreic field (" + res.toString() + ")");
				db.setLevelWarning(2, "Invalid digit, " + res.charAt(p)
						+ ", in numreic field (" + res.toString() + ")", 0);
				throw new Exception("Invalid digit, " + res.charAt(p)
						+ ", in numreic field (" + res.toString() + ")");
			}
		}
		if (dec > 0) {
			res.insert(res.length() - dec, '.');
		}
		return res.toString();
	}

	public static String FormatNumber(int n, String fmt) {
		NumberFormat numberFormat = NumberFormat.getNumberInstance();
		DecimalFormat decimalFormat = (DecimalFormat) numberFormat;
		decimalFormat.applyPattern(fmt);
		return (decimalFormat.format(n));
	}

	public static String FormatNumber(double n, String fmt) {
		NumberFormat numberFormat = NumberFormat.getNumberInstance();
		DecimalFormat decimalFormat = (DecimalFormat) numberFormat;
		decimalFormat.applyPattern(fmt);
		return (decimalFormat.format(n));
	}

	public static String FormatNumber(double n, int totalSize, int dec,
			String fmt) {
		NumberFormat numberFormat = NumberFormat.getNumberInstance();
		DecimalFormat decimalFormat = (DecimalFormat) numberFormat;
		decimalFormat.setMaximumFractionDigits(dec);
		decimalFormat.setMaximumIntegerDigits(totalSize - dec);
		decimalFormat.setMinimumIntegerDigits(decimalFormat
				.getMaximumIntegerDigits());
		decimalFormat.applyPattern(fmt);
		return (decimalFormat.format(n));
	}
}