package com.lcl.pcb.v9.utilities.fileReader.dataprovider;

import java.io.Reader;

import com.lcl.pcb.v9.idf.jobflow.config.IDFConfig;
import com.lcl.pcb.v9.idf.jobflow.config.IDFData;

public class LineNumberFileReaderFactory {

	public static V9LineNumberFileReader getReader(Reader in, String requestReaderType) {
		ReaderType readerType = ReaderType.DEFAULT;;
		IDFData idfData = IDFConfig.getInstance().getIdfData();
		try {
			readerType = ReaderType.valueOf(requestReaderType);
		} catch (Exception e) {
			readerType = ReaderType.DEFAULT;
		}

		V9LineNumberFileReader lineNumberFileReader = null;

		switch(readerType) {
		case CUSTOM:
			if(null!=idfData.getLineNumReader() && !"".equalsIgnoreCase(idfData.getLineNumReader()) && "Y".equalsIgnoreCase(idfData.getLineNumReader())){
				lineNumberFileReader = new DefaultLineNumberReader(in);
			}else
			lineNumberFileReader = new SecureCodeLineNumberReader(in); ;
			break;
		default:
			lineNumberFileReader = new DefaultLineNumberReader(in);
			break;
		}

		return lineNumberFileReader;
	}

	public enum ReaderType{
		CUSTOM, DEFAULT
	}
}
