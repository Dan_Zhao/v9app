package com.lcl.pcb.v9.utilities;

import java.io.File;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import com.lcl.pcb.v9.custom.multistage.idf.MultiStage;
import com.lcl.pcb.v9.idf.jobflow.config.CopyBookData;
import com.lcl.pcb.v9.idf.jobflow.config.ExtractorData;
import com.lcl.pcb.v9.idf.jobflow.config.IDFData;


/**
 * This utility class is used to store all xml related operations
 */
public class XmlUtil {
	
	//use jaxb to unmarshall idf file and store in an idfData object to be populated in idfConfig singleton
	public static IDFData readIdfData(String filename) {	
	 try {
			File file = new File(filename);
			JAXBContext jaxbContext = JAXBContext.newInstance(IDFData.class);
	 
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			IDFData idfData = (IDFData) jaxbUnmarshaller.unmarshal(file);
			
			return idfData;			
	  } catch (JAXBException e) {
		e.printStackTrace();
        System.err.println("Cannot access file "+filename);
        System.exit(1);			
	  }	
	  return null;
	}
	
	//use jaxb to unmarshall idf file and store in an MultiStage object to be populated in idfConfig singleton
		public static MultiStage readMultiStageData(String filename) {	
		 try {
				File file = new File(filename);
				JAXBContext jaxbContext = JAXBContext.newInstance(MultiStage.class);
		 
				Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
				MultiStage multiStage = (MultiStage) jaxbUnmarshaller.unmarshal(file);
				
				return multiStage;			
		  } catch (JAXBException e) {
			e.printStackTrace();
	        System.err.println("Cannot access file "+filename);
	        System.exit(1);			
		  }	
		  return null;
		}
	
	public static CopyBookData readCopyBookData(String filename) {	
		 try {
				File file = new File(filename);
				JAXBContext jaxbContext = JAXBContext.newInstance(CopyBookData.class);
		 
				Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
				CopyBookData copyBookData = (CopyBookData) jaxbUnmarshaller.unmarshal(file);
				
				return copyBookData;			
		  } catch (JAXBException e) {
			e.printStackTrace();
	        System.err.println("Cannot access file "+filename);
	        System.exit(1);				
		  }	
		  return null;
		}
	
	public static ExtractorData readExtractorData(String filename) {	
		 try {
				File file = new File(filename);
				JAXBContext jaxbContext = JAXBContext.newInstance(ExtractorData.class);
		 
				Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
				ExtractorData extractorData = (ExtractorData) jaxbUnmarshaller.unmarshal(file);
				
				return extractorData;			
		  } catch (JAXBException e) {
			e.printStackTrace();
	        System.err.println("Cannot access file "+filename);
	        System.exit(1);				
		  }	
		  return null;
		}	

}
