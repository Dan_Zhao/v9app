/*
 * FileSegmentBase.java
 *
 * Created on January 26, 2007, 2:59 PM
 */

package com.lcl.pcb.v9.utilities.fileReader.segments;

import java.util.*;
import java.util.regex.Pattern;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.lcl.pcb.v9.dao.V9DAOManager;
import com.lcl.pcb.v9.idf.jobflow.config.IDFConfig;
import com.lcl.pcb.v9.idf.jobflow.config.Property;
import com.lcl.pcb.v9.utilities.fileReader.FileUtil;
import com.lcl.pcb.v9.utilities.fileReader.FileField;
import com.lcl.pcb.v9.utilities.fileReader.FileProp;
import com.lcl.pcb.v9.utilities.fileReader.dataprovider.FileLine;

/**
 * This class Parse and Validates the InterfaceLoader.xml file. 
 * @author fasghed
 */
public class FileSegmentBase {

	Logger log = LogManager.getLogger(FileSegmentBase.class);

	public static final String MANDATORY_SUFFIX = "mandatory";
	public static final String TYPE_SUFFIX = "type";
	public static final String TOTALSIZE_SUFFIX = "totalsize";
	public static final String DECIMAL_SUFFIX = "Decimal";
	public static final String SIGN_SUFFIX = "Signed";
	public static final String IMPORT_SUFFIX = "Import";
	public static final String FORMAT_SUFFIX = "Format";
	public static final String EBCDIC_SUFFIX = "Ebcdic";
	public static final String CONSTANT_SUFFIX = "Constant";
	public static final String IGNORE_SPACES_SUFFIX = "IgnoreSpaces";

	// public static final int FILE_SEGMENT = 1;
	public static final int HEADER_SEGMENT = 2;
	public static final int BODY_SEGMENT = 3;
	public static final int TRAILER_SEGMENT = 4;

	private String name = "";
	private ArrayList Fields = new ArrayList();
	private int segmentType = 0;
	private FileProp fileProprParent = null;
	private Object parent = null;
	private boolean validated = false;
	// private boolean ebcdic = false;
	private int segTotalSize = 0;
	private int segSizeInFile = 0;
	private boolean dataPresent = false;

	private int processedRecordCount = 0;
	private int parsedRecordCount = 0;
	private int errorRecordCount = 0;

	public static final String ControlChars = "\b\f\n\r\t";
	public static Pattern[] ctrlPat = new Pattern[5];
	static {
		ctrlPat[0] = Pattern.compile("\\\\b");
		ctrlPat[1] = Pattern.compile("\\\\f");
		ctrlPat[2] = Pattern.compile("\\\\n");
		ctrlPat[3] = Pattern.compile("\\\\r");
		ctrlPat[4] = Pattern.compile("\\\\t");
	}
	V9DAOManager db = new V9DAOManager();
	/** Creates a new instance of FileSegmentBase */
	public FileSegmentBase(String nm, int type) throws Exception {
		setName(nm);
		setSegmentType(type);
		log.info("Segment " + nm + " Created");
	}

	public FileSegmentBase(String nm, int type, FileProp p)
			throws Exception {
		setName(nm);
		setSegmentType(type);
		setParent(p);
		fileProprParent = this.getFilePropParent();
		log.info("Segment " + nm + " Created");
	}

	public int getProcessedRecordCount() {
		return processedRecordCount;
	}

	public int IncProcessedRecordCount() {
		++processedRecordCount;
		return processedRecordCount;
	}

	public void setProcessedRecordCount(int n) {
		processedRecordCount = n;
	}

	public int getParsedRecordCount() {
		return parsedRecordCount;
	}

	public int IncParsedRecordCount() {
		++parsedRecordCount;
		return parsedRecordCount;
	}

	public void setParsedRecordCount(int n) {
		parsedRecordCount = n;
	}

	public int getErrorRecordCount() {
		return errorRecordCount;
	}

	public int IncErrorRecordCount() {
		++errorRecordCount;
		return errorRecordCount;
	}

	public void setErrorRecordCount(int n) {
		errorRecordCount = n;
	}

	public boolean hasData() {
		return dataPresent;
	}

	public boolean hasData(boolean b) {
		dataPresent = b;
		if (b)
			IncProcessedRecordCount();
		return b;
	}

	public ArrayList getFields() {
		return Fields;
	}

	public FileField getField(int k) {
		if (k < 0 || k >= Fields.size())
			return null;
		return (FileField) Fields.get(k);
	}

	public int getSegTotalSize() {
		return segTotalSize;
	}

	public int getSizeInFile() {
		return segSizeInFile;
	}

	public FileProp getFilePropParent() throws Exception {
		Object pp = parent;
		while (true) {
			if (pp instanceof FileProp) {
				FileProp p = (FileProp) pp;
				return p;
			} else if (pp instanceof FileSegmentBase) {
				pp = ((FileSegmentBase) pp).getParent();
			} else{
				log.error("Parent object has unexpected type");
    		    db.setLevelWarning(2, "Parent object has unexpected type", 0);
				//throw new FileException("Parent object has unexpected type");
			}
		}
	}

	public void ClearValues() throws Exception {
		for (int k = 0; k < Fields.size(); k++) {
			FileField ff = (FileField) Fields.get(k);
			ff.clearRawData();
		}
		this.hasData(false);
	}

	public boolean isEbcdic() {
		return fileProprParent.isEbcdic();
	}

	public Object getParent() {
		return parent;
	}

	public void setParent(FileProp p) {
		parent = p;
	}

	public void setParent(FileSegmentBase p) {
		parent = p;
	}

	public String getName() {
		return name;
	}

	public boolean isDelimited() {
		return fileProprParent.isDelimited();
	}

	/*public char getDelimiter() {
		return fileProprParent.getDelimiter();
	}*/
	public String getDelimiter() {
        return fileProprParent.getDelimiter();
  }


	public boolean isVariableSize() {
		return fileProprParent.isVariableSize();
	}

	private void setName(String nm) throws Exception {
		if (nm == null || nm.trim().length() == 0){
			log.error("Segment name cannot be null");
		    db.setLevelWarning(2, "Segment name cannot be null", 0);
			//throw new FileException("Segment name cannot be null");
		}
		name = nm.toUpperCase();
	}

	public FileField getFieldByName(String nm) {
		if (nm == null || nm.length() == 0)
			return null;
		for (int k = 0; k < Fields.size(); k++) {
			if (nm.equalsIgnoreCase(((FileField) Fields.get(k)).getName()))
				return ((FileField) Fields.get(k));
		}
		return null;
	}

	public int getSegmentType() {
		return segmentType;
	}

	private void setSegmentType(int type) throws Exception {
		if (type < HEADER_SEGMENT || type > TRAILER_SEGMENT){
			log.error("Unknown segment type");
		    db.setLevelWarning(2, "Unknown segment type", 0);
			//throw new FileException("Unknown segment type");
		}
		segmentType = type;
	}

	public void AddField(FileField f) throws Exception {
		if (getFieldByName(f.getName()) == null)
			Fields.add(f);
		else{
			log.error(f.getName()+ " is already defined in this segment : " + name);
		    db.setLevelWarning(2, f.getName()+ " is already defined in this segment : " + name, 0);
			//throw new FileException(f.getName()+ " is already defined in this segment : " + name);
		}
	}

	public int Validate() throws Exception {
		validated = false;
		if (parent == null) {
			log.error("Segment should have a parent");
		    db.setLevelWarning(2, "Segment should have a parent", 0);
			//throw new FileException("Segment should have a parent");
		}
		segTotalSize = 0;
		segSizeInFile = 0;
		for (int k = 0; k < Fields.size(); k++) {
			int m = ((FileField) Fields.get(k)).Validate();
			if (m != 0){
				log.error("Error " + m+ " validating field "+ ((FileField) Fields.get(k)).getName());
				db.setLevelWarning(2, "Error " + m+ " validating field "+ ((FileField) Fields.get(k)).getName(), 0);
				//throw new FileException("Error " + m+ " validating field "+ ((FileField) Fields.get(k)).getName());
			}
			segTotalSize += ((FileField) Fields.get(k)).getTotSize();
			segSizeInFile += ((FileField) Fields.get(k)).getSizeInFile();
			if (k != Fields.size() - 1 && this.isDelimited()) {
				segTotalSize++;
				segSizeInFile++;
			}
		}
		validated = true;
		return 0;
	}

	public boolean isValid() {
		return validated;
	}

	// public boolean Parse(InputStream ist) {
	// try {
	// for(int m = 0; m < Fields.size(); m++) {
	// FileField ff = (FileField)Fields.get(m);
	// byte[] b = new byte[ff.getSizeInFile()];
	// ist.read(b);
	// if(!ff.validateData(b)) {
	// System.out.println("Field " + ff.getOwner() + "." + ff.getName() + "
	// failed validation. ");
	// System.out.println("Data in file = " + FieldUtil.getString(b) + " (" +
	// FieldUtil.getHexVal(b) + ").");
	// }
	// }
	// }
	// catch (Exception ex) {
	// System.out.println("Error in parsing : " + ex.getMessage());
	// return false;
	// }
	// return true;
	// }

	private boolean ParseDelimited(String str) {
		int a = 0;
		try {
			//char del = this.getDelimiter();
			String del = this.getDelimiter();
			for (int m = 0; m < Fields.size(); m++) {
				FileField ff = (FileField) Fields.get(m);
				String data = "";
				String enableCustomDelimiter = IDFConfig.getInstance().getIdfData().getEnableCustomDelimiter();
				String customEndDelimiter="";
				if("Y".equalsIgnoreCase(enableCustomDelimiter)){
					customEndDelimiter=del.substring(0, 1);
				}
				int eb = str.indexOf(del, a);
				if (eb != -1) {
					//data = str.substring(a, eb);
					//a = eb + 1;
					data = str.substring(a, eb)+customEndDelimiter;
					a = eb + del.length();
				} else
					data = str.substring(a);
				if (data.length() != ff.getTotSize()) {
					if (this.isVariableSize()) {
						if (data.length() > ff.getTotSize()){
							log.error("Data too big for field : "+ ff.getName() + "  Size="+ ff.getTotSize() + " found size = "+ data.length());
						    db.setLevelWarning(2, "Data too big for field : "+ ff.getName() + "  Size="+ ff.getTotSize() + " found size = "+ data.length(), 0);
							//throw new Exception("Data too big for field : "+ ff.getName() + "  Size="+ ff.getTotSize() + " found size = "+ data.length());
						}
					} else{
						log.error("Data size does not match definition for field : "+ ff.getName() + "  Size="+ ff.getTotSize() + " found size = "+ data.length());
					    db.setLevelWarning(2, "Data size does not match definition for field : "+ ff.getName() + "  Size="+ ff.getTotSize() + " found size = "+ data.length(), 0);
						//throw new FileException("Data size does not match definition for field : "+ ff.getName() + "  Size="+ ff.getTotSize() + " found size = "+ data.length());
					}
				}
				// byte[] b = new byte[data.length()];
				// for(int k = 0; k < data.length(); k++)
				// b[k] = (byte)data.charAt(k);
				if (!ff.validateData(data)) {
					String msg = "Field " + ff.getOwner().getName() + "."
							+ ff.getName() + " failed validation. ";
					//System.out.println(msg);
					log.error(msg);
				    db.setLevelWarning(2, msg, 0);
					//throw new FileException(msg);
					// System.out.println("Data in file = " + data + " (" +
					// FieldUtil.getHexVal(b) + ").");

				}
				//System.out.println(ff.getName() + " = '" + ff.getRawData() +
				//"' ('" + ff.getValue() + "'");
			}
		} catch (Exception ex) {
			log.error("Error in parsing : " + ex.getMessage());
			IncErrorRecordCount();
			IncParsedRecordCount();
			return false;
		}
		IncParsedRecordCount();
		return true;
	}

	private boolean ParseNotDelimited(String str) {
		int a = 0;
		FileField ff = null;
		String data = "";
		int m = 0, sz = 0;
		try {
			if (this.getSizeInFile() > str.length()){
				log.error("Buffer too small");
			    db.setLevelWarning(2, "Buffer too small", 0);
				//throw new FileException("Buffer too small");
			}
			for (m = 0; m < Fields.size(); m++) {
				ff = (FileField) Fields.get(m);
				// byte[] b = new byte[ff.getSizeInFile()];
				sz = ff.getSizeInFile();
				data = str.substring(a, a + sz);
				a = a + sz;
				// for(int k = 0; k < sz; k++)
				// b[k] = (byte)str.charAt(a++);
				if (ff.getName().equals("ECP_TX")) {
					a = a;
				}
				// NOTE: this code is for handling Equifax_Credit_Score date
				// birth -rahul June 10,2009
				// If there is a format exception or the date of birth is not
				// present then null will be inserted in the database
				if (ff.getName().equals("ECR_BIRTH_DATE")) {
					try {

						java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat(
								ff.getDateFormat());
						sdf.setLenient(false);
						sdf.parse(data);
					} catch (Exception e) {
						log.info("got exception for data="+ data+ ",null would be inserted for this date field");
						data = "";
					}
				}

				//String msg1 = "Field " + ff.getOwner().getName() + "."+ ff.getName() + " failed validation. ";
				// System.out.println(msg1);
				 //System.out.println("Data in file = " + data);			
				
				 if (!ff.validateData(data)) {
					String msg = "Field " + ff.getOwner().getName() + "."
							+ ff.getName() + " failed validation. ";
					// System.out.println(msg);
					// System.out.println("Data in file = " +
					// FieldUtil.getString(b) + " (" + FieldUtil.getHexVal(b) +
					// ").");
					log.error(msg);
				    db.setLevelWarning(2, msg, 0);
					//throw new FileException(msg);
				}
			}
		} catch (Exception ex) {
			log.error("Error in parsing : " + ff.getName() + "\n"
					+ ex.getMessage());
			log.error("Data in error : " + FileUtil.getHexVal(data));
			System.out.println("Data in error : " + FileUtil.getHexVal(data));
			IncErrorRecordCount();
			IncParsedRecordCount();
			return false;
		}
		IncParsedRecordCount();
		return true;
	}

	public boolean Parse(FileLine fl) {
		boolean b = false;
		if (this.isDelimited())
			b = ParseDelimited(fl.lineStr);
		else
			b = ParseNotDelimited(fl.lineStr);
		if (b)
			fl.dirty = true;
		return b;
	}

	public void processFieldDefinition(ArrayList al) throws Exception {
		String FieldId = this.getName() + ".Field.";
		for (int r = 0; r < al.size(); r++) {
			String s = ((Property) al.get(r)).getKey();
			if (s != null && s.toUpperCase().startsWith(FieldId.toUpperCase())
					&& s.length() > FieldId.length()) {
				String key1 = s.substring(FieldId.length()).trim();
				processEntry(key1, ((Property) al.get(r)).getValue());
			}
		}
		Validate();
	}

	public static String ConvertControlChars(String s) {
		if (s != null) {
			for (int k = 0; k < ctrlPat.length; k++) {
				s = ctrlPat[k].matcher(s).replaceAll(
						"" + ControlChars.charAt(k));
			}
		}
		return s;
	}

	private void processEntry(String s, String s1) throws Exception {
		StringTokenizer stringtokenizer = new StringTokenizer(s, ".");
		int i = stringtokenizer.countTokens();
		if (i != 2){
			log.error("Invalid property found " + s);
			db.setLevelWarning(2, "Invalid property found " + s, 0);
			//throw new FileException("Invalid property found " + s);
		}
		String fieldNm = stringtokenizer.nextToken();
		String propName = stringtokenizer.nextToken();
		FileField ff = getFieldByName(fieldNm);
		if (ff == null) {
			ff = new FileField(fieldNm, this);
			AddField(ff);
		}
		if (propName.equalsIgnoreCase(MANDATORY_SUFFIX))
			ff.isMandatory(s1.equalsIgnoreCase("Y"));
		else if (propName.equalsIgnoreCase(TYPE_SUFFIX))
			ff.setType(s1);
		else if (propName.equalsIgnoreCase(TOTALSIZE_SUFFIX))
			ff.setTotSize(Integer.parseInt(s1));
		else if (propName.equalsIgnoreCase(DECIMAL_SUFFIX))
			ff.setDecimal(Integer.parseInt(s1));
		else if (propName.equalsIgnoreCase(SIGN_SUFFIX))
			ff.setSigned(s1.equalsIgnoreCase("Y"));
		else if (propName.equalsIgnoreCase(IMPORT_SUFFIX))
			ff.setImported(s1.equalsIgnoreCase("Y"));
		else if (propName.equalsIgnoreCase(FORMAT_SUFFIX))
			ff.setFormat(s1);
		else if (propName.equalsIgnoreCase(CONSTANT_SUFFIX))
			ff.setConstant(ConvertControlChars(s1));
		else if (propName.equalsIgnoreCase(IGNORE_SPACES_SUFFIX))
			ff.CanIgnoreSpaces(s1.equalsIgnoreCase("Y"));
		else{
			log.error("Unknown field property : " + propName+ " in field " + getName() + "." + fieldNm);
			db.setLevelWarning(2, "Unknown field property : " + propName+ " in field " + getName() + "." + fieldNm, 0);
			//throw new FileException("Unknown field property : " + propName+ " in field " + getName() + "." + fieldNm);
		}
	}

}
