package com.lcl.pcb.v9.idf.jobflow.config;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Class to store unmarshalled customized interface information 
 * loaded via JaxB from Interface Extractor.xml. 
 */
@XmlRootElement(name = "ExtractorXmlFile")
public class ExtractorData {
	private String interfaceName;

	public String getInterfaceName() {
		return interfaceName;
	}

	@XmlElement(name = "InterfaceName")
	public void setInterfaceName(String interfaceName) {
		this.interfaceName = interfaceName;
	}
	
	private String query;
	
	public String getQuery() {
		return query;
	}

	@XmlElement(name = "Query")
	public void setQuery(String query) {
		this.query = query;
	}	
	
	private String queryChanges;
	
	public String getQueryChanges() {
		return queryChanges;
	}

	@XmlElement(name = "QueryChanges")
	public void setCustomize(String queryChanges) {
		this.queryChanges = queryChanges;
	}		

	private ExtractorData.Layout layout;
		public ExtractorData.Layout getLayout() {
		return layout;
	}

	@XmlElement(name = "Layout")
	public void setLayout(ExtractorData.Layout layout) {
		this.layout = layout;
	}


	public static class Layout {
		
		private ArrayList<LField> lField;

		public ArrayList<LField> getLField() {
			return lField;
		}

		@XmlElement(name = "LField")
		public void setLField(ArrayList<LField> lField) { 
			this.lField = lField;
		}
	}

	public static class LField {
		
		private String FieldName;
		private String MaxLength;
		private String Description;

		public String getFieldName() {
			return FieldName;
		}

		@XmlElement(name = "FieldName")
		public void setFieldName(String fieldName) {
			FieldName = fieldName;
		}

		public String getMaxLength() {
			return MaxLength;
		}

		@XmlElement(name = "MaxLength")
		public void setMaxLength(String maxLength) {
			MaxLength = maxLength;
		}

		public String getDescription() {
			return Description;
		}

		@XmlElement(name = "Description")
		public void setDescription(String description) {
			Description = description;
		}
	
	}	

}
