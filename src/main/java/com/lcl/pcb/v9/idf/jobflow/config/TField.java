package com.lcl.pcb.v9.idf.jobflow.config;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Class to store unmarshalled TrailerFields information 
 * loaded via JaxB from CopyBook.xml/InterfaceLoader.xml
 */
@XmlRootElement(name = "TSegmentField")
public class TField {
	
	private String TFieldName;
	private String TFieldMandatory;
	private String TFieldType;
	private String TFieldSize;
	private String TFieldDecimal;
	private String TFieldSigned;
	private String TFieldImport;
	public String getTFieldName() {
		return TFieldName;
	}
	@XmlElement(name = "TFieldName")
	public void setTFieldName(String tFieldName) {
		TFieldName = tFieldName;
	}
	public String getTFieldMandatory() {
		return TFieldMandatory;
	}
	@XmlElement(name = "TFieldMandatory")
	public void setTFieldMandatory(String tFieldMandatory) {
		TFieldMandatory = tFieldMandatory;
	}
	public String getTFieldType() {
		return TFieldType;
	}
	@XmlElement(name = "TFieldType")
	public void setTFieldType(String tFieldType) {
		TFieldType = tFieldType;
	}
	public String getTFieldSize() {
		return TFieldSize;
	}
	@XmlElement(name = "TFieldSize")
	public void setTFieldSize(String tFieldSize) {
		TFieldSize = tFieldSize;
	}
	public String getTFieldDecimal() {
		return TFieldDecimal;
	}
	@XmlElement(name = "TFieldDecimal")
	public void setTFieldDecimal(String tFieldDecimal) {
		TFieldDecimal = tFieldDecimal;
	}
	public String getTFieldSigned() {
		return TFieldSigned;
	}
	@XmlElement(name = "TFieldSigned")
	public void setTFieldSigned(String tFieldSigned) {
		TFieldSigned = tFieldSigned;
	}
	public String getTFieldImport() {
		return TFieldImport;
	}
	@XmlElement(name = "TFieldImport")
	public void setTFieldImport(String tFieldImport) {
		TFieldImport = tFieldImport;
	}

}
