package com.lcl.pcb.v9.idf.jobflow;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.lcl.pcb.v9.dao.V9DAOManager;
import com.lcl.pcb.v9.generic.security.QuantumProcessDecrypt;
import com.lcl.pcb.v9.generic.security.QuantumProcessEncrypt;
import com.lcl.pcb.v9.generic.security.V9SecureAES;
import com.lcl.pcb.v9.idf.jobflow.config.IDFConfig;
import com.lcl.pcb.v9.idf.jobflow.factory.V9Factory;
import com.lcl.pcb.v9.idf.jobflow.factory.V9Interface;


/**
 * Generic class to process the interface.
 * Customization for the interface is handled through the 
 * interface definition files in properties folder containing idf. 
 */
public class V9Client {

	/**
	 * Main method takes input parameter of interface name 
	 * reads corresponding idf file to get customized values for
	 * the interface and stores it in a singleton for future access.
	 * 
	 * @param  args[] the interface name and base directory
	 * @return void
	 */
	public static void main(String[] args) throws Exception { 
		
		//local config:
		
		/*String interfaceName = "HO70";
		String interfaceBaseDir = "InterfaceDefinition/HO70";	
		String datafileLocation = "InterfaceDefinition/HO70/properties/tsysADMdisposition.uatv";
		String fileSize = "-9"
		args = new String[4];
		args[0] = "VO56A";
		args[1] = "InterfaceDefinition/VO56A";	
		//args[2] = "InterfaceDefinition/VO56A/properties/BHN_PCB_GPR_MC_NFEXT_20160219_223031.txt";
		args[2] = "InterfaceDefinition/VO56A/properties/BHN_PCB_GPR_MC_NFEXT_20160219_223031.txt";
		//args[2] = "InterfaceDefinition/VO56A/properties/PCBANK_DETAILED_AGENTDAILY_YYYYMMDD.uatv";
		args[3] = "-9";
		args[0] = "VO58A";
		args[1] = "InterfaceDefinition/VO58A";	
		args[2] = "InterfaceDefinition/VO58A/properties/BHN_PCB_GPR_MC_AUTHEXT_20160226_224039.txt";
		args[3] = "-9";
		args[0] = "VI15";
		args[1] = "InterfaceDefinition/VI15";	
		args[2] = "InterfaceDefinition/VI15/properties/BHN_PCB_GPR_MC_AUTHEXT_20160226_224039.txt";
		args[3] = "-9";*/

		//dev server config:
		if (args.length != 4) {
			System.err
					.println("Syntax is :\n\t V9_Loader [InterfaceName] [InterfaceBaseDir] [DataFile] [FileSize]");
			System.exit(1);
		}		
		String interfaceName=args[0];
		String interfaceBaseDir=args[1];	
		String datafileLocation=args[2];
		String fileSize = args[3];
				
		//V9 Initial Setup
		IDFConfig idfConfig = IDFConfig.getInstance(); // singleton to store
		idfConfig.readConfigIdf(interfaceName, interfaceBaseDir);
		
		//Setting the Datafileloacation(arg[2]) and interfacename(args[0]) in IDFData.
		idfConfig.getIdfData().setDatafileDatafileLocation(datafileLocation);
		idfConfig.getIdfData().setFilePropName(interfaceName);
		idfConfig.getIdfData().setFileSize(fileSize);
		
		//Decrypt all encrypted data in idf file using V9SecureAES class
		//Passwords in idf file are encrypted using java.security and javax.crypto.cipher libraries		
		V9SecureAES security = new V9SecureAES();		
		String encryptedText="";
		String decryptedText="";
		encryptedText = idfConfig.getIdfData().getLogDblogPass();
			decryptedText=security.decrypt(encryptedText);	
			idfConfig.getIdfData().setLogDblogPass(decryptedText);
		encryptedText = idfConfig.getIdfData().getCsvloadDbPass();
			decryptedText=security.decrypt(encryptedText);
			idfConfig.getIdfData().setCsvloadDbPass(decryptedText);		
		// Added below part on 5th Feb for PGP Decrypt Logic
		encryptedText = idfConfig.getIdfData().getDecryptDbPass();
			decryptedText = security.decrypt(encryptedText);
			idfConfig.getIdfData().setDecryptDbPass(decryptedText);
		// Added below part on 25th Feb for PGP Encrypt Logic
		encryptedText = idfConfig.getIdfData().getEncryptDbPass();
			decryptedText = security.decrypt(encryptedText);
			idfConfig.getIdfData().setEncryptDbPass(decryptedText);

		//NOTE: no file logging can happen before this point (V9 Initial Setup)
		//because configLog4J2() sets the log file naming and level
		//This means IDFConfig/XmlUtil/V9dDAOMAanger/V9DAOConnectionManager can not execute logging
		//Since they are needed in determining the name of the log file and level and job sequence number
		configLog4j2(); 			
		Logger logger = LogManager.getLogger(V9Client.class.getName());		
		
		
		logger.debug(System.getProperty("log4j2LogFile"));	
		logger.debug(System.getProperty("log4j2LogFileLevel"));	
		
		//Below Part was added for the V9 PGP Decryption logic as part of Quantum 2 Release.
		//For this all the respective IDF files will be modified to include the flag <decryptProcessFile> as Y indicating for that job
		//incoming file will be PGP encrypted and V9 Framework needs to decrypt the file
		if(null!=idfConfig.getIdfData().getDecryptProcessFile() && "Y".equalsIgnoreCase(idfConfig.getIdfData().getDecryptProcessFile())){
			logger.debug("Executing V9 Decryption Logic");
			QuantumProcessDecrypt quantumProcessDecrypt = new QuantumProcessDecrypt();
			quantumProcessDecrypt.execute();
		}else{
			logger.debug("Skipping V9 Decryption Logic");
		}
		
		logger.debug("V9 INITIAL SETUP COMPLETED-----------");
		logger.info("IDFConfig called");
		logger.info("IDFConfig singleton instantiated and IDF file read");
		logger.info("IDFConfig called XmlUtil to unmarshal IDF file and put into idfData");
		logger.info("IDFConfig singleton populated with idfData info");
		logger.info("V9DAO job sequence number generated");
		logger.info("V9DAO monitoring log ready");		
		
		logger.debug("JOB STARTED----------");
		logger.info("Call V9Factory to obtain IDF class type (Loader Extractor or Reporter)");
		V9Interface idfType = V9Factory.getIdfType(); // factory will access IDFConfig singleton for idf field info

		logger.info("Call execute method of class type returned by V9Factory");
		try {
			idfType.execute();
			
			// Below Part was added for the V9 PGP Encryption logic as part of
			// Quantum 2 Release.
			// For this all the respective IDF files will be modified to include the
			// flag <encryptProcessFile> as Y indicating for that job
			// outgoing file needs to be PGP encrypted and V9 Framework needs to
			// generated encrypted
			// file
			if (null != idfConfig.getIdfData().getEncryptProcessFile()
					&& "Y".equalsIgnoreCase(idfConfig.getIdfData()
							.getEncryptProcessFile())) {
				logger.debug("Executing V9 Encryption Logic");
				QuantumProcessEncrypt quantumProcessEncrypt = new QuantumProcessEncrypt();
				quantumProcessEncrypt.execute();
			} else {
				logger.debug("Skipping V9 Encryption Logic");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			logger.debug("Could not execute Loader/Reporter/Extractor operation");			
            System.err.println("Could not execute Loader/Reporter/Extractor operation");
            System.exit(1);
		}
			
		V9DAOManager db = new V9DAOManager();
		db.closeV9A0ConnectionManager();		
		
		logger.info("Interface Main method completed");
		logger.debug("JOB COMPLETED----------");		

	}


	/**
	 * This method sets up the Log4j2 text file logging for the 
	 * application
	 * It uses information from the singleton and log database to
	 * create the name of the log file
	 *
	 * @param logger for log4j logging 
	 * @return void
	 */
	private static void configLog4j2() throws Exception {
		
		String interfaceName = IDFConfig.getInstance().getIdfData().getFilePropName();
		
		V9DAOManager db = new V9DAOManager();
		int sequenceNum = db.getJobSequenceNumber();	
		
		IDFConfig.getInstance().setSequenceNum(sequenceNum);

		
		String uniqueID = IDFConfig.getInstance().getIdfData().getFilePropIdfUniqueId();

		Date date = Calendar.getInstance().getTime();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		String logFileLocation=IDFConfig.getInstance().getIdfData().getLogFilelogFilelogLocation();
		String logFileLevel=IDFConfig.getInstance().getIdfData().getLogFilelogFileLevel();
		System.setProperty("log4j2LogFile", logFileLocation + "/" + interfaceName + "_" + sequenceNum + "_"	+ uniqueID + "_" + sdf.format(date) + ".log");		
		System.setProperty("log4j2LogFileLevel", logFileLevel);		
		
	}

}
