package com.lcl.pcb.v9.idf.jobflow.factory;

import java.io.File;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import com.lcl.pcb.v9.dao.V9DAOManager;
import com.lcl.pcb.v9.generic.loader.LoaderUtil;
import com.lcl.pcb.v9.generic.loader.V9SqlLoader;
import com.lcl.pcb.v9.idf.jobflow.config.IDFConfig;
import com.lcl.pcb.v9.utilities.audit.AuditTablePojo;
import com.lcl.pcb.v9.utilities.audit.AuditTableUtil;
import com.lcl.pcb.v9.utilities.fileReader.FileUtil;

/**
 * Implements V9Interface This class is used to execute the reading of the
 * interface data file and create a csv file. The csv file is then uploaded to
 * the database tables.
 */
public class V9Loader implements V9Interface {
	public static int totalRecCount = 0;
	public void execute() throws Exception {

		V9DAOManager db = new V9DAOManager();
		ExecutorService pool = Executors.newFixedThreadPool(10);
		List<Future<?>> submitted = new ArrayList<Future<?>>();
		boolean completed = false;
		boolean notDone = false;
		FileUtil fileUtilObj = null;
		String copyBookLocation = "";
		String dataFileLoacation = "";
		String bodySegmentsMandatory = "";
		ArrayList<String> totalCSVFilesList = new ArrayList<String>();
		HashMap<String, Integer> totRecParsed = new HashMap<String, Integer>();
		HashMap<String, AuditTablePojo> auditTableMap = new HashMap<String, AuditTablePojo>(); 
		String csvNames = "";
		List<String> ctlfilename = new ArrayList<String>();
		List<String> logfilename = new ArrayList<String>();
		List<String> csvfilename = new ArrayList<String>();
		List<String> badcsvfilename = new ArrayList<String>();
		String validCtlName = "";
		String validName = "";
		String logNameFormat = "";
		String[] segMentName ;
		int seqNum = IDFConfig.getInstance().getSequenceNum();
		String interfaceName= IDFConfig.getInstance().getIdfData().getFilePropName();
		String auditFlag = IDFConfig.getInstance().getIdfData().getAuditFlag(); 
		String seqNumVal = String.valueOf(seqNum);
		Class[] paramList = new Class[1];
		paramList[0] = ArrayList.class;
		Class noparams[] = {};
		boolean success = true;
		int recordsInserted = 0;
		int badrecords = 0;
		String fileSizeVal = null;
		Date date = Calendar.getInstance().getTime();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		db.setLevel(1, 0, 0);
		logger.debug("(LEVEL 1) - Start");

		logger.debug("execute() method");
		logger.debug("INPUT: "
				+ IDFConfig.getInstance().getIdfData().getFilePropName()
				+ "Loader.xml");

		db.setLevel(2, 0, 0);
		logger.debug("(LEVEL 2) - Extract Data, transformation, apply validation");

		// Interface property file is always xml (HO10Loader.xml this is the
		// properties manually made version of the interface's copybook)
		// read HO10Loader.property file xml format (input)

		// get from IdfConfig singleton file type ASCII/ebcdic/xml (this is the
		// data file type)
		String filePropType = IDFConfig.getInstance().getIdfData()
				.getFilePropType();

		logger.debug("INPUT: Data File of type: " + filePropType);
		// read data file (input)
		// <datafile-location>/y/app/pcb/TSYS/</datafile-location>

		// If the data file is EBCDIC format,will parse the data and create the
		// CSV files.
		if (filePropType.equals(EBCDIC)) {

			logger.debug("EBCDIC Parsing");
			logger.debug("(LEVEL 2) -Start EBCDIC Parsing");
			// Read idfxml file
			copyBookLocation = IDFConfig.getInstance().getIdfData()
					.getCopybookxmlFileLocation();
			dataFileLoacation = IDFConfig.getInstance().getIdfData()
					.getDatafileDatafileLocation();
			fileUtilObj = new FileUtil(copyBookLocation, dataFileLoacation);
			fileUtilObj.process(copyBookLocation, dataFileLoacation);
			// All Segments CSV files added to the totalCSVFileList.
			totalCSVFilesList = FileUtil.totalCSVFileNamesList;
			// Validation for All Segments are Mandatory.
			bodySegmentsMandatory = IDFConfig.getInstance().getIdfData()
					.getBodySegmentsMandatory();
			if (bodySegmentsMandatory.equalsIgnoreCase("Y")) {
				if (!(totalCSVFilesList.size() == FileUtil.getBodySegLength() + 1)) {

					logger.error("All Segments are Mandatory.");
					db.setLevelWarning(2, "All Segments are Mandatory.", 0);
				}
			}

		} else if (filePropType.equals(ASCII)) {
			
			// Read idfxml file
			copyBookLocation = IDFConfig.getInstance().getIdfData()
					.getCopybookxmlFileLocation();
			dataFileLoacation = IDFConfig.getInstance().getIdfData()
					.getDatafileDatafileLocation();
			fileUtilObj = new FileUtil(copyBookLocation, dataFileLoacation);
			fileUtilObj.process(copyBookLocation, dataFileLoacation);
			// All Segments CSV files added to the totalCSVFileList.
			totalCSVFilesList = FileUtil.totalCSVFileNamesList;
			// Validation for All Segments are Mandatory.
			bodySegmentsMandatory = IDFConfig.getInstance().getIdfData()
					.getBodySegmentsMandatory();
			if (bodySegmentsMandatory.equalsIgnoreCase("Y")) {
				if (!(totalCSVFilesList.size() == FileUtil.getBodySegLength() + 1)) {

					logger.error("All Segments are Mandatory.");
					db.setLevelWarning(2, "All Segments are Mandatory.", 0);
				}
			}


		} else if (filePropType.equals(XML)) {

		}

		// check if there are errors. if there is call this and pass a warning
		// message
		// db.setLevelWarning(2,"Warning Message");

		db.setLevel(3, 0, 0);
		logger.debug("(LEVEL 3) -Prepare CSV");

		// output csv file (output)
		// csv file goes to
		// csv-file-location>/VApp/InterfaceDefination/csv</csv-file-location>

		logger.debug("OUTPUT: CSV files");

		// check if there are errors. if there is call this and pass a warning
		// message
		// db.setLevelWarning(3,"Warning Message");

		// int rowCount = 123;
		// Manjula to set rowCount from csv files
		// total segments records list.
		totRecParsed = FileUtil.totalRecParsedList;

		for (Entry<String, Integer> entry : totRecParsed.entrySet()) {
				totalRecCount += (entry.getValue());
		}

		logger.debug("Total Records Processed:" + totalRecCount);

		int rowCount = totalRecCount;
		db.setLevel(4, rowCount, 0);
		
		// If Audit flag is true insert Audit record into LOAD_RESULT table.
		if (auditFlag.equalsIgnoreCase("Y")){ 
			
			if (null != totalCSVFilesList && totalCSVFilesList.size() != 0) { 
				for (int i = 0; i < totalCSVFilesList.size(); i++) {
					
					csvNames = totalCSVFilesList.get(i).substring(0,
							totalCSVFilesList.get(i).lastIndexOf("."));
					validName = csvNames.substring(csvNames
							.lastIndexOf("/") + 1);
					if (validName != null
							&& !validName.equalsIgnoreCase("")) {
						validCtlName = validName
								.substring(
										0,
										validName.lastIndexOf("_"
												+ seqNumVal + ""));
					}
					
					
					AuditTablePojo auditPojo = new AuditTablePojo();
					auditPojo.setProcessNumber(seqNum+"");
					auditPojo.setSchemaName(IDFConfig.getInstance().getIdfData().getCsvloadDbSchema());
					auditPojo.setSourceInterface(interfaceName);
					auditPojo.setSourceNM(IDFConfig.getInstance().getIdfData()
							.getDatafileDatafileLocation().substring(IDFConfig.getInstance().getIdfData()
									.getDatafileDatafileLocation().lastIndexOf("/") + 1));
					fileSizeVal = IDFConfig.getInstance().getIdfData().getFileSize();
					if(fileSizeVal != null && !fileSizeVal.isEmpty()) {
						auditPojo.setSourceSize(fileSizeVal);
					}else{
						auditPojo.setSourceSize("-9");
					}
									
					auditPojo.setTableName("");
					
					for (Entry<String, Integer> entry : totRecParsed.entrySet()) {
						if(validName.toUpperCase().contains(entry.getKey().toUpperCase())){
							auditPojo.setSourceDtlRec(entry.getValue()+"");
							break;
						}
								
					}
						
					String ctlFileName = IDFConfig.getInstance().getIdfData()
							.getDatafileSqlloaderCtlFileLocation()
							+ "/"
							+ validCtlName
							+ IDFConfig.getInstance().getIdfData()
									.getDatafileSqlloaderCtlFilePattern();
					
					auditPojo.setTableName(AuditTableUtil.getTableName(ctlFileName));
					auditPojo = AuditTableUtil.insertAudit(auditPojo);
					auditTableMap.put(auditPojo.getTableName(),auditPojo);
					auditPojo = null;	
					csvNames = null;
					validName = null;
					validCtlName = null;
					ctlFileName = null;
				} // end for loop  totlaCSVFileList
			}//end if for totlaCSVFileList check
		}// audit flag if ends here

		logger.debug("CSV files done, countinue to update Database");

		// custom validation and logics for each interface
		// validationClassName will loaded dynamically based on interface name
		// For Each interface "{interfacename}Validation" class should be
		// created with default methods
		if (totalCSVFilesList != null) {
			String validationClassName = "com.lcl.pcb.v9.custom.loader."
					+ IDFConfig.getInstance().getIdfData().getFilePropName()
					+ "Validation";
			Class<?> cls = Class.forName(validationClassName);
			Object obj = cls.newInstance();
			Method method = cls.getDeclaredMethod("doValidation", paramList);
			success = (Boolean) method.invoke(obj, totalCSVFilesList);
			if (!success) {
				db.setLevelWarning(4, "Validation Failed", 0);
			}
		}

		logger.debug("Read "
				+ IDFConfig.getInstance().getIdfData().getFilePropName()
				+ "Update.xml");
		// UPDATE HO10Update.xml from csv to database
		// get from IdfConfig singleton loadMethod
		// SQLLOADER/SQLPREPARED/STOREDPROCEDURE
		String loadMethod = IDFConfig.getInstance().getIdfData()
				.getLoadFeaturesloadMethod(); // this is from singleton

		if (loadMethod.equals(SQLLOADER)) {
			logger.debug("SQLLOADER");
			logger.debug("(LEVEL 4) -Start SQL Loader");

			// new from V8
			String csvLoadParallel = IDFConfig.getInstance().getIdfData()
					.getLoadFeaturesloadMethodCsvLoadParallel();
			if (csvLoadParallel.equals(YES)) { // this is from singleton
				logger.debug("Parallel");
				// Getting the list of csv files generated and mapping it with
				// ctl files to get loaded in tables using SQL Loader-parallely
				// by use of ExecutorService
				logger.debug("Total CSV Files Generated:"
						+ totalCSVFilesList.size());
				for (int j = 0; j < totalCSVFilesList.size(); j++) {
					csvNames = totalCSVFilesList.get(j).substring(0,
							totalCSVFilesList.get(j).lastIndexOf("."));
					validName = csvNames
							.substring(csvNames.lastIndexOf("/") + 1);
					logger.debug("Generated CSV File Name: " + validName);
				}
				if (null != totalCSVFilesList && totalCSVFilesList.size() != 0) {
					for (int i = 0; i < totalCSVFilesList.size(); i++) {
						csvNames = totalCSVFilesList.get(i).substring(0,
								totalCSVFilesList.get(i).lastIndexOf("."));
						validName = csvNames.substring(csvNames
								.lastIndexOf("/") + 1);
						if (validName != null
								&& !validName.equalsIgnoreCase("")) {
							validCtlName = validName
									.substring(
											0,
											validName.lastIndexOf("_"
													+ seqNumVal + ""));
						}
						//Creating Dynamic ctl file for LOAD_RESULT_UUID
						if (auditFlag.equalsIgnoreCase("Y")){
							
							validCtlName = AuditTableUtil.rewriteCtlFile(auditTableMap,IDFConfig.getInstance().getIdfData()
									.getDatafileSqlloaderCtlFileLocation()
									+ "/"
									+ validCtlName
									+ IDFConfig.getInstance().getIdfData()
											.getDatafileSqlloaderCtlFilePattern());
							ctlfilename.add(validCtlName);
							
						}
						else{
						ctlfilename.add(IDFConfig.getInstance().getIdfData()
								.getDatafileSqlloaderCtlFileLocation()
								+ "/"
								+ validCtlName
								+ IDFConfig.getInstance().getIdfData()
										.getDatafileSqlloaderCtlFilePattern());
						}
						segMentName=validName.split("_");
						logNameFormat=interfaceName+"_"+seqNumVal+"_"+segMentName[1]+"_"+segMentName[2]+"_"+sdf.format(date);
						logfilename.add(IDFConfig.getInstance().getIdfData()
								.getLogFilelogFilelogLocation()
								+ "/" + logNameFormat + "." + "log");
						csvfilename.add(IDFConfig.getInstance().getIdfData()
								.getDatafileCsvFileLocation()
								+ "/"
								+ validName
								+ IDFConfig.getInstance().getIdfData()
										.getDatafileCsvNamePattern());
						badcsvfilename.add(IDFConfig.getInstance().getIdfData()
								.getDatafileBadFileLocation()
								+ "/"
								+ validName
								+ IDFConfig.getInstance().getIdfData()
										.getDatafileBadNamePattern());
						try {
							logger.debug("Call to V9 SqlLoader Class to Load Csv File:"
									+ csvfilename.get(i));
							submitted.add(pool.submit(new V9SqlLoader(ctlfilename.get(i),
									logfilename.get(i), csvfilename.get(i),
									badcsvfilename.get(i))));
						} catch (Exception e) {
							throw e;
						}
					}
				}				
				
				while( !completed ) {
				  for( Future<?> task : submitted ) {
				     if(  !task.isDone() ) {
				          notDone = true;
				          break;
				       }
				  }
				  if( notDone ) {
				     Thread.sleep(60,000);
				     notDone = false;
				  } else {
					  logger.debug("The Pool task completed, program will continue to next steps");
				     completed = true;
				  }
				} 

				pool.shutdown();
				// Process waits until all log file gets generated
				Integer fileCount = 0;
				List<String> fileStatus = new ArrayList<String>(logfilename);
				while (!(fileCount == logfilename.size())) {
					for (int i = 0; i < fileStatus.size(); i++) {
						File f = new File(fileStatus.get(i).toString());
						if (f.exists()) {
							if (f.length() > 0) {
								fileCount++;
								fileStatus.remove(i);
							}

						}

					}
				}
				fileStatus.clear();
				fileStatus = null;

			} else if (csvLoadParallel.equals(NO)) {
				logger.debug("Not Parallel");
			}
		} else if (loadMethod.equals(SQLPREPARED)) {
			logger.debug("SQLPREPARED");
			// insert into table (loop) from CSV this is already in v8
		} else if (loadMethod.equals(STOREDPROCEDURE)) {
			logger.debug("STOREDPROCEDURE");
			// (similar to sql loader but calls stored procedure)
		}
		logger.debug("DB Load completed");

		// check if there are errors. if there is call this and pass a warning
		// message
		// db.setLevelWarning(4,"Warning Message");

		// Getting the success record count by reading the log files generated
		// by SQL Loader for load of CSV files
		LoaderUtil loaderUtilObj = new LoaderUtil(logfilename, csvfilename);
		recordsInserted = LoaderUtil.successLoadCount;
		
		//if LoadResult/Audit flag is true update Audit
		if (auditFlag.equalsIgnoreCase("Y")){
			for(String key: auditTableMap.keySet())
			{
				AuditTablePojo auditPojo = auditTableMap.get(key);
				String recCount = LoaderUtil.loadCount.get(key);
				if(recCount!=null){
					String[] recCountArry = recCount.split("\\|");
					String successSegCount = recCountArry[0];
					String failureSegCount = recCountArry[1];
					auditPojo.setLoadError(failureSegCount);
					auditPojo.setTotalRecordLoad(successSegCount);
					AuditTableUtil.updateAudit(auditPojo);
					auditPojo = null;
				}
			}
			
			// deleting temp ctl files
			String tempCtlFilePath = IDFConfig.getInstance().getIdfData().getDatafileSqlloaderCtlFileLocation();
			File f = new File(tempCtlFilePath); // ctl file path
			File[] files = f.listFiles();
			for (File file : files) {
				if (file.isDirectory()) {
					logger.debug("Directory");
				} else {
					if(file.getName().contains("_temp")){
					   file.delete();
						
					}
				}
			}// End of temp ctl files delete	
		}
		
		String validationfinalizeClassName = "com.lcl.pcb.v9.custom.loader."
			+ IDFConfig.getInstance().getIdfData().getFilePropName()
			+ "Validation";
		Class<?> cls = Class.forName(validationfinalizeClassName);
		Object obj = cls.newInstance();
		Method method1 = cls.getDeclaredMethod("doFinalize", paramList);
		logger.debug("doFinalize method called.");
		method1.invoke(obj, totalCSVFilesList);
		
		logger.debug("Total Number of Good Records Loaded into Tables:"
				+ recordsInserted);
		// int recordsInserted = 123;
		// Saranya to set recordsInserted into database
		db.setLevel(5, rowCount, recordsInserted);
		logger.debug("(LEVEL 5) - Error logging");

		
		//WO chnage :
		int headerCount=0;
		String multiHeaderTrailerFlag = IDFConfig.getInstance().getIdfData().getMultiHeaderTrailerFlag();
		if (!(multiHeaderTrailerFlag.equalsIgnoreCase("")) && multiHeaderTrailerFlag.equalsIgnoreCase("Y"))
		{
			 headerCount =FileUtil.headerTrailerCount;
			
		}

		// check if there are errors. if there is call this and pass a warning
		// message
		// db.setLevelWarning(5,"Warning Message");

		// error logging code
		// Getting the bad record count by reading the log files generated by
		// SQL Loader for load of CSV files
		badrecords = LoaderUtil.errorLoadCount;

		if (badrecords != 0) {
			logger.error("Total Bad Records Found While Loading To Tables:"
					+ badrecords);

			String validationClassName = "com.lcl.pcb.v9.custom.loader."
					+ IDFConfig.getInstance().getIdfData().getFilePropName()
					+ "Validation";
			Class<?> cls1 = Class.forName(validationClassName);
			Object obj1 = cls1.newInstance();
			Method method = cls1.getDeclaredMethod("doErrorLog", noparams);
			method.invoke(obj1, null);
			db.setLevelWarning(5, "Bad Data Found while Loading CSV File",
					badrecords);
		} else if ((totalRecCount-(headerCount)) != recordsInserted && IDFConfig.getInstance().getIdfData().isRecordCountValidation() ) {
			logger.error("DB error occured while Loading CSV File total records  " + totalRecCount + " and total processed records " + recordsInserted);
			db.setLevelWarning(5, "DB error occured while Loading CSV File", totalRecCount);

		} else {
			logger.debug("Total Number of Bad Records Found:" + badrecords);
		}

		db.setLevelEnd(5);
		logger.debug("(LEVEL 5) - End");

	}
	@Override
	public void addProperties(Map<String, String> properties) throws Exception {
		// TODO Auto-generated method stub
		
	}
}
