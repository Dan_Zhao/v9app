package com.lcl.pcb.v9.idf.jobflow.factory;

import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Interface class that is implemented by the V9Loader
 * V9Extractor and V9Reporter objects which are returned
 * by the V9Factory to the V9Client
 */

public interface V9Interface {

	static final Logger logger = LogManager.getLogger(V9Interface.class.getName());	
	
	
	public static final String XML = "XML";
	public static final String EBCDIC = "EBCDIC";
	public static final String ASCII = "ASCII";		
	
	public static final String SQLLOADER = "SQLLOADER";
	public static final String SQLPREPARED = "SQLPREPARED";
	public static final String STOREDPROCEDURE = "STOREDPROCEDURE";		
	
	public static final String YES = "YES";
	public static final String NO = "NO";	
	
	public void addProperties(Map<String, String> properties) throws Exception;
	//This will be used for setting StartLevelLogging and BaseInterfaceDir

	public void execute() throws Exception;
    
}
