package com.lcl.pcb.v9.idf.jobflow.config;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Class to store unmarshalled BodyFields information 
 * loaded via JaxB from Interface Loader.xml
 */
@XmlRootElement(name = "BSegmentField")
public class BField {
	
	private String BFieldName;
	private String BFieldMandatory;
	private String BFieldType;
	private String BFieldSize;
	private String BFieldDecimal;
	private String BFieldSigned;
	private String BFieldImport;
	
	public String getBFieldName() {
		return BFieldName;
	}
	@XmlElement(name = "BFieldName")
	public void setBFieldName(String bFieldName) {
		BFieldName = bFieldName;
	}
	public String getBFieldMandatory() {
		return BFieldMandatory;
	}
	@XmlElement(name = "BFieldMandatory")
	public void setBFieldMandatory(String bFieldMandatory) {
		BFieldMandatory = bFieldMandatory;
	}
	public String getBFieldType() {
		return BFieldType;
	}
	@XmlElement(name = "BFieldType")
	public void setBFieldType(String bFieldType) {
		BFieldType = bFieldType;
	}
	public String getBFieldSize() {
		return BFieldSize;
	}
	@XmlElement(name = "BFieldSize")
	public void setBFieldSize(String bFieldSize) {
		BFieldSize = bFieldSize;
	}
	public String getBFieldDecimal() {
		return BFieldDecimal;
	}
	@XmlElement(name = "BFieldDecimal")
	public void setBFieldDecimal(String bFieldDecimal) {
		BFieldDecimal = bFieldDecimal;
	}
	public String getBFieldSigned() {
		return BFieldSigned;
	}
	@XmlElement(name = "BFieldSigned")
	public void setBFieldSigned(String bFieldSigned) {
		BFieldSigned = bFieldSigned;
	}
	public String getBFieldImport() {
		return BFieldImport;
	}
	@XmlElement(name = "BFieldImport")
	public void setBFieldImport(String bFieldImport) {
		BFieldImport = bFieldImport;
	}


}
