package com.lcl.pcb.v9.idf.jobflow.config;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Class to store unmarshalled HeaderFields information 
 * loaded via JaxB from CopyBook.xml/InterfaceLoader.xml
 */
@XmlRootElement(name = "HSegmentField")
public class HField {
	private String HFieldName;
	private String HFieldMandatory;
	private String HFieldType;
	private String HFieldSize;
	private String HFieldDecimal;
	private String HFieldSigned;
	private String HFieldImport;
	private String HFieldFormat;

	public String getHFieldFormat() {
		return HFieldFormat;
	}

	@XmlElement(name = "HFieldFormat")
	public void setHFieldFormat(String hFieldFormat) {
		HFieldFormat = hFieldFormat;
	}

	public String getHFieldName() {
		return HFieldName;
	}

	@XmlElement(name = "HFieldName")
	public void setHFieldName(String hFieldName) {
		HFieldName = hFieldName;
	}

	public String getHFieldMandatory() {
		return HFieldMandatory;
	}

	@XmlElement(name = "HFieldMandatory")
	public void setHFieldMandatory(String hFieldMandatory) {
		HFieldMandatory = hFieldMandatory;
	}

	public String getHFieldType() {
		return HFieldType;
	}

	@XmlElement(name = "HFieldType")
	public void setHFieldType(String hFieldType) {
		HFieldType = hFieldType;
	}

	public String getHFieldSize() {
		return HFieldSize;
	}

	@XmlElement(name = "HFieldSize")
	public void setHFieldSize(String hFieldSize) {
		HFieldSize = hFieldSize;
	}

	public String getHFieldDecimal() {
		return HFieldDecimal;
	}

	@XmlElement(name = "HFieldDecimal")
	public void setHFieldDecimal(String hFieldDecimal) {
		HFieldDecimal = hFieldDecimal;
	}

	public String getHFieldSigned() {
		return HFieldSigned;
	}

	@XmlElement(name = "HFieldSigned")
	public void setHFieldSigned(String hFieldSigned) {
		HFieldSigned = hFieldSigned;
	}

	public String getHFieldImport() {
		return HFieldImport;
	}

	@XmlElement(name = "HFieldImport")
	public void setHFieldImport(String hFieldImport) {
		HFieldImport = hFieldImport;
	}


}
