package com.lcl.pcb.v9.idf.jobflow.config;

import com.lcl.pcb.v9.utilities.XmlUtil;

/**
 * Singleton class that populates CopyBookData with the unmarshalled 
 * information from the CopyBook.xml file/InterfaceLoader.xml file. 
 * This class is referenced throughout the application for 
 * interface specific logic.
 */

public class CopyBookConfig {
	
	// Singleton Pattern
	//private static final Logger logger = LogManager.getLogger(CopyBookConfig.class.getName());

	private CopyBookData copyBookData = null;

	public CopyBookData getCopyBookData() {
		return copyBookData;
	}

	public void setCopyBookData(CopyBookData copyBookData) {
		this.copyBookData = copyBookData;
	}

	public static void setInstance(IDFConfig instance) {

		// logger.info("Interface Main method started and called HO70Config singleton to read H070 file");

	}

	private static CopyBookConfig instance = null;

	protected CopyBookConfig(){
		// Exists to defeat instantiation.
		// logger.debug("Singleton Instantiated");
	}

	public static CopyBookConfig getInstance() {
		if (instance == null) {
			instance = new CopyBookConfig();
		}
		return instance;
	}


	/**
	 * This method calls the xml utility to do the JaxB
	 * conversion to pupulate this Singletonn
	 * It uses information from the singleton and log database to
	 * create the name of the log file
	 *
	 * @param  interfaceName	the name of the interface  
	 * @return void
	 */	
	public void readCopybook(String interfaceName) {
		// logger.debug("Read IDF file and populate singleton");

		String copybookFileName = IDFConfig.getInstance().getIdfData().getCopybookxmlFileLocation();
		//System.out.println("copybookFilename Path:"+copybookFileName);
		this.setCopyBookData(XmlUtil.readCopyBookData(copybookFileName));

		// logger.debug("Singleton HO70Config populated with ho70Data info");

	}


}
