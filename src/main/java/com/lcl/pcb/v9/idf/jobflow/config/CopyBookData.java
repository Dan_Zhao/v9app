package com.lcl.pcb.v9.idf.jobflow.config;

import java.util.ArrayList;

import javax.xml.bind.annotation.*;

/**
 * Class to store unmarshalled customized interface information 
 * loaded via JaxB from CopyBook.xml/InterfaceLoader.xml.
 */
@XmlRootElement(name = "CopyBookXmlFile")
public class CopyBookData {
	private String interfaceName;
	private String ebcdic;
	private String binary;
	private String variableSize;
	private String delimiterChar;

	public String getDelimiterChar() {
		return delimiterChar;
	}

	public String getInterfaceName() {
		return interfaceName;
	}

	@XmlElement(name = "InterfaceName")
	public void setInterfaceName(String interfaceName) {
		this.interfaceName = interfaceName;
	}

	@XmlElement(name = "DelimiterChar")
	public void setDelimiterChar(String delimiterChar) {
		this.delimiterChar = delimiterChar;
	}

	public String getBinary() {
		return binary;
	}

	@XmlElement(name = "Binary")
	public void setBinary(String binary) {
		this.binary = binary;
	}

	public String getVariableSize() {
		return variableSize;
	}

	@XmlElement(name = "VariableSize")
	public void setVariableSize(String variableSize) {
		this.variableSize = variableSize;
	}

	private CopyBookData.Header header;
	
	private CopyBookData.Body body;
	
	private CopyBookData.Trailer trailer;

	public CopyBookData.Trailer getTrailer() {
		return trailer;
	}

	@XmlElement(name = "Trailer")
	public void setTrailer(CopyBookData.Trailer trailer) {
		this.trailer = trailer;
	}

	public CopyBookData.Body getBody() {
		return body;
	}

	@XmlElement(name = "Body")
	public void setBody(CopyBookData.Body body) {
		this.body = body;
	}

	public CopyBookData.Header getHeader() {
		return header;
	}

	@XmlElement(name = "Header")
	public void setHeader(CopyBookData.Header header) {
		this.header = header;
	}

	public String getEbcdic() {
		return ebcdic;
	}

	@XmlElement(name = "Ebcdic")
	public void setEbcdic(String ebcdic) {
		this.ebcdic = ebcdic;
	}

	public static class Header {

		private String hName;

		public String gethName() {
			return hName;
		}

		@XmlElement(name = "HName")
		public void sethName(String hName) {
			this.hName = hName;
		}

		private CopyBookData.Header.HSegmentField hSegmentField;

		public CopyBookData.Header.HSegmentField gethSegmentField() {
			return hSegmentField;
		}

		@XmlElement(name = "HSegmentField")
		public void sethSegmentField(
				CopyBookData.Header.HSegmentField hSegmentField) {
			this.hSegmentField = hSegmentField;
		}

		public static class HSegmentField {

			private String hSegmentName;
			
			private String hRecordType;
			
			public String gethRecordType() {
				return hRecordType;
			}

			@XmlElement(name = "HRecordType")
			public void sethRecordType(String hRecordType) {
				this.hRecordType = hRecordType;
			}

			public String gethSegmentName() {
				return hSegmentName;
			}

			@XmlElement(name = "HSegmentName")
			public void sethSegmentName(String hSegmentName) {
				this.hSegmentName = hSegmentName;
			}

			private ArrayList<HField> hField;

			public ArrayList<HField> gethField() {
				return hField;
			}

			@XmlElement(name = "HField")
			public void sethField(ArrayList<HField> hField) {
				this.hField = hField;
			}

		}

	}

	public static class Body {

		private String bName;

		public String getbName() {
			return bName;
		}

		@XmlElement(name = "BName")
		public void setbName(String bName) {
			this.bName = bName;
		}

		//private CopyBookData.Body.BSegmentField bSegmentField;
		
		private ArrayList<BSegmentField> bSegmentField;

		public ArrayList<BSegmentField> getbSegmentField() {
			return bSegmentField;
		}

		@XmlElement(name = "BSegmentField")
		public void setbSegmentField(ArrayList<BSegmentField> bSegmentField) {
			this.bSegmentField = bSegmentField;
		}

		public static class BSegmentField {

			private String bSegmentName;

			private ArrayList<BField> bField;

			public ArrayList<BField> getbField() {
				return bField;
			}

			@XmlElement(name = "BField")
			public void setbField(ArrayList<BField> bField) {
				this.bField = bField;
			}

			public String getbSegmentName() {
				return bSegmentName;
			}

			@XmlElement(name = "BSegmentName")
			public void setbSegmentName(String bSegmentName) {
				this.bSegmentName = bSegmentName;
			}

		}

	}
	
	public static class Trailer {

		private String tName;

		private CopyBookData.Trailer.TSegmentField tSegmentField;

		public CopyBookData.Trailer.TSegmentField gettSegmentField() {
			return tSegmentField;
		}

		@XmlElement(name = "TSegmentField")
		public void settSegmentField(CopyBookData.Trailer.TSegmentField tSegmentField) {
			this.tSegmentField = tSegmentField;
		}

		public String gettName() {
			return tName;
		}

		@XmlElement(name = "TName")
		public void settName(String tName) {
			this.tName = tName;
		}

		public static class TSegmentField {

			private String tSegmentName;

			public String gettSegmentName() {
				return tSegmentName;
			}

			@XmlElement(name = "TSegmentName")
			public void settSegmentName(String tSegmentName) {
				this.tSegmentName = tSegmentName;
			}

			private ArrayList<TField> tField;

			public ArrayList<TField> gettField() {
				return tField;
			}

			@XmlElement(name = "TField")
			public void settField(ArrayList<TField> tField) {
				this.tField = tField;
			}

		}

	}
}
