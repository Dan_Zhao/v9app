package com.lcl.pcb.v9.idf.jobflow.config;


import javax.xml.bind.annotation.XmlRootElement;

/**
 * Class to store unmarshalled customized interface information 
 * loaded via JaxB from idf.xml
 */
@XmlRootElement(name="idf")
public class IDFData {

	private String idfType="";
	private String idfTypeCustomClassName="";
	private String lineNumReader="";
	private String skipHeadRow="";
	private String skipTrailRow="";
	private String encloseDataBy="";
	private String enableCustomDelimiter="";
	private String fileParsingCharset="";
	
	private String filePropName="";
	private String filePropType="";	
	private String filePropIdfUniqueId="";

	private String logDblogUser="";
	private String logDblogPass="";
	private String logDblogSchema="";
	private String logDblogUrl="";
	
	
	private String csvloadDbUser="";
	private String csvloadDbPass="";
	private String csvloadDbSchema="";
	private String csvloadDbUrl="";
	private String csvloadDb="";
	
	//Added below part on 5th Feb for PGP Decrypt Logic
	private String decryptProcessFile="";
	private String decryptDbUser="";
	private String decryptDbPass="";
	private String decryptDbSchema="";
	private String decryptDbUrl="";
	private String decryptTempFileLocation="";
	private String decryptFileLocation="";					//NW: Added on 15th September
	private String decryptPgpProgramDirectory="";
	private String decryptedFileName="";
	private String decryptFileNameChange="";                //SR: Added on 20th December
	
	// Added below part on 5th Feb for PGP Decrypt Logic
	private String encryptProcessFile = "";
	private String encryptDbUser = "";
	private String encryptDbPass = "";
	private String encryptDbSchema = "";
	private String encryptDbUrl = "";
	private String encryptTempFileLocation = "";
	private String encryptFileLocation = "";				//NW: Added on 15th September
	private String encryptPgpProgramDirectory = "";
	private String encryptedFileName = "";
	
	private String logFilelogFilelogLocation="";
	private String logFilelogFileLevel="";	
	private String logFileLogFileLogNamePattern="";

	private String datafileDatafileLocation="";
	private String datafileNamePattern="";
	private String datafileCsvFileLocation="";
	private String datafileCsvNamePattern="";
	private String datafileBadFileLocation="";
	private String datafileBadNamePattern="";
	private String datafileSqlloaderCtlFilePattern="";
	private String datafileSqlloaderCtlFileLocation="";
	private String datafileSqlloaderCtlFiles="";
	
	private String loadType="";
	private String loadFeaturesloadMethod="";
	private String loadFeaturesloadMethodCsvLoadParallel="";
	
	private String loadFeaturesloadMethodDisableEnableIndexStg="";
	private String loadFeaturesloadMethodDisableEnableIndexMain="";
	
	private String l1desc="";	
	private String l2desc="";
	private String l3desc="";
	private String l4desc="";
	private String l5desc="";
	
	private String copybookxmlFileLocation="";
	private String interfacename="";
	
	private String headerTrailerSegmentName="";
	private String csvFileSeparator="";
	private String bodySegmentsMandatory="";
	private String trailerRecordType="";
	private String insertjobseqtodatatables="";
	private String bodyRecTypeStartPosition="";
	private String bodyRecTypeEndPosition="";
	private String headerRecordType="";
	private String headerRecTypeStartPosition="";
	private String headerRecTypeEndPosition="";
	private String trailerRecTypeStartPosition="";
	private String trailerRecTypeEndPosition="";
	private String multipleBodySegments="";
	private String bodySegmentName="";
	private String singleBodySegPosition="";
	private String auditFlag="";
	private String fileSize="";
	private String hName="";
	private String tName="";
	private String sqlLoaderDirect="";
	private String doubleQuotes="";

//Added by Ankita for ADM error handling
	
	private boolean recordCountValidation = false;
	public boolean isRecordCountValidation() {
		return recordCountValidation;
	}
	
	public void setRecordCountValidation(boolean recordCountValidation) {
		this.recordCountValidation = recordCountValidation;
	}
	public String getEncryptFileLocation() {
		return encryptFileLocation;
	}
	public void setEncryptFileLocation(String encryptFileLocation) {
		this.encryptFileLocation = encryptFileLocation;
	}
	public String getDecryptFileLocation() {
		return decryptFileLocation;
	}
	public void setDecryptFileLocation(String decryptFileLocation) {
		this.decryptFileLocation = decryptFileLocation;
	}
	public String getDecryptFileNameChange() {
		return decryptFileNameChange;
	}
	public void setDecryptFileNameChange(String decryptFileNameChange) {
		this.decryptFileNameChange = decryptFileNameChange;
	}
	public String getEnableCustomDelimiter() {
		return enableCustomDelimiter;
	}
	public void setEnableCustomDelimiter(String enableCustomDelimiter) {
		this.enableCustomDelimiter = enableCustomDelimiter;
	}
	public String getLineNumReader() {
		return lineNumReader;
	}
	public void setLineNumReader(String lineNumReader) {
		this.lineNumReader = lineNumReader;
	}
	public String getFileParsingCharset() {
		return fileParsingCharset;
	}
	public void setFileParsingCharset(String fileParsingCharset) {
		this.fileParsingCharset = fileParsingCharset;
	}
	public String getEncloseDataBy() {
		return encloseDataBy;
	}
	public void setEncloseDataBy(String encloseDataBy) {
		this.encloseDataBy = encloseDataBy;
	}
	public String getSkipTrailRow() {
		return skipTrailRow;
	}
	public void setSkipTrailRow(String skipTrailRow) {
		this.skipTrailRow = skipTrailRow;
	}
	public String getSkipHeadRow() {
		return skipHeadRow;
	}
	public void setSkipHeadRow(String skipHeadRow) {
		this.skipHeadRow = skipHeadRow;
	}
	public String getIdfTypeCustomClassName() {
		return idfTypeCustomClassName;
	}
	public void setIdfTypeCustomClassName(String idfTypeCustomClassName) {
		this.idfTypeCustomClassName = idfTypeCustomClassName;
	}
	public String getEncryptProcessFile() {
		return encryptProcessFile;
	}
	public void setEncryptProcessFile(String encryptProcessFile) {
		this.encryptProcessFile = encryptProcessFile;
	}
	public String getEncryptDbUser() {
		return encryptDbUser;
	}
	public void setEncryptDbUser(String encryptDbUser) {
		this.encryptDbUser = encryptDbUser;
	}
	public String getEncryptDbPass() {
		return encryptDbPass;
	}
	public void setEncryptDbPass(String encryptDbPass) {
		this.encryptDbPass = encryptDbPass;
	}
	public String getEncryptDbSchema() {
		return encryptDbSchema;
	}
	public void setEncryptDbSchema(String encryptDbSchema) {
		this.encryptDbSchema = encryptDbSchema;
	}
	public String getEncryptDbUrl() {
		return encryptDbUrl;
	}
	public void setEncryptDbUrl(String encryptDbUrl) {
		this.encryptDbUrl = encryptDbUrl;
	}
	public String getEncryptTempFileLocation() {
		return encryptTempFileLocation;
	}
	public void setEncryptTempFileLocation(String encryptTempFileLocation) {
		this.encryptTempFileLocation = encryptTempFileLocation;
	}
	public String getEncryptPgpProgramDirectory() {
		return encryptPgpProgramDirectory;
	}
	public void setEncryptPgpProgramDirectory(String encryptPgpProgramDirectory) {
		this.encryptPgpProgramDirectory = encryptPgpProgramDirectory;
	}
	public String getEncryptedFileName() {
		return encryptedFileName;
	}
	public void setEncryptedFileName(String encryptedFileName) {
		this.encryptedFileName = encryptedFileName;
	}
	public String getDecryptedFileName() {
		return decryptedFileName;
	}
	public void setDecryptedFileName(String decryptedFileName) {
		this.decryptedFileName = decryptedFileName;
	}
	public String getDecryptProcessFile() {
		return decryptProcessFile;
	}
	public void setDecryptProcessFile(String decryptProcessFile) {
		this.decryptProcessFile = decryptProcessFile;
	}
	public String getDecryptPgpProgramDirectory() {
		return decryptPgpProgramDirectory;
	}
	public void setDecryptPgpProgramDirectory(String decryptPgpProgramDirectory) {
		this.decryptPgpProgramDirectory = decryptPgpProgramDirectory;
	}
	public String getDecryptDbUser() {
		return decryptDbUser;
	}
	public void setDecryptDbUser(String decryptDbUser) {
		this.decryptDbUser = decryptDbUser;
	}
	public String getDecryptDbPass() {
		return decryptDbPass;
	}
	public void setDecryptDbPass(String decryptDbPass) {
		this.decryptDbPass = decryptDbPass;
	}
	public String getDecryptDbSchema() {
		return decryptDbSchema;
	}
	public void setDecryptDbSchema(String decryptDbSchema) {
		this.decryptDbSchema = decryptDbSchema;
	}
	public String getDecryptDbUrl() {
		return decryptDbUrl;
	}
	public void setDecryptDbUrl(String decryptDbUrl) {
		this.decryptDbUrl = decryptDbUrl;
	}
	public String getDecryptTempFileLocation() {
		return decryptTempFileLocation;
	}
	public void setDecryptTempFileLocation(String decryptTempFileLocation) {
		this.decryptTempFileLocation = decryptTempFileLocation;
	}
	public String getDoubleQuotes() {
		return doubleQuotes;
	}
	public void setDoubleQuotes(String doubleQuotes) {
		this.doubleQuotes = doubleQuotes;
	}
	public String getSqlLoaderDirect() {
		return sqlLoaderDirect;
	}
	public void setSqlLoaderDirect(String sqlLoaderDirect) {
		this.sqlLoaderDirect = sqlLoaderDirect;
	}
	public String gethName() {
		return hName;
	}
	public void sethName(String hName) {
		this.hName = hName;
	}
	
	public String gettName() {
		return tName;
	}
	public void settName(String tName) {
		this.tName = tName;
	}

	private String headerFieldPosition="";
	private String trailerFieldPosition="";
	private String multiHeaderTrailerFlag="";
	private String sqlErrorsAllowed="";
	
	public String getSqlErrorsAllowed() {
		return sqlErrorsAllowed;
	}
	public void setSqlErrorsAllowed(String sqlErrorsAllowed) {
		this.sqlErrorsAllowed = sqlErrorsAllowed;
	}
	public String getMultiHeaderTrailerFlag() {
		return multiHeaderTrailerFlag;
	}
	public void setMultiHeaderTrailerFlag(String multiHeaderTrailerFlag) {
		this.multiHeaderTrailerFlag = multiHeaderTrailerFlag;
	}
	public String getHeaderFieldPosition() {
		return headerFieldPosition;
	}
	public void setHeaderFieldPosition(String headerFieldPosition) {
		this.headerFieldPosition = headerFieldPosition;
	}
	public String getTrailerFieldPosition() {
		return trailerFieldPosition;
	}
	public void setTrailerFieldPosition(String trailerFieldPosition) {
		this.trailerFieldPosition = trailerFieldPosition;
	}
	public String getFileSize() {
		return fileSize;
	}
	public void setFileSize(String fileSize) {
		this.fileSize = fileSize;
	}
	public String getAuditFlag() {
		return auditFlag;
	}
	public void setAuditFlag(String auditFlag) {
		this.auditFlag = auditFlag;
	}
	public String getSingleBodySegPosition() {
		return singleBodySegPosition;
	}
	public void setSingleBodySegPosition(String singleBodySegPosition) {
		this.singleBodySegPosition = singleBodySegPosition;
	}
	public String getSingleBodySegStartPosition() {
		return singleBodySegStartPosition;
	}
	public void setSingleBodySegStartPosition(String singleBodySegStartPosition) {
		this.singleBodySegStartPosition = singleBodySegStartPosition;
	}
	public String getSingleBodySegEndPosition() {
		return singleBodySegEndPosition;
	}
	public void setSingleBodySegEndPosition(String singleBodySegEndPosition) {
		this.singleBodySegEndPosition = singleBodySegEndPosition;
	}
	private String singleBodySegStartPosition="";
	private String singleBodySegEndPosition="";
	
	public String getBodySegmentName() {
		return bodySegmentName;
	}
	public void setBodySegmentName(String bodySegmentName) {
		this.bodySegmentName = bodySegmentName;
	}
	public String getMultipleBodySegments() {
		return multipleBodySegments;
	}
	public void setMultipleBodySegments(String multipleBodySegments) {
		this.multipleBodySegments = multipleBodySegments;
	}
	public String getTrailerRecTypeStartPosition() {
		return trailerRecTypeStartPosition;
	}
	public void setTrailerRecTypeStartPosition(String trailerRecTypeStartPosition) {
		this.trailerRecTypeStartPosition = trailerRecTypeStartPosition;
	}
	public String getTrailerRecTypeEndPosition() {
		return trailerRecTypeEndPosition;
	}
	public void setTrailerRecTypeEndPosition(String trailerRecTypeEndPosition) {
		this.trailerRecTypeEndPosition = trailerRecTypeEndPosition;
	}
	
	
	
	public String getHeaderRecTypeStartPosition() {
		return headerRecTypeStartPosition;
	}
	public void setHeaderRecTypeStartPosition(String headerRecTypeStartPosition) {
		this.headerRecTypeStartPosition = headerRecTypeStartPosition;
	}
	public String getHeaderRecTypeEndPosition() {
		return headerRecTypeEndPosition;
	}
	public void setHeaderRecTypeEndPosition(String headerRecTypeEndPosition) {
		this.headerRecTypeEndPosition = headerRecTypeEndPosition;
	}
	
	
	public String getHeaderRecordType() {
		return headerRecordType;
	}
	public void setHeaderRecordType(String headerRecordType) {
		this.headerRecordType = headerRecordType;
	}
	public String getBodyRecTypeStartPosition() {
		return bodyRecTypeStartPosition;
	}
	public void setBodyRecTypeStartPosition(String bodyRecTypeStartPosition) {
		this.bodyRecTypeStartPosition = bodyRecTypeStartPosition;
	}
	public String getBodyRecTypeEndPosition() {
		return bodyRecTypeEndPosition;
	}
	public void setBodyRecTypeEndPosition(String bodyRecTypeEndPosition) {
		this.bodyRecTypeEndPosition = bodyRecTypeEndPosition;
	}
	
	
	public String getInsertjobseqtodatatables() {
		return insertjobseqtodatatables;
	}
	public void setInsertjobseqtodatatables(String insertjobseqtodatatables) {
		this.insertjobseqtodatatables = insertjobseqtodatatables;
	}
	public String getTrailerRecordType() {
		return trailerRecordType;
	}
	public void setTrailerRecordType(String trailerRecordType) {
		this.trailerRecordType = trailerRecordType;
	}
	public String getBodySegmentsMandatory() {
		return bodySegmentsMandatory;
	}
	public void setBodySegmentsMandatory(String bodySegmentsMandatory) {
		this.bodySegmentsMandatory = bodySegmentsMandatory;
	}
	public String getCsvFileSeparator() {
		return csvFileSeparator;
	}
	public void setCsvFileSeparator(String csvFileSeparator) {
		this.csvFileSeparator = csvFileSeparator;
	}
	public String getHeaderTrailerSegmentName() {
		return headerTrailerSegmentName;
	}
	public void setHeaderTrailerSegmentName(String headerTrailerSegmentName) {
		this.headerTrailerSegmentName = headerTrailerSegmentName;
	}
	public String getCopybookxmlFileLocation() {
		return copybookxmlFileLocation;
	}
	public void setCopybookxmlFileLocation(String copybookxmlFileLocation) {
		this.copybookxmlFileLocation = copybookxmlFileLocation;
	}
	public String getInterfacename() {
		return interfacename;
	}
	public void setInterfacename(String interfacename) {
		this.interfacename = interfacename;
	}
	public String getIdfType() {
		return idfType;
	}
	public void setIdfType(String idfType) {
		this.idfType = idfType;
	}
	public String getFilePropName() {
		return filePropName;
	}
	public void setFilePropName(String filePropName) {
		this.filePropName = filePropName;
	}
	public String getFilePropType() {
		return filePropType;
	}
	public void setFilePropType(String filePropType) {
		this.filePropType = filePropType;
	}
	public String getFilePropIdfUniqueId() {
		return filePropIdfUniqueId;
	}
	public void setFilePropIdfUniqueId(String filePropIdfUniqueId) {
		this.filePropIdfUniqueId = filePropIdfUniqueId;
	}
	public String getLogDblogUser() {
		return logDblogUser;
	}
	public void setLogDblogUser(String logDblogUser) {
		this.logDblogUser = logDblogUser;
	}
	public String getLogDblogPass() {
		return logDblogPass;
	}
	public void setLogDblogPass(String logDblogPass) {
		this.logDblogPass = logDblogPass;
	}
	public String getLogDblogSchema() {
		return logDblogSchema;
	}
	public void setLogDblogSchema(String logDblogSchema) {
		this.logDblogSchema = logDblogSchema;
	}
	public String getLogDblogUrl() {
		return logDblogUrl;
	}
	public void setLogDblogUrl(String logDblogUrl) {
		this.logDblogUrl = logDblogUrl;
	}
	public String getCsvloadDbUser() {
		return csvloadDbUser;
	}
	public void setCsvloadDbUser(String csvloadDbUser) {
		this.csvloadDbUser = csvloadDbUser;
	}
	public String getCsvloadDbPass() {
		return csvloadDbPass;
	}
	public void setCsvloadDbPass(String csvloadDbPass) {
		this.csvloadDbPass = csvloadDbPass;
	}
	public String getCsvloadDbSchema() {
		return csvloadDbSchema;
	}
	public void setCsvloadDbSchema(String csvloadDbSchema) {
		this.csvloadDbSchema = csvloadDbSchema;
	}
	public String getCsvloadDbUrl() {
		return csvloadDbUrl;
	}
	public void setCsvloadDbUrl(String csvloadDbUrl) {
		this.csvloadDbUrl = csvloadDbUrl;
	}
	public String getCsvloadDb() {
		return csvloadDb;
	}
	public void setCsvloadDb(String csvloadDb) {
		this.csvloadDb = csvloadDb;
	}
	public String getLogFilelogFilelogLocation() {
		return logFilelogFilelogLocation;
	}
	public void setLogFilelogFilelogLocation(String logFilelogFilelogLocation) {
		this.logFilelogFilelogLocation = logFilelogFilelogLocation;
	}             
	public String getLogFilelogFileLevel() {
		return logFilelogFileLevel;
	}
	public void setLogFilelogFileLevel(String logFilelogFileLevel) {
		this.logFilelogFileLevel = logFilelogFileLevel;
	}	
	public String getLogFileLogFileLogNamePattern() {
		return logFileLogFileLogNamePattern;
	}
	public void setLogFileLogFileLogNamePattern(String logFileLogFileLogNamePattern) {
		this.logFileLogFileLogNamePattern = logFileLogFileLogNamePattern;
	}
	public String getDatafileDatafileLocation() {
		return datafileDatafileLocation;
	}
	public void setDatafileDatafileLocation(String datafileDatafileLocation) {
		this.datafileDatafileLocation = datafileDatafileLocation;
	}
	public String getDatafileNamePattern() {
		return datafileNamePattern;
	}
	public void setDatafileNamePattern(String datafileNamePattern) {
		this.datafileNamePattern = datafileNamePattern;
	}
	public String getDatafileCsvFileLocation() {
		return datafileCsvFileLocation;
	}
	public void setDatafileCsvFileLocation(String datafileCsvFileLocation) {
		this.datafileCsvFileLocation = datafileCsvFileLocation;
	}
	public String getDatafileCsvNamePattern() {
		return datafileCsvNamePattern;
	}
	public void setDatafileCsvNamePattern(String datafileCsvNamePattern) {
		this.datafileCsvNamePattern = datafileCsvNamePattern;
	}
	public String getDatafileBadFileLocation() {
		return datafileBadFileLocation;
	}
	public void setDatafileBadFileLocation(String datafileBadFileLocation) {
		this.datafileBadFileLocation = datafileBadFileLocation;
	}
	public String getDatafileBadNamePattern() {
		return datafileBadNamePattern;
	}
	public void setDatafileBadNamePattern(String datafileBadNamePattern) {
		this.datafileBadNamePattern = datafileBadNamePattern;
	}
	public String getDatafileSqlloaderCtlFilePattern() {
		return datafileSqlloaderCtlFilePattern;
	}
	public void setDatafileSqlloaderCtlFilePattern(
			String datafileSqlloaderCtlFilePattern) {
		this.datafileSqlloaderCtlFilePattern = datafileSqlloaderCtlFilePattern;
	}
	public String getDatafileSqlloaderCtlFileLocation() {
		return datafileSqlloaderCtlFileLocation;
	}
	public void setDatafileSqlloaderCtlFileLocation(
			String datafileSqlloaderCtlFileLocation) {
		this.datafileSqlloaderCtlFileLocation = datafileSqlloaderCtlFileLocation;
	}
	public String getDatafileSqlloaderCtlFiles() {
		return datafileSqlloaderCtlFiles;
	}
	public void setDatafileSqlloaderCtlFiles(String datafileSqlloaderCtlFiles) {
		this.datafileSqlloaderCtlFiles = datafileSqlloaderCtlFiles;
	}
	public String getLoadType() {
		return loadType;
	}
	public void setLoadType(String loadType) {
		this.loadType = loadType;
	}
	public String getLoadFeaturesloadMethod() {
		return loadFeaturesloadMethod;
	}
	public void setLoadFeaturesloadMethod(String loadFeaturesloadMethod) {
		this.loadFeaturesloadMethod = loadFeaturesloadMethod;
	}
	public String getLoadFeaturesloadMethodCsvLoadParallel() {
		return loadFeaturesloadMethodCsvLoadParallel;
	}
	public void setLoadFeaturesloadMethodCsvLoadParallel(
			String loadFeaturesloadMethodCsvLoadParallel) {
		this.loadFeaturesloadMethodCsvLoadParallel = loadFeaturesloadMethodCsvLoadParallel;
	}
	public String getLoadFeaturesloadMethodDisableEnableIndexStg() {
		return loadFeaturesloadMethodDisableEnableIndexStg;
	}
	public void setLoadFeaturesloadMethodDisableEnableIndexStg(
			String loadFeaturesloadMethodDisableEnableIndexStg) {
		this.loadFeaturesloadMethodDisableEnableIndexStg = loadFeaturesloadMethodDisableEnableIndexStg;
	}
	public String getLoadFeaturesloadMethodDisableEnableIndexMain() {
		return loadFeaturesloadMethodDisableEnableIndexMain;
	}
	public void setLoadFeaturesloadMethodDisableEnableIndexMain(
			String loadFeaturesloadMethodDisableEnableIndexMain) {
		this.loadFeaturesloadMethodDisableEnableIndexMain = loadFeaturesloadMethodDisableEnableIndexMain;
	}	
	public String getL1desc() {
		return l1desc;
	}
	public void setL1desc(String l1desc) {
		this.l1desc = l1desc;
	}
	public String getL2desc() {
		return l2desc;
	}
	public void setL2desc(String l2desc) {
		this.l2desc = l2desc;
	}
	public String getL3desc() {
		return l3desc;
	}
	public void setL3desc(String l3desc) {
		this.l3desc = l3desc;
	}
	public String getL4desc() {
		return l4desc;
	}
	public void setL4desc(String l4desc) {
		this.l4desc = l4desc;
	}
	public String getL5desc() {
		return l5desc;
	}
	public void setL5desc(String l5desc) {
		this.l5desc = l5desc;
	}	
}
