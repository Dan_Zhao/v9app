package com.lcl.pcb.v9.idf.jobflow.config;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.lcl.pcb.v9.utilities.XmlUtil;

/**
 * Singleton class that populates ExtractorData with the unmarshalled 
 * information from the Extractor.xml file/InterfaceLoader.xml file. 
 * This class is referenced throughout the application for 
 * interface specific logic.
 */

public class ExtractorConfig {
	
	// Singleton Pattern
	private static final Logger logger = LogManager.getLogger(ExtractorConfig.class
			.getName());

	private ExtractorData ExtractorData = null;

	public ExtractorData getExtractorData() {
		return ExtractorData;
	}

	public void setExtractorData(ExtractorData ExtractorData) {
		this.ExtractorData = ExtractorData;
	}

	public static void setInstance(IDFConfig instance) { 

		// logger.info("Interface Main method started and called HO70Config singleton to read H070 file");

	}

	private static ExtractorConfig instance = null;

	protected ExtractorConfig(){
		// Exists to defeat instantiation.
		// logger.debug("Singleton Instantiated");
	}

	public static ExtractorConfig getInstance() {
		if (instance == null) {
			instance = new ExtractorConfig();
		}
		return instance;
	}


	/**
	 * This method calls the xml utility to do the JaxB
	 * conversion to populate this Singleton
	 * It uses information from the singleton 
	 * for the layout of the csv file for extraction
	 *
	 * @param  layoutFile	the name of the xml layoutFile  
	 * @return void
	 */	
	public void readExtractorData(String layoutFile) {
		this.setExtractorData(XmlUtil.readExtractorData(layoutFile));
	}


}
