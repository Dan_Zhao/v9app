package com.lcl.pcb.v9.idf.jobflow.factory;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.lcl.pcb.v9.idf.jobflow.config.IDFConfig;
/**
 * Factory class that returns a V9Loader V9Extractor or V9Reporter
 * object to V9Client.  The execute method of the object
 * that is returned is called by V9Client without having to
 * know if it is a Loader Extractor or Reporter
 */
public class V9Factory {
	

	private static final Logger logger = LogManager.getLogger(V9Factory.class.getName());	
	
	private static final String LOAD = "Load";
	private static final String EXTRACT = "Extract";
	private static final String REPORTS = "Reports";		
	private static final String CUSTOM = "CUSTOM";		
	
	public V9Factory() {
	}
	
	public static V9Interface getIdfType() {
		
		logger.debug("Check IDF type");		

		String idfType=IDFConfig.getInstance().getIdfData().getIdfType();

		if (idfType.equals(LOAD)) {
			logger.debug("Load");	
			logger.debug("Factory returns V9Loader");				
			return new V9Loader();
		} else if (idfType.equals(EXTRACT)) {
			logger.debug("Extract");
			logger.debug("Factory returns V9Extractor");				
			return new V9Extractor();
		} else if (idfType.equals(REPORTS)) {
			logger.debug("Reports");	
			logger.debug("Factory returns V9Reporter");				
			return new V9Reporter();
		} else if (idfType.equals(CUSTOM)) {
			logger.debug("CUSTOM");
			V9Interface v9i=null;
			try {
				v9i = (V9Interface) Class.forName(IDFConfig.getInstance().getIdfData().getIdfTypeCustomClassName()).newInstance();
				//v9i.execute();
				//Initialize the properties
			} catch (IllegalAccessException e) {
				logger.error("V9Factory: Error has occurred for CUSTOM IDFType IllegalAccessException: ",e);
			} catch (InstantiationException e) {
				logger.error("V9Factory: Error has occurred for CUSTOM IDFType InstantiationException: ",e);
			} catch (ClassNotFoundException e) {
				logger.error("V9Factory: Error has occurred for CUSTOM IDFType ClassNotFoundException: ",e);
			} catch (Exception e) {
				logger.error("V9Factory: Error has occurred for CUSTOM IDFType: ",e);
			}
			logger.debug("Factory returns "+IDFConfig.getInstance().getIdfData().getIdfTypeCustomClassName());				
			return v9i;
		} else {	
			return null;
		}
	}	

}
