package com.lcl.pcb.v9.idf.jobflow.factory;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

import com.lcl.pcb.v9.dao.V9DAOManager;
import com.lcl.pcb.v9.generic.extractor.V9ExtractQueryDaoManager;
import com.lcl.pcb.v9.idf.jobflow.config.ExtractorConfig;
import com.lcl.pcb.v9.idf.jobflow.config.IDFConfig;

/**
 * Implements V9Interface This class is used to execute the query of data
 * and custom modifications to this data to create a csv file.
 * This extracted data is then available in a csv file for further processing.
 */
public class V9Extractor implements V9Interface {
	
	V9DAOManager db = new V9DAOManager();
	Class noparams[] = {};	
	
	/**
	 * execute method is the main processing method of the 
	 * V9Interface.  In this implementation it does all processing
	 * for the Extractor process flow and process logs
	 * This includes calling helper classes to execute the query
	 * modify the data custom to the interface, write to logs and
	 * convert data to csv format
	 * 
	 * @param  void
	 * @return void
	 */
	public void execute() throws Exception {

		V9DAOManager db = new V9DAOManager();
		logger.debug("Extractor");

		db.setLevel(1, 0, 0);
		logger.debug("(LEVEL 1) - Start");
		logger.debug("Extractor Job Flow has been started");
		logger.info("Call execute() method");
		
		// Setup Extractor Layout data for csv and sql query
		IDFConfig idfConfig = IDFConfig.getInstance(); // singleton				
		String layoutFile=idfConfig.getIdfData().getCopybookxmlFileLocation(); // for extractor this is the extract layout file  (not a copybook)
		ExtractorConfig extractorConfig = ExtractorConfig.getInstance();
		extractorConfig.readExtractorData(layoutFile);				

		db.setLevel(2, 0, 0);
		logger.debug("(LEVEL 2) - Execute SQL query");		
		logger.debug("Get SQL query from Extractor.xml");	
		logger.info("SQL query is the main extract before modifications");		
		logger.info("Call V9ExtractQueryDaoManager to open database connection");
		
		V9ExtractQueryDaoManager v9ExtractQuery = new V9ExtractQueryDaoManager();
		v9ExtractQuery.open();
		
		logger.debug("Execute the SQL query");		
		logger.info("Inject SQL into V9ExtractQueryDaoManager to run query");
		
		ResultSet rs=null;
		try {
			rs = v9ExtractQuery.extractSQL();	
		} catch (SQLException e) {	
			String errorMsg = e.getMessage();
			logger.error("(Level 3) Result set not returned "+errorMsg);				
			db.setLevelWarning(3, "(Level 3) "+errorMsg, 0);	
			v9ExtractQuery.close();	
		}
		v9ExtractQuery.close();	
		logger.info("Close V9ExtractQueryDaoManager after main Query executed");
		
		// convert result set to Array if anything was returned
		// it runs the GarbageCollector
		System.gc();
		logger.info("Convert Result Set to 2 dimensional array for modifications if needed, and csv output");
		String[][] rsArray=convertRStoArray(rs);
		System.gc();
		
		logger.debug("(LEVEL 3) - Result set returned successfuly and ready for modifications");	
		logger.debug("Check if modifications to main SQL are required (custom extractor class code)");		
		
		if (extractorConfig.getExtractorData().getQueryChanges().equals("Y")) {
			logger.debug("Interface requires modifications to result set returned by Query");				
			String className="com.lcl.pcb.v9.custom.extractor."+extractorConfig.getExtractorData().getInterfaceName()+"QueryChanges";
			Class<?> clazz = Class.forName(className);
			Constructor<?> cls = clazz.getConstructor(String[][].class);
			Object obj = cls.newInstance(new Object[] { rsArray });
			Method method = clazz.getDeclaredMethod("execute", noparams);
			rsArray=(String[][]) method.invoke(obj);	
			logger.debug("Modifications made successfuly");				
		} else {
			logger.debug("Interface does not require modifications to result set returned by Query ");			
		}		

		int rowCount=rsArray.length-1; //first row is header column titles, do not count in number of rows
		db.setLevel(3, rowCount, 0);
	
		
		db.setLevel(4, rowCount, 0);
		logger.debug("(LEVEL 4) - Extract to CSV");
		logger.debug("OUTPUT: CSV file");		
		logger.debug("Create filename for CSV file");	
		
		// Setup Filename as PCCIF_YYYYMMDDHHMMSS.csv for csv
		Date date = Calendar.getInstance().getTime();		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");		
		// int seqName = IDFConfig.getInstance().getSequenceNum();
		// String seqNameVal = String.valueOf(seqName);
		//idfConfig.getIdfData().setDatafileDatafileLocation(idfConfig.getIdfData().getDatafileCsvFileLocation()+"/PCCIF_"+sdf.format(date)+IDFConfig.getInstance().getIdfData().getDatafileCsvNamePattern());
		//Changed above to below to remove the hardcoding of the output file name pattern PCCIF_ and rather read from IDF file - 14th March 2016
		idfConfig.getIdfData().setDatafileDatafileLocation(idfConfig.getIdfData().getDatafileCsvFileLocation()+"/"+idfConfig.getIdfData().getDatafileNamePattern()+sdf.format(date)+IDFConfig.getInstance().getIdfData().getDatafileCsvNamePattern());
		logger.debug("Filename of CSV file is: "+idfConfig.getIdfData().getDatafileDatafileLocation());		
		logger.info("Call create CSV function");	
		
		System.gc();
		int recordsInserted=createCSVFromRSArray(rsArray);	
		logger.debug("CSV file created successfuly with "+recordsInserted+" rows ");
		System.gc();
		
		db.setLevel(5, rowCount, recordsInserted);
		logger.debug("(LEVEL 5) - Finalize Extract"); 
		logger.debug("Completed final stage of Extractor Job Flow");
		
		db.setLevelEnd(5);
		logger.debug("(LEVEL 5) - End");		
	}
	
	/**
	 * convertRStoArray method takes the result set that was returned
	 * from the database query and converts it to a two dimensional
	 * array of String.  This allows for custom data modifications
	 * that do not affect the values in the database (these modifications
	 * are only for the csv output).  If the interface does not have
	 * any custom modifications, this two dimensional array remains
	 * as just a representation of the same data that is in the result set
	 * 
	 * @param  rs Result Set from query
	 * @return String[][] two dimensional array representation of result set
	 */	
	public String[][] convertRStoArray(ResultSet rs) throws Exception {
	    ResultSetMetaData rsMetaData = rs.getMetaData();
	    int columnCount = rsMetaData.getColumnCount();
	    ArrayList<String[]> result = new ArrayList<String[]>();
	    String[] header = new String[columnCount];
	    for (int i=1; i <= columnCount; ++i){
	        String label = rsMetaData.getColumnLabel(i).toString();
	        header[i-1] = label;
	    }
	    boolean empty=true;
	    while (rs.next()){
	    	empty=false;
	        String[] str = new String[columnCount];
	        for (int i=1; i <= columnCount; ++i){
	        	if (rs.getObject(i)!=null) {
		            String value = rs.getObject(i).toString();
		            str[i-1] = value;      		
	        	} else {
	        		str[i-1]="";	
	        	}
	        }
	        result.add(str);
	    }
	    if (empty==true) {
			logger.error("(Level 3) No Values Returned in Result Set");		
			db.setLevelWarning(3, "(Level 3) No Values Returned in Result Set", 0);
	    }
	    int resultLength = result.size();
	    String[][] rsArray = new String[resultLength+1][columnCount];
	    rsArray[0] = header;
	    for(int i=1;i<=resultLength;++i){
	        String[] row = result.get(i-1);
	        rsArray[i] = row;
	    }
	    return rsArray;
	}
	
	/**
	 * createCSVFromRSArray method takes input parameter of rsArray 
	 * (a two dimensional array representation of data with/without
	 * modifications) and writes to a csv file to complete the
	 * Extraction process flow.  The int that is returned is the
	 * number of rows that have successfully been written out to the csv file
	 * 
	 * @param  rsArray String[][]
	 * @return int
	 */	
	public int createCSVFromRSArray(String[][] rsArray) throws Exception {
		String fileName  = IDFConfig.getInstance().getIdfData().getDatafileDatafileLocation();			
		String delimeterPattern = IDFConfig.getInstance().getIdfData().getCsvFileSeparator();
		String jobseqflag = IDFConfig.getInstance().getIdfData().getInsertjobseqtodatatables();
		int seqNumVal = IDFConfig.getInstance().getSequenceNum();
		String seqNum = String.valueOf(seqNumVal);
		String doubleQuotesflag = IDFConfig.getInstance().getIdfData().getDoubleQuotes();
				
		//loop through each row of rsArray and write to CSV
		int rowCount=rsArray.length;
		int colCount=rsArray[0].length;
		int numRowsCompleted=0;
		FileWriter fwriter;
		BufferedWriter bwriter;
		for (int i = 0; i < rowCount; i++) {
			try {
				fwriter = new FileWriter(fileName, true);
				bwriter = new BufferedWriter(fwriter);
				StringBuilder sb = new StringBuilder();
				for (int j = 0; j < colCount; j++) {
					if(doubleQuotesflag.equalsIgnoreCase("Y")){
						String value = rsArray[i][j];
						if (j == colCount) {
							if (jobseqflag.equalsIgnoreCase("Y")) {
								sb.append("\"");
								sb.append(value.toString().trim());
								sb.append("\"");
								sb.append(delimeterPattern);
								sb.append(seqNum);
							} else {
								sb.append("\"");
								sb.append(value.trim());
								sb.append("\"");
								//sb.append(delimeterPattern);
								//sb.append("");
							}
						} else {
							sb.append("\"");
							sb.append(value.trim());
							sb.append("\"");
							sb.append(delimeterPattern);
						}
					}
					else{
						String value = rsArray[i][j];
						if (j == colCount) {
							if (jobseqflag.equalsIgnoreCase("Y")) {
								sb.append(value.toString().trim());
								sb.append(delimeterPattern);
								sb.append(seqNum);
							} else {
								sb.append(value.trim());
								//sb.append(delimeterPattern);
								//sb.append("");
							}
						} else {
							sb.append(value.trim());
							sb.append(delimeterPattern);
						}
					}
				}
				bwriter.write(sb.toString());
				bwriter.newLine();
				bwriter.flush();
				bwriter.close();
				if (i > 0) numRowsCompleted++; //first row is header column titles, should not be counted
			} catch (IOException e) {
				logger.error("CSV File Creation Failed.");		
				db.setLevelWarning(4, "CSV File Creation Failed at row "+numRowsCompleted, 0);
				e.printStackTrace();
				return numRowsCompleted;
			}
		}
		return numRowsCompleted;
	}

	@Override
	public void addProperties(Map<String, String> properties) throws Exception {
		// TODO Auto-generated method stub
		
	}	

}
