package com.lcl.pcb.v9.idf.jobflow.config;

/**
 * 
 * @author
 */

import com.lcl.pcb.v9.utilities.XmlUtil;
/**
 * Singleton class that populates IDFData with the unmarshalled 
 * information from the idf.xml file. 
 * This class is referenced throughout the application for 
 * interface specific logic.
 */
public class IDFConfig {
	

	private IDFData idfData = null;
	private int sequenceNum = 0;
	
	
	public int getSequenceNum() {
		return sequenceNum;
	}

	public void setSequenceNum(int sequenceNum) {
		this.sequenceNum = sequenceNum;
	}

	public IDFData getIdfData() {
		return idfData;
	}

	public void setIdfData(IDFData idfData) {
		this.idfData = idfData;
	}

	public static void setInstance(IDFConfig instance) {
	}

	private static IDFConfig instance = null;

	protected IDFConfig() {
		// Exists to defeat instantiation.
	}

	public static IDFConfig getInstance() {
		if (instance == null) {
			instance = new IDFConfig();
		}
		return instance;
	}


	/**
	 * This method calls the xml utility to do the JaxB
	 * conversion to populate this Singleton
	 * It uses information from the singleton and log database to
	 * create the name of the log file
	 *
	 * @param  interfaceName the name of the interface  
	 * @param  interfaceBaseDir the server base directory   
	 * @return void
	 */	
	public void readConfigIdf(String interfaceName, String interfaceBaseDir) {
		String filename = interfaceBaseDir + "/properties/" + interfaceName + "idf.xml";	
		this.setIdfData(XmlUtil.readIdfData(filename));
	}
	
}
