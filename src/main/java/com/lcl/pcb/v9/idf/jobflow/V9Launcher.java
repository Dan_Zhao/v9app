package com.lcl.pcb.v9.idf.jobflow;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.sql.Timestamp;
import java.util.LinkedList;
import java.util.List;

import com.lcl.pcb.v9.utilities.CommandUtils;
import com.lcl.pcb.v9.utilities.FileFinder;

/**
 * This class takes the responsibilities of the XXXPre.scr, esp. for the File
 * loader processes. All the tasks of finding the file (either unique or
 * multiple) is done by this class now onwards. Even if there's no files
 * involved, it is gracefully handled by the class. <br/>
 * In order to call the class and utilize it's functionalities you may need to
 * export some variables on the XXXPre.scr file, which will be picked up by the
 * classes and be done with the jobs accordingly.
 * <p>
 * <b>List of Variables and their purposes -</b>
 * </p>
 * <ul>
 * <li><b>InterfaceName</b> - being set by v9.run - used to determine which
 * interface it is. <i><code>(no action from developer)</code></i></li>
 * <li><b>v9BaseDir</b> - being set by v9.run & v9.env - used to determine the
 * base directory of V9. Usually (/x/prod/pcb/V9App/). <i><code>(no action from
 * developer)</code></i></li>
 * <li><b>MultiFileLoadFlag</b> - being set by v9.run/command line - this is
 * based on the second parameter while triggering "v9.run" as "v9.run XXX01 Y",
 * it says that in the source folder, there might exist multiple source files,
 * and trigger the interface for each of them. <code>Y/Yes/true/on</code>
 * irrespective of casing is considered as flag on, anything else, or not having
 * a value is considered as flag down. <i>
 * <code>(no action from developer)</code></i></li>
 * <li><b>ProcessName</b> - to be set in XXXPre.scr - instead of
 * "loaderProcess", use "ProcessName" as the variable name.</li>
 * <li><b>InputFileDirectory</b> - to be set in XXXPre.scr - provide path of the
 * directory where the input file needs to be looked in.</li>
 * <li><b>InputFilenamePattern</b> - to be set in XXXPre.scr - the usual Unix
 * based file name pattern for the data file(s) to be picked up.</li>
 * <li><b>KeepSourceFile</b> - to be set in XXXPre.scr - used to command the
 * functionality to keep the source file(s) in input directory after processing.
 * <code>Y/Yes/true/on</code> irrespective of casing is considered as flag on,
 * anything else, or not having a value is considered as flag down.</li>
 * <li><b>KeepDecryptedFile</b> - to be set in XXXPre.scr - used to command the
 * functionality to keep the decrypted copy/copies of the source file(s) in
 * input directory after processing. In case a decrypted file being used for the
 * processing, i.e. file name is not suffixed with ".pgp", this flag has no
 * effect. <code>Y/Yes/true/on</code> irrespective of casing is considered as flag
 * on, anything else, or not having a value is considered as flag down.</li>
 * <li><b>CallToExecutorScript</b> - to be set in XXXPre.scr - in case the
 * developer wants to call to a customized script other than V9FileLoader or
 * V9FileExtractor, or even call V9FileLoader when no file is involved or
 * V9FileExtractor even when there are files involved, export the full path to
 * this variable. Absolute path is always preferred, however, if Relative path
 * being used, it should be relative to the XXXPre.scr script, and make sure it
 * is working.</li>
 * </ul>
 * 
 * @author Arindam Biswas
 * @since V9 // April 26, 2018
 * @version 1.0
 */
public class V9Launcher {
	
	/**
	 * disabled constructor
	 */
	private V9Launcher() {
		throw new UnsupportedOperationException("instantiation is disabled");
	}

	/**
	 * Execution Begins
	 * @param args
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public static void main(String[] args) {
		int returnCode;
		try {
			// collect values which were set using export keyword in script
			// e.g. export FILE_PATTERN="XYZ_*.uatv"
			// if available here as System.getenv("FILE_PATTERN")
			final String interfaceName = getEnvElseThrow("InterfaceName");
			final String loaderProcess = getEnvOrDefault("ProcessName", interfaceName);
			final String v9BaseDir = getEnvElseThrow("v9BaseDir");
			final Path inputFileDirectory = toPath(System.getenv("InputFileDirectory"));
			final String inputFilenamePattern = System.getenv("InputFilenamePattern");
			final boolean keepSourceFile = isYes(System.getenv("KeepSourceFile"));
			final boolean keepDecryptedFile = isYes(System.getenv("KeepDecryptedFile"));
			final boolean processMultipleFiles = isYes(System.getenv("MultiFileLoadFlag"));
			// collecting input parameters complete
			
			System.out.printf("[INFO] %s : %s started%n", new Timestamp(System.currentTimeMillis()), loaderProcess); // NOSONAR logger isn't initiated yet.
	
			returnCode = process(loaderProcess, v9BaseDir, inputFileDirectory, inputFilenamePattern,
					keepSourceFile, keepDecryptedFile, processMultipleFiles);
		} catch(Exception e) {
			System.err.println("Error Launching V9 Interfaces."); // NOSONAR logger isn't initialized
			e.printStackTrace(System.err);// NOSONAR logger isn't initialized
			returnCode = (e instanceof InterruptedException) ? 3 : ((e instanceof IOException) ? 2 : 1); // NOSONAR avoiding repetition of same code blocks across catch blocks
		}
		
		// if it is 0 already, let JVM handle it.
		// if not, then terminate with exit code.
		if( returnCode != 0 ) {
			System.exit(returnCode);
		}
	}

	private static String enclose(final String who, final String prefixBy, final String suffixBy, final int repeatBy) {
		return String.format("%s %s %s", repeat(prefixBy, repeatBy), who, repeat(suffixBy, repeatBy));
	}

	private static String repeat(String who, int repeatBy) {
		final StringBuilder text = new StringBuilder(who.length() * repeatBy);
		for( int counter = 1; counter <= repeatBy; counter++ ) {
			text.append(who);
		}
		return text.toString();
	}

	/**
	 * @param v9BaseDir
	 * @param inputFileDirectory
	 * @param inputFilenamePattern
	 * @param keepSourceFile
	 * @param keepDecryptedFile
	 * @param processMultipleFiles
	 * @return
	 * @throws IOException
	 * @throws InterruptedException
	 * @throws FileNotFoundException
	 */
	private static int process(final String loaderProcess, final String v9BaseDir, final Path inputFileDirectory, final String inputFilenamePattern,
			final boolean keepSourceFile, final boolean keepDecryptedFile, final boolean processMultipleFiles)
			throws IOException, InterruptedException {
		int returnCode = 0;
		if( inputFileDirectory == null || isEmpty(inputFilenamePattern) ) {
			if( processMultipleFiles ) {
				throw new IllegalArgumentException(String.format(
						"Input File Directory & File Name Pattern were expected. Received: Directory: %s, Pattern: %s",
						inputFileDirectory, inputFilenamePattern));
			} else {
				System.out.println("InputFileDirectory or InputFilenamePattern not provided. Using \"dummy\" as filename to indicate not a file."); // NOSONAR logger isn't initiated yet.
				returnCode = processNoFile(loaderProcess, v9BaseDir);
			}
		} else {
			if( Files.notExists(inputFileDirectory) ) {
				throw new FileNotFoundException("Input Directory does not exist or inaccessible at: " + inputFileDirectory);
			}

			final List<Path> filesToBeDeleted = new LinkedList<>();
			final List<Path> filesToBeRenamedWithError = new LinkedList<>();
			
			try {
				// file to be searched for
				if( processMultipleFiles ) {
					returnCode = processMultifiles(loaderProcess, v9BaseDir, inputFileDirectory, inputFilenamePattern,
							filesToBeDeleted, filesToBeRenamedWithError);
				} else {
					returnCode = processUnifile(loaderProcess, v9BaseDir, inputFileDirectory, inputFilenamePattern, filesToBeDeleted,
							filesToBeRenamedWithError);
				}
			} finally {
				deleteFiles(filesToBeDeleted, keepSourceFile, keepDecryptedFile);
				markFilesWithError(filesToBeRenamedWithError, keepDecryptedFile);
			}
		}
		return returnCode;
	}

	/**
	 * @param v9BaseDir
	 * @param inputFileDirectory
	 * @param inputFilenamePattern
	 * @param filesToBeDeleted
	 * @param filesToBeRenamedWithError
	 * @return
	 * @throws IOException
	 * @throws InterruptedException
	 */
	private static int processUnifile(final String loaderProcess, final String v9BaseDir, final Path inputFileDirectory,
			final String inputFilenamePattern, final List<Path> filesToBeDeleted,
			final List<Path> filesToBeRenamedWithError) throws IOException, InterruptedException {
		int returnCode;
		final Path uniqueFile = FileFinder.findUnique(inputFileDirectory, inputFilenamePattern);
		returnCode = processFile(loaderProcess, v9BaseDir, uniqueFile);
		if( returnCode != 0 ) {
			System.err.println("Process failed for unique file: " + uniqueFile.toAbsolutePath()); // NOSONAR logger isn't initiated yet.
			filesToBeRenamedWithError.add(uniqueFile);
		} else {
			filesToBeDeleted.add(uniqueFile);
		}
		return returnCode;
	}

	/**
	 * @param v9BaseDir
	 * @param inputFileDirectory
	 * @param inputFilenamePattern
	 * @param filesToBeDeleted
	 * @param filesToBeRenamedWithError
	 * @return
	 * @throws IOException
	 * @throws FileNotFoundException
	 * @throws InterruptedException
	 */
	private static int processMultifiles(final String loaderProcess, final String v9BaseDir,
			final Path inputFileDirectory, final String inputFilenamePattern, final List<Path> filesToBeDeleted,
			final List<Path> filesToBeRenamedWithError) throws IOException, InterruptedException {
		int returnCode;
		final List<Path> files = FileFinder.findAll(inputFileDirectory, inputFilenamePattern);
		if( files == null || files.isEmpty() ) {
			throw new FileNotFoundException(
					String.format("No files found in dorectory \"%s\" with pattern \"%s\"",
							inputFileDirectory, inputFilenamePattern));
		}
		int latestErrorReturnCode = 0;
		for( Path file : files ) {
			returnCode = processFile(loaderProcess, v9BaseDir, file);
			if( returnCode != 0 ) {
				latestErrorReturnCode = returnCode;
				System.err.println("Process failed for file: " + file.toAbsolutePath()); // NOSONAR logger isn't initiated yet.
				filesToBeRenamedWithError.add(file);
			} else {
				filesToBeDeleted.add(file);
			}
		}
		
		returnCode = latestErrorReturnCode;
		return returnCode;
	}

	/**
	 * @param filesToBeRenamedWithError
	 * @param keepDecryptedFile
	 */
	private static void markFilesWithError(final List<Path> filesToBeRenamedWithError, boolean keepDecryptedFile) {
		deleteFiles(filesToBeRenamedWithError, true, keepDecryptedFile);
		
		String filename;
		Path errorSibling;
		for( Path file : filesToBeRenamedWithError ) {
			filename = "with_error_" + file.getFileName().toString();
			errorSibling = file.resolveSibling(filename);
			try {
				Files.move(file, errorSibling, StandardCopyOption.REPLACE_EXISTING);
			} catch (Exception e) { // NOSONAR Exception being logged on console
				System.err.println("Error renaming file: " + file);  // NOSONAR logger isn't initiated yet.
				e.printStackTrace(System.err); // NOSONAR logger isn't initiated yet.
			}
		}
	}

	/**
	 * @param filesToBeDeleted
	 * @param keepSourceFile
	 * @param keepDecryptedFile
	 */
	private static void deleteFiles(final List<Path> filesToBeDeleted,
			final boolean keepSourceFile, final boolean keepDecryptedFile) {
		
		if( !keepDecryptedFile ) {
			String filename;
			Path decryptedSibling;
			for( Path file : filesToBeDeleted ) {
				filename = file.getFileName().toString();
				if( filename.endsWith(".pgp") ) {
					filename = filename.replaceFirst("\\.pgp$", "");
					decryptedSibling = file.resolveSibling(filename);
					tryDelete(decryptedSibling);
				}
			}
		}
		
		if( !keepSourceFile ) {
			for( Path file : filesToBeDeleted ) {
				tryDelete(file);
			}
		}
		
	}
	
	/**
	 * @param file attempts to the file. in case of Exception, logs it on console, and proceeds.
	 */
	private static void tryDelete(final Path file) {
		try {
			Files.deleteIfExists(file);
		} catch(Exception e) { // NOSONAR Exception being logged on console
			System.err.println("Error deleting file: " + file); // NOSONAR logger isn't initiated yet.
			e.printStackTrace(System.err); // NOSONAR logger isn't initiated yet.
		}
	}

	/**
	 * @param filepath
	 * @return Path if a filepath is provided, else null.
	 */
	private static Path toPath(final String filepath) {
		return isEmpty(filepath) ? null : Paths.get(filepath);
	}

	/**
	 * @param v9BaseDir
	 * @return
	 * @throws IOException
	 * @throws InterruptedException
	 */
	private static int processNoFile(final String loaderProcess, final String v9BaseDir) throws IOException, InterruptedException {
		return runScript(loaderProcess, v9BaseDir, "%s dummy 0", "V9FileExtrator.scr");
	}

	/**
	 * @param v9BaseDir
	 * @param file
	 * @return
	 * @throws IOException
	 * @throws InterruptedException
	 */
	private static int processFile(final String loaderProcess, final String v9BaseDir, final Path file) throws IOException, InterruptedException {
		return runScript(loaderProcess, v9BaseDir, "%s " + String.format("%s %s", file.toAbsolutePath(), Files.size(file)), "V9FileLoader.scr");
	}
	
	/**
	 * @param v9BaseDir
	 * @param commandBase
	 * @param scriptName
	 * @return
	 * @throws IOException
	 * @throws InterruptedException
	 */
	private static int runScript(final String loaderProcess, final String v9BaseDir, final String commandBase, final String scriptName) throws IOException, InterruptedException {
		String finalScript = null;
		
		final Path customScript = toPath(System.getenv("CallToExecutorScript"));
		if( customScript != null ) {
			if( Files.notExists(customScript) ) {
				System.err.println("Custom Script does not exist or inaccessible at: " + customScript.toString()); // NOSONAR logger isn't initiated yet.
			} else if( !Files.isExecutable(customScript) ) {
				System.err.println("Provided Script does not have required permissions to be executed : " + customScript.toString()); // NOSONAR logger isn't initiated yet.
			} else {
				finalScript = customScript.toAbsolutePath().toString();
			}
		}
		
		if( finalScript == null ) {
			finalScript = endWithIfNot(v9BaseDir, "/") + scriptName;
		}
		
		System.out.printf("Processing: %s%n", enclose(loaderProcess, ">", "<", 50)); // NOSONAR logger isn't initiated yet.
		
		final String command = String.format(commandBase, finalScript);
		System.out.println("Calling V9 Runner Script: " + command); // NOSONAR logger isn't initialized yet.
		
		return CommandUtils.run(command);
	}
	
	/**
	 * @param string
	 * @param endWith
	 * @return
	 */
	private static String endWithIfNot(final String string, final String endWith) {
		return string.endsWith(endWith) ? string : string + endWith;
	}

	/**
	 * @param name
	 * @param defaultValue
	 * @return
	 */
	private static String getEnvOrDefault(final String name, final String defaultValue) {
		final String value = System.getenv(name);
		return getOrDefault(value, defaultValue);
	}
	/**
	 * @param value
	 * @param defaultValue
	 * @return
	 */
	private static String getOrDefault(final String value, final String defaultValue) {
		return isEmpty(value) ? defaultValue : value;
	}
	
	/**
	 * @param name
	 * @return
	 */
	private static String getEnvElseThrow(final String name) {
		return requireNotEmpty(System.getenv(name), name + " not provided");
	}
	/**
	 * @param value
	 * @param message
	 * @return
	 */
	private static String requireNotEmpty(final String value, final String message) {
		if( isEmpty(value) ) {
			throw new NullPointerException(message);
		}
		return value;
	}

	/**
	 * @param flag
	 * @return true if flag is Y/Yes/true/on irrespective of casing
	 */
	private static boolean isYes(final String flag) {
		return inAnyCase(flag, "Y", "Yes", "true", "on");
	}
	private static boolean inAnyCase(final String value, final String...list) {
		for( String item : list ) {
			if( equalsAnyCase(value, item) ) {
				return true;
			}
		}
		return false;
	}

	/**
	 * @param a
	 * @param b
	 * @return
	 */
	private static boolean equalsAnyCase(final String a, final String b) {
		return isEmpty(a) || isEmpty(b) ? isEmpty(a) && isEmpty(b) : a.equalsIgnoreCase(b);
	}
	
	/**
	 * @param string
	 * @return
	 */
	private static boolean isEmpty(final String string) {
		return string == null || string.isEmpty();
	}
}
