/*
 * Property.java
 *
 * Created on January 21, 2007, 11:29 AM
 */

package com.lcl.pcb.v9.idf.jobflow.config;

/**
 * Property Class stores Key and Value for Loader Properties.
 * @author  fasghed
 */

public class Property {

  
	String key = "";
	  String value = null;
	  /** Creates a new instance of Property */
	  public Property(String k, String v) {
	    key = k;
	    value = v;
	  }
	  
	  public Property(String k) {
	    key = k;
	    value = null;
	  }
	  
	  public String getKey() {
	    return key;
	  }
	  
	  public String getValue() {
	    return value;
	  }




}
