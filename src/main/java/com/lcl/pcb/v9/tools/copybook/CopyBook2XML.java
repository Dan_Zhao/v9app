package com.lcl.pcb.v9.tools.copybook;

import java.io.BufferedReader;
import java.io.FileReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

public class CopyBook2XML  {

  BufferedReader inputFile;
  StreamResult outputFile;
  
  Document xmldoc;
  Element root;
  Element header;
  Element hSegmentField;  
  Element body;
  Element bSegmentField;    
  Element trailer;
  Element tSegmentField;    
  
  String currStr="";
  String prevStr="";  
 
  public void writeXML() throws TransformerConfigurationException, TransformerException {
	    DOMSource domSource = new DOMSource(xmldoc);
	    TransformerFactory tf = TransformerFactory.newInstance();
	    Transformer transformer = tf.newTransformer();
	    //
	    transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
	    transformer.setOutputProperty(OutputKeys.METHOD, "xml");
	    transformer.setOutputProperty(OutputKeys.ENCODING,"ISO-8859-1");
	    // XML output formatting
	    transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
	    transformer.setOutputProperty(OutputKeys.INDENT, "yes");
	    //
	    transformer.transform(domSource, outputFile);
	    /*
	      get the XML in a String
	          java.io.StringWriter sw = new java.io.StringWriter();
	          StreamResult sr = new StreamResult(sw);
	          transformer.transform(domSource, sr);
	          return sw.toString();
	    */  
	  }
	    
  public Element processHeader(String fieldName, String fieldSize, String fieldImport) {

	    Element e0 = xmldoc.createElement("HField");

	    Element e1 = xmldoc.createElement("HFieldName");
	    Node  n1 = xmldoc.createTextNode(fieldName);
	    e1.appendChild(n1);

	    Element e2 = xmldoc.createElement("HFieldMandatory");
	    Node  n2 = xmldoc.createTextNode("N");
	    e2.appendChild(n2);
	    
	    Element e3 = xmldoc.createElement("HFieldType");
	    Node  n3 = xmldoc.createTextNode("STRING");
	    e3.appendChild(n3);

	    Element e4 = xmldoc.createElement("HFieldSize");
	    Node  n4 = xmldoc.createTextNode(fieldSize);
	    e4.appendChild(n4);	    
	    
	    Element e5 = xmldoc.createElement("HFieldDecimal");
	    Node  n5 = xmldoc.createTextNode("0");
	    e5.appendChild(n5);
	    
	    Element e6 = xmldoc.createElement("HFieldSigned");
	    Node  n6 = xmldoc.createTextNode("N");
	    e6.appendChild(n6);

	    Element e7 = xmldoc.createElement("HFieldImport");
	    Node  n7 = xmldoc.createTextNode(fieldImport);
	    e7.appendChild(n7);	    	    

	    e0.appendChild(e1);
	    e0.appendChild(e2);
	    e0.appendChild(e3);
	    e0.appendChild(e4);	    
	    e0.appendChild(e5);
	    e0.appendChild(e6);
	    e0.appendChild(e7);   
	    
	    return e0;

	  }  
  
  public Element processBody(String fieldName, String fieldSize, String fieldImport) {
	    Element e0 = xmldoc.createElement("BField");

	    Element e1 = xmldoc.createElement("BFieldName");
	    Node  n1 = xmldoc.createTextNode(fieldName);
	    e1.appendChild(n1);

	    Element e2 = xmldoc.createElement("BFieldMandatory");
	    Node  n2 = xmldoc.createTextNode("N");
	    e2.appendChild(n2);
	    
	    Element e3 = xmldoc.createElement("BFieldType");
	    Node  n3 = xmldoc.createTextNode("STRING");
	    e3.appendChild(n3);

	    Element e4 = xmldoc.createElement("BFieldSize");
	    Node  n4 = xmldoc.createTextNode(fieldSize);
	    e4.appendChild(n4);	    
	    
	    Element e5 = xmldoc.createElement("BFieldDecimal");
	    Node  n5 = xmldoc.createTextNode("0");
	    e5.appendChild(n5);
	    
	    Element e6 = xmldoc.createElement("BFieldSigned");
	    Node  n6 = xmldoc.createTextNode("N");
	    e6.appendChild(n6);

	    Element e7 = xmldoc.createElement("BFieldImport");
	    Node  n7 = xmldoc.createTextNode(fieldImport);
	    e7.appendChild(n7);	    	    

	    e0.appendChild(e1);
	    e0.appendChild(e2);
	    e0.appendChild(e3);
	    e0.appendChild(e4);	    
	    e0.appendChild(e5);
	    e0.appendChild(e6);
	    e0.appendChild(e7);   
	    
	    return e0;
	  }    
  
  public Element processTrailer(String fieldName, String fieldSize, String fieldImport) {
	    Element e0 = xmldoc.createElement("TField");

	    Element e1 = xmldoc.createElement("TFieldName");
	    Node  n1 = xmldoc.createTextNode(fieldName);
	    e1.appendChild(n1);

	    Element e2 = xmldoc.createElement("TFieldMandatory");
	    Node  n2 = xmldoc.createTextNode("N");
	    e2.appendChild(n2);
	    
	    Element e3 = xmldoc.createElement("TFieldType");
	    Node  n3 = xmldoc.createTextNode("STRING");
	    e3.appendChild(n3);

	    Element e4 = xmldoc.createElement("TFieldSize");
	    Node  n4 = xmldoc.createTextNode(fieldSize);
	    e4.appendChild(n4);	    
	    
	    Element e5 = xmldoc.createElement("TFieldDecimal");
	    Node  n5 = xmldoc.createTextNode("0");
	    e5.appendChild(n5);
	    
	    Element e6 = xmldoc.createElement("TFieldSigned");
	    Node  n6 = xmldoc.createTextNode("N");
	    e6.appendChild(n6);

	    Element e7 = xmldoc.createElement("TFieldImport");
	    Node  n7 = xmldoc.createTextNode(fieldImport);
	    e7.appendChild(n7);	    	    

	    e0.appendChild(e1);
	    e0.appendChild(e2);
	    e0.appendChild(e3);
	    e0.appendChild(e4);	    
	    e0.appendChild(e5);
	    e0.appendChild(e6);
	    e0.appendChild(e7);   
	    
	    return e0;
	  }    
  
  public void process() {
	  	int picX=currStr.indexOf("PIC 9("); //numeric
	  	int pic9=currStr.indexOf("PIC X("); //alpha
	  	if (!(picX==-1) || !(pic9==-1)) { //if this is a line where a field is defined
	  		
	  		//get field name
	  		String[] parts = currStr.split("\\s+");	
	  		int fieldNameIndex =0;
	  		for (int i=0; i<parts.length; i++) {
	  			if (parts[i].equals("PIC")) {
	  				fieldNameIndex=i-1;
	  			}
	  		}
	  		String fieldName=parts[fieldNameIndex];
	  		
	  		//get size of field
	  		String size="";	 
	  		if (!(picX==-1)) {
	  			size=currStr.substring((picX+6),currStr.length());
	  		} else if (!(pic9==-1)) {
	  			size=currStr.substring((pic9+6),currStr.length()); 			
	  		}
  			size=size.substring(0,size.indexOf(")"));	 
	  		
	  		int HDR=currStr.indexOf("-HDR-");
	  		int TLR=currStr.indexOf("-TLR-");	  	  		
	  		if (!(HDR==-1)) { //this is a header field
	  		    hSegmentField.appendChild(processHeader(fieldName,size,"Y"));
	  		} else if (!(TLR==-1)) { //this is a trailer field
	  		    tSegmentField.appendChild(processTrailer(fieldName,size,"Y"));	  			
	  		} else { //this is a body field
	  		    bSegmentField.appendChild(processBody(fieldName,size,"Y"));	
	  		}
	  	}
	  }
  
  public void initXML() throws ParserConfigurationException{
	    // JAXP + DOM
	    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
	    DocumentBuilder builder = factory.newDocumentBuilder();
	    DOMImplementation impl = builder.getDOMImplementation();

	    xmldoc = impl.createDocument(null, "CopyBookXmlFile", null);
	    root = xmldoc.getDocumentElement();
	    
	    Element iName = xmldoc.createElement("InterfaceName");
	    Node inName = xmldoc.createTextNode("INTERFACENAME");
	    iName.appendChild(inName);	
	    root.appendChild(iName);	
	    
	    Element ebcdic = xmldoc.createElement("Ebcdic");
	    Node ebcdicNode = xmldoc.createTextNode("Y");
	    ebcdic.appendChild(ebcdicNode);	
	    root.appendChild(ebcdic);		
	    
	    Element binary = xmldoc.createElement("Binary");
	    Node binaryNode = xmldoc.createTextNode("Y");
	    binary.appendChild(binaryNode);	
	    root.appendChild(binary);		    
	    Element vSize = xmldoc.createElement("VariableSize");
	    root.appendChild(vSize);		 
	    Element dChar = xmldoc.createElement("DelimiterChar");
	    root.appendChild(dChar);		    
	    
	    header = xmldoc.createElement("Header");
	    Element hName = xmldoc.createElement("HName");
	    Node hnName = xmldoc.createTextNode("HEADER");
	    hName.appendChild(hnName);	
	    header.appendChild(hName);
	    hSegmentField = xmldoc.createElement("HSegmentField");	
	    Element hSegmentName = xmldoc.createElement("HSegmentName");
	    Node hnSegmentName = xmldoc.createTextNode("HEDR");
	    hSegmentName.appendChild(hnSegmentName);
	    hSegmentField.appendChild(hSegmentName);
	    header.appendChild(hSegmentField);	    
	    root.appendChild(header);		    
	    
	    body = xmldoc.createElement("Body");
	    Element bName = xmldoc.createElement("BName");
	    Node bnName = xmldoc.createTextNode("BODY");
	    bName.appendChild(bnName);	
	    body.appendChild(bName);
	    bSegmentField = xmldoc.createElement("BSegmentField");	
	    Element bSegmentName = xmldoc.createElement("BSegmentName");
	    Node bnSegmentName = xmldoc.createTextNode("BDY");
	    bSegmentName.appendChild(bnSegmentName);
	    bSegmentField.appendChild(bSegmentName);
	    body.appendChild(bSegmentField);	    
	    root.appendChild(body);		    
	    
	    trailer = xmldoc.createElement("Trailer");
	    Element tName = xmldoc.createElement("TName");
	    Node tnName = xmldoc.createTextNode("TRAILER");
	    tName.appendChild(tnName);	
	    trailer.appendChild(tName);
	    tSegmentField = xmldoc.createElement("TSegmentField");	
	    Element tSegmentName = xmldoc.createElement("TSegmentName");
	    Node tnSegmentName = xmldoc.createTextNode("TRLR");
	    tSegmentName.appendChild(tnSegmentName);
	    tSegmentField.appendChild(tSegmentName);
	    trailer.appendChild(tSegmentField);	    
	    root.appendChild(trailer);		 	    
  
	  }  
  
  public void fillerXML() throws ParserConfigurationException{
	    hSegmentField.appendChild(processHeader("HFILLER","1","N"));
	    tSegmentField.appendChild(processTrailer("TFILLER","1","N"));	  			
	    bSegmentField.appendChild(processBody("BFILLER","1","N"));	 
	  }    
  
  public void convert(String inputFilename, String outputFilename) {
    try{
      inputFile = new BufferedReader(new FileReader(inputFilename));
      outputFile = new StreamResult(outputFilename);
      initXML();
      while ((currStr = inputFile.readLine()) != null) {
         process();
         prevStr=currStr;
      }
      fillerXML();
      inputFile.close();
      writeXML();
    }  catch (Exception e) {
    	System.err.println("> Error Occured...");    	
    	System.err.println("> CopyBook Conversion - Failed\n");   	
    	System.err.println("> Error Stack Trace: ");
    	e.printStackTrace();
     }
  }
  
  public static void main (String args[]) {
	  System.out.println("V9 CopyBook2XML Utility\n");	 		  
	  System.out.println("This program takes a CopyBook text file as input and outputs an xml template guide");	 
	  System.out.println("to serve as the starting point for development of new Loader Interfaces for the V9 Framework.");
	  System.out.println("The program will create one Header segment, one Body segment and one Trailer segment.");	 	  
	  System.out.println("Developer will need to verify xml nodes and modify for multiple segments if required.\n");	 	  	 	  
		if (args.length != 2) {
			System.err
					.println("Error - Syntax is:\n\t CopyBook2XML [InputFileWithPath] [OutputFileWithPath] ");
			System.exit(1);
		}		  
	  System.out.println("> CopyBook Conversion - Started");
	  System.out.println("> Processing...");  	  
	  String inputFilename=args[0]; // copybook.txt (name of copybook data file) ie. Final-disp-ext-layout.txt
	  String outputFilename=args[1]; // loader.xml (name of interface loader xml file) ie. HO70Loader.xml
      new CopyBook2XML().convert(inputFilename, outputFilename);    
	  System.out.println("> CopyBook Conversion - Completed");
  }  

}