package com.lcl.pcb.v9.dao;

import java.sql.Connection;
import java.sql.SQLException;
/**
 * This is the abstract class for all of the
 * DAO framework tables.
 * All DAO tables extend this class
 */
public abstract class V9DAO<T> {

	public abstract int count() throws SQLException;

	// Protected
	protected final String tableName;
	protected Connection con;

	protected V9DAO(Connection con, String tableName) {
		this.tableName = tableName;
		this.con = con;
	}

}
