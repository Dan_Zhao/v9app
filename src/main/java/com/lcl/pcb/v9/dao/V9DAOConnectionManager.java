package com.lcl.pcb.v9.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


import sun.jdbc.odbc.ee.DataSource;

import com.lcl.pcb.v9.idf.jobflow.config.IDFConfig;

/**
 * This DAO framework class is used to establish
 * and manage the database connections
 * It uses the factory pattern to return the correct
 * database table that is needed by the V9DAOManager
 */
public class V9DAOConnectionManager {	

	private DataSource src;
	private Connection con;

	public enum Table {
		Applications, IDFAutosys, IDFLoadDBProps, IDFMonitoringLog, IDFStatusLog, IDFStatusLogDetail, IDFStatusRecords, IDFWarning, InterfaceDefinition, JobCtrl, LoadASCIIProps, LoadEBDICProps, LogLevels, ProfileAccessLookup, Profiles, UserFavourites, Users
	}

	private V9DAOConnectionManager() throws Exception {
		
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
		} catch(ClassNotFoundException ex) {			
		   System.out.println("Error: unable to load driver class for Oracle");
		   System.exit(1);
		}		
		

		IDFConfig idfConfig = IDFConfig.getInstance();	
		
//		String URL = "jdbc:oracle:thin:@lclvad246.ngco.com:1521:dv053";
//		String USER = "V9";
//		String PASS = "textme1";		
		
		String URL = "jdbc:oracle:thin:@"+idfConfig.getIdfData().getLogDblogUrl();
		String USER = idfConfig.getIdfData().getLogDblogUser();
		String PASS = idfConfig.getIdfData().getLogDblogPass();
		
		this.con = DriverManager.getConnection(URL, USER, PASS);
		
//		try {
//
//			InitialContext ctx = new InitialContext();
//			this.src = (DataSource) ctx.lookup("jndi/MYSQL");
//		} catch (Exception e) {
//			throw e;
//		}

	}

	private static class DAOManagerSingleton {

		public static final ThreadLocal<V9DAOConnectionManager> INSTANCE;
		static {
			ThreadLocal<V9DAOConnectionManager> dm;
			try {
				dm = new ThreadLocal<V9DAOConnectionManager>() {
					@Override
					protected V9DAOConnectionManager initialValue() {
						try {
							return new V9DAOConnectionManager();
						} catch (Exception e) {
							return null;
						}
					}
				};
			} catch (Exception e) {
				dm = null;
			}
			INSTANCE = dm;
		}

	}

	public static V9DAOConnectionManager getInstance() {
		return DAOManagerSingleton.INSTANCE.get();
	}

	public void open() throws SQLException {
		try {
			if (this.con == null || this.con.isClosed())
				this.con = src.getConnection();
		} catch (SQLException e) {
			throw e;
		}
	}

	public void close() throws SQLException {
		try {
			if (this.con != null && !this.con.isClosed())
				this.con.close();
		} catch (SQLException e) {
			throw e;
		}
	}

	public V9DAO<?> getDAO(Table t) throws SQLException {

		try {
			if (this.con == null || this.con.isClosed())
				this.open();
		} catch (SQLException e) {
			throw e;
		}

		switch (t) {
		case Applications:
			return new ApplicationsDAO(this.con);
		case IDFAutosys:
			return new IDFAutosysDAO(this.con);
		case IDFLoadDBProps:
			return new IDFLoadDBPropsDAO(this.con);
		case IDFMonitoringLog:
			return new IDFMonitoringLogDAO(this.con);
		case IDFStatusLog:
			return new IDFStatusLogDAO(this.con);
		case IDFStatusLogDetail:
			return new IDFStatusLogDetailDAO(this.con);
		case IDFStatusRecords:
			return new IDFStatusRecordsDAO(this.con);
		case IDFWarning:
			return new IDFWarningDAO(this.con);
		case InterfaceDefinition:
			return new InterfaceDefinitionDAO(this.con);
		case JobCtrl:
			return new JobCtrlDAO(this.con);
		case LoadASCIIProps:
			return new LoadASCIIPropsDAO(this.con);
		case LoadEBDICProps:
			return new LoadEBDICPropsDAO(this.con);
		case LogLevels:
			return new LogLevelsDAO(this.con);
		case ProfileAccessLookup:
			return new ProfileAccessLookupDAO(this.con);
		case Profiles:
			return new ProfilesDAO(this.con);
		default:
			throw new SQLException("Trying to link to an non existant table.");
		}

	}

}
