package com.lcl.pcb.v9.dao;

import com.lcl.pcb.v9.dao.V9DAOConnectionManager.Table;

/**
 * This DAO framework class is used to store
 * all data related method calls that the application
 * uses, it is the single point of contact to the DAO framework
 * All V9 classes requiring data updates to the monitoring logs use this class
 */
public class V9DAOManager {		
	
	/**
	 * This method gets the Job Sequence number and populates
	 * the field in all data logging tables for reference to any particular job run
	 * @param  none 
	 * @return int The unique database Sequence Number used to track the job runs
	 */	
	public int getJobSequenceNumber() throws Exception {
		V9DAOConnectionManager daoCM = V9DAOConnectionManager.getInstance();
		InterfaceDefinitionDAO interfaceDefinitionDao = (InterfaceDefinitionDAO) daoCM.getDAO(Table.InterfaceDefinition);
		return interfaceDefinitionDao.getSeqNum();
	}
	
	
	/**
	 * This method sets the logging for Level 1-5 with special
	 * behaviour for level 1
	 * SQL Update and Inserts
	 * @param x The level number
	 * @param rowcount the number of rows in the csv at this level number
	 * @parm recordsInserted the number of records inserted on a loaad at this level number
	 * @return none
	 */	
	public void setLevel(int x, int rowCount, int recordsInserted) throws Exception {
		
		V9DAOConnectionManager daoCM = V9DAOConnectionManager.getInstance();
		
		if (x==1) {
			//only for level 1
			JobCtrlDAO jobCtrlDao = (JobCtrlDAO) daoCM.getDAO(Table.JobCtrl);	
			jobCtrlDao.level1Insert(); // one insert for job
			
			IDFStatusLogDAO idfStatusLogDao = (IDFStatusLogDAO) daoCM.getDAO(Table.IDFStatusLog);
			idfStatusLogDao.level1Insert(); // one insert then subsequent updates
			
		} else if ((x>1) && (x<6)) {
			//for levels 2-5
			JobCtrlDAO jobCtrlDao = (JobCtrlDAO) daoCM.getDAO(Table.JobCtrl);	
			jobCtrlDao.levelXUpdate(x); 
			
			IDFStatusLogDAO idfStatusLogDao = (IDFStatusLogDAO) daoCM.getDAO(Table.IDFStatusLog);
			idfStatusLogDao.levelXUpdate(x, rowCount);  //  multiple updates
			
			IDFMonitoringLogDAO idfMonitoringLogUpdate= (IDFMonitoringLogDAO) daoCM.getDAO(Table.IDFMonitoringLog);
			idfMonitoringLogUpdate.levelXUpdate(x-1); // update with completed status for level					
		}
		
		//insert for levels 1-5
		
		IDFMonitoringLogDAO idfMonitoringLog= (IDFMonitoringLogDAO) daoCM.getDAO(Table.IDFMonitoringLog);
		idfMonitoringLog.levelXInsert(x,rowCount); // mulitiple inserts				
		
		IDFStatusLogDetailDAO idfStatusLogDetailDao = (IDFStatusLogDetailDAO) daoCM.getDAO(Table.IDFStatusLogDetail);
		idfStatusLogDetailDao.levelXInsert(x); // multiple inserts
		
		IDFStatusRecordsDAO idfStatusRecordsDao = (IDFStatusRecordsDAO) daoCM.getDAO(Table.IDFStatusRecords);
		idfStatusRecordsDao.levelXInsert(x,recordsInserted); // multiple inserts
		
		IDFWarningDAO idfWarningDao = (IDFWarningDAO) daoCM.getDAO(Table.IDFWarning);
		idfWarningDao.levelXInsert(x); // multiple inserts
	

	}	
	
	
	/**
	 * This method sets the logging for when there is an error
	 * on a level during processing
	 * SQL Update IDFWarning and IDFStatusLog tables with error information
	 * @param  x The level number
	 * @param  message The error message sent by Loader/Extractor/Reporter code
	 * @return none
	 */	
	public void setLevelWarning(int x, String message, int badRecordCt) throws Exception {
		
		V9DAOConnectionManager daoCM = V9DAOConnectionManager.getInstance();
		
		JobCtrlDAO jobCtrlDao = (JobCtrlDAO) daoCM.getDAO(Table.JobCtrl);	
		jobCtrlDao.levelXWarnUpdate(x); 		
		
		IDFWarningDAO idfWarningDao = (IDFWarningDAO) daoCM.getDAO(Table.IDFWarning);
		idfWarningDao.levelXWarnUpdate(x, message); 		
		
		IDFStatusLogDetailDAO idfStatusLogDetailDao = (IDFStatusLogDetailDAO) daoCM.getDAO(Table.IDFStatusLogDetail);
		idfStatusLogDetailDao.levelXWarnUpdate(x, message);	

		IDFStatusLogDAO idfStatusLogDao = (IDFStatusLogDAO) daoCM.getDAO(Table.IDFStatusLog);
		idfStatusLogDao.levelXWarnUpdate(x, badRecordCt); 
		
		IDFMonitoringLogDAO idfMonitoringLogDao= (IDFMonitoringLogDAO) daoCM.getDAO(Table.IDFMonitoringLog);
		idfMonitoringLogDao.levelXWarnUpdate(x); // update with completed status for level				

		daoCM.close();

        System.err.println("Job Failed at Level " +x);
        System.exit(1);

	}		
	
	/**
	 * This method sets the final logging updates
	 * The final logging for the level specified
	 * SQL Update and Inserts
	 * ie. levels 1-5: level 1 ends where level 2 begins and so forth.  For level 5 there needs to be an end state since there is no level 6)
	 * @param  x The level number 
	 * @return none
	 */	
	public void setLevelEnd(int x) throws Exception {
		
		V9DAOConnectionManager daoCM = V9DAOConnectionManager.getInstance();
		
		JobCtrlDAO jobCtrlDao = (JobCtrlDAO) daoCM.getDAO(Table.JobCtrl);	
		jobCtrlDao.levelXEndUpdate(x);		
		
		IDFStatusLogDAO idfStatusLogDao = (IDFStatusLogDAO) daoCM.getDAO(Table.IDFStatusLog);
		idfStatusLogDao.levelXEndUpdate(x);  // update with completed status for level	
		
		IDFMonitoringLogDAO idfMonitoringLogUpdate= (IDFMonitoringLogDAO) daoCM.getDAO(Table.IDFMonitoringLog);
		idfMonitoringLogUpdate.levelXEndUpdate(x); // update with completed status for level					

	}		
	
		
	/**
	 * This method gets the count of the number of records in the Applications table
	 * This is a sample method call to obtain a row count for Applications table but can be called on any table DAO
	 * @param  none 
	 * @return int The count of records in the Applications table
	 */		
	public int getCountofApplications() throws Exception {
		V9DAOConnectionManager daoCM = V9DAOConnectionManager.getInstance();
		ApplicationsDAO applicationsDao = (ApplicationsDAO) daoCM.getDAO(Table.Applications);
		return applicationsDao.count();
	}
	
	/**
	 * This method closes the connection Manager 
	 * ie. Gets the singleton and closes 
	 * @param  none 
	 * @return none
	 */	
	public void closeV9A0ConnectionManager() throws Exception {
		V9DAOConnectionManager daoCM = V9DAOConnectionManager.getInstance();
		daoCM.close();
	}	

}
