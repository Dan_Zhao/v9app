package com.lcl.pcb.v9.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * This is the DAO (data access object) for the
 * Users table
 * Inherits from V9DAO
 */
public class UsersDAO extends V9DAO<UsersDAO> {

    public UsersDAO(Connection con) {
        super(con, "Users");
    }

    @Override
    public int count() throws SQLException {
        String query = "SELECT COUNT(*) AS count FROM Users";
        PreparedStatement counter;
        try
        {
	        counter = this.con.prepareStatement(query);
	        ResultSet res = counter.executeQuery();
	        res.next();
	        return res.getInt("count");
        }
	        catch(SQLException e){ throw e; 
	    }
    }


}
