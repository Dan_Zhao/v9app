package com.lcl.pcb.v9.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * This is the DAO (data access object) for the
 * LogLevels table
 * Inherits from V9DAO
 */
public class LogLevelsDAO extends V9DAO<LogLevelsDAO> {


    public LogLevelsDAO(Connection con) {
        super(con, "LOG_LEVELS");
    }

    @Override
    public int count() throws SQLException {
        String query = "SELECT COUNT(*) AS count FROM LOG_LEVELS";
        PreparedStatement counter;
        try
        {
	        counter = this.con.prepareStatement(query);
	        ResultSet res = counter.executeQuery();
	        res.next();
	        return res.getInt("count");
        }
	        catch(SQLException e){ throw e; 
	    }
    }


}
