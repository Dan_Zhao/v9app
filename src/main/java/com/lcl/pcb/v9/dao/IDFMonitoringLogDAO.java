package com.lcl.pcb.v9.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.lcl.pcb.v9.idf.jobflow.config.IDFConfig;
/**
 * This is the DAO (data access object) for the
 * IDFMonitoringLog table
 * Inherits from V9DAO
 */
public class IDFMonitoringLogDAO extends V9DAO<IDFMonitoringLogDAO> {
	
	public static final String FAILED="Failed";
	public static final String COMPLETED="Completed";
	public static final String INPROGRESS="InProgress";

	
    private int jobSequenceNo; //Sequence number from Q1
    private int interfaceID; //Interface id
    private String operationDescription; //Level description as a string
    private String status; //Level 1 in progress
    private int rowCount; //Total records inserted to DB from the file at that point- applicable from level4
    private java.sql.Date recTMS; //sysdate
    private String logLevel;

    public IDFMonitoringLogDAO(Connection con) {
        super(con, "IDF_MONITORING_LOG");
    }
    
    public void levelXInsert(int x, int rCount) throws SQLException {
    	
    	//set previous level to 'Completed'
    	if (x>1) {
    		levelXUpdate(x-1);
    	}
    	
		int sequenceNum = IDFConfig.getInstance().getSequenceNum();
		String uniqueID = IDFConfig.getInstance().getIdfData().getFilePropIdfUniqueId();
		
		
	    jobSequenceNo=sequenceNum;
	    interfaceID=Integer.parseInt(uniqueID); 
	    operationDescription="Level "+x;
    	status=INPROGRESS;
    	
	    rowCount=rCount; 
	    java.util.Date date = new java.util.Date();
	    long t = date.getTime();
	    java.sql.Date sqlDate = new java.sql.Date(t);
	    //java.sql.Time sqlTime = new java.sql.Time(t);
	    //java.sql.Timestamp sqlTimestamp = new java.sql.Timestamp(t);
	    //recTMS=sqlDate;	
	    logLevel="L"+x;	    
	    
    	String query="INSERT INTO IDF_MONITORING_LOG (JOB_SEQUENCE_NO, INTERFACE_ID, OPERATION_DESCRIPTION, STATUS, ROW_COUNT, REC_TMS, LOG_LEVEL) VALUES (?,?,?,?,?,sysdate,?)"; 	
        PreparedStatement sqlStatement = this.con.prepareStatement(query);        
    	sqlStatement.setInt(1,jobSequenceNo);
    	sqlStatement.setInt(2,interfaceID);
    	sqlStatement.setString(3,operationDescription);
    	sqlStatement.setString(4,status);
    	sqlStatement.setInt(5,rowCount);
    	//sqlStatement.setDate(6,recTMS);
    	//sqlStatement.setString(7,logLevel);
    	sqlStatement.setString(6,logLevel);
    	
    	try
        {
	        sqlStatement.executeQuery();    
        }
	        catch(SQLException e){ throw e; 
	    }
    }

    public void levelXUpdate(int x) throws SQLException {
    		        
		int sequenceNum = IDFConfig.getInstance().getSequenceNum();
		String uniqueID = IDFConfig.getInstance().getIdfData().getFilePropIdfUniqueId();
		
	    jobSequenceNo=sequenceNum;
	    interfaceID=Integer.parseInt(uniqueID); 	
	    status=COMPLETED;
	    logLevel="L"+x;
	    
    	String query="UPDATE IDF_MONITORING_LOG SET STATUS=? WHERE JOB_SEQUENCE_NO=? AND INTERFACE_ID=? AND LOG_LEVEL=?"; 	
        PreparedStatement sqlStatement = this.con.prepareStatement(query);        
    	sqlStatement.setString(1,status);
    	//where
    	sqlStatement.setInt(2,jobSequenceNo);
    	sqlStatement.setInt(3,interfaceID);
    	sqlStatement.setString(4,logLevel);  	    
		
    	try
        {
	        sqlStatement.executeUpdate();    
        }
	        catch(SQLException e){ throw e; 
	    }	        
        
    }        
    
    public void levelXWarnUpdate(int x) throws SQLException {
        
		int sequenceNum = IDFConfig.getInstance().getSequenceNum();
		String uniqueID = IDFConfig.getInstance().getIdfData().getFilePropIdfUniqueId();
		
	    jobSequenceNo=sequenceNum;
	    interfaceID=Integer.parseInt(uniqueID); 	
	    status=FAILED;
	    logLevel="L"+x;
	    
    	String query="UPDATE IDF_MONITORING_LOG SET STATUS=? WHERE JOB_SEQUENCE_NO=? AND INTERFACE_ID=? AND LOG_LEVEL=?"; 	
        PreparedStatement sqlStatement = this.con.prepareStatement(query);        
    	sqlStatement.setString(1,status);
    	//where
    	sqlStatement.setInt(2,jobSequenceNo);
    	sqlStatement.setInt(3,interfaceID);
    	sqlStatement.setString(4,logLevel);  	    
		
    	try
        {
	        sqlStatement.executeUpdate();    
        }
	        catch(SQLException e){ throw e; 
	    }	        
        
    }    
    
    public void levelXEndUpdate(int x) throws SQLException {
		levelXUpdate(x);     
    }      

    @Override
    public int count() throws SQLException {
        String query = "SELECT COUNT(*) AS count FROM IDF_MONITORING_LOG";
        PreparedStatement counter;
        try
        {
	        counter = this.con.prepareStatement(query);
	        ResultSet res = counter.executeQuery();
	        res.next();
	        return res.getInt("count");
        }
	        catch(SQLException e){ throw e; 
	    }
    }


}
