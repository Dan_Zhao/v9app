package com.lcl.pcb.v9.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * This is the DAO (data access object) for the
 * ProfileAccessLookup table
 * Inherits from V9DAO
 */
public class ProfileAccessLookupDAO extends V9DAO<ProfileAccessLookupDAO> {

    public ProfileAccessLookupDAO(Connection con) {
        super(con, "PROFILE_ACCESS_LOOKUP");
    }

    @Override
    public int count() throws SQLException {
        String query = "SELECT COUNT(*) AS count FROM PROFILE_ACCESS_LOOKUP";
        PreparedStatement counter;
        try
        {
	        counter = this.con.prepareStatement(query);
	        ResultSet res = counter.executeQuery();
	        res.next();
	        return res.getInt("count");
        }
	        catch(SQLException e){ throw e; 
	    }
    }


}
