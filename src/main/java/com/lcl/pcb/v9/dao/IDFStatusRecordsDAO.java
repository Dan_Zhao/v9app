package com.lcl.pcb.v9.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import com.lcl.pcb.v9.idf.jobflow.config.IDFConfig;

/**
 * This is the DAO (data access object) for the
 * IDFStatusRecords table
 * Inherits from V9DAO
 */
public class IDFStatusRecordsDAO extends V9DAO<IDFStatusRecordsDAO> {
	

    private java.sql.Date logDt; //sysdate
    private java.sql.Timestamp logTimeSt; //sysDate
    private String logLevel; //L1
    private int recordsInserted; //Records inserted to database until that point in code- could be calculated only from level4
    private int interfaceID; //Interface id from idf
    private int jobSequenceNo; //sequence no       

    public IDFStatusRecordsDAO(Connection con) {
        super(con, "IDF_STATUS_RECORDS");
    }
    
    public void levelXInsert(int x, int rInserted) throws SQLException {
 
		int sequenceNum = IDFConfig.getInstance().getSequenceNum();    	
		String uniqueID = IDFConfig.getInstance().getIdfData().getFilePropIdfUniqueId();
	    java.util.Date date = new java.util.Date();
	    long t = date.getTime();
	    java.sql.Date sqlDate = new java.sql.Date(t);
	    //java.sql.Time sqlTime = new java.sql.Time(t);
	    java.sql.Timestamp sqlTimestamp = new java.sql.Timestamp(t);		
		//logDt=sqlDate;
		//logTimeSt=sqlTimestamp;
		logLevel="L"+x;
		recordsInserted=rInserted;
	    interfaceID=Integer.parseInt(uniqueID); 
	    jobSequenceNo=sequenceNum;	    
	    
    	String query="INSERT INTO IDF_STATUS_RECORDS (LOG_DT, LOG_TIMEST, LOG_LEVEL, RECORDS_INSERTED, INTERFACE_ID, JOB_SEQUENCE_NO) VALUES (sysdate,systimestamp,?,?,?,?)"; 	
        PreparedStatement sqlStatement = this.con.prepareStatement(query);        
    	//sqlStatement.setDate(1,logDt);
    	//sqlStatement.setTimestamp(2,logTimeSt);
    	sqlStatement.setString(1,logLevel);
    	sqlStatement.setInt(2,recordsInserted);
    	sqlStatement.setInt(3,interfaceID);
    	sqlStatement.setInt(4,jobSequenceNo);
    	
    	try
        {
	        sqlStatement.executeQuery();    
        }
	        catch(SQLException e){ throw e; 
	    }	    

    }

    public void levelXUpdate(int x) throws SQLException {

    	 
		int sequenceNum = IDFConfig.getInstance().getSequenceNum();    	
		String uniqueID = IDFConfig.getInstance().getIdfData().getFilePropIdfUniqueId();
	    java.util.Date date = new java.util.Date();
	    long t = date.getTime();
	    java.sql.Date sqlDate = new java.sql.Date(t);
	    //java.sql.Time sqlTime = new java.sql.Time(t);
	    java.sql.Timestamp sqlTimestamp = new java.sql.Timestamp(t);		
		//logDt=sqlDate;
		//logTimeSt=sqlTimestamp;
		logLevel="L"+x;
		recordsInserted=888;
	    interfaceID=Integer.parseInt(uniqueID); 
	    jobSequenceNo=sequenceNum;	    
	    
    	String query="UPDATE IDF_STATUS_RECORDS SET LOG_DT=sysdate, LOG_TIMEST=systimestamp, LOG_LEVEL=?, RECORDS_INSERTED=?, INTERFACE_ID=?, JOB_SEQUENCE_NO=? WHERE JOB_SEQUENCE_NO=?"; 	
        PreparedStatement sqlStatement = this.con.prepareStatement(query);        
    	//sqlStatement.setDate(1,logDt);
    	//sqlStatement.setTimestamp(2,logTimeSt);
    	sqlStatement.setString(1,logLevel);
    	sqlStatement.setInt(2,recordsInserted);
    	sqlStatement.setInt(3,interfaceID);
    	sqlStatement.setInt(4,jobSequenceNo);
    	//where
    	sqlStatement.setInt(5,jobSequenceNo);    	
    	
    	try
        {
	        sqlStatement.executeUpdate();    
        }
	        catch(SQLException e){ throw e; 
	    }	    
    }        

    @Override
    public int count() throws SQLException {
        String query = "SELECT COUNT(*) AS count FROM IDF_STATUS_RECORDS";
        PreparedStatement counter;
        try
        {
	        counter = this.con.prepareStatement(query);
	        ResultSet res = counter.executeQuery();
	        res.next();
	        return res.getInt("count");
        }
	        catch(SQLException e){ throw e; 
	    }
    }


}
