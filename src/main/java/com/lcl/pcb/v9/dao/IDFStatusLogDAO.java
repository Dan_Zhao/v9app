package com.lcl.pcb.v9.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.lcl.pcb.v9.idf.jobflow.config.IDFConfig;
import com.lcl.pcb.v9.idf.jobflow.factory.V9Loader;

/**
 * This is the DAO (data access object) for the
 * IDFStatusLogDAO table
 * Inherits from V9DAO
 */
public class IDFStatusLogDAO extends V9DAO<IDFStatusLogDAO> {
	
	public static final String INITIATED="Initiated";	
	public static final String INPROGRESS="InProgress";		
	public static final String FAILED="Failed";		
	public static final String COMPLETED="Completed";	
	
	public static final String NA="NA";		
	public static final String ADM="ADM";		
	
    private String currentLevel; //L1
    private String overallStatus; //Initiated
    private java.sql.Date jobStartDt; //Overall start date
    private java.sql.Date jobEndDt; //Overall end date
    private java.sql.Date l1StartDt; //L1 start date
    private java.sql.Date l1EndDt; //L1 end date
    private java.sql.Date l2StartDt; //L2 start date
    private java.sql.Date l2EndDt; //L2 end date
    private java.sql.Date l3StartDt; //L3 start date
    private java.sql.Date l3EndDt; //L3 end date
    private java.sql.Date l4StartDt; //L4 start date
    private java.sql.Date l4EndDt; //L4 end date
    private java.sql.Date l5StartDt; //L5 start date
    private java.sql.Date l5EndDt; //L5 end date
    private int interfaceID; //interface ID from the ID file
    private String sourceTable; //NA
    private String targetTable; //ADM
    private int totalRecords; //Total records in the file'
    private int badRecordCt; //Bad record count
    private int jobSequenceNo;  //job seq number

    public IDFStatusLogDAO(Connection con) {
        super(con, "IDF_STATUS_LOG");
    }
   
    public void level1Insert() throws SQLException {
    	
		int sequenceNum = IDFConfig.getInstance().getSequenceNum();
		String uniqueID = IDFConfig.getInstance().getIdfData().getFilePropIdfUniqueId();
	    java.util.Date date = new java.util.Date();
	    long t = date.getTime();   
	    java.sql.Date sqlDate = new java.sql.Date(t);

	    currentLevel="L1";
	    overallStatus="Initiated";
	    //jobStartDt=sqlDate;	
	    jobEndDt=null;
	    //l1StartDt=sqlDate;
	    l1EndDt=null;
	    l2StartDt=null;
	    l2EndDt=null;
	    l3StartDt=null;
	    l3EndDt=null;
	    l4StartDt=null;
	    l4EndDt=null;
	    l5StartDt=null;
	    l5EndDt=null;
	    interfaceID=Integer.parseInt(uniqueID); 
	    sourceTable="NA";
	    targetTable="ADM";
	    totalRecords=0;
	    badRecordCt=0;
	    jobSequenceNo=sequenceNum;	
    	
    	String query="INSERT INTO IDF_STATUS_LOG ( CURRENT_LEVEL, OVERALL_STATUS, JOB_START_DT, JOB_END_DT, L1_START_DT, L1_END_DT, L2_START_DT, L2_END_DT, L3_START_DT, L3_END_DT, L4_START_DT, L4_END_DT, L5_START_DT, L5_END_DT, INTERFACE_ID, SOURCE_TABLE, TARGET_TABLE, TOTAL_RECORDS, BAD_RECORD_CT, JOB_SEQUENCE_NO) VALUES (?,?,sysdate,?,sysdate,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"; 	
        PreparedStatement sqlStatement = this.con.prepareStatement(query);        
    	sqlStatement.setString(1,currentLevel);
    	sqlStatement.setString(2,overallStatus);
    	//sqlStatement.setDate(3,jobStartDt);
    	sqlStatement.setDate(3,jobEndDt);
    	//sqlStatement.setDate(5,l1StartDt);
    	sqlStatement.setDate(4,l1EndDt);
    	sqlStatement.setDate(5,l2StartDt);
    	sqlStatement.setDate(6,l2EndDt);
    	sqlStatement.setDate(7,l3StartDt);
    	sqlStatement.setDate(8,l3EndDt);
    	sqlStatement.setDate(9,l4StartDt);
    	sqlStatement.setDate(10,l4EndDt);
    	sqlStatement.setDate(11,l5StartDt);
    	sqlStatement.setDate(12,l5EndDt);    	
    	sqlStatement.setInt(13,interfaceID);
    	sqlStatement.setString(14,sourceTable);
    	sqlStatement.setString(15,targetTable);
    	sqlStatement.setInt(16,totalRecords);
    	sqlStatement.setInt(17,badRecordCt);
    	sqlStatement.setInt(18,jobSequenceNo);
    	try
        {
	        sqlStatement.executeQuery();    
        }
	        catch(SQLException e){ throw e; 
	    }	      	    

    }

    public void levelXUpdate(int x, int rowCount) throws SQLException {
    	
		int sequenceNum = IDFConfig.getInstance().getSequenceNum();
		String uniqueID = IDFConfig.getInstance().getIdfData().getFilePropIdfUniqueId();
	    java.util.Date date = new java.util.Date();
	    long t = date.getTime();
	    java.sql.Date sqlDate = new java.sql.Date(t);

	    currentLevel="L"+x;
	    overallStatus=INITIATED;
	    //jobStartDt=sqlDate;	
	    //jobEndDt=sqlDate;
	    //l1StartDt=sqlDate;
	    //l1EndDt=sqlDate;
	    //l2StartDt=sqlDate;
	    //l2EndDt=sqlDate;
	    //l3StartDt=sqlDate;
	    //l3EndDt=sqlDate;
	    //l4StartDt=sqlDate;
	    //l4EndDt=sqlDate;
	    //l5StartDt=sqlDate;
	    //l5EndDt=sqlDate;
	    interfaceID=Integer.parseInt(uniqueID); 
	    sourceTable=NA;
	    targetTable=ADM;
	    //totalRecords=888;
	    //totalRecords=V9Loader.totalRecCount;
	    totalRecords=rowCount;
	    badRecordCt=0;
	    jobSequenceNo=sequenceNum;	

	    String query=" ";
	    PreparedStatement sqlStatement=null;
	    
	    if (x==2) {
	    	query="UPDATE IDF_STATUS_LOG SET CURRENT_LEVEL=?, OVERALL_STATUS=?,TOTAL_RECORDS=?, L1_END_DT=sysdate, L2_START_DT=sysdate WHERE JOB_SEQUENCE_NO=?"; 	
	        sqlStatement = this.con.prepareStatement(query);        
	    	sqlStatement.setString(1,currentLevel);
		    overallStatus=INPROGRESS;
	    	sqlStatement.setString(2,overallStatus);
	    	sqlStatement.setInt(3, totalRecords);
	    	//sqlStatement.setDate(3,l2EndDt);
	    	//sqlStatement.setDate(4,l3StartDt);
	    	//where
	    	sqlStatement.setInt(4,jobSequenceNo);	    	
	    } else if (x==3) {
	    	query="UPDATE IDF_STATUS_LOG SET CURRENT_LEVEL=?, OVERALL_STATUS=?,TOTAL_RECORDS=?, L2_END_DT=sysdate, L3_START_DT=sysdate WHERE JOB_SEQUENCE_NO=?"; 	
	        sqlStatement = this.con.prepareStatement(query);        
	    	sqlStatement.setString(1,currentLevel);
		    overallStatus=INPROGRESS;	    	
	    	sqlStatement.setString(2,overallStatus);
	    	sqlStatement.setInt(3, totalRecords);
	    	//sqlStatement.setDate(3,l2EndDt);
	    	//sqlStatement.setDate(4,l3StartDt);
	    	//where
	    	sqlStatement.setInt(4,jobSequenceNo);	    	
	    } else if (x==4) {
	    	query="UPDATE IDF_STATUS_LOG SET CURRENT_LEVEL=?, OVERALL_STATUS=?,TOTAL_RECORDS=?, L3_END_DT=sysdate, L4_START_DT=sysdate WHERE JOB_SEQUENCE_NO=?"; 	
	        sqlStatement = this.con.prepareStatement(query);        
	    	sqlStatement.setString(1,currentLevel);
		    overallStatus=INPROGRESS;
	    	sqlStatement.setString(2,overallStatus);
	    	sqlStatement.setInt(3, totalRecords);
	    	//sqlStatement.setDate(3,l3EndDt);
	    	//sqlStatement.setDate(4,l4StartDt);
	    	//where
	    	sqlStatement.setInt(4,jobSequenceNo);    	
	    } else if (x==5) {
	    	query="UPDATE IDF_STATUS_LOG SET CURRENT_LEVEL=?, OVERALL_STATUS=?,TOTAL_RECORDS=?, JOB_END_DT=sysdate, L4_END_DT=sysdate, L5_START_DT=sysdate WHERE JOB_SEQUENCE_NO=?"; 	
	        sqlStatement = this.con.prepareStatement(query);        
	    	sqlStatement.setString(1,currentLevel);
		    overallStatus=INPROGRESS;
	    	sqlStatement.setString(2,overallStatus);
	    	sqlStatement.setInt(3, totalRecords);
	    	//sqlStatement.setDate(3,jobEndDt);	    	
	    	//sqlStatement.setDate(4,l4EndDt);
	    	//sqlStatement.setDate(5,l5StartDt);    	
	    	//where
	    	sqlStatement.setInt(4,jobSequenceNo);       	
	    }
    	
    	try
        {
	        sqlStatement.executeUpdate();    
        }
	        catch(SQLException e){ throw e; 
	    }	      	    
    }
    
    public void levelXWarnUpdate(int x, int badRecordCt) throws SQLException {
    	
		int sequenceNum = IDFConfig.getInstance().getSequenceNum();
	    java.util.Date date = new java.util.Date();
	    long t = date.getTime();
	    java.sql.Date sqlDate = new java.sql.Date(t);
		
	    currentLevel="L"+x;
	    overallStatus=FAILED;
	    //jobEndDt=sqlDate;
	    jobSequenceNo=sequenceNum;	
   

	    String query=" ";
	    PreparedStatement sqlStatement=null;
	    
    	query="UPDATE IDF_STATUS_LOG SET CURRENT_LEVEL=?, OVERALL_STATUS=?, JOB_END_DT=sysdate, BAD_RECORD_CT=? WHERE JOB_SEQUENCE_NO=?"; 	
        sqlStatement = this.con.prepareStatement(query);        
    	sqlStatement.setString(1,currentLevel);
    	sqlStatement.setString(2,overallStatus);
    	//sqlStatement.setDate(3,jobEndDt);
    	sqlStatement.setInt(3,badRecordCt);	     	
    	//where
    	sqlStatement.setInt(4,jobSequenceNo);  
     	
    	
    	try
        {
	        sqlStatement.executeUpdate();    
        }
	        catch(SQLException e){ throw e; 
	    }	      	    
    }    
    
    public void levelXEndUpdate(int x) throws SQLException {
    	
		int sequenceNum = IDFConfig.getInstance().getSequenceNum();
		String uniqueID = IDFConfig.getInstance().getIdfData().getFilePropIdfUniqueId();
	    java.util.Date date = new java.util.Date();
	    long t = date.getTime();
	    java.sql.Date sqlDate = new java.sql.Date(t);
	    //java.sql.Time sqlTime = new java.sql.Time(t);
	   // java.sql.Timestamp sqlTimestamp = new java.sql.Timestamp(t);			
		
	    currentLevel="L"+x;
	    overallStatus=COMPLETED;
	    //jobEndDt=sqlDate;
	    //l5EndDt=sqlDate;
	    interfaceID=Integer.parseInt(uniqueID); 
	    jobSequenceNo=sequenceNum;	
    	
	    String query=" ";
	    PreparedStatement sqlStatement=null;

    	query="UPDATE IDF_STATUS_LOG SET CURRENT_LEVEL=?, OVERALL_STATUS=?, JOB_END_DT=sysdate, L5_END_DT=sysdate WHERE JOB_SEQUENCE_NO=?";  	
        sqlStatement = this.con.prepareStatement(query);        
    	sqlStatement.setString(1,currentLevel);
    	sqlStatement.setString(2,overallStatus);
    	//sqlStatement.setDate(3,jobEndDt);	    	
    	//sqlStatement.setDate(4,l5EndDt);    	
    	//where
    	sqlStatement.setInt(3,jobSequenceNo); 
    	
    	try
        {
	        sqlStatement.executeUpdate();    
        }
	        catch(SQLException e){ throw e; 
	    }	      	    
    }        
    
    
    @Override
    public int count() throws SQLException {
        String query = "SELECT COUNT(*) AS count FROM IDF_STATUS_LOG";
        PreparedStatement counter;
        try
        {
	        counter = this.con.prepareStatement(query);
	        ResultSet res = counter.executeQuery();
	        res.next();
	        return res.getInt("count");
        }
	        catch(SQLException e){ throw e; 
	    }
    }    

}
