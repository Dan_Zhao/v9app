package com.lcl.pcb.v9.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * This is the DAO (data access object) for the
 * LoadASCIIProps table
 * Inherits from V9DAO
 */
public class LoadASCIIPropsDAO extends V9DAO<LoadASCIIPropsDAO> {

    public LoadASCIIPropsDAO(Connection con) {
        super(con, "LOAD_ASCII_PROPS");
    }

    @Override
    public int count() throws SQLException {
        String query = "SELECT COUNT(*) AS count FROM LOAD_ASCII_PROPS";
        PreparedStatement counter;
        try
        {
	        counter = this.con.prepareStatement(query);
	        ResultSet res = counter.executeQuery();
	        res.next();
	        return res.getInt("count");
        }
	        catch(SQLException e){ throw e; 
	    }
    }


}
