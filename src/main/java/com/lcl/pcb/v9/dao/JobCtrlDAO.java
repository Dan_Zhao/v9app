package com.lcl.pcb.v9.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.lcl.pcb.v9.idf.jobflow.config.IDFConfig;


/**
 * This is the DAO (data access object) for the
 * JobCtrl table
 * Inherits from V9DAO
 */
public class JobCtrlDAO extends V9DAO<JobCtrlDAO> {
	
	public static final String DESCRIPTION="ADM Data Extraction";
	public static final String RECCHNGID="V9 Load";
	public static final String STCD="A";
	
    private String jobName; //job name
    private int jobSequenceNo; //sequence no   
    private String description; //job name  
    private java.sql.Timestamp recCreateTms; //sysdate
    private java.sql.Timestamp recChngTms; //sysdate    
    private String recChngID; //recChngID
    private String stCD; //stCD     
    private java.sql.Date stDt; //stDt
    private int interfaceID; // interface id from idf
    private String idfAutosysJobName; //autosys job name


    public JobCtrlDAO(Connection con) {
        super(con, "JOB_CTRL");
    }
    
    public void level1Insert() throws SQLException {
    	
		int sequenceNum = IDFConfig.getInstance().getSequenceNum();
		String uniqueID = IDFConfig.getInstance().getIdfData().getFilePropIdfUniqueId();
		String filePropName=IDFConfig.getInstance().getIdfData().getFilePropName();
	    java.util.Date date = new java.util.Date();
	    long t = date.getTime();
	    java.sql.Date sqlDate = new java.sql.Date(t);
	    //java.sql.Time sqlTime = new java.sql.Time(t);
	    java.sql.Timestamp sqlTimestamp = new java.sql.Timestamp(t);			
		
	    jobName=filePropName;
	    jobSequenceNo=sequenceNum;
	    description=DESCRIPTION; 
	    recCreateTms=sqlTimestamp;
	    recChngTms=sqlTimestamp; 
	    recChngID=RECCHNGID; 
	    stCD=STCD;
	    stDt=sqlDate; 
	    interfaceID=Integer.parseInt(uniqueID); 
	    idfAutosysJobName=filePropName;
	    
	   	String query="INSERT INTO JOB_CTRL ( JOB_NAME, JOB_SEQUENCE_NO, DESCRIPTION, REC_CREATE_TMS, REC_CHNG_TMS, REC_CHNG_ID, ST_CD, ST_DT, INTERFACE_ID, IDF_AUTOSYS_JOB_NAME) VALUES (?,?,?,?,?,?,?,?,?,?)"; 	
        PreparedStatement sqlStatement = this.con.prepareStatement(query);        
    	sqlStatement.setString(1,jobName);
    	sqlStatement.setInt(2,jobSequenceNo);
    	sqlStatement.setString(3,description);
    	sqlStatement.setTimestamp(4,recCreateTms);
    	sqlStatement.setTimestamp(5,recChngTms);
    	sqlStatement.setString(6,recChngID);    	
    	sqlStatement.setString(7,stCD);
    	sqlStatement.setDate(8,stDt);
    	sqlStatement.setInt(9,interfaceID);
    	sqlStatement.setString(10,idfAutosysJobName);
    	
    	try
        {
	        sqlStatement.executeQuery();    
        }
	        catch(SQLException e){ throw e; 
	    }	    
	    
    }
    
    public void levelXUpdate(int x) throws SQLException {
    	
		int sequenceNum = IDFConfig.getInstance().getSequenceNum();
	    java.util.Date date = new java.util.Date();
	    long t = date.getTime();
	    java.sql.Timestamp sqlTimestamp = new java.sql.Timestamp(t);			
		

	    jobSequenceNo=sequenceNum;
	    recChngTms=sqlTimestamp; 

	    String query=" ";
	    PreparedStatement sqlStatement=null;
	    
    	query="UPDATE JOB_CTRL SET REC_CHNG_TMS=? WHERE JOB_SEQUENCE_NO=?"; 	
        sqlStatement = this.con.prepareStatement(query);        
    	sqlStatement.setTimestamp(1,recChngTms);

    	//where
    	sqlStatement.setInt(2,jobSequenceNo);	    	
    	
    	try
        {
	        sqlStatement.executeUpdate();    
        }
	        catch(SQLException e){ throw e; 
	    }	      	    
    }   
    
    public void levelXWarnUpdate(int x) throws SQLException {
		levelXUpdate(x);     
    }       
    
    public void levelXEndUpdate(int x) throws SQLException {
		levelXUpdate(x);     
    }        
    

    @Override
    public int count() throws SQLException {
        String query = "SELECT COUNT(*) AS count FROM JOB_CTRL";
        PreparedStatement counter;
        try
        {
	        counter = this.con.prepareStatement(query);
	        ResultSet res = counter.executeQuery();
	        res.next();
	        return res.getInt("count");
        }
	        catch(SQLException e){ throw e; 
	    }
    }


}
