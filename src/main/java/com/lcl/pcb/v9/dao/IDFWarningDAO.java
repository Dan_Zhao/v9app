package com.lcl.pcb.v9.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.lcl.pcb.v9.idf.jobflow.config.IDFConfig;

/**
 * This is the DAO (data access object) for the
 * IDFWarning table
 * Inherits from V9DAO
 */
public class IDFWarningDAO extends V9DAO<IDFWarningDAO> {
	
    private String logLevel; //L1
    private String warningMsg; //Any warning from sql loader, jvm or oracle at that point else Null
    private String warningOverride; //Y
    private int interfaceID; //Interface id
    private int jobSequenceNo; //Sequence number
    private java.sql.Timestamp recCreateTms; //sysdate    

    public IDFWarningDAO(Connection con) {
        super(con, "IDF_WARNING");
    }
    
    public void levelXInsert(int x) throws SQLException {
    	
		int sequenceNum = IDFConfig.getInstance().getSequenceNum();
		String uniqueID = IDFConfig.getInstance().getIdfData().getFilePropIdfUniqueId();
		
	    java.util.Date date = new java.util.Date();
	    long t = date.getTime();
	    //java.sql.Date sqlDate = new java.sql.Date(t);
	    //java.sql.Time sqlTime = new java.sql.Time(t);
	    java.sql.Timestamp sqlTimestamp = new java.sql.Timestamp(t);			
		
		logLevel="L"+x;
		warningMsg=" ";
		warningOverride="Y";
	    interfaceID=Integer.parseInt(uniqueID); 
	    jobSequenceNo=sequenceNum;	
	    recCreateTms=sqlTimestamp;

    	String query = "INSERT INTO IDF_WARNING (LOG_LEVEL, WARNING_MSG, WARNING_OVERRIDE, INTERFACE_ID, JOB_SEQUENCE_NO, REC_CREATE_TMS) VALUES (?,?,?,?,?,?)"; 	
        PreparedStatement sqlStatement = this.con.prepareStatement(query);        
    	sqlStatement.setString(1,logLevel);
    	sqlStatement.setString(2,warningMsg);
    	sqlStatement.setString(3,warningOverride);
    	sqlStatement.setInt(4,interfaceID);
    	sqlStatement.setInt(5,jobSequenceNo);
    	sqlStatement.setTimestamp(6, recCreateTms);
    	
    	try
        {
	        sqlStatement.executeQuery();    
        }
	        catch(SQLException e){ throw e; 
	    }	    

    }

    public void levelXUpdate(int x) throws SQLException {
    	
		int sequenceNum = IDFConfig.getInstance().getSequenceNum();
		String uniqueID = IDFConfig.getInstance().getIdfData().getFilePropIdfUniqueId();
		
	    java.util.Date date = new java.util.Date();
	    long t = date.getTime();
	    //java.sql.Date sqlDate = new java.sql.Date(t);
	    //java.sql.Time sqlTime = new java.sql.Time(t);
	    java.sql.Timestamp sqlTimestamp = new java.sql.Timestamp(t);			
		
		logLevel="L"+x;
		warningMsg="sqlloader/jvm/oracle/null";
		warningOverride="Y";
	    interfaceID=Integer.parseInt(uniqueID); 
	    jobSequenceNo=sequenceNum;	
	    recCreateTms=sqlTimestamp;	    

    	String query = "UPDATE IDF_WARNING SET LOG_LEVEL=?, WARNING_MSG=?, WARNING_OVERRIDE=?, INTERFACE_ID=?, JOB_SEQUENCE_NO=?, REC_CREATE_TMS=? WHERE JOB_SEQUENCE_NO=? AND INTERFACE_ID=? AND LOG_LEVEL=?"; 	
        PreparedStatement sqlStatement = this.con.prepareStatement(query);        
    	sqlStatement.setString(1,logLevel);
    	sqlStatement.setString(2,warningMsg);
    	sqlStatement.setString(3,warningOverride);
    	sqlStatement.setInt(4,interfaceID);
    	sqlStatement.setInt(5,jobSequenceNo);
    	sqlStatement.setTimestamp(6, recCreateTms);
    	//where
    	sqlStatement.setInt(7,jobSequenceNo);
    	sqlStatement.setInt(8,interfaceID);
    	sqlStatement.setString(9,logLevel);      	
    	
    	try
        {
	        sqlStatement.executeUpdate();    
        }
	        catch(SQLException e){ throw e; 
	    }	    
    }        
    
    public void levelXWarnUpdate(int x, String message) throws SQLException {
    	
		int sequenceNum = IDFConfig.getInstance().getSequenceNum();
		String uniqueID = IDFConfig.getInstance().getIdfData().getFilePropIdfUniqueId();
		
	    java.util.Date date = new java.util.Date();
	    long t = date.getTime();
	    //java.sql.Date sqlDate = new java.sql.Date(t);
	    //java.sql.Time sqlTime = new java.sql.Time(t);
	    java.sql.Timestamp sqlTimestamp = new java.sql.Timestamp(t);		
		
		logLevel="L"+x;
		warningMsg=message;
		warningOverride="Y";
	    interfaceID=Integer.parseInt(uniqueID); 
	    jobSequenceNo=sequenceNum;	
	    recCreateTms=sqlTimestamp;	     

    	String query = "UPDATE IDF_WARNING SET LOG_LEVEL=?, WARNING_MSG=?, WARNING_OVERRIDE=?, INTERFACE_ID=?, JOB_SEQUENCE_NO=?, REC_CREATE_TMS=? WHERE JOB_SEQUENCE_NO=? AND INTERFACE_ID=? AND LOG_LEVEL=?"; 	
        PreparedStatement sqlStatement = this.con.prepareStatement(query);        
    	sqlStatement.setString(1,logLevel);
    	sqlStatement.setString(2,warningMsg);
    	sqlStatement.setString(3,warningOverride);
    	sqlStatement.setInt(4,interfaceID);
    	sqlStatement.setInt(5,jobSequenceNo);
    	sqlStatement.setTimestamp(6, recCreateTms);    	
    	//where
    	sqlStatement.setInt(7,jobSequenceNo);
    	sqlStatement.setInt(8,interfaceID);
    	sqlStatement.setString(9,logLevel);      	
    	
    	try
        {
	        sqlStatement.executeUpdate();    
        }
	        catch(SQLException e){ throw e; 
	    }	    
    }     

    @Override
    public int count() throws SQLException {
        String query = "SELECT COUNT(*) AS count FROM IDF_WARNING";
        PreparedStatement counter;
        try
        {
	        counter = this.con.prepareStatement(query);
	        ResultSet res = counter.executeQuery();
	        res.next();
	        return res.getInt("count");
        }
	        catch(SQLException e){ throw e; 
	    }
    }


}
