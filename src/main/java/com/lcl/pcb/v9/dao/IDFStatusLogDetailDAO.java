package com.lcl.pcb.v9.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import com.lcl.pcb.v9.idf.jobflow.config.IDFConfig;


/**
 * This is the DAO (data access object) for the
 * IDFStatusLogDetailDAO table
 * Inherits from V9DAO
 */
public class IDFStatusLogDetailDAO extends V9DAO<IDFStatusLogDetailDAO> {	
	
    private String fileName; //datafilename
    private java.sql.Date fileDt; //filedatetimestamp if any
    private String logLevel; //L1
    private java.sql.Date logDt; //sysdate
    private java.sql.Timestamp logTimeSt; //sysDate
    private String detailLog; //description of log level	
    private int interfaceID; //interface id from idf
    private int jobSequenceNo; //sequence no
    private String errorLog; //error message

    public IDFStatusLogDetailDAO(Connection con) {
        super(con, "IDF_STATUS_LOG_DETAIL");
    }
    
    public void levelXInsert(int x) throws SQLException {
    	
		int sequenceNum = IDFConfig.getInstance().getSequenceNum();
		String uniqueID = IDFConfig.getInstance().getIdfData().getFilePropIdfUniqueId();
		String fName=IDFConfig.getInstance().getIdfData().getDatafileDatafileLocation();
	    java.util.Date date = new java.util.Date();
	    long t = date.getTime();
	    java.sql.Date sqlDate = new java.sql.Date(t);
	    //java.sql.Time sqlTime = new java.sql.Time(t);
	    java.sql.Timestamp sqlTimestamp = new java.sql.Timestamp(t);			
		
		fileName=fName;	
		//fileDt=sqlDate; //this will be the file last modified date when we check if file exists after reading IDF (Exception Handling)
		logLevel="L"+x;
		//logDt=sqlDate;
		//logTimeSt=sqlTimestamp;
		switch (x) {
	        case 1:  detailLog = IDFConfig.getInstance().getIdfData().getL1desc();
	                 break;
	        case 2:  detailLog = IDFConfig.getInstance().getIdfData().getL2desc();
	                 break;
	        case 3:  detailLog = IDFConfig.getInstance().getIdfData().getL3desc();
	                 break;
	        case 4:  detailLog = IDFConfig.getInstance().getIdfData().getL4desc();
	                 break;
	        case 5:  detailLog = IDFConfig.getInstance().getIdfData().getL5desc();
	                 break;
		}
	    interfaceID=Integer.parseInt(uniqueID); 
	    jobSequenceNo=sequenceNum;	
	    
	   	String query="INSERT INTO IDF_STATUS_LOG_DETAIL ( FILE_NAME, FILE_DT, LOG_LEVEL, LOG_DT, LOG_TIMEST, DETAIL_LOG, INTERFACE_ID, JOB_SEQUENCE_NO) VALUES (?,sysdate,?,sysdate,systimestamp,?,?,?)"; 	
        PreparedStatement sqlStatement = this.con.prepareStatement(query);        
    	sqlStatement.setString(1,fileName);
    	//sqlStatement.setDate(2,fileDt);
    	sqlStatement.setString(2,logLevel);
    	//sqlStatement.setDate(4,logDt);
    	//sqlStatement.setTimestamp(5,logTimeSt);
    	sqlStatement.setString(3,detailLog);
    	sqlStatement.setInt(4,interfaceID);
    	sqlStatement.setInt(5,jobSequenceNo);
    	
    	try
        {
	        sqlStatement.executeQuery();    
        }
	        catch(SQLException e){ throw e; 
	    }	    
	    
    }

    public void levelXUpdate(int x) throws SQLException {
    	
		int sequenceNum = IDFConfig.getInstance().getSequenceNum();
		String uniqueID = IDFConfig.getInstance().getIdfData().getFilePropIdfUniqueId();
		String fName=IDFConfig.getInstance().getIdfData().getDatafileDatafileLocation(); 
		java.util.Date date = new java.util.Date();
	    long t = date.getTime();
	    java.sql.Date sqlDate = new java.sql.Date(t);
	    java.sql.Timestamp sqlTimestamp = new java.sql.Timestamp(t);			
		
		fileName=fName;	
		fileDt=sqlDate;
		logLevel="L"+x;
		logDt=sqlDate;
		logTimeSt=sqlTimestamp;
		detailLog="Level "+x;
	    interfaceID=Integer.parseInt(uniqueID); 
	    jobSequenceNo=sequenceNum;	
	    
	   	String query="UPDATE IDF_STATUS_LOG_DETAIL SET FILE_NAME=?, FILE_DT=?, LOG_LEVEL=?, LOG_DT=?, LOG_TIMEST=?, DETAIL_LOG=?, INTERFACE_ID=?, JOB_SEQUENCE_NO=? WHERE JOB_SEQUENCE_NO=?"; 	
        PreparedStatement sqlStatement = this.con.prepareStatement(query);        
    	sqlStatement.setString(1,fileName);
    	sqlStatement.setDate(2,fileDt);
    	sqlStatement.setString(3,logLevel);
    	sqlStatement.setDate(4,logDt);
    	sqlStatement.setTimestamp(5,logTimeSt);
    	sqlStatement.setString(6,detailLog);
    	sqlStatement.setInt(7,interfaceID);
    	sqlStatement.setInt(8,jobSequenceNo);
    	//where
    	sqlStatement.setInt(9,jobSequenceNo);
    	
    	try
        {
	        sqlStatement.executeUpdate();    
        }
	        catch(SQLException e){ throw e; 
	    }	    
    }    

    public void levelXWarnUpdate(int x, String message) throws SQLException {
		
		int sequenceNum = IDFConfig.getInstance().getSequenceNum();
		String uniqueID = IDFConfig.getInstance().getIdfData().getFilePropIdfUniqueId();

		logLevel="L"+x;
	    interfaceID=Integer.parseInt(uniqueID); 
	    jobSequenceNo=sequenceNum;	
	    errorLog=message;
	    
    	String query = "UPDATE IDF_STATUS_LOG_DETAIL SET ERROR_LOG=? WHERE JOB_SEQUENCE_NO=? AND INTERFACE_ID=? AND LOG_LEVEL=?";
	    
        PreparedStatement sqlStatement = this.con.prepareStatement(query);        
    	sqlStatement.setString(1,errorLog); 	
    	//where
    	sqlStatement.setInt(2,jobSequenceNo);
    	sqlStatement.setInt(3,interfaceID);
    	sqlStatement.setString(4,logLevel);      	
    	
    	try
        {
	        sqlStatement.executeUpdate();    
        }
	        catch(SQLException e){ throw e; 
	    }	    
    }        
    
    @Override
    public int count() throws SQLException {
        String query = "SELECT COUNT(*) AS count FROM IDF_STATUS_LOG_DETAIL";
        PreparedStatement counter;
        try
        {
	        counter = this.con.prepareStatement(query);
	        ResultSet res = counter.executeQuery();
	        res.next();
	        return res.getInt("count");
        }
	        catch(SQLException e){ throw e; 
	    }
    }


}
