package com.lcl.pcb.v9.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * This is the DAO (data access object) for the
 * InterfaceDefinition table
 * Inherits from V9DAO
 */
public class InterfaceDefinitionDAO extends V9DAO<InterfaceDefinitionDAO> {

	public InterfaceDefinitionDAO(Connection con) {
        super(con, "INTERFACE_DEFINITION");
    }
	
    public int getSeqNum() throws SQLException {

        String query = "select JOB_SEQUENCE_NO.nextval from dual";
        PreparedStatement seqNum;
        try
        {
	        seqNum = this.con.prepareStatement(query);
	        ResultSet res = seqNum.executeQuery();
	        res.next();
	        return res.getInt("NEXTVAL");
        }
	        catch(SQLException e){ throw e; 
	    }
    }	

    @Override
    public int count() throws SQLException {
        String query = "SELECT COUNT(*) AS count FROM INTERFACE_DEFINITION";
        PreparedStatement counter;
        try
        {
	        counter = this.con.prepareStatement(query);
	        ResultSet res = counter.executeQuery();
	        res.next();
	        return res.getInt("count");
        }
	        catch(SQLException e){ throw e; 
	    }
    }


}
