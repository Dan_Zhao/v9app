package com.lcl.pcb.v9.utilities.fileReader;

import com.lcl.pcb.v9.idf.jobflow.config.IDFConfig;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

public class FileUtilTest {

    @Disabled("local test only")
    @Test
    public void testADMDispositionFileParsing() throws Exception{

        IDFConfig.getInstance().readConfigIdf("HO70", "/Users/danzhao/Work/data/uat/adm");

        String copyBookLocation = "/Users/danzhao/Work/data/uat/adm/properties/HO70Loader.xml";
        String dataFileLoacation = "/Users/danzhao/Work/data/uat/adm/TS2_ADM_dispo.20220112010128.uatv-20220112_010300";
        FileUtil fileUtil = new FileUtil(copyBookLocation, dataFileLoacation);
        fileUtil.process(copyBookLocation, dataFileLoacation);
    }
}
